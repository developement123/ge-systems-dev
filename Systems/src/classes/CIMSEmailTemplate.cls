/*
** Created By Name : Narayana Gaddam ( 502323200 )
** Created Date : 2015-08-05
** Last Modified By Name : Narayana Gaddam ( 502323200 )
** Last Modified Date : 2015-08-13
** Class Name : CIMSEmailTemplate.
** Source : CIMS Form from GE Support Central.
** Exception mails Re-directed to: cimsSupport@ge.com
** Used to: Create a Record from email which is sent through webservice (form GE Support Central CIMS Form).
** Description: This will create record with Attachment in Submit_CI & Approve_CI object and Handles most of the Exception.
** Email Service : CIMS.
** Revision History: Jagan Endarapu : Added logic in line 32 for data truncating issue(SFDC-GEA-1182) 
*/

global class CIMSEmailTemplate implements Messaging.InboundEmailHandler
{
    boolean isDiscarded = false;
    string discardedStr = '';
    Map<String, Integer> monthMap = new Map<String, Integer>{'January'=>01,'February'=>02,'March'=>03,'April'=>04,'May'=>05,'June'=>06,'July'=>07,'August'=>08,'September'=>09,'October'=>10,'November'=>11,'December'=>12};
    global Messaging.InboundEmailResult handleInboundEmail (Messaging.InboundEmail email,  Messaging.InboundEnvelope envelope)
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Map<String, String> bodyLabelValueMap = new Map<String, String>();
        String subject = email.subject;
        try
        {
            System.debug('CIMS HTML Email Body----    '+email.htmlBody);
             for( String str : email.htmlBody.split('</p><p>') ) 
            {
                if(str != null)
                {   
                    string[] strList = str.split(':'); 
                    system.debug('strList ======='+strList);
                    String strKeyValue='';
                    for(Integer i=1; i<strList.size(); i++)
                    strKeyValue= strKeyValue+''+strList[i].remove('</p>').remove('</P>').trim().remove('&nbsp;');             
                    if( strList.size() >= 2 )
                    {                                     
                        bodyLabelValueMap.put( strList[0].remove('<br>').remove('<p>').remove('<P>').trim(), strKeyValue );
                         system.debug('bodyLabelValueMap 00000 ======='+bodyLabelValueMap.values());
                    }
                }
            }
            system.debug('bodyLabelValueMap Key======='+bodyLabelValueMap.keyset());
            system.debug('bodyLabelValueMap= Value ======'+bodyLabelValueMap.values());
            List<User> usr= [SELECT ID, Name, Profile_Name__c,Email,FederationIdentifier From User  WHERE FederationIdentifier =: bodyLabelValueMap.get('SSO') AND isActive= true LIMIT 1]; // Active is reffereing to vaid user.
            Submit_CI__c submitRecord = new Submit_CI__c( status__c = 'Submitted', IsEmailServiceRecord__c = true ,Submission_Date__c = date.Today() );
            
            if( usr.size()==0 || ( usr.size() > 0 && usr[0].Profile_Name__c.contains('Chatter') ) )
            {
                submitRecord.OwnerId = label.CI_Provider_Id ; 
            }
            else
            {
               submitRecord.OwnerId = usr[0].id;
            }
            if( bodyLabelValueMap.containsKey('Sumitter Name') ) 
            {
                submitRecord.Submitted_By__c = bodyLabelValueMap.get('Sumitter Name') ;
                system.debug('bodyLabelValueMap.get(Sumitter Name) ====='+bodyLabelValueMap.get('Sumitter Name')); 
            }
            
            if( bodyLabelValueMap.containsKey('SSO') ) 
            {
                submitRecord.Submitter_SSO__c = bodyLabelValueMap.get('SSO') ;
               system.debug('bodyLabelValueMap.get(SSO) ====='+bodyLabelValueMap.get('SSO')); 
            }
            
            if( bodyLabelValueMap.containsKey('Email Address') ) 
            {
                if( usr.size()!=0)
                {
                    submitRecord.Submitter_Email_ID__c = usr[0].Email ;
                }
                else
                {
                    submitRecord.Submitter_Email_ID__c =  bodyLabelValueMap.get('Email Address');
                }
            }
            
            if( bodyLabelValueMap.containsKey('Relevant Products') ) 
            {
                submitRecord.Business_Relevant__c = bodyLabelValueMap.get('Relevant Products') ;
            }
            
            if( bodyLabelValueMap.containsKey('Summary CI') ) 
            {
                submitRecord.CIS__c = bodyLabelValueMap.get('Summary CI') ;
            }
            
            if( bodyLabelValueMap.containsKey('Date CI') ) 
            {
                String dateCI = bodyLabelValueMap.get('Date CI') ;
                //dateCI = dateCI.trim();
                String[] arryDateStrings = new String[]{};
                for(String str : dateCI.split(' '))
                {
                    System.debug('=============str = '+str);
                    arryDateStrings.add(str.remove(','));
                }
                System.debug('=============arryDateStrings = '+arryDateStrings);
                List<string> dtlst = arryDateStrings[0].split('/');
                //System.debug('=============arryDateStrings[2] = '+arryDateStrings[2]);
                Integer myIntDate = null;
                Integer myIntMonth = null;
                Integer myIntYear = null;
                if(monthMap.containsKey(arryDateStrings[1]))
                {
                    myIntDate = integer.valueOf(arryDateStrings[2].trim());
                    myIntMonth = integer.ValueOf(monthMap.get(arryDateStrings[1]));
                    myIntYear = integer.valueOf(arryDateStrings[3].remove('</p>').remove('</P>').trim());
                }
                else
                {
                    myIntMonth = integer.valueOf(dtlst[0].trim());
                    myIntDate = integer.valueOf(dtlst[1].trim());
                    myIntYear = integer.valueOf(dtlst[2].trim());
                }
                system.debug('dtae----------****'+myIntDate+'======'+myIntMonth+'&&&&&&&'+myIntYear);
                if(myIntYear != null && myIntMonth != null && myIntDate != null)
                //if(myIntYear != null )
                {                   
                    submitRecord.DCIRO__c = Date.newInstance(myIntYear,myIntMonth,myIntDate);
                }
            }
            
            if( bodyLabelValueMap.containsKey('Location CI') ) 
            {
                String locationCI = bodyLabelValueMap.get('Location CI') ;
                if( locationCI.length() > 1000 )
                {
                    submitRecord.LCIRO__c = locationCI.substring(0,999);                    
                    isDiscarded = true;
                    discardedStr = discardedStr + ' \n Location CI Exceeds Char Limit: ' + locationCI.substring( 1000,locationCI.length()-1 );  
                }
                else
                {
                    submitRecord.LCIRO__c = bodyLabelValueMap.get('Location CI') ;
                }
            }
            
            if( bodyLabelValueMap.containsKey('Source CI') ) 
            {
                String sourceCI = bodyLabelValueMap.get('Source CI') ;
                if( sourceCI.length() > 1000)
                {
                    submitRecord.SOCI__c = sourceCI.substring(0,999);
                    isDiscarded = true;
                    discardedStr = discardedStr + ' \n Source CI Exceeds Char Limit: ' + sourceCI.substring( 1000,sourceCI.length()-1 );
                }
                else
                {
                    submitRecord.SOCI__c = bodyLabelValueMap.get('Source CI') ;
                }
            }
            
            if( bodyLabelValueMap.containsKey('CircumSummary CI') ) 
            {
                String circumsummaryCI = bodyLabelValueMap.get('CircumSummary CI') ;
                if(circumsummaryCI.length() > 1000)
                {
                    submitRecord.SOCOR__c = circumsummaryCI.substring( 0,999);
                    isDiscarded = true;
                    discardedStr = discardedStr + ' \n Circumstance Summary CI Exceeds Char Limit: ' + circumsummaryCI.substring( 1000,circumsummaryCI.length() - 1 );
                }
                else
                {
                    submitRecord.SOCOR__c = bodyLabelValueMap.get('CircumSummary CI') ;
                }
            }
                        
            if( bodyLabelValueMap.containsKey('Public CI') ) 
            {
                submitRecord.WCIRPF__c = bodyLabelValueMap.get('Public CI') ;
            }
            
            if( bodyLabelValueMap.containsKey('Proprietary Markings') ) 
            {
                submitRecord.DACPM__c = bodyLabelValueMap.get('Proprietary Markings') ;
            }
            
            if( bodyLabelValueMap.containsKey('Govt/Military CI') ) 
            {
                submitRecord.DCIRUSFGMP__c = bodyLabelValueMap.get('Govt/Military CI') ;
            }
            
            if( bodyLabelValueMap.containsKey('Note for Approver') ) 
            {
                String noteForApprover = bodyLabelValueMap.get('Note for Approver') ;
                  system.debug('noteForApprover ()' +noteForApprover.length());
                if(noteForApprover.length() > 1000)
                {
                    submitRecord.AONP__c = noteForApprover.substring(0,999);
                    isDiscarded = true;
                    discardedStr = discardedStr + ' \n Note for Approver Exceeds Char Limit: ' + noteForApprover.substring( 1000,noteForApprover.length() - 1 );
                }
                else
                {
                    submitRecord.AONP__c = bodyLabelValueMap.get('Note for Approver') ;
                }
            }
            
            if( bodyLabelValueMap.containsKey('Competitor bid CI') ) 
            {
                submitRecord.DCIRCBP__c = bodyLabelValueMap.get('Competitor bid CI') ;
            }
            
            if( bodyLabelValueMap.containsKey('Govt/Military Source Selection') ) 
            {
                submitRecord.DCIRGMSSP__c = bodyLabelValueMap.get('Govt/Military Source Selection') ;
            }
            
            if( bodyLabelValueMap.containsKey('Govt/Military Office Authorize') ) 
            {
                submitRecord.DGMCA__c = bodyLabelValueMap.get('Govt/Military Office Authorize') ;
            }
            
            if( bodyLabelValueMap.containsKey('Owner CI') ) 
            {
                String ownerCI = bodyLabelValueMap.get('Owner CI') ;
                 system.debug('ownerCI length()' +ownerCI.length());
                if(ownerCI.length() > 1000)
                {
                    submitRecord.OCI__c = ownerCI.substring(0,999);
                    isDiscarded = true;
                    discardedStr = discardedStr + ' \n Onwer CI Exceeds Char Limit: ' + ownerCI.substring( 1000,ownerCI.length() - 1 );
                }
                else
                {
                    submitRecord.OCI__c = bodyLabelValueMap.get('Owner CI') ;
                }
            }
            
            if( bodyLabelValueMap.containsKey('Is it Export Control Data') )
           
            {
                submitRecord.Is_Export_Control_Data__c = bodyLabelValueMap.get('Is it Export Control Data');
                if( submitRecord.Is_Export_Control_Data__c == 'Yes' )
                    submitRecord.Confirm_do_not_have_Export_Control_Data__c = true;
            }
            system.debug('======= submitRecord ********'+submitRecord);
            if(submitRecord != null)
            {
                insert submitRecord;
                System.debug('cims record id---  '+submitRecord);
            }
            
            if(submitRecord.id != null)
            {
                //attachment code needs to be included
                Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.BinaryAttachments;   //binary attach--ok
                //system.debug('binary attact ' + bAttachments);
                EmailParsingActivity emailActivity = new EmailParsingActivity();
                emailActivity.attachToRecord(bAttachments,submitRecord.Id);
                
                // this condition is copying the attachments from SubmitCI to ApproveCI 
                if(submitRecord.status__c == 'Submitted')
                {
                    CheckApproveCI.CopySubmitCIToApproveCI(submitRecord.Id);
                }  
            }
        }
        catch( ListException e )
        {
            sendEmail('cimssupport@ge.com','Other Exception',string.valueof(e));
            return null;
        }
        
        if(isDiscarded) {
            //send email
            sendEmail('cimssupport@ge.com',discardedStr,email.htmlBody);            
        }
        
        return Result;
    }
    
    public void sendEmail(string toAddr,string discard, string ex){
        String[] toAddresses = new String[] {toAddr}; 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('rajesh.thonduru@ge.com');
        mail.setSenderDisplayName('Salesforce Admin');
        mail.setSubject('Excess String From Production: Details below');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setPlainTextBody('Error Occured: ' +discard + ex );
        List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.Email[] { mail });     
    }
}