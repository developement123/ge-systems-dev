@isTest(SeeAllData=true)
public class EPAclosed_relationship_update_response
{/*phase5 
 static testmethod void mytest()
    {
  Test.startTest();
    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        
        
        Test.setMock(HttpCalloutMock.class, mock);
     Test.StopTest();   
        
     List<Opportunity> oppsList= new list<Opportunity>();
    List<Sales_Request__c> srList= new list<Sales_Request__c>();
    List<OpportunityLineItem> olList= new list<OpportunityLineItem>();
    list<User> UserList= new list<User>();
    list<Oppty_SR_Intersection__c>  list_osr= new list<Oppty_SR_Intersection__c>();
     id bA_Profile_id;
     id Sys_admin_Profile_id;
     
     Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
     insert plat;
     
     Account acc = new Account(Name='Test Account SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS');
     insert acc; 
     Account acc2 = new Account(Name='GE GEAS');
     insert acc2; 

     Forecast__c fore=new Forecast__c(Airframer__c=acc.id,Forecast_Platform__c=plat.id);
     insert fore;
      
     list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
   for(Profile p: pro)
   {
     if(p.name=='Systems Business Admin')
     {
        bA_Profile_id=p.Id;
     }
     else
     {
        Sys_admin_Profile_id=p.id;
     }
   }
     User u =new User( alias = 'skapo',email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=bA_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor@birlasoft12.com.sys.dev');
               UserList.add(u);
     User u2 =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft123.com.sys.dev');
               UserList.add(u2);
               insert UserList;
               system.debug('#############################');
     
     Contact con= new Contact();
     con.AccountId=acc2.id;
     con.Email='jeetesh.bahuguna@ge.com.test';
     con.LastName='testContact';
     con.FirstName='007';
     con.MailingCity='XYZ';
     con.MailingCountry='ABC';
     con.Function__c='Engineering';
     insert con;
    
    Class_TriggerCheck.IsPreventing = false;
    Class_TriggerCheck.IsWindchill_UpdatedCA = false;
    Class_TriggerCheck.Isclosed_CA = false;
    Class_TriggerCheck.Var_ShouldEPA_update = false;
                   
     EPA__c epa=new EPA__c(Name='Test epa',Closed__c=False);
     epa.OwnerId=u.Id;
     insert epa;
     epa.OwnerId=u2.Id;
     update epa;
     
     Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
     insert hierarchy;
     
    Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name='Test hierarchy1',Product_Group__c='Test group1',Product_Area__c='Test area1',Product_Family__c='Test family1',Product_Child__c='Test child1');
    insert hierarchy1;  

    list<Program__c> list_prog_toadd= new  list<Program__c>();
 phase5 */
   /*deployment  Program__c prog=new Program__c(Product_Group__c='Power',
     Product_Area__c='Power Conversion & Control',Product__c='Power Conversion & Control',
     Product_Family_Child__c='Regulated Transformer Rectifier Units (RTRUs)',Product_Sales_Owner__c=u.id,
     Forecast__c=fore.id,Program_Probability__c='100% - Funded');
     deployment*/
 /*phase5 
     Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id, Product_Sales_Owner__c=u.id,
     Forecast__c=fore.id,Program_Probability__c='100% - Funded');
     list_prog_toadd.add(prog);
    phase5 */
    /*Deployment Program__c prog2=new Program__c(Product_Group__c='Avionics',Product_Area__c=Label.ProductHierarchyMultiple,
     Product__c=Label.ProductHierarchyMultiple, Product_Family_Child__c=Label.ProductHierarchyMultiple, Product_Sales_Owner__c=u.id,
     Forecast__c=fore.id,Program_Probability__c='100% - Funded');
     list_prog_toadd.add(prog2);
     deployment*/
     
   /*phase5 Program__c prog2=new Program__c(Lookup_hiearchyname__c=hierarchy1.id, Product_Sales_Owner__c=u.id,
     Forecast__c=fore.id,Program_Probability__c='100% - Funded');
     list_prog_toadd.add(prog2);
     insert list_prog_toadd;
 
   
    Opportunity opp10=new opportunity();
    opp10.Name='Test opp';
    opp10.StageName='Prospecting';
    opp10.CloseDate=date.today();
    opp10.Program__c=prog.id;
    opp10.Amount=0;
    opp10.Firm_Anticipated__c='Anticipated';
    opp10.Probability__c='100% - Won';
    //opp10.deal_type__c='Others';
    oppsList.add(opp10);
    //insert ;
   
   
      
    Opportunity opp=new opportunity();
    opp.Name='Test oppSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS';
    opp.StageName='Prospecting';
    opp.CloseDate=date.today();
    opp.Program__c=prog.id;
    opp.Amount=1000;
    opp.Firm_Anticipated__c='Anticipated';
    opp.Probability__c='100% - Won';
    opp.deal_type__c='Services';
    oppsList.add(opp);
   
    Opportunity opp1=new opportunity();
    opp1.Name='Test opp';
    opp1.StageName='Prospecting';
    opp1.CloseDate=date.today();
    opp1.Program__c=prog2.id;
    opp1.Amount=0;
    opp1.Firm_Anticipated__c='Anticipated';
    opp1.Probability__c='100% - Won';
    opp1.deal_type__c='Others';
    oppsList.add(opp1);

    
    Opportunity opp3=new opportunity();
    opp3.Name='Test opp3';
    opp3.StageName='Win';
    opp3.CloseDate=date.today();
    opp3.Program__c=prog.id;
    opp3.Amount=1000;
    opp3.Firm_Anticipated__c='Anticipated';
    opp3.Probability__c='100% - Won';
    opp3.deal_type__c='Momentum';
    oppsList.add(opp3);
    
    Opportunity opp4=new opportunity();
    opp4.Name='Test opp4';
    opp4.StageName='Win';
    opp4.CloseDate=date.today();
    opp4.Program__c=prog.id;
    opp4.Amount=1000;
    opp4.Firm_Anticipated__c='Anticipated';
    opp4.Probability__c='100% - Won';
    opp4.deal_type__c='Momentum with NRE';
    oppsList.add(opp4); 
   
    
    Opportunity opp2=new opportunity();
    opp2.Name='Test opp2';
    opp2.StageName='Prospecting';
    opp2.CloseDate=date.today();
    opp2.Program__c=prog.id;
    opp2.Amount=1000;
    opp2.Firm_Anticipated__c='Anticipated';
    opp2.Probability__c='100% - Won';
    opp2.deal_type__c='NPI';
    oppsList.add(opp2); 
    insert oppsList;
    
   
 
   
    prog.Program_Probability__c='25% - Potential funding with negative outlook';
    update prog;
    
    Sales_Request__c sr2=new Sales_Request__c(Windchill_US_UK__c ='Windchill US',Name='Test response2',  Program__c=prog.id, Proposal_Coordinator__c = con.Id);
    srList.add(sr2);
    
    Sales_Request__c sr=new Sales_Request__c(Windchill_US_UK__c ='Windchill US',Name='Test response',Engineering_Lead__c=con.id,Program__c=prog.id, Proposal_Coordinator__c = con.Id);
    srList.add(sr);
    try{
    insert srList;
    }
    catch(Exception e)
    {
        system.debug('Test Callout error---> '+e);
    }
     
    Cost_Authorization__c ca=new Cost_Authorization__c(EPA_Number__c=epa.id,
    Sales_Request__c=sr.id);
    try{
    insert ca;
    }catch(exception s){system.debug('Test Callout error--s->'+s);}
    ca.Description__c='cbscdscs';
    ca.Close_Cost_Authorization__c=true;
    try
    {
    update ca;
    }catch(exception CAexception){system.debug('exception eee'+CAexception);}
    
    epa.Closed__c=True;
    update epa;
  //  update ca;
    
    
    Oppty_SR_Intersection__c osr1=new Oppty_SR_Intersection__c(Opportunities__c=opp10.id,
    Sales_Request__c=sr.id);
    list_osr.add(osr1);
   
    Oppty_SR_Intersection__c osr=new Oppty_SR_Intersection__c(Opportunities__c=opp.id,
    Sales_Request__c=sr.id);
    list_osr.add(osr);

    
    Oppty_SR_Intersection__c osr3=new Oppty_SR_Intersection__c(Opportunities__c=opp3.id,
    Sales_Request__c=sr.id);
    list_osr.add( osr3);
   
   
    Oppty_SR_Intersection__c osr4=new Oppty_SR_Intersection__c(Opportunities__c=opp4.id,
    Sales_Request__c=sr.id);
    list_osr.add( osr4);
   
   
   
    Oppty_SR_Intersection__c osr2=new Oppty_SR_Intersection__c(Opportunities__c=opp2.id,
    Sales_Request__c=sr.id);
    list_osr.add( osr2);
     
    
    Oppty_SR_Intersection__c osr5=new Oppty_SR_Intersection__c(Opportunities__c=opp3.id,
    Sales_Request__c=sr2.id);
   list_osr.add( osr5);
    
    Oppty_SR_Intersection__c osr6=new Oppty_SR_Intersection__c(Opportunities__c=opp4.id,
    Sales_Request__c=sr2.id);
   list_osr.add(osr6);        
    insert list_osr;  
    
   phase5 */
   /*deployment Product2 p2 = new Product2(Name='Test Product',Product_Group__c='Power',
    Product_Area__c='Marine',Product_Family__c='Marine',
    Product_Family_Child__c='Marine Transducers (*YMF)');
    deployment*/
  /*phase5 
    Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id, Name='Test Product');
    insert p2;
 
    String standardPriceBookId ='';  

    PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
    standardPriceBookId = pb2Standard.Id;
      
    PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, 
    Product2Id=p2.Id, UnitPrice=99, isActive=true);
    insert pbe;


    OpportunityLineItem ol2 = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=opp.Id,
    Quantity=1,Probability__c=50,UnitPrice=0,Revenue_Stream_top_level__c='Services',
    Revenue_Stream_child_level__c='Other');
    olList.add(ol2);

    OpportunityLineItem ol = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=opp.Id,
    Quantity=1,Probability__c=50,UnitPrice=50,Revenue_Stream_top_level__c='Services',
    Revenue_Stream_child_level__c='Other');
    olList.add(ol);
    
    
    OpportunityLineItem ol3 = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=opp.Id,
    Quantity=1,Probability__c=50,Revenue_Stream_top_level__c='Hardware/OE',
    Revenue_Stream_child_level__c='Momentum Production',TotalPrice=300);
    olList.add(ol3);
    
    OpportunityLineItem ol4 = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=opp.Id,
    Quantity=1,Probability__c=50,Revenue_Stream_top_level__c='Engineering',
    Revenue_Stream_child_level__c='New Product Development',TotalPrice=200);
    olList.add(ol4);
    insert olList;
    
    
    
    
Class_TriggerCheck.Var_IsProgramUPdated=false;  
opp1.Name='tesdsddasdssd';
try{
    update opp1;
   }catch(Exception oppE){}  
Class_TriggerCheck.Var_IsProgramUPdated=false;  
 opp1.Program__c=prog2.id;
 try{
    update opp1;
   }catch(Exception oppE){}  
Class_TriggerCheck.Var_IsProgramUPdated=false;  
//deployment opp1.Product_group__c='Avionics';
   try{
    update opp1;
   }catch(Exception oppE){}  
    }phase5 */
    }