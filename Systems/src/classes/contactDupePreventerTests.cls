@isTest
public class contactDupePreventerTests{
    static testMethod void testContactDupPreventer() {
        
      // First make sure there are no contact already in the system  
    
      // that have the email addresses used for testing  
    
      Set<String> testEmailAddress = new Set<String>();
      testEmailAddress.add('test1@duptest.com');
      testEmailAddress.add('test2@duptest.com');
      testEmailAddress.add('test3@duptest.com');
      testEmailAddress.add('test4@duptest.com');
      testEmailAddress.add('test5@duptest.com');
      System.debug('Mayank Test');
      System.assert([SELECT count() FROM Contact
                     WHERE Email IN :testEmailAddress] == 0);
        
      // Seed the database with some contacts, and make sure they can  
    
      // be bulk inserted successfully.  
    
      Contact contact1= new Contact (LastName='Test1', FirstName='FirstName1',
                            Email='test1@duptest.com',MailingCity='Delhi',
                            MailingCountry='India');
      Contact contact2 = new Contact (LastName='Test2', FirstName='FirstName2',
                            Email='test4@duptest.com',MailingCity='Noida',
                            MailingCountry='India');
      Contact contact3= new Contact (LastName='Test3', FirstName='FirstName3',

                            Email='test5@duptest.com',MailingCity='Gurgaon',
                            MailingCountry='India');
      Contact [] contacts= new Contact [] {contact1, contact2, contact3};
      insert contacts;
        
      // Now make sure that some of these contacts can be changed and  
    
      // then bulk updated successfully. Note that contact1 is not  
    
      // being changed, but is still being passed to the update  
    
      // call. This should be OK.  
    
      contact2.Email = 'test2@duptest.com';
      contact3.Email = 'test3@duptest.com';
      update contacts;
        
      // Make sure that single row contact duplication prevention works  
    
      // on insert.  
    
      Contact dup1 = new Contact (LastName='Test1', FirstName='FirstName1',
                          
                           Email='test1@duptest.com',MailingCity='Delhi',
                            MailingCountry='India');
      try {
         insert dup1;
         System.debug('Pant1');
         System.assert(false);
      } catch (DmlException e) {
               
         }
      
        
      // Make sure that single row contact duplication prevention works  
    
      // on update.  
    
      dup1 = new Contact (Id = contact1.Id, LastName='Test2', FirstName='FirstName2',
                               Email='test2@duptest.com',MailingCity='Noida',
                            MailingCountry='India');
      try {
         update dup1;
         System.assert(false);
      } catch (DmlException e) {
        
        }
    
      // Make sure that bulk contact duplication prevention works on  
    
      // insert. Note that the first item being inserted is fine,  
    
      // but the second and third items are duplicates. Note also  
    
      // that since at least one record insert fails, the entire  
    
      // transaction will be rolled back.  
    
      dup1 = new Contact(LastName='Test1', FirstName='FirstName1', 
                      Email='test4@duptest.com',MailingCity='Delhi',
                            MailingCountry='India');
      Contact dup2 = new Contact (LastName='Test2', FirstName='FirstName2',
                                    Email='test2@duptest.com',MailingCity='Noida',
                            MailingCountry='India');
      Contact dup3 = new Contact (LastName='Test3', FirstName='FirstName3',
                           
                           Email='test3@duptest.com',MailingCity='Gurgaon',
                            MailingCountry='India');
      Contact [] dups = new Contact [] {dup1, dup2, dup3};
      try {
         insert dups;
         System.assert(false);
      } catch (DmlException e) {
         
      }
    
      // Make sure that bulk contact duplication prevention works on  
    
      // update. Note that the first item being updated is fine,  
    
      // because the email address is new, and the second item is  
    
      // also fine, but in this case it's because the email  
    
      // address doesn't change. The third case is flagged as an  
    
      // error because it is a duplicate of the email address of the  
    
      // first contact's value in the database, even though that value  
    
      // is changing in this same update call. It would be an  
    
      // interesting exercise to rewrite the trigger to allow this  
    
      // case. Note also that since at least one record update  
    
      // fails, the entire transaction will be rolled back.  
  
    
      dup1 = new Contact (LastName='Test1',  FirstName='FirstName1',
                      Email='test4@duptest.com',MailingCity='Delhi',
                            MailingCountry='India');
      dup2 = new Contact (LastName='Test2', FirstName='FirstName2',
                      Email='test4@duptest.com',MailingCity='Noida',
                            MailingCountry='India');
      dup3 = new Contact (LastName='Test3', FirstName='FirstName3',
                      Email='test3@duptest.com',MailingCity='Gurgaon',
                            MailingCountry='India');
      dups = new Contact [] {dup1, dup2, dup3};
      try {
         insert dups;
         System.assert(false);
      } catch (DmlException e) {
         
      }
   }
}