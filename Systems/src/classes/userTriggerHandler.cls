/******************************************************************************************
    Author   : Abhishek
    Date     : 12/April/2016
    Action   : This class is handler class of trigger 'TriggerOnUser' - Jira 546
    
*********************************************************************************************/

public with sharing class userTriggerHandler { 


// Method 1 : This method is created to create contact/assign chatter group. 
//              Also, it calls future method to assin permissionSet/group/gridbuddyLicense (setup objects) 

public void assignPermsandOthersOnUser(List<user> newList,  Map<Id, user> newMap, 
                                        Map<Id, user> oldMap,String eventType, String triggerType ){
    
  list<contact> listOfContactstoInsert = new list<contact>();
  list<user> userToAdd = new list<user>();
  set<Id> UserIds = new set<Id>();
  set<id> BusinessUserId = new set<id>();
  list<CollaborationGroup> listOfCollaborationGroup  = new list<CollaborationGroup>();
  list<CollaborationGroupMember> chatterGroupMembers = new List<CollaborationGroupMember>(); 
  
  set<string> existingContacts = new set<string>();
  map<string, id> contacts =   new map<string, id>();
  map<string, set<id>> chatterUsers =   new map<string, set<id>>();
  set<id> chatterGroupId = new set<id>();
  
    if(label.Label_assignPermsandOthersOnUser=='true'){
         listOfCollaborationGroup = [select id, Name from CollaborationGroup where name  = 'All GE Aviation Systems'];  
         
         if(!listOfCollaborationGroup.isEmpty()){
            chatterGroupId = (new Map<Id,SObject>(listOfCollaborationGroup)).keySet();
         }
         
         // cannot use lable in SOQL, if label contains multiple values.seems dynamic query would be needed
         Map<ID, Profile> AllowedProfiles = new map<id, Profile>([select id, name from Profile where name  in ('System Administrator','Systems Business Admin', 'Systems Standard User') ]);
         
         // To get Profile ID of 'Systems Business Admin'
         Id ProfileId;
         for(id adminprofile : AllowedProfiles.keySet()){
            if(AllowedProfiles.get(adminprofile).name == 'Systems Business Admin'){
                ProfileId = adminprofile;
            }
         }
         
         for(user myUser : newList){
            if(AllowedProfiles.containsKey(myUser.profileid)){
                
                if(eventType=='before' && triggerType=='insert' && myUser.isActive){
                    userToAdd.add(myUser);
                    UserIds.add(myUser.Id);
                }
                if(eventType=='before' && triggerType=='update' && newMap.get(myUser.id).isActive!=oldMap.get(myUser.id).isActive &&  newMap.get(myUser.id).isActive){
                    userToAdd.add(myUser);
                    UserIds.add(myUser.Id);
                }
                
                
            }
            if(ProfileId!=null)
                if(myUser.ProfileId == ProfileId && myUser.isActive ){
                    if(eventType=='before' && triggerType=='insert'){
                        BusinessUserId.add(myUser.Id); // Id of business admin users
                       }
                    if(eventType=='before' && triggerType=='update' && newMap.get(myUser.id).isActive!=oldMap.get(myUser.id).isActive &&  newMap.get(myUser.id).isActive){
                            BusinessUserId.add(myUser.Id); // Id of business admin users
                        }
                }
                existingContacts.add(myUser.Email);
         }
         system.debug('All user Ids ++ ' + UserIds);
         
         for(contact orgContacts : [select id, email from contact where Email=: existingContacts and accountId=:Label.GE_GEAS_AccountId]){
                contacts.put(orgContacts.email, orgContacts.id);
         
         }
         
         for(CollaborationGroupMember chatUser : [select id, MemberId, CollaborationGroupId from CollaborationGroupMember where MemberId=: UserIds and CollaborationGroupId=: chatterGroupId]){
              if(chatterUsers.containsKey(chatUser.CollaborationGroupId))
                  chatterUsers.get(chatUser.CollaborationGroupId).add(chatUser.MemberId);
              else
                  chatterUsers.put(chatUser.CollaborationGroupId,new set<id>{chatUser.MemberId});
         }
         
         set<id> AssignedUsersToChatter = new set<id>();
         
         
         for(User ur : userToAdd){
         
            if (!(contacts.containsKey(ur.email))) {
                // Insert Contacts
                contact newContact = new contact(LastName =  ur.LastName, FirstName = ur.FirstName, Email = ur.Email, accountId = Label.GE_GEAS_AccountId);
                listOfContactstoInsert.add(newContact);
            } 
            
                for(CollaborationGroup CG : listOfCollaborationGroup){
                if(chatterUsers.containsKey(CG.Id)){
                    AssignedUsersToChatter = chatterUsers.get(CG.Id);
                    if(!AssignedUsersToChatter.isEmpty()){
                         if(!(AssignedUsersToChatter.contains(ur.id))){
                            CollaborationGroupMember cand1 = new CollaborationGroupMember( CollaborationGroupId=CG.id,MemberId = ur.Id);
                            chatterGroupMembers.add(cand1);
                         }
                     
                    }
                }else{
                        CollaborationGroupMember cand = new CollaborationGroupMember( CollaborationGroupId=CG.id,MemberId = ur.Id);
                        chatterGroupMembers.add(cand);
                }
             }

         }
         
        
         try{
              if(!listOfContactstoInsert.isEmpty()){
                  insert listOfContactstoInsert;
                }
              if(!chatterGroupMembers.isEmpty()){
                  insert chatterGroupMembers;
                }
            } Catch(exception ex){
                System.debug(' Following exception occured on insertion in method: assignPermsandOthersOnUser and class : userTriggerHandler ' + ex.getMessage());
                }
         // Call Future mehod to avoid mixed DML exception while adding users to pemission set and group. 
         ScheduleUtility.AddUserToPermissionSetAndGroups(BusinessUserId, UserIds);
         
     }
 }
 
 public void updatePermsOnUserUpdate(List<User> newList, List<User> oldList, Map<Id, User> oldMap, Map<Id, User> newMap, String eventType, String triggerType) {
    
    if(label.Label_updatePermsOnUserUpdate=='true'){
        set<id> PerspecSysUserId = new set<id>();
        list<PermissionSetAssignment> listOfPSToDelete = new list<PermissionSetAssignment>();
        set<id> BusinessAdmins = new set<id>();
        set<Id> FakeUserIds = new set<Id>();
        
        string profileId;
        
        profileId = [select id from Profile where name='Systems Business Admin'].Id;
        
        
        for(user u : newList){
            if( (newMap.get(u.id).Perspecsys_Access__c != oldMap.get(u.id).Perspecsys_Access__c) &&  newMap.get(u.id).Perspecsys_Access__c==true)
                PerspecSysUserId.add(u.id);
            
            if((newMap.get(u.id).profileId != oldMap.get(u.id).profileId) && newMap.get(u.id).profileId==profileId){
                BusinessAdmins.add(u.Id);
            }
        }
        
        list<permissionSet> idOfSSOPermSet = [select id from permissionSet where name= 'Enable_Single_Sign_On'];
        if(!idOfSSOPermSet.isEmpty()){
            for(PermissionSetAssignment ps : [select id from PermissionSetAssignment where PermissionSetId = : idOfSSOPermSet[0].id and AssigneeId =: PerspecSysUserId])
                listOfPSToDelete.add(ps);
        }
        
        if(!listOfPSToDelete.isEmpty()){
            try{
                delete listOfPSToDelete;
            }Catch(exception ex){
                System.debug(' Following exception occured on deletion in method: updatePermsOnUserUpdate and class : userTriggerHandler ' + ex.getMessage());
            }
        }
        if(!BusinessAdmins.isEmpty()){
            ScheduleUtility.AddUserToPermissionSetAndGroups(BusinessAdmins, FakeUserIds); // FakeUserIds will be empty
        }
    }
 }  
    
    public void DeletePermsOnUserUpdate(List<User> newList, Map<Id, User> oldMap, Map<Id, User> newMap, String eventType, String triggerType) {
         
         list<user> userToAdd = new list<user>();
         list<PermissionSet> permissionSets = new list<PermissionSet>();
         List<PackageLicense> gridBuddyLicense = new List<PackageLicense>();
         list<GroupMember> groupMembersToDel =   new list<GroupMember>();
         list<PermissionSetAssignment> PermissionSetusersToDel =   new list<PermissionSetAssignment>();
         list<UserPackageLicense> ManagePackageUserToDel =  new list<UserPackageLicense>();
         permissionSets =  [select id, Name from PermissionSet where Name in ('API_Enable','Enable_Single_Sign_On')];
          
         list<Group> listGroupToAdd = [select id, Name from Group where DeveloperName  = 'Opp_Sharing_Rule'];
         gridBuddyLicense = [select AllowedLicenses, UsedLicenses, Id from PackageLicense where NamespacePrefix = 'GBLite'];
        
        set<id> AllGroupId = new set<id>();
        if(!listGroupToAdd.isEmpty()){
            AllGroupId = (new Map<Id,SObject>(listGroupToAdd)).keySet();
        }
        
        set<id> AllPermissionSetId = new set<id>();
        if(!permissionSets.isEmpty()){
            AllPermissionSetId = (new Map<Id,SObject>(permissionSets)).keySet();
        }
        
        set<id> managePackageId = new set<id>();
        if(!gridBuddyLicense.isEmpty()){
            managePackageId = (new Map<Id,SObject>(gridBuddyLicense)).keySet();
        }
         
        for(user myUser : newList){
            
                if(eventType=='after' && triggerType=='update' && newMap.get(myUser.id).isActive!=oldMap.get(myUser.id).isActive &&  (!newMap.get(myUser.id).isActive)){
                    userToAdd.add(myUser);
                }
                
        }
        if(!userToAdd.isEmpty()){
            for(GroupMember groupUser : [select id, GroupId, UserOrGroupId from GroupMember where UserOrGroupId=: userToAdd and GroupId=: AllGroupId]){
                groupMembersToDel.add(groupUser);
            }
            
            for(PermissionSetAssignment PermUser : [select id, PermissionSetId, AssigneeId from PermissionSetAssignment  where AssigneeId=: userToAdd and PermissionSetId=: AllPermissionSetId]){
               PermissionSetusersToDel.add(PermUser);
            }
            
            for(UserPackageLicense PackageUser : [select id, PackageLicenseId, UserId from UserPackageLicense  where UserId=: userToAdd and PackageLicenseId=: managePackageId]){
               ManagePackageUserToDel.add(PackageUser);
            }
            
            try{
                delete groupMembersToDel;
                delete PermissionSetusersToDel;
                delete ManagePackageUserToDel;
            }Catch(exception ex){
                System.debug(' Following exception occured on deletion in method: DeletePermsOnUserUpdate and class : userTriggerHandler ' + ex.getMessage());
            }
            
        }
    }

}