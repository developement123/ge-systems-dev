/*********************************************************************************
    Author : Jhansi
    Date   : 09 Dec. 2015
    
    Action : This class is used as controller of vf page : AutoGenerateOpportunitiesThroughContract
             This vfpage/class is used to auto generate PO Forecast Opportunity against Contract for offering type = 'OE/Services'
             This is related to JIRA: ASSFDC-458: Opportunity Optimization Requirement
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        09/08/2016      adderd code to identify if quantity has been changed(IsQuantityOverride__c) and also hold a map of PO OLI to Pipelien OLI.Also modifies the wrapper classs wrapperOli - US15668
    Abhishek        09/13/2016      Modified method "Generate" to show min/max order date and also to add validation mentioned here US15675(Req 3)
    Abhishek        04/17/2017      replaced platform&win probability with GE standard Probability
    Abhishek        06/07/2017      added GE_X_Sell__Opportunity_External_ID__c for lead sharing issue      
    Abhishek        07/20/2017      set Corporate_Sharing__c to false for PO forecast opp
*******************************************************************************/
public with sharing class AutoGenerateOpportunitiesController {
    
        public Contract_PO__c con_record;
        public Order__c orderRec;
        public opportunity opp;
        public Id contractId;
        public list<wrapperOli> wrap_Oli{get;set;}
        public List<AggregateResult> orderRecs;
        Date minProdSchStartDate ; // to hold minimum schedule start date across all associated products
        date poLastdate; // max PO Award endate
        public string minPOFirstShipdate{get;private set;} // min PO first ship date
        public string maxPOLastShipdate{get;private set;} // max PO last ship date
        
        //Wrapper Class
        public class wrapperOli{
            public boolean Select_Oli{get;set;}
            public double Prodquantity;
            //public String RevenueIncrement{get;set;}
            //public Decimal noofRevenueIncrement{get;set;}
            public Opportunity_Line_Item__c Oli{get;set;}
        
      
            public wrapperOli(Boolean bool, Opportunity_Line_Item__c OPLI) {
                this.Select_Oli = bool;
                this.Oli = OPLI;
                this.Prodquantity = OPLI.Quantity__c;
                //this.RevenueIncrement = revenue;
                //this.noofRevenueIncrement = noofRevenueIncrement;
            }
        }
        
        //Constructor
        public AutoGenerateOpportunitiesController(ApexPages.StandardController controller) {
        
            wrap_Oli = new list<wrapperOli>();
            orderRecs = new List<AggregateResult>{};
            
            contractId = apexpages.currentpage().getparameters().get('id');
            //Contract Query
           // con_record = [SELECT id,name,Pipeline_Opportunity__c,Price_Escalation_COLA__c, Price_Escalation_Start_Date__c, Program_Manager__c,First_PO_Award_Date__c,Revenue_Increment__c FROM Contract_PO__c WHERE id=:contractId limit 1];
             con_record = [SELECT id,name,Pipeline_Opportunity__c,Price_Escalation_COLA__c, Price_Escalation_Start_Date__c, Program_Manager__c,First_PO_Award_Date__c,Revenue_Increment__c, First_PO_Ship_Date__c FROM Contract_PO__c WHERE id=:contractId limit 1];
            //get last PO Award date from Order Table
            orderRecs = [SELECT MAX(PO_Award_Date__c)aver, max(PO_Award_End_Date__c)POLastDate FROM Order__c where Contract1__c =:contractId]; 
            
            // New code for section "PO dates"
            minPOFirstShipdate = con_record.First_PO_Ship_Date__c.format();
             poLastdate = (date)(orderRecs[0].get('POLastDate'));
            maxPOLastShipdate = poLastdate.format();
            
            String opportunityQuery = 'SELECT ';   
            opportunityQuery += fetchFieldsApiNames('Opportunity');        
            opportunityQuery += '(SELECT ' + fetchFieldsApiNames('Opportunity_Line_Item__c');        
            opportunityQuery = opportunityQuery.subString(0, opportunityQuery.length()-1);        
            opportunityQuery += ' from Opportunity_Line_Items__r order by Id asc) ';    
            opportunityQuery += ' from Opportunity where Id = \'' + con_record.Pipeline_Opportunity__c +'\' limit 1';   
            opp = Database.Query(opportunityQuery);
            if(opp != null){
                if(opp.Opportunity_Line_Items__r != null && opp.Opportunity_Line_Items__r.size()>0){
                     for(Opportunity_Line_Item__c Olis : opp.Opportunity_Line_Items__r){ 
                        Olis.Quantity__c = Math.floor(Olis.Quantity__c);
                        if(con_record.Revenue_Increment__c == 'Annually' && Olis.of_Installments__c != null){
                            Olis.of_Installments__c =  Olis.of_Installments__c * 1;
                        }
                        else if(con_record.Revenue_Increment__c == 'Bi-annually' && Olis.of_Installments__c != null){
                            Olis.of_Installments__c = Olis.of_Installments__c * 2;
                        }
                        else if(con_record.Revenue_Increment__c == 'Quarterly' && Olis.of_Installments__c != null){
                            Olis.of_Installments__c = Olis.of_Installments__c * 4;
                        }
                        else if(con_record.Revenue_Increment__c == 'Monthly' && Olis.of_Installments__c != null){
                            Olis.of_Installments__c = Olis.of_Installments__c * 12;
                        }
                        else if(con_record.Revenue_Increment__c == 'Weekly' && Olis.of_Installments__c != null){
                            Olis.of_Installments__c = Olis.of_Installments__c * 52;
                        }
                        Olis.Installment_Periods__c = con_record.Revenue_Increment__c;
                        wrap_Oli.add(new wrapperOli(false,Olis));
                    }
                }
            }

        }
        
        //This method is invoked when 'Generate' button is clicked on the screen.
        public pageReference Generate() { 
            try {
                date maxProbableScheduleDate,ProbableLastScheduleDate;
                Integer durationToAdd=0;
                // map to hold no of monhs/days as per revenue increment
                map<string, Integer> ReventIncrementToDuration = new map<string, Integer>();
                ReventIncrementToDuration.put('Bi-Annually',6);
                ReventIncrementToDuration.put('Quarterly',3);
                ReventIncrementToDuration.put('Monthly',1);
                ReventIncrementToDuration.put('Weekly',7);
                ReventIncrementToDuration.put('Daily',1);
                ReventIncrementToDuration.put('Annually',12);
                
                Opportunity cloneOpp = opp.clone(false,true);
                cloneOpp.OwnerId = con_record.Program_Manager__c;
                cloneOpp.StageName = label.OpportunityStage10; // open
                cloneOpp.GE_Standard_Probability__c = label.GEStandardProb3; // 3= High
                cloneOpp.GE_X_Sell__Opportunity_External_ID__c = null;
                cloneOpp.Corporate_Sharing__c = false;
                cloneOpp.Reason_Won_Lost__c = '';               
                
                if(orderRecs.size() > 0)
                    cloneOpp.CloseDate = Date.valueOf(orderRecs[0].get('aver'));
                
                cloneOpp.ContractID1__c = con_record.Id;
                cloneOpp.recordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Label.Label_POForecastOpportunity).getRecordTypeId();
                cloneOpp.Price_Escalation_COLA__c = con_record.Price_Escalation_COLA__c; 
                cloneOpp.Price_Escalation_Start_Date__c = con_record.Price_Escalation_Start_Date__c;
                
                list<Opportunity_Line_Item__c> OPLI_list = new list<Opportunity_Line_Item__c>();
                
                if(wrap_Oli.size() > 0)
                {
                   integer count = 0;
                   for(wrapperOli wrap : wrap_Oli) {
                        if(wrap.Select_Oli == true) {
                            Opportunity_Line_Item__c temp = wrap.Oli.clone(false,true);
                            // to caluculate maximum possible schedule date across all associated products
                            durationToAdd = Integer.valueOf( ReventIncrementToDuration.get(con_record.Revenue_Increment__c)*(wrap.Oli.of_Installments__c-1));
                            
                            if( con_record.Revenue_Increment__c =='Weekly' ||   con_record.Revenue_Increment__c == 'Daily'){
                                ProbableLastScheduleDate = temp.Schedule_Start_Date__c.addDays(durationToAdd);
                            }Else{
                                ProbableLastScheduleDate = temp.Schedule_Start_Date__c.addMonths(durationToAdd);
                            }
                            
                            // to caluculate minimum schedule start date across all associated products
                            if(count == 0){
                                minProdSchStartDate = temp.Schedule_Start_Date__c;
                                maxProbableScheduleDate = ProbableLastScheduleDate;
                            }Else If(ProbableLastScheduleDate>maxProbableScheduleDate){
                                maxProbableScheduleDate = ProbableLastScheduleDate;
                            }
                            
                            
                            if(minProdSchStartDate > temp.Schedule_Start_Date__c)
                            {
                                minProdSchStartDate = temp.Schedule_Start_Date__c;
                            }
                            
                            // new code to keep a map of PO's OLI to Pipeline's OLI (Pipeline_Associated_Product__c=>Opportunity_Line_Item__c) - US15668
                            temp.Pipeline_Associated_Product__c = wrap.Oli.id;
                            if(wrap.Prodquantity != wrap.Oli.Quantity__c){ // if there is a different in original quantity and user's supplied quantity
                                temp.IsQuantityOverride__c = true;
                                temp.DistributeQuantityAsPerPipeline__c = false;
                            }
                            temp.DistributeQuantityAsPerPipeline__c = true;
                        
                            
                            OPLI_list.add(temp);
                            count++;
                        }
                        
                    } 
                    
                }
                
                if(OPLI_list.size() <= 0 ){ // Displays the Error message 'Generate' button is clicked if check box is False
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.AutoGenerateOpportunitiesThroughContract_APValidation));    
                    return null;
                }
                // checking minimum schedule start is equal to contract First Ship / Service Date
                if(minProdSchStartDate != con_record.First_PO_Ship_Date__c && System.Label.AutoGenerateOpportunitiesThroughContract_IsFirstShipValidationRequired=='true')
                {
                  Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.AutoGenerateOpportunitiesThroughContract_FirstShipValidation + ' <b> ' + con_record.First_PO_Ship_Date__c.format() + '</b>'));    
                  return null;
                }
                // Check if max possible schedule date greater than last PO order date then throw error
                if(maxProbableScheduleDate > poLastdate && Label.ValidateOrderOnSchedule=='true'){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,System.Label.AutoGenerateOpportunitiesThroughContract_OrderRequired + ' <b> ' + poLastdate.format() + '</b>'));    
                  return null;
                }
                
                //inserting PO Forecast Opp
                if(cloneOpp != null)
                   insert cloneOpp;
                   
                //Assigning Opp Id to AP
                if(!OPLI_list.isEmpty())
                {   
                    for(Opportunity_Line_Item__c OPL : OPLI_list)
                        OPL.Opportunity__c = cloneOpp.id;
                }
                 
                if(!OPLI_list.isEmpty())// insert the related  associated products under the Opportunity if the list is not empty
                    insert OPLI_list;
                    
                pageReference pg = new pageReference('/'+cloneOpp.id);
                return pg;
            
            }
            catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                return null;
            
            }
        }
        
        //action method on Cancel button
        public pageReference cancel() {
            pageReference pg = new pageReference('/'+contractId);
            return pg;
        }
    
        //fetching field names for dynamic query
        public static String fetchFieldsApiNames(String SobjectApiName) {
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
            String queryString = '';
            for(String fieldName : fieldMap.keyset()) {
                queryString += fieldName + ',';    
            }        
            return queryString;
        }

}