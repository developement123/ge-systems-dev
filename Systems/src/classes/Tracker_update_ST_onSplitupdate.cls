/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - update_ST_onSplitupdate
    
    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          12/02/2014        Initial Creation
                Abhishek             29/02/2016        Modfied for JIRA 498
    last modified by : Mohan Kumar(20 May 2015)
    Last modified by : Abhishek(28 April 2015) - Commented all as per JIRA 553.
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_update_ST_onSplitupdate {
   /*
    static testmethod void test_update_ST_onSplitupdate() {
        
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;  
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;      

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
        
        List<User> listOfUsers = new List<User>();
        
        listOfUsers.add(new User(alias = 'skapoor1', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor1', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal1@ge.com.sys.dev'));
        listOfUsers.add(new User(alias = 'skapoor2', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal1@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor2', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal2@ge.com.sys.dev')); 
        listOfUsers.add(new User(alias = 'skapoor6', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal5@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor6', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal6@ge.com.sys.dev'));
        
        insert listOfUsers;
        
        Account acc = new Account(Name = 'Hello');
        insert acc;

        System.runAs(listOfUsers[0]) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc.id,
                                              Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group',
                                               Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = listOfUsers[0].id,
                                             Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
            List<Sales_Target__c> listofST = new List<Sales_Target__c>();
            
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[0].id, Sales_Year__c= String.valueOf(System.Today().Year()),
                                             Growth_Achieved__c = 10,Growth_Pipeline__c = 15, MTG_Achieved__c= 20, MTG_Pipeline__c= 25));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[0].id, Sales_Year__c = String.valueOf(System.Today().Year() +1), 
                                             Growth_Achieved__c = 10,Growth_Pipeline__c = 15, MTG_Achieved__c= 20, MTG_Pipeline__c= 25));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[1].id, Sales_Year__c= String.valueOf(System.Today().Year()), 
                                             Growth_Achieved__c = 10,Growth_Pipeline__c = 15, MTG_Achieved__c= 20, MTG_Pipeline__c= 25));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[1].id, Sales_Year__c = String.valueOf(System.Today().Year() +1), 
                                             Growth_Achieved__c = 10,Growth_Pipeline__c = 15, MTG_Achieved__c= 20, MTG_Pipeline__c= 25));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[2].id, Sales_Year__c= String.valueOf(System.Today().Year()),  
                                             Growth_Achieved__c = 10,Growth_Pipeline__c = 15, MTG_Achieved__c= 20, MTG_Pipeline__c= 25));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[2].id, Sales_Year__c = String.valueOf(System.Today().Year() +1),  
                            Growth_Achieved__c = 10,Growth_Pipeline__c = 15, MTG_Achieved__c= 20, MTG_Pipeline__c= 25));       
             
            insert listofST;
            
        Test.startTest(); 
            Class_TriggerCheck.var_update_ST_onSplitupdateTrg = true;
            List<Opportunity> listOfOppy = new List<Opportunity>();
            // added Account_Platform__c for JIRA 498
            listOfOppy.add(new opportunity(Name = 'Test opp1', StageName = 'Prospecting', CloseDate = date.today(), CY_Margin_Escalator1__c = 500,
                                          Program__c = prog.id, Amount = 100, Growth__c = true, Firm_Anticipated__c = 'Anticipated',
                                          Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id, 
                                          MTG_Year__c = String.valueOf(System.Today().Year()), Account_Platform__c = fore.Id, Contract_Award_Date__c = System.Today()));
            // added Account_Platform__c for JIRA 498
            listOfOppy.add(new opportunity(Name = 'Test opp2', StageName = 'Contract Awarded', CloseDate = date.today(),
                                          Program__c = prog.id, Amount = 500, Growth__c = true, Firm_Anticipated__c = 'Anticipated',
                                          Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id, MTG_Year__c = String.valueOf(System.Today().Year()), Account_Platform__c = fore.Id,  Contract_Award_Date__c = System.Today()));                                 
            insert listOfOppy;
             
            OpportunitySplitType splitTypeCYME = [select id from OpportunitySplitType where masterLabel = 'CY Margin Escalator' LIMIT 1];
            OpportunitySplitType splitTypeGrwth = [select id from OpportunitySplitType where masterLabel = 'Growth' LIMIT 1];
            
            List<opportunitySplit> lstToBeUpdated = new List<opportunitySplit>();
            OpportunitySplit osCYME = new opportunitySplit(opportunityId = listOfOppy[0].Id, SplitOwnerId = listOfUsers[2].id, splitPercentage = 50, splitTypeId = splitTypeCYME.Id);              
            OpportunitySplit osGrwth = new opportunitySplit(opportunityId = listOfOppy[0].Id, SplitOwnerId = listOfUsers[2].id, splitPercentage = 50, splitTypeId = splitTypeGrwth.Id);
            
            OpportunitySplit osCYME_2 = new opportunitySplit(opportunityId = listOfOppy[1].Id, SplitOwnerId = listOfUsers[2].id, splitPercentage = 50, splitTypeId = splitTypeCYME.Id);              
            OpportunitySplit osGrwth_2 = new opportunitySplit(opportunityId = listOfOppy[1].Id, SplitOwnerId = listOfUsers[2].id, splitPercentage = 50, splitTypeId = splitTypeGrwth.Id);
            
            lstToBeUpdated.add(osCYME);
            lstToBeUpdated.add(osGrwth);
            
            lstToBeUpdated.add(osCYME_2);
            lstToBeUpdated.add(osGrwth_2);
            insert lstToBeUpdated;  
            
            Set<opportunitySplit> setToUpsert = new Set<opportunitySplit>();
            List<opportunitySplit> listToUpsert = new List<opportunitySplit>();
            OpportunitySplit osQuery = [select Id, splitPercentage from OpportunitySplit where OpportunityId =: listOfOppy[0].Id AND splitType.masterLabel = 'CY Margin Escalator' LIMIT 1];
            OpportunitySplit osQuerygrwth = [select Id, splitPercentage from OpportunitySplit where OpportunityId =: listOfOppy[0].Id AND splitType.masterLabel = 'Growth' LIMIT 1];          
            
            OpportunitySplit osQuery_2 = [select Id, splitPercentage from OpportunitySplit where OpportunityId =: listOfOppy[1].Id AND splitType.masterLabel = 'CY Margin Escalator' LIMIT 1];
            OpportunitySplit osQuerygrwth_2 = [select Id, splitPercentage from OpportunitySplit where OpportunityId =: listOfOppy[1].Id AND splitType.masterLabel = 'Growth' LIMIT 1];          
               
            osQuery.splitPercentage = 30;
            osQuerygrwth.splitPercentage = 30;
            
            osQuery_2.splitPercentage = 30;
            osQuerygrwth_2.splitPercentage = 30;
            
            setToUpsert.add(osQuery);
            setToUpsert.add(osQuerygrwth);
            
            setToUpsert.add(osQuery_2);
            setToUpsert.add(osQuerygrwth_2);
            
            OpportunitySplit osCYME1 = new opportunitySplit(opportunityId = listOfOppy[0].Id, SplitOwnerId = listOfUsers[1].id, splitPercentage = 20, splitTypeId = splitTypeCYME.Id);              
            OpportunitySplit osGrwth1 = new opportunitySplit(opportunityId = listOfOppy[0].Id, SplitOwnerId = listOfUsers[1].id, splitPercentage = 20, splitTypeId = splitTypeGrwth.Id);              
            
            OpportunitySplit osCYME1_2 = new opportunitySplit(opportunityId = listOfOppy[1].Id, SplitOwnerId = listOfUsers[1].id, splitPercentage = 20, splitTypeId = splitTypeCYME.Id);              
            OpportunitySplit osGrwth1_2 = new opportunitySplit(opportunityId = listOfOppy[1].Id, SplitOwnerId = listOfUsers[1].id, splitPercentage = 20, splitTypeId = splitTypeGrwth.Id);              
            
            setToUpsert.add(osCYME1);
            setToUpsert.add(osCYME);
            
            setToUpsert.add(osGrwth1);
            setToUpsert.add(osGrwth);
            
            setToUpsert.add(osCYME1_2);
            setToUpsert.add(osCYME_2);
            
            setToUpsert.add(osGrwth1_2);
            setToUpsert.add(osGrwth_2);
            
           // Class_TriggerCheck.var_update_ST_onSplitupdateTrg = true;
            listToUpsert.addAll(setToUpsert);
            try {
                update listToUpsert;
            }
            catch(Exception ex) {
            }           
            
         //   Class_TriggerCheck.var_update_ST_onSplitupdateTrg = true;
            osCYME1.splitPercentage = 30;
            osCYME.splitPercentage = 10;
            osQuery.splitPercentage = 60;
            
            osCYME1_2.splitPercentage = 30;
            osCYME_2.splitPercentage = 10;
            osQuery_2.splitPercentage = 60;
            
            osGrwth1.splitPercentage = 30;
            osGrwth.splitPercentage = 10;
            osQuerygrwth.splitPercentage = 60;
            
            osGrwth1_2.splitPercentage = 30;
            osGrwth_2.splitPercentage = 10;
            osQuerygrwth_2.splitPercentage = 60;
            
            try {
                upsert listToUpsert;
            }
            catch(Exception ex) {
            } 
            
            List<OpportunitySplit> listToBeDeleted = new List<OpportunitySplit>();
            listToBeDeleted.add(osCYME);
            listToBeDeleted.add(osGrwth);
            listToBeDeleted.add(osCYME_2);
            listToBeDeleted.add(osGrwth_2);
            
            listToBeDeleted.add(osCYME1);
            listToBeDeleted.add(osGrwth1);
            listToBeDeleted.add(osCYME1_2);
            listToBeDeleted.add(osGrwth1_2); 
           // Class_TriggerCheck.var_update_ST_onSplitupdateTrg = true;
            
            try {
                delete listToBeDeleted;
            }
            catch(Exception ex) {
            }
        Test.stopTest();   
        }
    }
    
    static testmethod void test_update_ST_onSplitupdate1() {
        
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;    
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;      

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
        
        List<User> listOfUsers = new List<User>();
        
        listOfUsers.add(new User(alias = 'skapoor1', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor1', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal1@ge.com.sys.dev'));
        
        listOfUsers.add(new User(alias = 'skapoor2', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal1@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor2', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal2@ge.com.sys.dev')); 
        
        listOfUsers.add(new User(alias = 'skapoor6', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal5@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor6', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal6@ge.com.sys.dev'));
        
        insert listOfUsers;
        
        Account acc = new Account(Name = 'Hello');
        insert acc;

        System.runAs(listOfUsers[0]) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc.id,
                                              Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = listOfUsers[0].id,
                                             Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
            List<Sales_Target__c> listofST = new List<Sales_Target__c>();
            
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[0].id, Sales_Year__c= String.valueOf(System.Today().Year())));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[0].id, Sales_Year__c = String.valueOf(System.Today().Year() +1)));
            
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[1].id, Sales_Year__c= String.valueOf(System.Today().Year())));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[1].id, Sales_Year__c = String.valueOf(System.Today().Year() +1)));
            
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[2].id, Sales_Year__c= String.valueOf(System.Today().Year())));
            listofST.add(new Sales_Target__c(Sales_Person__c = listOfUsers[2].id, Sales_Year__c = String.valueOf(System.Today().Year() +1)));
             
            insert listofST;
            
        Test.startTest(); 
            Class_TriggerCheck.var_update_ST_onSplitupdateTrg = true;
            List<Opportunity> listOfOppy = new List<Opportunity>();
            // added Account_Platform__c for JIRA 498
            listOfOppy.add(new opportunity(Name = 'Test opp1', StageName = 'Prospecting', CloseDate = date.today(),
                                          Program__c = prog.id, Amount = 500, Growth__c = true, Firm_Anticipated__c = 'Anticipated',
                                          Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id, Contract_Award_Date__c = date.today(), MTG_Year__c = String.valueOf(System.Today().Year()), Account_Platform__c = fore.Id));
            // added Account_Platform__c for JIRA 498
            listOfOppy.add(new opportunity(Name = 'Test opp2', StageName = 'Contract Awarded', CloseDate = date.today(),
                                          Program__c = prog.id, Amount = 500, Growth__c = true, Firm_Anticipated__c = 'Anticipated',
                                          Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id, Contract_Award_Date__c = date.today(), MTG_Year__c = String.valueOf(System.Today().Year()), Account_Platform__c = fore.Id));                                 
            try {
                insert listOfOppy;
            }
            catch(Exception ex) {
            }
        Test.stopTest();   
        }
    } */
}