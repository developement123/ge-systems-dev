/****************************************************************************
    Developer : Abhishek
    Date      : 29/June/2016    
    Action    : This is a handler class of trigger "TriggerOnPlaformMaster"

    Change desription:
    Developer         Date                  Description
    Abhishek          09/13/2017            replaced account platform checks with opportunity and major responses. 
*****************************************************************************/
public with sharing class PlatformMasterTriggerHandler{
    
    // Method 1 :  To prevent Platfrom master deletion, if it is being used in Opportunity or major response
    
    public void ValidatePlatformMasterOnDelete(Map<id,Platform_Master__c> oldMap, String eventType, String triggerType){
        if(Label.Label_ValidatePlatformMasterOnDelete == 'true'){  
               set<id>pmasterId = new set<id>();
                for(Opportunity Opp : [select id, Platform_New__c from Opportunity where Platform_New__c=:oldMap.keyset() ]){
                   pmasterId.add(Opp.Platform_New__c);
                }
                
                for(Major_Proposal__c mp : [select id, Platform__c from Major_Proposal__c where Platform__c=:oldMap.keyset()]){
                    pmasterId.add(mp.Platform__c); 
                }
                if(eventType == 'Before' && triggerType == 'Delete') {
                    for(Platform_Master__c pMaster : oldMap.values()){
                        if(pmasterId.contains(pMaster.id)){
                              pMaster.adderror(Label.ValidatePlatformMasterOnDelete);
                        }
                    }
                }
        }
    }

}