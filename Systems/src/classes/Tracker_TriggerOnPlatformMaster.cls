/*****************************************************************
This test class was created to provide code coverage for the following trigger/Class:
- TriggerOnPlatformMaster/PlatformMasterTriggerHandler

Version     Author(s)               Date              Details 
--------------------------------------------------------------------------------------
1.0         Abhishek          07/14/2016        Initial Creation
            Abhishek          09/18/2017        Removed account platform
*******************************************************************/   

@isTest
private class Tracker_TriggerOnPlatformMaster{

    static testMethod void  testAccountValidationOnDelete(){
        Class_TriggerCheck.var_TriggerOnAccount = false;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
         System.runAs(u) {
            Account acc1 = new Account(Name = 'Hello');
            insert acc1;
            
            Account childacc = new Account(Name = 'childacc', Parentid = acc1.id);
            insert childacc;
         
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;
            
            Major_Proposal__c mp = new Major_Proposal__c(Name = 'Test Proposal',Account__c=acc1.id,Platform__c=pm.id);
            
            try{
                delete pm;
           } catch(exception e) {
              boolean isException = e.getMessage().contains('account platform first') ? true : false ;
                    system.assertEquals(isException , true);
           }
            
         }

    }

}