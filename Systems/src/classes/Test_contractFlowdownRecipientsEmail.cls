/*********************************************
Created By Tim Lomison
Created On Jan 20 2017
Description To test trigger contractFlowdownRecipientsEmail
*************************************************/
@isTest
public Class Test_contractFlowdownRecipientsEmail {        
   public static testMethod void Method1()
   {
   
       Contract_Flowdown__c cfd = new Contract_Flowdown__c();
                cfd.Document_Type__c = 'Digital Solutions';
                cfd.Data_Share_Language__c = true;
             insert cfd;
         
         
        Contract_Flowdown_Recipients__c Cfrc = new Contract_Flowdown_Recipients__c();
                    Cfrc.Name = 'Test';
                    Cfrc.Document_Types__c = 'Digital Solutions';
                    Cfrc.Email__c = 'test@test.comx';
                    Cfrc.Role__c = 'testing';
                    Cfrc.Data_Share_Language__c = true;
                insert Cfrc;
        Contract_Flowdown_Recipients__c Cfrc1 = new Contract_Flowdown_Recipients__c();
                    Cfrc1.Name = 'Test';
                    Cfrc1.Document_Types__c = 'Digital Solutions';
                    Cfrc1.Email__c = 'test@test.comx1';
                    Cfrc1.Role__c = 'testing';
                    Cfrc1.Data_Share_Language__c = true;
                insert Cfrc1;
        
        Contract_Flowdown_Recipients__c Cfrc2 = new Contract_Flowdown_Recipients__c();
                    Cfrc2.Name = 'Test';
                    Cfrc2.Document_Types__c = 'Digital Solutions';
                    Cfrc2.Email__c = 'test@test.comx2';
                    Cfrc2.Role__c = 'testing';
                    Cfrc2.Data_Share_Language__c = true;
                insert Cfrc2;
        
        Contract_Flowdown_Recipients__c Cfrc3 = new Contract_Flowdown_Recipients__c();
                    Cfrc3.Name = 'Test';
                    Cfrc3.Document_Types__c = 'Digital Solutions';
                    Cfrc3.Email__c = 'test@test.comx3';
                    Cfrc3.Role__c = 'testing';
                    Cfrc3.Data_Share_Language__c = true;
                insert Cfrc3;
        
        Contract_Flowdown_Recipients__c Cfrc4 = new Contract_Flowdown_Recipients__c();
                    Cfrc4.Name = 'Test';
                    Cfrc4.Document_Types__c = 'Digital Solutions';
                    Cfrc4.Email__c = 'test@test.comx4';
                    Cfrc4.Role__c = 'testing';
                    Cfrc4.Data_Share_Language__c = true;
                insert Cfrc4;
        
        Contract_Flowdown_Recipients__c Cfrc5 = new Contract_Flowdown_Recipients__c();
                    Cfrc5.Name = 'Test';
                    Cfrc5.Document_Types__c = 'Digital Solutions';
                    Cfrc5.Email__c = 'test@test.comx5';
                    Cfrc5.Role__c = 'testing';
                    Cfrc5.Data_Share_Language__c = true;
                insert Cfrc5;
        
        Contract_Flowdown_Recipients__c Cfrc6 = new Contract_Flowdown_Recipients__c();
                    Cfrc6.Name = 'Test';
                    Cfrc6.Document_Types__c = 'Digital Solutions';
                    Cfrc6.Email__c = 'test@test.comx6';
                    Cfrc6.Role__c = 'testing';
                    Cfrc6.Data_Share_Language__c = true;
                insert Cfrc6;
        
        Contract_Flowdown_Recipients__c Cfrc7 = new Contract_Flowdown_Recipients__c();
                    Cfrc7.Name = 'Test';
                    Cfrc7.Document_Types__c = 'Digital Solutions';
                    Cfrc7.Email__c = 'test@test.comx7';
                    Cfrc7.Role__c = 'testing';
                    Cfrc7.Data_Share_Language__c = true;
                insert Cfrc7;
        
        Contract_Flowdown_Recipients__c Cfrc8 = new Contract_Flowdown_Recipients__c();
                    Cfrc8.Name = 'Test';
                    Cfrc8.Document_Types__c = 'Digital Solutions';
                    Cfrc8.Email__c = 'test@test.comx8';
                    Cfrc8.Role__c = 'testing';
                    Cfrc8.Data_Share_Language__c = true;
                insert Cfrc8;
        
        Contract_Flowdown_Recipients__c Cfrc9 = new Contract_Flowdown_Recipients__c();
                    Cfrc9.Name = 'Test';
                    Cfrc9.Document_Types__c = 'Digital Solutions';
                    Cfrc9.Email__c = 'test@test.comx9';
                    Cfrc9.Role__c = 'testing';
                    Cfrc9.Data_Share_Language__c = true;
                insert Cfrc9;
        
        Contract_Flowdown_Recipients__c Cfrc10 = new Contract_Flowdown_Recipients__c();
                    Cfrc10.Name = 'Test';
                    Cfrc10.Document_Types__c = 'Digital Solutions';
                    Cfrc10.Email__c = 'test@test.comx10';
                    Cfrc10.Role__c = 'testing';
                    Cfrc10.Data_Share_Language__c = true;
                insert Cfrc10;
        
        Contract_Flowdown_Recipients__c Cfrc11 = new Contract_Flowdown_Recipients__c();
                    Cfrc11.Name = 'Test';
                    Cfrc11.Document_Types__c = 'Digital Solutions';
                    Cfrc11.Email__c = 'test@test.comx11';
                    Cfrc11.Role__c = 'testing';
                    Cfrc11.Data_Share_Language__c = true;
                insert Cfrc11;
        
        Contract_Flowdown_Recipients__c Cfrc12 = new Contract_Flowdown_Recipients__c();
                    Cfrc12.Name = 'Test';
                    Cfrc12.Document_Types__c = 'Digital Solutions';
                    Cfrc12.Email__c = 'test@test.comx12';
                    Cfrc12.Role__c = 'testing';
                    Cfrc12.Data_Share_Language__c = true;
                insert Cfrc12;
        
        Contract_Flowdown_Recipients__c Cfrc13 = new Contract_Flowdown_Recipients__c();
                    Cfrc13.Name = 'Test';
                    Cfrc13.Document_Types__c = 'Digital Solutions';
                    Cfrc13.Email__c = 'test@test.comx13';
                    Cfrc13.Role__c = 'testing';
                    Cfrc13.Data_Share_Language__c = true;
                insert Cfrc13;
        
        Contract_Flowdown_Recipients__c Cfrc14 = new Contract_Flowdown_Recipients__c();
                    Cfrc14.Name = 'Test';
                    Cfrc14.Document_Types__c = 'Digital Solutions';
                    Cfrc14.Email__c = 'test@test.comx14';
                    Cfrc14.Role__c = 'testing';
                    Cfrc14.Data_Share_Language__c = true;
                insert Cfrc14;
        
        Contract_Flowdown_Recipients__c Cfrc15 = new Contract_Flowdown_Recipients__c();
                    Cfrc15.Name = 'Test';
                    Cfrc15.Document_Types__c = 'Digital Solutions';
                    Cfrc15.Email__c = 'test@test.comx15';
                    Cfrc15.Role__c = 'testing';
                    Cfrc15.Data_Share_Language__c = true;
                insert Cfrc15;

   }
   
   public static testMethod void Method2()
   {
    
        Contract_Flowdown__c cfd = new Contract_Flowdown__c();
        cfd.Document_Type__c = 'Avionics Civil OE';
        cfd.Data_Share_Language__c = true;
        insert cfd;
        
         
        Contract_Flowdown_Recipients__c Cfrc = new Contract_Flowdown_Recipients__c();
                    Cfrc.Name = 'Test';
                    Cfrc.Document_Types__c = 'Digital Solutions';
                    Cfrc.Email__c = 'test@test.comx';
                    Cfrc.Role__c = 'testing';
                    Cfrc.Data_Share_Language__c = true;
                insert Cfrc;
        Contract_Flowdown_Recipients__c Cfrc1 = new Contract_Flowdown_Recipients__c();
                    Cfrc1.Name = 'Test';
                    Cfrc1.Document_Types__c = 'Digital Solutions';
                    Cfrc1.Email__c = 'test@test.comx1';
                    Cfrc1.Role__c = 'testing';
                    Cfrc1.Data_Share_Language__c = true;
                insert Cfrc1;
        
        Contract_Flowdown_Recipients__c Cfrc2 = new Contract_Flowdown_Recipients__c();
                    Cfrc2.Name = 'Test';
                    Cfrc2.Document_Types__c = 'Digital Solutions';
                    Cfrc2.Email__c = 'test@test.comx2';
                    Cfrc2.Role__c = 'testing';
                    Cfrc2.Data_Share_Language__c = true;
                insert Cfrc2;
        
        Contract_Flowdown_Recipients__c Cfrc3 = new Contract_Flowdown_Recipients__c();
                    Cfrc3.Name = 'Test';
                    Cfrc3.Document_Types__c = 'Digital Solutions';
                    Cfrc3.Email__c = 'test@test.comx3';
                    Cfrc3.Role__c = 'testing';
                    Cfrc3.Data_Share_Language__c = true;
                insert Cfrc3;
        
        Contract_Flowdown_Recipients__c Cfrc4 = new Contract_Flowdown_Recipients__c();
                    Cfrc4.Name = 'Test';
                    Cfrc4.Document_Types__c = 'Digital Solutions';
                    Cfrc4.Email__c = 'test@test.comx4';
                    Cfrc4.Role__c = 'testing';
                    Cfrc4.Data_Share_Language__c = true;
                insert Cfrc4;
        
        Contract_Flowdown_Recipients__c Cfrc5 = new Contract_Flowdown_Recipients__c();
                    Cfrc5.Name = 'Test';
                    Cfrc5.Document_Types__c = 'Digital Solutions';
                    Cfrc5.Email__c = 'test@test.comx5';
                    Cfrc5.Role__c = 'testing';
                    Cfrc5.Data_Share_Language__c = true;
                insert Cfrc5;
        
        Contract_Flowdown_Recipients__c Cfrc6 = new Contract_Flowdown_Recipients__c();
                    Cfrc6.Name = 'Test';
                    Cfrc6.Document_Types__c = 'Digital Solutions';
                    Cfrc6.Email__c = 'test@test.comx6';
                    Cfrc6.Role__c = 'testing';
                    Cfrc6.Data_Share_Language__c = true;
                insert Cfrc6;
        
        Contract_Flowdown_Recipients__c Cfrc7 = new Contract_Flowdown_Recipients__c();
                    Cfrc7.Name = 'Test';
                    Cfrc7.Document_Types__c = 'Digital Solutions';
                    Cfrc7.Email__c = 'test@test.comx7';
                    Cfrc7.Role__c = 'testing';
                    Cfrc7.Data_Share_Language__c = true;
                insert Cfrc7;
        
        Contract_Flowdown_Recipients__c Cfrc8 = new Contract_Flowdown_Recipients__c();
                    Cfrc8.Name = 'Test';
                    Cfrc8.Document_Types__c = 'Digital Solutions';
                    Cfrc8.Email__c = 'test@test.comx8';
                    Cfrc8.Role__c = 'testing';
                    Cfrc8.Data_Share_Language__c = true;
                insert Cfrc8;
        
        Contract_Flowdown_Recipients__c Cfrc9 = new Contract_Flowdown_Recipients__c();
                    Cfrc9.Name = 'Test';
                    Cfrc9.Document_Types__c = 'Digital Solutions';
                    Cfrc9.Email__c = 'test@test.comx9';
                    Cfrc9.Role__c = 'testing';
                    Cfrc9.Data_Share_Language__c = true;
                insert Cfrc9;
        
        Contract_Flowdown_Recipients__c Cfrc10 = new Contract_Flowdown_Recipients__c();
                    Cfrc10.Name = 'Test';
                    Cfrc10.Document_Types__c = 'Digital Solutions';
                    Cfrc10.Email__c = 'test@test.comx10';
                    Cfrc10.Role__c = 'testing';
                    Cfrc10.Data_Share_Language__c = true;
                insert Cfrc10;
        
        Contract_Flowdown_Recipients__c Cfrc11 = new Contract_Flowdown_Recipients__c();
                    Cfrc11.Name = 'Test';
                    Cfrc11.Document_Types__c = 'Digital Solutions';
                    Cfrc11.Email__c = 'test@test.comx11';
                    Cfrc11.Role__c = 'testing';
                    Cfrc11.Data_Share_Language__c = true;
                insert Cfrc11;
        
        Contract_Flowdown_Recipients__c Cfrc12 = new Contract_Flowdown_Recipients__c();
                    Cfrc12.Name = 'Test';
                    Cfrc12.Document_Types__c = 'Digital Solutions';
                    Cfrc12.Email__c = 'test@test.comx12';
                    Cfrc12.Role__c = 'testing';
                    Cfrc12.Data_Share_Language__c = true;
                insert Cfrc12;
        
        Contract_Flowdown_Recipients__c Cfrc13 = new Contract_Flowdown_Recipients__c();
                    Cfrc13.Name = 'Test';
                    Cfrc13.Document_Types__c = 'Digital Solutions';
                    Cfrc13.Email__c = 'test@test.comx13';
                    Cfrc13.Role__c = 'testing';
                    Cfrc13.Data_Share_Language__c = true;
                insert Cfrc13;
        
        Contract_Flowdown_Recipients__c Cfrc14 = new Contract_Flowdown_Recipients__c();
                    Cfrc14.Name = 'Test';
                    Cfrc14.Document_Types__c = 'Digital Solutions';
                    Cfrc14.Email__c = 'test@test.comx14';
                    Cfrc14.Role__c = 'testing';
                    Cfrc14.Data_Share_Language__c = true;
                insert Cfrc14;
        
        Contract_Flowdown_Recipients__c Cfrc15 = new Contract_Flowdown_Recipients__c();
                    Cfrc15.Name = 'Test';
                    Cfrc15.Document_Types__c = 'Digital Solutions';
                    Cfrc15.Email__c = 'test@test.comx15';
                    Cfrc15.Role__c = 'testing';
                    Cfrc15.Data_Share_Language__c = true;
                insert Cfrc15;
        cfd.Document_Type__c = 'Digital Solutions';
        update cfd;

   }
}