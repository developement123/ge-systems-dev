/*
  Author: Mayank Joshi
  Date: 3/4/2013 6:00 AM
  Action: Logic is written for Multiple selection of Responses on the button Associated Response(s)  .
  Showing only Responses from Major Response>Acc Platform>Programs .
  Logic Last Modified By: Mayank Joshi,  3/7/2013 2:50 AM .
  Logic Last Modified By: Nupur Gupta, 05/22/2014
  Change History
    =============
    Developer       Modified On     Change description
    Abhishek        09/12/2017      replaced account platform with account and platform master in "SOQL,GET Params" in URL
*/
public class Controller_Selected_Responses
{
    Public List<cObject> ResponseList {get; set;}
    Set<Id> ProgIds = new Set<Id> ();
    public String MjrRespId;
    
    public String searchString {get;set;}
    String pltfid, accid;
    
    Public List<cObject> wrapRespDataList{get;set;}
    Public List<cObject> checkedWrapList = new List<cObject>();
    
    Set<Id> wrapRespIdSet = new Set<Id>();
    Set<Id> idsToRemove = new Set<Id>();
    
    public integer intSelectedPageNum; // selected Pageination number
    public integer intItemsPerPage = 20; // number of Items to be displayed per Page
    public integer intRemainingItemsInLastPage; // no. of Items in last Page
    public integer intTotalPages; // Total number of Pages
    
    public boolean hasPrevious {get; set;}
    public boolean hasNext {get; set;}
             
    Public void SearchResponses()
    {
        pltfid = System.currentPageReference().getParameters().get('platformid');  //  Platform ID
        accid = System.currentPageReference().getParameters().get('accid');        
        //List<Program__c> Lst_Prog = [Select Id from Program__c WHERE Forecast__c =:AccPltfId];
              
        //For(Program__c Pro_loop1 :Lst_Prog){
        //    ProgIds.add(Pro_loop1.ID);
        //}   
        
        List<Opportunity> MyOpp = new list<Opportunity>();
        MyOpp = [Select Id from Opportunity WHERE accountId =:accid and Platform_New__c =:pltfid ]; // replace account platform with account and platform master
        if(!MyOpp.isEmpty()){
            For(Opportunity O :MyOpp){
                ProgIds.add(O.ID);
            } 
        }
        ResponseList = new List<cObject>();
        List<Sales_Request__c> respList = new List<Sales_Request__c>();
                
        if(searchString == NULL || searchString==''){
            respList = [Select Id,Name,Response_ID2__c,Program__c,Deal_Type__c,CM2__c,Total_Amount2__c, Pipeline_Opportunity__c from Sales_Request__c where (Program__c =: progIds or Pipeline_Opportunity__c =:progIds)];
        }
        
        If(searchString!=null && searchString!=''){
            respList = [Select Id,Name,Response_ID2__c,Program__c,Deal_Type__c,CM2__c,Total_Amount2__c, Pipeline_Opportunity__c from Sales_Request__c where (Program__c =: progIds or Pipeline_Opportunity__c =:progIds) and Response_ID2__c like: '%'+searchString+'%'];
        }
        
        For(Sales_Request__c c: respList) {
            ResponseList.add(new cObject(c));
        }
        
        if(!ResponseList.isempty()){
            intSelectedPageNum = 1;
            PropertyListPagiNation();
            GetPropertyListForSelectedPageNumber(); // Pagination method 
        }else{
            hasNext = false;
            hasPrevious = false;
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.Info,'Response with this ID does not exists.')); 
        }
        
    }

    // Logic Written for Multiple Selection of Response Records and Redirected to associated Major Response Record after SAVE .
    Public PageReference processSelected() 
    {              
        List<Sales_Request__c> selectedResponses = new List<Sales_Request__c>(); // Selected Responses from All Filtered Responses .
        List<Sales_Request__c> Lst_Responses= new List <Sales_Request__c>(); // List to Update Responses with Current Major Response . So that ,it will get related to Each other. 
              
        For(cObject cCon: wrapRespDataList) 
        { 
            if(cCon.selected == true) 
            {
                if(!wrapRespIdSet.contains(cCon.Con.Id)){
                    checkedWrapList.add(cCon);
                }
                wrapRespIdSet.add(cCon.Con.Id);
            }
            if(cCon.selected == false){ // storing the ids of the records which are unchecked.
                if(wrapRespIdSet.contains(cCon.Con.Id)){
                    idsToRemove.add(cCon.Con.Id);
                }
            }   
                
            //  selectedResponses.add(cCon.con); // Selected Responses to add in Collection.
            
        } 
        
        for(cObject objWrp : checkedWrapList){
            if(!idsToRemove.contains(objWrp.con.Id)){
                selectedResponses.add(objWrp.con);
            }
        }   
                   
        For(Major_Proposal__c MjResp: [Select id from Major_Proposal__c where Account__c =:accid and Platform__c =:pltfid])
        { 
            // Iterating on the selected Responses 
            
            for(Sales_Request__c con: selectedResponses){
                con.Major_Proposals__c = MjResp.id ;     // Updating Selected Responses with Current Major Response ID (under Lookup Field) .          
                Lst_Responses.add(con); 
            }
        }
        Update Lst_Responses; 
       
        //**  Below Logic to get ID for current Major Response into String ,for redirecting page to correct Mojor Response record after performing action.            
        Map<Id, Major_Proposal__c> nmap = new Map<Id, Major_Proposal__c> ([Select id from Major_Proposal__c where Account__c =:accid and Platform__c =:pltfid]);
        List<Id> nmaps = new List<Id>();
        nmaps.addall(nmap.keyset());
               
        String s = String.join(nmaps,'\',\'') ;
           
        PageReference newPage;
        newPage = new PageReference('/'+ s);
        return newPage.setRedirect(true);
        //**         
    } 
   
    // Logic written for CANCEL Method 
    Public PageReference DoCancel()
    {   
        Map<Id, Major_Proposal__c> nmap = new Map<Id, Major_Proposal__c> ([Select id from Major_Proposal__c where Account__c =:accid and Platform__c =:pltfid]);
        List<Id> nmaps = new List<Id>();
        nmaps.addall(nmap.keyset());
        String s = String.join(nmaps,'\',\'') ;
       
        PageReference newPage;
        newPage = new PageReference('/'+ s);
        return newPage.setRedirect(true);
    }

    Public class cObject
    {
        Public Sales_Request__c con {get; set;}
        Public Boolean selected {get; set;}
        Public cObject(Sales_Request__c c) 
        {
            con = c;
            selected = false;
        }
    }
    
    // Method to Bind and handle Pagination Numbers in VF page
    public void PropertyListPagiNation()
    {   
        if(ResponseList.size() >= intItemsPerPage ){
            intTotalPages = ResponseList.size() / intItemsPerPage;
            intRemainingItemsInLastPage = ResponseList.size() - (intTotalPages * intItemsPerPage);
            if(ResponseList.size() >= intItemsPerPage)
            {
                if(intRemainingItemsInLastPage > 0 )
                {
                    intTotalPages += 1;
                }
            }
        } else{
            intTotalPages = 1;
            intRemainingItemsInLastPage = ResponseList.size() ;
        }
    } 
    
    // Method to Bind Data for the Selected Page
    public void GetPropertyListForSelectedPageNumber()
    {   
        integer intStartIndex = intItemsPerPage * (intSelectedPageNum - 1);
        integer intEndIndex = (intItemsPerPage * intSelectedPageNum) -1;
        if(intEndIndex >= ResponseList.size())
        {
            intEndIndex = intStartIndex + intRemainingItemsInLastPage -1;
        }
        
        if(intEndIndex < ResponseList.size()-1)
            hasNext = true;
        else
            hasNext = false;
        if(intStartIndex <= 0)
            hasPrevious = false;
        else
            hasPrevious = true;
        wrapRespDataList = new List<cObject>();
        for(integer i = intStartIndex; i <= intEndIndex ; i++){
            wrapRespDataList.add(ResponseList[i]);
        }
    }
    
    public void GetFirst(){
        intSelectedPageNum = 1;
        for(cObject w : wrapRespDataList){ // This section adds checked opportunity records to a list before moving to next page.
            if(w.selected == true){
                if(!wrapRespIdSet.contains(w.con.Id)){
                    checkedWrapList.add(w);
                }
                wrapRespIdSet.add(w.con.Id);
            }
            if(w.selected == false){
                if(wrapRespIdSet.contains(w.con.Id)){ // storing the ids of the records which are unchecked.
                    idsToRemove.add(w.con.Id);
                }
            }
        }
        GetPropertyListForSelectedPageNumber();
    }
    
    public void GetLast(){
        intSelectedPageNum = intTotalPages;
        for(cObject w : wrapRespDataList){ // This section adds checked opportunity records to a list before moving to next page.
            if(w.selected == true){
                if(!wrapRespIdSet.contains(w.con.Id)){
                    checkedWrapList.add(w);
                }
                wrapRespIdSet.add(w.con.Id);
            }
            if(w.selected == false){ // storing the ids of the records which are unchecked.
                if(wrapRespIdSet.contains(w.con.Id)){
                    idsToRemove.add(w.con.Id);
                }
            }
        }
        GetPropertyListForSelectedPageNumber();
    }
        
    public void GetPrevious(){
        intSelectedPageNum -= 1;
        for(cObject w : wrapRespDataList){ // This section adds checked opportunity records to a list before moving to next page.
            if(w.selected == true){
                if(!wrapRespIdSet.contains(w.con.Id)){
                    checkedWrapList.add(w);
                }
                wrapRespIdSet.add(w.con.Id);
            }
            if(w.selected == false){ // storing the ids of the records which are unchecked.
                if(wrapRespIdSet.contains(w.con.Id)){
                    idsToRemove.add(w.con.Id);
                }
            }
        }
        GetPropertyListForSelectedPageNumber();
    }
   
    public void GetNext(){
        intSelectedPageNum += 1;
        for(cObject w : wrapRespDataList){ // This section adds checked opportunity records to a list before moving to next page.
            if(w.selected == true){
                if(!wrapRespIdSet.contains(w.con.Id)){
                    checkedWrapList.add(w);
                }
                wrapRespIdSet.add(w.con.Id);
            }
            if(w.selected == false){ // storing the ids of the records which are unchecked.
                if(wrapRespIdSet.contains(w.con.Id)){
                    idsToRemove.add(w.con.Id);
                }
            }
        }
        GetPropertyListForSelectedPageNumber();
    } 
    
}