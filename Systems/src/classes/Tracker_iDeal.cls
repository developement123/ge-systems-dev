/*************************************************************************************************************************************************
    Following test class was created to provide code coverage for the following classes and trigger:
    - extDisplayResponseLineItems
    - extDisplayRliHistory
    - ResponseLF
    - ResponseLineItemDetailCls
    - iDealUploadAttachcls
    - Trigger : RLI_AutoNameGenrationtrigger 
     
    Version       Author(s)             Date                Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
      1.0         Mohan kumar          5/May/2015        Initial Creation
                  Abhishek              18/sep/2017      removed account platform
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_iDeal {
  
    public static Sales_Request__c sr;
    
    public static void loadTest() {  
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false; 
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(Name = 'Hello');
        insert acc1;        

        System.runAs(u) {            

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                 Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
             // added Account_Platform__c for JIRA 498
            
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
       
            insert opp;
            
            sr = new Sales_Request__c(Is_Tokenized__c = false, Windchill_US_UK__c = 'Windchill US', Name = 'Test response', Program__c = prog.id, Pipeline_Opportunity__c=opp.id, Pricing_Type__c = 'Direct Foreign Military');
            insert sr;
            
        }
    }
    
   
    //    Following 2 testMethods is to provide the test coverage to 
    //    'extDisplayResponseLineItems' class.        
    
   

    static testMethod void test_extDisplayResponseLineItems() {
        loadTest();
        
        Test.startTest();
            ApexPages.StandardController obj = new ApexPages.StandardController(sr);
            extDisplayResponseLineItems controller = new extDisplayResponseLineItems(obj);
            
            List<Response_Line_Item__c> listOfRLI = new List<Response_Line_Item__c>();
            
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            
            insert listOfRLI;
            
            extDisplayResponseLineItems.WrapperClass newWrap = new extDisplayResponseLineItems.WrapperClass(listOfRLI[0]);
            
            controller.listOfWrapper = new List<extDisplayResponseLineItems.WrapperClass>();
            controller.listOfWrapper.add(new extDisplayResponseLineItems.WrapperClass(listOfRLI[1]));
            controller.listOfWrapper.add(new extDisplayResponseLineItems.WrapperClass(listOfRLI[2]));
            
            controller.saveRec();
            controller.idToBeDeleted = listOfRLI[0].Id;
            controller.DeleteRec();
        Test.stopTest();
    }   
    
    static testMethod void test_extDisplayResponseLineItems1() {
        loadTest(); 
           
        Test.startTest();
            ApexPages.StandardController obj = new ApexPages.StandardController(sr);
            extDisplayResponseLineItems controller = new extDisplayResponseLineItems(obj);
            
            List<Response_Line_Item__c> listOfRLI = new List<Response_Line_Item__c>();
            
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            
            insert listOfRLI;
            
            extDisplayResponseLineItems.WrapperClass newWrap = new extDisplayResponseLineItems.WrapperClass(listOfRLI[0]);
            
            controller.listOfWrapper = new List<extDisplayResponseLineItems.WrapperClass>();
            controller.listOfWrapper.add(new extDisplayResponseLineItems.WrapperClass(listOfRLI[1]));
            controller.listOfWrapper.add(new extDisplayResponseLineItems.WrapperClass(listOfRLI[2]));
            
            listOfRLI[1].iDealRunName__c = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
            
            controller.saveRec();
            controller.idToBeDeleted = listOfRLI[0].Id;
            controller.DeleteRec();
        Test.stopTest();            
    }
    
    
    //    Following testMethod is to provide the test coverage to 2 classes.
    //    'extDisplayRliHistory' and 'ResponseLF'.
    
    
   
    static testMethod void test_extDisplayRliHistory() {
        loadTest();
            
        List<Response_Line_Item__c> listOfRLI = new List<Response_Line_Item__c>();
        
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        
        insert listOfRLI;
        
        Test.startTest();
            ApexPages.StandardController obj = new ApexPages.StandardController(sr);
            extDisplayRliHistory controller = new extDisplayRliHistory(obj);
            
            Pagereference pageref= System.currentPageReference();
            pageref.getParameters().put('id', sr.id);   
            Test.setCurrentPageReference(pageref);
            
            ApexPages.StandardController obj1 = new ApexPages.StandardController(sr);
            ResponseLF controller1 = new ResponseLF(obj1);
        Test.stopTest();
    }   
    
    
    //    Following testMethod is to provide the test coverage to
    //    'ResponseLineItemDetailCls' class.        
    
    
    
    static testMethod void test_ResponseLineItemDetailCls() {        
       
        loadTest();
            
        List<Response_Line_Item__c> listOfRLI = new List<Response_Line_Item__c>();
        
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        
        insert listOfRLI;
        
        Test.startTest();
            Pagereference pageref = System.currentPageReference();
            pageref.getParameters().put('Id', listOfRLI[0].id);
            pageref.getParameters().put('respId', sr.id);
            pageref.getParameters().put('identifier', 'edit');   
            Test.setCurrentPageReference(pageref);
            
            ApexPages.StandardController obj1 = new ApexPages.StandardController(listOfRLI[0]);
            ResponseLineItemDetailCls controller1 = new ResponseLineItemDetailCls(obj1);
        
            controller1.ShowEditPage();
            controller1.Cancel();
            controller1.customSave();
            controller1.DeleteRec();                
            controller1.DeleteRec();
            controller1.customSave();
            
            Pagereference pageref1 = System.currentPageReference();
            pageref1.getParameters().put('Id', listOfRLI[1].id);
            pageref1.getParameters().put('respId', sr.id);
            pageref1.getParameters().put('identifier', 'new');   
            Test.setCurrentPageReference(pageref1);
            
            ApexPages.StandardController obj2 = new ApexPages.StandardController(listOfRLI[1]);
            ResponseLineItemDetailCls controller2 = new ResponseLineItemDetailCls(obj2);
        
            controller2.currentRecordIdFrmUrl = null; 
            controller2.parentIdFrmUrl = sr.Id;              
            controller2.customSave();    
        Test.stopTest();
    }     
    
    
    //    Following testMethod is to provide the test coverage to
    //    'iDealUploadAttachcls' class.        
    
    
    
    static testMethod void test_iDealUploadAttachcls() {
        loadTest();  
       
        List<Response_Line_Item__c> listOfRLI = new List<Response_Line_Item__c>();
        
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
        
        insert listOfRLI;
                
        Test.startTest();
            Pagereference pageref = System.currentPageReference();
            pageref.getParameters().put('respId', sr.id);                  
            Test.setCurrentPageReference(pageref);
            
            iDealUploadAttachcls controller = new iDealUploadAttachcls();
        
            String bodyStr = '<?xml version="1.0" ?>'+
                             '<idealkeyfinancials>'+                                
                                '<iDealRunName>Run Name1</iDealRunName>'+
                                '<iteration_id>3</iteration_id>'+                                
                                '<Current>'+
                                    '<SalesOE>0.6296953</SalesOE>'+
                                    '<ContributionMarginOE>0.256312080349</ContributionMarginOE>'+
                                    '<ContributionMarginOEPER>0.407041437897027</ContributionMarginOEPER>'+
                                    '<BaseCostCFDOE>0.01</BaseCostCFDOE>'+
                                    '<BaseCostRTSOE>0.037781718</BaseCostRTSOE>'+
                                    '<OMOE>0.23390908716994</OMOE>'+
                                    '<OMPERCOE>0.371463924170849</OMPERCOE>'+
                                    '<PriceEscalationOE>0.03</PriceEscalationOE>'+
                                    '<MaterialInflationOE>0.027</MaterialInflationOE>'+
                                    '<LaborInflationOE>0.025</LaborInflationOE>'+
                                    '<CurrencyFx>GBP/1.6</CurrencyFx>'+
                                    '<LearningCurveProductivity>None</LearningCurveProductivity>'+
                                    '<SGARateOE>0.06</SGARateOE>'+
                                    '<SalesAM>0.2</SalesAM>'+
                                    '<ContributionMarginAM>0.1</ContributionMarginAM>'+
                                    '<ContributionMarginAMPER>0.5</ContributionMarginAMPER>'+
                                    '<BaseCostRTSAM>0.012</BaseCostRTSAM>'+
                                    '<OMAM>0.088</OMAM>'+
                                    '<OMPERCAM>0.44</OMPERCAM>'+
                                    '<PriceEscalationAM>0.03</PriceEscalationAM>'+
                                    '<MaterialInflationAM>0.027</MaterialInflationAM>'+
                                    '<LaborInflationAM>0.025</LaborInflationAM>'+
                                    '<AMMarketshare>0.3</AMMarketshare>'+
                                    '<SGARateAM>0.06</SGARateAM>'+
                                    '<Volume>200</Volume>'+
                                    '<PriceBaseyr>629.6953</PriceBaseyr>'+
                                    '<PriceAvg>633</PriceAvg>'+
                                    '<SalesTotal>0.8296953</SalesTotal>'+
                                    '<ContributionMarginTotal>0.356312080349</ContributionMarginTotal>'+
                                    '<ContributionMarginTotalPER>0.429449317537414</ContributionMarginTotalPER>'+
                                    '<BaseCostCFDTotal>0.01</BaseCostCFDTotal>'+
                                    '<BaseCostRTSTotal>0.049781718</BaseCostRTSTotal>'+
                                    '<OMTotal>0.32190908716994</OMTotal>'+
                                    '<OMPERCTOTAL>0.387984706156513</OMPERCTOTAL>'+
                                    '<MaxCashInvestment>0</MaxCashInvestment>'+
                                    '<IRR>N?A</IRR>'+
                                    '<NPV>0.152808709477494</NPV>'+
                                    '<Payback>1.2</Payback>'+
                                    '<HeadroomTotal>0.4</HeadroomTotal>'+
                                    '<PricingStrategyTotal>Commercial Pricing</PricingStrategyTotal>'+
                                    '<WACCTotal>0.12</WACCTotal>'+
                                    '<SGARateTotal>0.06</SGARateTotal>'+
                                    '<NRERates>150</NRERates>'+
                                    '<NRERiskReserve>0</NRERiskReserve>'+
                                    '<CapitalizationNRE>Yes</CapitalizationNRE>'+
                                    '<OtherOE>Test OE</OtherOE>'+
                                    '<OtherAM>Test AM</OtherAM>'+
                                    '<OtherTotal>Test Total</OtherTotal>'+
                                    '<MaxDeferredTotal>45.6</MaxDeferredTotal>'+
                                '</Current>'+
                                '<Walkaway>'+
                                   '<SalesOE>0.5</SalesOE>'+
                                   '<ContributionMarginOE>0.2</ContributionMarginOE>'+
                                   '<ContributionMarginOEPER>0.4</ContributionMarginOEPER>'+
                                   '<BaseCostCFDOE>0.01</BaseCostCFDOE>'+
                                    '<BaseCostRTSOE>0.03</BaseCostRTSOE>'+
                                    '<OMOE>0.23390908716994</OMOE>'+
                                    '<OMPERCOE>0.371463924170849</OMPERCOE>'+
                                    '<PriceEscalationOE>0.025</PriceEscalationOE>'+
                                   ' <MaterialInflationOE>0.027</MaterialInflationOE>'+
                                    '<LaborInflationOE>0.025</LaborInflationOE>'+
                                    '<CurrencyFx>GBP/1.6</CurrencyFx>'+
                                    '<LearningCurveProductivity>None</LearningCurveProductivity>'+
                                    '<SGARateOE>0.06</SGARateOE>'+
                                    '<SalesAM>0.15</SalesAM>'+
                                    '<ContributionMarginAM>0.1</ContributionMarginAM>'+
                                    '<ContributionMarginAMPER>0.666666666666667</ContributionMarginAMPER>'+
                                    '<BaseCostRTSAM>0.009</BaseCostRTSAM>'+
                                    '<OMAM>0.091</OMAM>'+
                                    '<OMPERCAM>0.606666666666667</OMPERCAM>'+
                                    '<PriceEscalationAM>0.025</PriceEscalationAM>'+
                                    '<MaterialInflationAM>0.027</MaterialInflationAM>'+
                                    '<LaborInflationAM>0.025</LaborInflationAM>'+
                                    '<AMMarketshare>0.3</AMMarketshare>'+
                                    '<SGARateAM>0.06</SGARateAM>'+
                                    '<Volume>200</Volume>'+
                                    '<PriceBaseyr>625</PriceBaseyr>'+
                                    '<PriceAvg>630</PriceAvg>'+
                                    '<SalesTotal>0.65</SalesTotal>'+
                                    '<ContributionMarginTotal>0.3</ContributionMarginTotal>'+
                                    '<ContributionMarginTotalPER>0.461538461538462</ContributionMarginTotalPER>'+
                                    '<BaseCostCFDTotal>0.01</BaseCostCFDTotal>'+
                                    '<BaseCostRTSTotal>0.039</BaseCostRTSTotal>'+
                                    '<OMTotal>0.32490908716994</OMTotal>'+
                                    '<OMPERCTOTAL>0.4998601341076</OMPERCTOTAL>'+
                                    '<MaxCashInvestment>0</MaxCashInvestment>'+
                                    '<IRR>N/A</IRR>'+
                                    '<NPV>0.152808709477494</NPV>'+
                                    '<Payback>1.5</Payback>'+
                                    '<HeadroomTotal>0.4</HeadroomTotal>'+
                                    '<PricingStrategyTotal>Commercial Pricing</PricingStrategyTotal>'+
                                    '<WACCTotal>0.12</WACCTotal>'+
                                    '<SGARateTotal>0.06</SGARateTotal>'+
                                    '<NRERates>150</NRERates>'+
                                    '<NRERiskReserve>0.1</NRERiskReserve>'+
                                    '<CapitalizationNRE>Yes</CapitalizationNRE>'+
                                    '<OtherOE>Test OE Walkaway</OtherOE>'+
                                    '<OtherAM>Test AM Walkaway</OtherAM>'+
                                    '<OtherTotal>Test Total Walkaway</OtherTotal>'+
                                   '<MaxDeferredTotal>45.6</MaxDeferredTotal>'+
                               '</Walkaway>'+
                            '</idealkeyfinancials>';
            controller.body = blob.valueOf(bodyStr);                
            controller.iDealSubmit();  
            
            try {
                controller.body = null;
                controller.iDealSubmit();
            }
            catch(Exception ex) {}
            controller.iDealSubmit();
        Test.stopTest();
    }
    
    
    //    Following testMethod is to provide the test coverage to 
    //    'RLITrigger_Handler' class.        
    
    
    
    static testMethod void test_RLITrigger_Handler() {
        loadTest();           
        Test.startTest();
            List<Response_Line_Item__c> listOfRLI = new List<Response_Line_Item__c>();
        
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            listOfRLI.add(new Response_Line_Item__c(Response__c = sr.Id, Type__c = 'Offer', OtherTotal__c = 'This is other field'));
            
            insert listOfRLI;  
            
            listOfRLI[0].iDealRunName__c = 'Ideal Run Name'; 
            listOfRLI[0].TG2__c = false;
            listOfRLI[0].Approved__c = true; 
            listOfRLI[0].Type__c = 'Won';
            
            update listOfRLI;
        Test.stopTest();
    } 
}