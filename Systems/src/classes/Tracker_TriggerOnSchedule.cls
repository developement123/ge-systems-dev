/*************************************************************************************************************************************************
    This test class was created to provide code coverage for Trigger "TriggerOnSchedule" on 
    schedule object and handler class "ScheduleTriggerHandler"               
          
    Version     Written by        last modified Date              
    ---------------------------------- --------------------------------------------------------------------------------------------------------------
    1.0         Abhishek            4th Jan 2016        
*************************************************************************************************************************************************/
@isTest
public class Tracker_TriggerOnSchedule{

    private static testmethod void testSchedule() {    
        
        
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;   
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_TriggerOnMarginEscalator = false;
        Class_TriggerCheck.var_updateMETrg = true;  // to cover method "updateCY_MEfromSchedules" of handler class "ScheduleTriggerHandler"
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_TriggerOnContract = false;
        
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];        
        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;  
       
    System.runAs(u) {   
         //inserting acount
            Account acc = new Account(Name='GE GEAS');
            insert acc; 
         //inserting contact
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                            MailingCity='Delhi', MailingCountry='India');
            insert con;    
        
         Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
         insert pm; 

         
         //inserting hierarchy.          
         Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
         insert hierarchy;  
         
         // Offering_Type__c == 'OE / Services' and recordType = PO Forecast
          
          RecordType RenewalRecordType = [select id from recordType where Name =:Label.Label_POForecastOpportunity];
            opportunity oppPoForecast = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3
                             , Lookup_hiearchyname__c = hierarchy.id, Contract_Award_Date__c = date.today()
                             , Margin_Escalator__c = true , Platform_New__c = pm.id, accountId= acc.id
                             , Offering_Type__c = 'OE / Services' , RecordTypeID =RenewalRecordType.Id  );
                             
            insert oppPoForecast;
            
            //inserting product.
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True,IsActive = True);
            insert p2;  
            
            //inserting Opportunity Line Item.
            list<Opportunity_Line_Item__c> oliList=new list<Opportunity_Line_Item__c>();
            Opportunity_Line_Item__c pli = new Opportunity_Line_Item__c();
            pli.Opportunity__c=oppPoForecast.id;
            pli.Product__c=p2.Id;
            pli.Probability__c= 20;
            pli.Revenue_Stream_top_level__c='Engineering';
            pli.Revenue_Stream_child_level__c='Aviall';
            pli.First_Ship_Date__c = system.today();
            pli.Last_Ship_Date__c = system.today();
            pli.Execution_Site__c='Austin';
            pli.Installment_Periods__c = 'Annually';
            pli.DistributeQuantityAsPerPipeline__c = true;
            oliList.add(pli);
            
            insert oliList;
            
            Date dateInstance  = System.Today(); //Date.newInstance(System.Today().Year(),1,30); 
            
            opportunity oppservicesAnnual = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc.id, Offering_Type__c = 'OE / Services' );
            insert oppservicesAnnual;
            
            Contract_PO__c ContrAnnual = new Contract_PO__c (Account_Name__c = acc.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today().addYears(2)
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = oppservicesAnnual.id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual'
                                       , First_PO_Ship_Date__c  = system.today()
                                       , Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false 
                                       );
            insert ContrAnnual;
            
            Order__c ord = new order__c();
            ord.name = 'test';
            ord.PO_Award_Start_Date__c = System.Today(); 
            ord.PO_Award_End_Date__c = system.today().addYears(2);
            ord.PO_Schedule_Opportunity__c = oppPoForecast.id; 
            ord.Contract1__c = ContrAnnual.Id;
            // orli.add(ord);
           
            insert ord;    
            
           
            
            Opportunity_Line_Item__c Opli = new Opportunity_Line_Item__c(
            Opportunity__c=oppPoForecast.id, Product__c=p2.Id, Probability__c= 20, 
            Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', 
            First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), 
            Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false, 
            Unit_Price__c = 50, DistributeQuantityAsPerPipeline__c = true);
            insert Opli;  
          
    Test.startTest();
    
             List<Schedule__c> lstOfSch = new List<Schedule__c>();
            for(Integer i=0;i<=2;i++)
            {
              Schedule__c sc = new Schedule__c();
              sc.Opportunity_Line_Item__c = oliList[0].id;
              sc.Schedule_Date__c  = dateInstance.addMonths(i);
              sc.Quantity__c = 1;
              sc.Escalated_Revenue__c = 3;
              sc.Order__c = ord.id;
              sc.Opportunity__c = oppPoForecast.ID;
              lstOfSch.add(sc);
            } 
            system.debug('distribute ++ ' + Opli.DistributeQuantityAsPerPipeline__c);
            for(Integer j=0;j<=2;j++)
            {
              Schedule__c scPo = new Schedule__c();
              scPo.Opportunity_Line_Item__c = Opli.id;
              scPo.Schedule_Date__c  = dateInstance.addMonths(j);
              scPo.Quantity__c = 1;
              scPo.Escalated_Revenue__c = 3;
              scPo.Opportunity__c = oppPoForecast.ID;
              scpo.Order__c = ord.id;
              lstOfSch.add(scPo);
            } 
            
         insert lstOfSch;
         
         Date dateInstance1 = Date.newInstance(System.Today().Year(),7,30); 
         
         List<Schedule__c> UpdatelstOfSch = new List<Schedule__c>();
            for(Schedule__c scu:lstOfSch){
                Integer i=20;
                scu.Schedule_Date__c = dateInstance1+i;
                scu.Escalated_Revenue__c = 150;
                i++;
                UpdatelstOfSch.add(scu);
            }    
            
        update UpdatelstOfSch;
         
         Delete UpdatelstOfSch;  
         
         UnDelete UpdatelstOfSch;
          
        Test.stopTest();
        }
    } 
}