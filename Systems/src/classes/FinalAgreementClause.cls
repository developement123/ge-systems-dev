global class FinalAgreementClause  implements Database.Batchable<sObject>{
    
    global string query;
    List<String> status = new List<String>{'Activated','Being Amended','Being Renewed','Superseded','Being Terminated','Terminated','Expired'};
    
    global list<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();

    global set<string> setstring = new set<string>();

    global Integer i;

    global List<Apttus__Agreement_Clause__c> tempClauseList ;

    global set<Apttus__Agreement_Clause__c> AgreementClauseToUpdate = new set<Apttus__Agreement_Clause__c>();

    global List<Apttus__Agreement_Clause__c> FinalAgreementClauseToUpdate = new List<Apttus__Agreement_Clause__c>();

    global Map<string,List<Apttus__Agreement_Clause__c>> Mapofstring = new Map<string,List<Apttus__Agreement_Clause__c>>();

    
    global Map<string,Apttus__Agreement_Clause__c> FinalMapOFString11;
            
    global List<Apttus__Agreement_Clause__c> listforsorting;
    
    global FinalAgreementClause()
    {
        //query = 'select id, name, Apttus__Status__c,clauses_processed__c, (select id,Apttus__Clause__c,LastModifiedDate,In_Final__c,Apttus__Action__c from Apttus__R00N50000001os4tEAA__r) from Apttus__APTS_Agreement__c where Apttus__Status__c = \''+status+'\' AND clauses_processed__c = false';
        query = 'select id, name, Apttus__Status__c,clauses_processed__c from Apttus__APTS_Agreement__c where Apttus__Status__c In :status AND clauses_processed__c = false';
        
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Apttus__APTS_Agreement__c> scope) {
        
        List<Apttus__APTS_Agreement__c> agr   = [select id, name, Apttus__Status__c,clauses_processed__c, (select id,Apttus__Clause__c,LastModifiedDate,In_Final__c,Apttus__Action__c from Apttus__R00N50000001os4tEAA__r) from Apttus__APTS_Agreement__c where id =:scope];
        
        
    for(Apttus__APTS_Agreement__c a:agr)
    {
      FinalMapOFString11 = new Map<string,Apttus__Agreement_Clause__c>();
      setstring = new set<String>();
      a.clauses_processed__c = true;
      agreementList.add(a);
      
      if(a.Apttus__R00N50000001os4tEAA__r!=null)
      {
      for(Apttus__Agreement_Clause__c ac : a.Apttus__R00N50000001os4tEAA__r)
        {
          setstring.add(ac.Apttus__Clause__c);
                  
        }
         
        
        for(String ss : setstring)
        {
            tempClauseList = new List<Apttus__Agreement_Clause__c>();
            
            for(Apttus__Agreement_Clause__c abc : a.Apttus__R00N50000001os4tEAA__r)
            {
                if(ss == abc.Apttus__Clause__c)
                {
                  tempClauseList.add(abc);
                }
                
            }
            
            Mapofstring.put(ss,tempClauseList);

        }
            for(String str : Mapofstring.keyset())
            {
                listforsorting = new List<Apttus__Agreement_Clause__c>();
                listforsorting = Mapofstring.get(str);
                

                if(listforsorting.size()>1)
                {
                    
                    for(i=1;i<listforsorting.size();i++)
                    {
                        if(listforsorting[0].Lastmodifieddate < listforsorting[i].Lastmodifieddate)
                        {
                            
                            listforsorting.add(0,listforsorting[i]);
                        }
                        
                    }
                    
                    
                    FinalMapOFString11.put(str,listforsorting[0]);
                    listforsorting.clear();
                
                }
                
                else if(listforsorting.size()==1){
                    
                    for(i=0;i<listforsorting.size();i++)
                    {
                        
                            AgreementClauseToUpdate.add(listforsorting[0]);
                        
                        
                    }
                        listforsorting.clear();
                    }
                    
                    
                }
                
                for(Apttus__Agreement_Clause__c  fl : FinalMapOFString11.values())
                {
                    AgreementClauseToUpdate.add(fl);    
                }
                
                FinalMapOFString11.clear();
            
                
                }
                
            }
            
        
        
        for(Apttus__Agreement_Clause__c sss: AgreementClauseToUpdate)
        {
            if(sss.Apttus__Action__c!='Deleted' && sss.In_Final__c <> true)
            {
                sss.In_Final__c = true;
                FinalAgreementClauseToUpdate.add(sss);
            }           
        }

        if(!FinalAgreementClauseToUpdate.isEmpty())
        {
            update FinalAgreementClauseToUpdate;
        }
        
        if(!agreementList.isEmpty())
        {
            update agreementList;
        }
        system.debug('!!!!!!!!'+FinalAgreementClauseToUpdate);
        system.debug('$$$$$$$$'+FinalAgreementClauseToUpdate.size());
        system.debug('@@@@@@@@'+agreementList);
        system.debug('########'+agreementList.size());
        
        
       
        }
        
        global void finish(Database.BatchableContext bc){
            
        }
        
    
}