/***************************************************************************************************************************************************
    Author : Mohan Kumar
    Date   : 15/10/2015
    Action : This batch class is to create Margin Escalators under Opportunity
             based on the label value 'ME_setup_year'. 
             This new version of class has been created as a part of Opportunity Optimization Requirement.
    
****************************************************************************************************************************************************/

global with sharing class createME_new implements Database.Batchable<sObject>, Database.Stateful {

    Map<Id, List<Schedule__c>> mapOls;    // this map will hold Oppy ids vs all related schedules.
    
    global createME_new() {               // this constructor is initializing map.
        mapOls = new Map<Id, List<Schedule__c>>();
    }
    
    global Database.querylocator start(Database.BatchableContext BC) {            
        Date fromdate  = Date.newInstance(Integer.valueOf(Label.ME_setup_year), 1, 1);
        Date todate    = Date.newInstance(Integer.valueOf(Label.ME_setup_year), 12, 31);    
     
        Set<Id> oppyIdSet = new Set<Id>();
        Set<Id> uniqueOppty = new Set<Id>();
        
        // following for loop is to check which opportunities are already having related ME for the particular year.
        for(Margin_Escalator__c me :[select Opportunity__c from Margin_Escalator__c where Year__c =: Label.ME_setup_year]) {
            oppyIdSet.add(me.Opportunity__c);    
        }
        
        // following for loop is to create a map of Opportunity id vs available schedules. 
        for(Schedule__c ols : [SELECT Id, Quantity__c, Schedule_Date__c, Escalated_Revenue__c, Opportunity_Line_Item__r.Probability__c, 
                                    Opportunity__c
                                    FROM Schedule__c
                                    where Opportunity__c NOT IN : oppyIdSet 
                                    AND Schedule_Date__c >=: fromdate 
                                    AND  Schedule_Date__c <=: todate]) {
            uniqueOppty.add(ols.Opportunity__c);
            if (mapOls.containsKey(ols.Opportunity__c)) 
                mapOls.get(ols.Opportunity__c).add(ols);               
            else         
                mapOls.put(ols.Opportunity__c, new List<Schedule__c>{ols});
        }
        
        return Database.getQueryLocator('Select Id, closeDate, Margin_Escalator__c from Opportunity where Id IN : uniqueOppty');
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope) {
       List<Margin_Escalator__c> meList = new List<Margin_Escalator__c>();
       List<Opportunity> listOfOpportunity = new List<Opportunity>();
       
       for(Opportunity opp : scope) {     
            Margin_Escalator__c me = new Margin_Escalator__c(); 
            Systems_CommonClass instance = new Systems_CommonClass();
           
            if(!mapOls.isEmpty() && mapOls.get(opp.Id) != null) {
                me =  instance.calculatingME(me, mapOls.get(opp.Id));   
                me.Opportunity__c = opp.Id;
                me.Year__c = Label.ME_setup_year;
               
                if(opp.Margin_Escalator__c
                    && opp.CloseDate.Year() == System.Today().Year() 
                    && Label.ME_setup_year == String.valueOf(System.Today().Year())) {    // added for JIRA 364.
                        me.MTG__c = true;
                        listOfOpportunity.add(new Opportunity(id = opp.Id, MTG_Year__c = Label.ME_setup_year));
               }   
               meList.add(me);
            }
        }
        
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
        
        if(!meList.isEmpty()) {  // this list will insert ME for the particular year.
            try {  
                Class_TriggerCheck.var_TriggerOnMarginEscalator= false;    // this static variable will prevent ME trigger (before Insert & before Update event) from firing.
                insert meList;
            }
            catch(Exception e) {
                System.debug('The following exception has occurred while inserting margin escalators in class: createME_new ' + e.getMessage());
            }
        }

        if(!listOfOpportunity.isEmpty()) {    // this list will update MTG year in Opportunties.
           try {
               update listOfOpportunity;
           }
           catch(Exception e) {
                System.debug('The following exception has occurred while updating opportunities in class: createME_new ' + e.getMessage());
           }
        }
    }   
    global void finish(Database.BatchableContext BC) {
    
    }        
}