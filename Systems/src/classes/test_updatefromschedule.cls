/**********************************************************************************************
    This test class has been modified to provide code coverage for the following new class:
    - ExtUpdateOppSchedules_New
    
    Version     Author(s)               Date             
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Jhansi          12/30/2014      
                Abhishek        05/24/2016 for Jira 523
                Abhishek        09/18/2017 removed account platform
*************************************************************************************************/      

@istest
private class test_updatefromschedule {
   static testmethod void mytest() {
    
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;  
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;
        Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
        
        Class_TriggerCheck.var_TriggerOnContract = false;
        Class_TriggerCheck.var_TriggerOnOrder = false;

        Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
        insert plat;

        Account acc = new Account(Name = 'GE GEAS');
        insert acc;

        Contact con = new Contact(AccountId = acc.Id, MailingCity = 'Gurgaon', MailingCountry = 'India', Email = 'neha.mittal@birlasoft.com', LastName = 'Test1', FirstName = 'FirstName1');
        insert con;

        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];
      
                
        Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
        insert hierarchy;
        
        
        Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
        insert p2;
        
        User u = new User(alias = 'skapo', email = 'sunny.kapoor21@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor21', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor2111@birlasoft50.com.sys.dev');
        insert u;

        System.runAs(u) {

         

        opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(),  Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id, GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Offering_Type__c = 'OE / Services', Price_Escalation_COLA__c = 0 );
        insert opp;

         Opportunity_Line_Item__c ol2 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D');
         insert ol2;
         
         // insert contract
        Contract_PO__c ContrMonthly = new Contract_PO__c (Account_Name__c = acc.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Number_of_PO_s__c = 2.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Monthly'
                                       , Revenue_Increment__c = 'Monthly', Pipeline_Opportunity__c = opp.Id); 
        insert ContrMonthly;
   
        // insert Order
        Order__c ord = new order__c(name = 'test', Contract1__c = ContrMonthly.id, Status__c='open', PO_Schedule_Opportunity__c=opp.id, PO_Award_Date__c = system.today(), PO_Award_Start_Date__c = system.today(), PO_Award_End_Date__c = system.today());
        insert ord; 

            
            PageReference pageRef = Page.UpdateOppSchedules_New;                           
            Test.setCurrentPage(pageRef );          
            ApexPages.CurrentPage().getparameters().put('id', opp.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            ExtUpdateOppSchedules_New controller = new ExtUpdateOppSchedules_New(sc);
            controller.isOrderDateMoveRequired = true;
            
        List<Opportunity_Line_Item__c> listOfOli = new List<Opportunity_Line_Item__c>(); 
        listOfOli.add(new Opportunity_Line_Item__c(Opportunity__c = opp.id,Product__c = p2.id, Installment_Periods__c = 'Annually'));
        listOfOli.add(new Opportunity_Line_Item__c(Opportunity__c = opp.id,Product__c = p2.id, Installment_Periods__c = 'Annually'));
        listOfOli.add(new Opportunity_Line_Item__c(Opportunity__c = opp.id,Product__c = p2.id, Installment_Periods__c = 'Annually'));
                        
        insert listOfOli;
             
        // ExtUpdateOppSchedules_New.wrapperOLI innercl = new ExtUpdateOppSchedules_New.wrapperOLI(listOfOli[0]);
          
        controller.lstwrapperOLI= new List<ExtUpdateOppSchedules_New.wrapperOLI>();
        controller.lstwrapperOLI.add(new ExtUpdateOppSchedules_New.wrapperOLI(listOfOli[1]));
        controller.lstwrapperOLI.add(new ExtUpdateOppSchedules_New.wrapperOLI(listOfOli[2]));
        controller.lstwrapperOLI[0].isProcess = true;
        //controller.lstwrapperOLI[1].isProcess = true;
        //controller.lstwrapperOLI[2].isProcess = true;
            
        controller.addNoOfDays = 5;            
        List<Schedule__c> APSch = new List<Schedule__c>();
        APSch.add(new Schedule__c (Opportunity__c = opp.id ,Opportunity_Line_Item__c = listOfOli[1].id, Schedule_Date__c  = system.today(), Quantity__c = 1, Order__c = ord.Id));
        
        Schedule__c Sch = new Schedule__c (Opportunity__c = opp.id ,Opportunity_Line_Item__c = listOfOli[1].id, Schedule_Date__c  = system.today(), Quantity__c = 1);
        APSch.add(Sch);
        insert APSch; 
        //APSch[1].Schedule_Date__c = APSch[0].Schedule_Date__c;
        //update APSch;
        controller.doUpdateSchedules();           
            
            controller.lstwrapperOLI= new List<ExtUpdateOppSchedules_New.wrapperOLI>();           
            controller.showSecondScreen();
            controller.backToFirstScreen();
            controller.showThirdScreen();
            controller.backToSecondScreen();
        }
    } 
}