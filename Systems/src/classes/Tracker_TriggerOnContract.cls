/********************************************************************************
    This test class was created to provide code coverage for the following class and trigger:
    - ContractTriggerHandler and TriggerOnContract
    - Modfied for JIRA 498 as test was failing with "required field missing" - ABhishek(March, 2nd, 2016)
     Version     Written by                last modified Date              
  --------------------------------------------------------------------------------------------------------------
                Abhishek                       07/19/2016      Custom contract changes 
                Abhishek                       09/18/2017      removed account platform
    ***************************************************************/      
@isTest
public class Tracker_TriggerOnContract {   
   
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }
   
    static testmethod void test_TriggerOnContract() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        
        Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;
        
        Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India');
        
         System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Digital Solutions', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Product_Hiearchy_Object__c hierarchy_OE = new Product_Hiearchy_Object__c(Name = 'OE / Services', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy_OE;
            
            // Offering_Type__c == 'OE / Services'
            opportunity oppservicesAnnual = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'OE / Services' );
            insert oppservicesAnnual;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy_OE.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True, IsActive = true);
            insert p2;     
            
            Opportunity_Line_Item__c oliAnnual = new Opportunity_Line_Item__c(Opportunity__c =  oppservicesAnnual.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id
                    , Installment_Periods__c = 'Annually') ;       
            insert oliAnnual;
            
            Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
            Date mydate1 = Date.newInstance(System.Today().Year(), 10, 5);
            
            List<Schedule__c> lstOfSch = new List<Schedule__c>();
            for(Integer i=0;i<=49;i++)
            {
              Schedule__c sc = new Schedule__c();
              sc.Opportunity_Line_Item__c = oliAnnual.id;
              sc.Schedule_Date__c  = mydate.addMonths(i);
              sc.Quantity__c = 1;
              sc.Escalated_Revenue__c = 3;
              sc.Opportunity__c = oppservicesAnnual.ID;
              lstOfSch.add(sc);
            } 
          
            // Offering type "OE / Services" ends here
          
            
            // Offering_Type__c == 'Digital Solutions'
             
            // added AccountId for JIRA 498
            opportunity oppDigital = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3
                             , Lookup_hiearchyname__c = hierarchy.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'Digital Solutions'
                             , Contract_Term__c = 2.0
                             );
            insert oppDigital;
            
            
          
             Opportunity_Line_Item__c oli1Digital = new Opportunity_Line_Item__c(Opportunity__c =  oppDigital.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id
                    , Installment_Periods__c = 'Annually') ;       
            insert oli1Digital;
            
             for(Integer i=0;i<=49;i++)
            {
              Schedule__c scdigital = new Schedule__c();
              scdigital.Opportunity_Line_Item__c = oli1Digital.id;
              scdigital.Schedule_Date__c  = mydate.addMonths(i);
              scdigital.Quantity__c = 1;
              scdigital.Escalated_Revenue__c = 3;
              scdigital.Opportunity__c = oppDigital.ID;
              lstOfSch.add(scdigital);
            } 
                   
            insert lstOfSch;
          
            
            
            // "Offering type" ends here
            
             Sales_Request__c response = new Sales_Request__c(Name = 'Test Response', Pipeline_Opportunity__c = oppservicesAnnual.id
                                                     , Type__c = 'Marketing Support', Proposal_Coordinator__c = con.ID
                                                     , Proposal_Manager__c = u.Id , Sales_Owner__c = u.Id
                                                     , Customer_Due_Date__c = system.today(), Agreed_Customer_Due_Date__c = system.today() );
            insert response;
            
           
            
            
            
            Test.startTest();
            
           
           
            // For renewal 
            RecordType RenewalRecordType = [select id from recordType where Name =:Label.Label_RenewalOpportunity];
            opportunity opprenewal = new opportunity(Name = 'Test opprenewal', StageName = label.OpportunityStage4
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'Digital Solutions'
                             , RecordTypeID =RenewalRecordType.Id );
            insert opprenewal;
         
             
            // ends here
            
                  Contract_PO__c ContrAnnual = new Contract_PO__c (Account_Name__c = acc1.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = oppservicesAnnual.id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual'
                                       , First_PO_Ship_Date__c  = system.today()
                                       , Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false 
                                       , Response__c = response.Id
                                       
                                       );
                 
                  Date ContractStartDate = Date.newInstance(System.Today().Year(), 10, 1);
                  Date InitialTermDate = Date.newInstance(System.Today().Year()+1, 10, 1);
                  
                  Contract_PO__c ContrDigital = new Contract_PO__c  (Account_Name__c = acc1.id
                                       , Contract_Start_Date__c = ContractStartDate, Contract_End_Date__c = system.today()
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = oppDigital.id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Monthly'
                                       , Revenue_Increment__c = 'Annually'
                                       , Renewed_no_changes__c = false
                                       , Initial_Term_End_Date__c = InitialTermDate 
                                       , Price_Escalation_COLA__c = 10  
                                       , Price_Escalation_Start_Date__c = system.today()
                                       , Renewal_Opportunity_1__c = opprenewal.Id
                                       , Renewed_with_Changes__c = false
                                       , Client_did_not_Renew__c = false
                                       , RecordTypeID = findRecordtypeId('Contract_PO__c', 'Digital_Solutions')
                                       , Status__c = 'Activated');
                                       
                 // InsertConList.add(ContrDigital);
                  
                  list<Contract_PO__c> ContractTODel = new list<Contract_PO__c>();
                  insert ContrAnnual; // covers "updateContractonOppRes" and "generateOrdersFromContract" method 
                  ContractTODel.add(ContrAnnual);
                  
                  insert ContrDigital ;
                  ContractTODel.add(ContrDigital);
                  
                  // to cover "Renewed_no_changes__c" field logic 
                  Opportunity updatecontractonopp = Tracker_TriggerOnContract.getOpportunity(opprenewal.Id);
                  updatecontractonopp.ContractID1__c = ContrDigital.Id;
                  updatecontractonopp.stagename = label.OpportunityStage4;
                  update updatecontractonopp;
                  
                  
                  
                  list<Contract_PO__c> fetchConList = [select id, Renewal_Opportunity_1__c, Renewed_no_changes__c,  Renewed_with_Changes__c, Client_did_not_Renew__c  from Contract_PO__c where id=:ContrDigital.Id];
                  list<Contract_PO__c> upsertconlist = new list<Contract_PO__c>();
                  for( Contract_PO__c updatecon : fetchConList)
                  {
                    
                    updatecon.Renewed_no_changes__c = true;
                    upsertconlist.add(updatecon);
                    
                  }
                   
                 update upsertconlist;
                 
                 // to cover "Renewed_with_Changes__c" field logic 
                 Opportunity resetOpp = Tracker_TriggerOnContract.getOpportunity(opprenewal.Id);
                 resetOpp.stagename = label.OpportunityStage4;
                 update resetOpp;
                
                  list<Contract_PO__c> RefetchConList = [select id, Renewal_Opportunity_1__c, Renewed_no_changes__c,  Renewed_with_Changes__c, Client_did_not_Renew__c  from Contract_PO__c where id=:ContrDigital.Id];
                  list<Contract_PO__c> reupsertconlist = new list<Contract_PO__c>();
                  for( Contract_PO__c reupdatecon : RefetchConList)
                  {
                     reupdatecon.Renewed_with_Changes__c = true;
                     reupdatecon.Client_did_not_Renew__c = true;
                     reupdatecon.Reason_not_Renewed__c = 'Selected another supplier';
                     reupsertconlist.add(reupdatecon);
                  }
                 update reupsertconlist;
                 
                 
                 
                 try{
                    //delete reupsertconlist;
                    database.delete(ContractTODel,false);
                 }
                 catch(exception e){
                    boolean isException = e.getMessage().contains('Renewal') ? true : false ;
                    system.assertEquals(isException , true);
                 }
                 
                 
                 
            Test.stopTest();
              
            
            }
    
    }
    
    static testmethod void test_AutoGenerateIntialRenewalOpp() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        
        Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;
        
        Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India', email = 'abhishek3.singh@ge.com');
        
         System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Digital Solutions', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True, IsActive = true);
            insert p2;     
            
            
            // Offering_Type__c == 'Digital Solutions'
            
            opportunity oppDigital = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3
                             , Lookup_hiearchyname__c = hierarchy.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'Digital Solutions'
                             , Contract_Term__c = 2.0
                             );
            insert oppDigital;
            
            
          
             Opportunity_Line_Item__c oli1Digital = new Opportunity_Line_Item__c(Opportunity__c =  oppDigital.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id
                    , Installment_Periods__c = 'Annually') ;       
            insert oli1Digital;
            
             List<Schedule__c> lstOfSch = new List<Schedule__c>();
             Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
             
             for(Integer i=0;i<=49;i++)
            {
              Schedule__c scdigital = new Schedule__c();
              scdigital.Opportunity_Line_Item__c = oli1Digital.id;
              scdigital.Schedule_Date__c  = mydate.addMonths(i);
              scdigital.Quantity__c = 1;
              scdigital.Escalated_Revenue__c = 3;
              scdigital.Opportunity__c = oppDigital.ID;
              lstOfSch.add(scdigital);
            } 
                   
            insert lstOfSch;
          
            
            
            // "Offering type" ends here
            
            
            Test.startTest();
                 
                  Date ContractStartDate = Date.newInstance(System.Today().Year(), 10, 1);
                  Date InitialTermDate = Date.newInstance(System.Today().Year()+1, 10, 1);
                  Contract_PO__c ContrDigital = new Contract_PO__c (Account_Name__c = acc1.id, Status__c = 'Activated'
                                       , Contract_Start_Date__c = ContractStartDate, Contract_End_Date__c = system.today()
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = oppDigital.id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Monthly'
                                       , Revenue_Increment__c = 'Annually'
                                       , Renewed_no_changes__c = false
                                       , Initial_Term_End_Date__c = InitialTermDate 
                                       , Price_Escalation_COLA__c = 10  
                                       , Price_Escalation_Start_Date__c = system.today()
                                       , Renewed_with_Changes__c = false
                                       , Client_did_not_Renew__c = false
                                       , no_automatic_renewals__c = 2
                                       , Automatic_Renewal_Term_months__c = 12
                                       , RecordTypeID = findRecordtypeId('Contract_PO__c', 'Digital_Solutions'));
                  
                  insert ContrDigital ;
                 
            Test.stopTest();
              
            
            }
    
    }
    
     static testmethod void test_generateOppOnIntialTermEndDateUpdate() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        
        Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;
        
        Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India', email = 'abhishek3.singh@ge.com');
        
         System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Digital Solutions', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True, IsActive = true);
            insert p2;     
            
            
            // Offering_Type__c == 'Digital Solutions'
            
            opportunity oppDigital = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3
                             , Lookup_hiearchyname__c = hierarchy.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'Digital Solutions'
                             , Contract_Term__c = 2.0
                             );
            insert oppDigital;
            
            
          
             Opportunity_Line_Item__c oli1Digital = new Opportunity_Line_Item__c(Opportunity__c =  oppDigital.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id
                    , Installment_Periods__c = 'Annually') ;       
            insert oli1Digital;
            
             List<Schedule__c> lstOfSch = new List<Schedule__c>();
             Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
             
             for(Integer i=0;i<=49;i++)
            {
              Schedule__c scdigital = new Schedule__c();
              scdigital.Opportunity_Line_Item__c = oli1Digital.id;
              scdigital.Schedule_Date__c  = mydate.addMonths(i);
              scdigital.Quantity__c = 1;
              scdigital.Escalated_Revenue__c = 3;
              scdigital.Opportunity__c = oppDigital.ID;
              lstOfSch.add(scdigital);
            } 
                   
            insert lstOfSch;
          
            
            
            // "Offering type" ends here
            
            
            Test.startTest();
                 
                  Date ContractStartDate = Date.newInstance(System.Today().Year(), 10, 1);
                  Date InitialTermDate = Date.newInstance(System.Today().Year()+1, 10, 1);
                  Contract_PO__c ContrDigital = new Contract_PO__c (Account_Name__c = acc1.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = ContractStartDate, Contract_End_Date__c = system.today()
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = oppDigital.id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Monthly'
                                       , Revenue_Increment__c = 'Annually'
                                       , Renewed_no_changes__c = false
                                       , Initial_Term_End_Date__c = InitialTermDate 
                                       , Price_Escalation_COLA__c = 10  
                                       , Price_Escalation_Start_Date__c = system.today()
                                       , Renewed_with_Changes__c = false
                                       , Client_did_not_Renew__c = false
                                       , no_automatic_renewals__c = 2
                                       , Automatic_Renewal_Term_months__c = 12
                                       , RecordTypeID = findRecordtypeId('Contract_PO__c', 'Digital_Solutions'));
                  
                  insert ContrDigital ;
                  opportunity updateOpp = [select id, StageName from Opportunity where id=:oppDigital.Id][0];
                  updateOpp.StageName = label.OpportunityStage5;
                  update updateOpp;
                  
                  ContrDigital.Status__c = 'Activated';
                  ContrDigital.Initial_Term_End_Date__c = Date.newInstance(System.Today().Year()+1, 10, 2);
                  update ContrDigital;
                 
            Test.stopTest();
              
            
            }
    
    }
    
    private static opportunity getOpportunity(id oppid)
    {
      
     return [select ContractID1__c, stagename from opportunity where id =:oppid];
    }
    
    static testmethod void test_generateOrders() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        
        Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;
        
        Account accchild = new Account(Name = 'Child Hello');
        insert accchild;
        
        Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India');
        
         System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;
            
            Product_Hiearchy_Object__c hierarchy_OE = new Product_Hiearchy_Object__c(Name = 'OE / Services', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy_OE;
            
            // Offering_Type__c == 'OE / Services'
            list<Opportunity> pipelineOPs = new list<Opportunity>();
            
            for(Integer i=0; i<6;i++){
            opportunity opp1 = new opportunity(Name = 'Test opp'+i, StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'OE / Services' );
            pipelineOPs.add(opp1);
            }
            insert pipelineOPs;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy_OE.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True, IsActive = true);
            insert p2;     
            
            list<Opportunity_Line_Item__c> pipelineOLIs = new list<Opportunity_Line_Item__c>();
            
            for(integer j=0;j<6;j++){
            
            Opportunity_Line_Item__c oliAnnual = new Opportunity_Line_Item__c(Opportunity__c =  pipelineOPs[j].id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id
                    , Installment_Periods__c = 'Annually') ;
            pipelineOLIs.add(oliAnnual);
            }
            insert pipelineOLIs;
            
            Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
            id myOliId;
            id myOppId;
            List<Schedule__c> lstOfSch = new List<Schedule__c>();
            for(Integer k=0;k<=49;k++)
            {
              if(k <= 5){
                myOliId = pipelineOLIs[0].id;
                myOppId = pipelineOPs[0].id;
              }else if(k>5 && k<=10){
                myOliId = pipelineOLIs[1].id;
                myOppId = pipelineOPs[1].id;
              }else if(k>10 && k<=15){
                myOliId = pipelineOLIs[2].id;
                myOppId = pipelineOPs[2].id;
              }else if(k>15 && k<=20){
                myOliId = pipelineOLIs[3].id;
                myOppId = pipelineOPs[3].id;
              }else if(k>20 && k<=25){
                myOliId = pipelineOLIs[4].id;
                myOppId = pipelineOPs[4].id;
              }else if(k>25){
                myOliId = pipelineOLIs[5].id;
                myOppId = pipelineOPs[5].id;
              }else{
                myOliId = pipelineOLIs[0].id;
                myOppId = pipelineOPs[0].id;
              }
                
              Schedule__c sc = new Schedule__c();
              sc.Opportunity_Line_Item__c = myOliId;
              sc.Schedule_Date__c  = mydate.addMonths(k);
              sc.Quantity__c = 1;
              sc.Escalated_Revenue__c = 3;
              sc.Opportunity__c = myOppId;
              lstOfSch.add(sc);
            }   
            insert lstOfSch;
            
            Test.startTest();
                list<Contract_PO__c> conTogenerate = new list<Contract_PO__c>();
                string poIncrement ;
                double noOfPO = 50.0;
                for(integer c=0;c<6;c++){
                    if(c==0)
                        poIncrement = 'Annual';
                    else if(c==1)
                        poIncrement = 'Bi-Annual';
                    else if(c==2)
                        poIncrement = 'Quarterly';
                    else if(c==3)
                        poIncrement = 'Monthly';
                    else if(c==4)
                        poIncrement = 'Weekly';
                    else if(c==5){
                        poIncrement = 'Single PO';
                        noOfPO = 1.0;
                        }
                    else
                        poIncrement = 'Annually';
                        
                  Contract_PO__c ContrAnnual = new Contract_PO__c (Account_Name__c = acc1.id, Status__c = 'Draft', Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today(), Legal__c = con.ID, Program_Manager__c = u.Id, Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id, Pipeline_Opportunity__c = pipelineOPs[c].id, Number_of_PO_s__c = noOfPO, First_PO_Award_Date__c = system.today(),PO_Increment__c = poIncrement, First_PO_Ship_Date__c  = system.today(), Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false, Child_Account__c = acc1.id );
                  
                 conTogenerate.add(ContrAnnual);
                }
                insert conTogenerate;
                // to cover following method "updateChildAccountOnPOForecastOpp"
                opportunity poForecastOpp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                 , CloseDate = date.today()
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc1.id
                                , recordtypeId = findRecordtypeId('Opportunity', 'POForecast')
                                , ContractID1__c=conTogenerate[0].Id
                                , Offering_Type__c = 'OE / Services');
               Insert poForecastOpp  ;
            
                Contract_PO__c conToUpdate = conTogenerate[0];
                conToUpdate.Child_Account__c = accchild.id;
                
                update conToUpdate;
                
            Test.stopTest();
              
            
            }
    
    }
    
}