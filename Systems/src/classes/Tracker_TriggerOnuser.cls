@IsTest(seeAlldata=true)
public class Tracker_TriggerOnuser
{

 private static testmethod void testUser()
 {
   
   integer count=1;
   list<user> userToInsert = new list<user>();
   list<user> userToUpdate = new list<user>();
   list<user> PersUserToUpdate = new list<user>();
   User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
   System.runAs ( thisUser ) {
       list<profile> profiles = [select Id, name from Profile where Name in ('System Administrator', 'Systems Business Admin')];  
       for(profile p : profiles){
               User u = new User(alias = 'skapo12'+count, email = 'test.test@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor'+count, languagelocalekey = 'en_US',
               localesidkey = 'en_US', profileid = p.id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev'+count, isActive=true);
               count = count + 1;
               userToInsert.add(u) ;  
           }
        insert userToInsert;
        
        
    }
     System.runAs ( thisUser ) {
        for(user myu : userToInsert){
            myu.Perspecsys_Access__c = true;
            PersUserToUpdate.add(myu);
        }
        update PersUserToUpdate;
    }
    
    System.runAs ( thisUser ) {
        for(user myu : userToInsert){
            myu.isActive = false;
            userToUpdate.add(myu);
        }
        update userToUpdate;
    }
 }
}