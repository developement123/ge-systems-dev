/*
        Developer : Abhishek
        Date: 16/Feb/2017    
        Action : This class is a custom iterable being used in batch class "batchEmailFieldUpdate"
                 as we are using dynamic SOQL to avoid seprate query for each object, we need this class
                 for batch processing. 
Change History
=============
Developer       Modified On     Change description
Abhishek                         change the logic of object selection and used this field deleteOnlyUK__c           
*/
global class CustomIterable implements Iterable<SObject>, Iterator<SObject>{
   List<SObject> sobjs {get; set;} 
   Integer i {get; set;} 
   public Iterator<SObject> iterator() { return this; }
   public CustomIterable(){ 
       sobjs = new List<SObject>();
       List<SObject> ukRecords;
       string sql;
        // Map from SObject to List of Email Fields
        list<UKObjectList__c> listOfObjectTodelete = new list<UKObjectList__c>();
        // get all objects from custom setting 
        for(UKObjectList__c objStr:[select name, ObjectAPI__c, Order__c, deleteOnlyUK__c from UKObjectList__c where wantToDelete__c=true order by Order__c]){
            listOfObjectTodelete.add(objStr); // objStr.ObjectAPI__c
        }
        for(UKObjectList__c myObj : listOfObjectTodelete){
             if(myObj.deleteOnlyUK__c==false)
                sql = 'select id from ' + myObj.ObjectAPI__c + ' order by createddate asc';
             else
                sql = 'select id from ' + myObj.ObjectAPI__c + ' where Is_Tokenized__c=true order by createddate asc';
                
             ukRecords = Database.query(sql);
             
             if(!ukRecords.isEmpty() || ukRecords!=null)
                sobjs.addAll(ukRecords);
        }
        
        sobjs.sort();
       i = 0; 
   }   
   global boolean hasNext(){ 
       if(i >= sobjs.size()) 
           return false; 
       else 
           return true; 
   }    
   global SObject next(){ 
       if(i == 8){ i++; return null;} 
       i=i+1; 
       return sobjs[i-1]; 
   }
}