/***********************************************************************************************************************************************************
    Author : Mohan Kumar
    Date   : 20 April 2015
    
    Action : This class is used as extensions in vf page : displayResponseLineItems

***********************************************************************************************************************************************************/

public with sharing class extDisplayResponseLineItems {
   
    public List<WrapperClass> listOfWrapper{get;set;}  
    public Id idfromController{get;set;}
    public Id idToBeDeleted{get;set;}
    
    public extDisplayResponseLineItems(ApexPages.StandardController controller) {       
        listOfWrapper = new List<WrapperClass>();
        idfromController = controller.getId();
        refreshLineItem();
    }
    
    public class WrapperClass {
        public Response_Line_Item__c rli{get;set;}
        public WrapperClass(Response_Line_Item__c respLineItem) {
            rli = respLineItem;           
        } 
    }    
    
    public void refreshLineItem() {
        listOfWrapper.clear();
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Response_Line_Item__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        
        String theQuery = 'select ';
        for(Schema.SObjectField s : fldObjMapValues) {           
           String theName = s.getDescribe().getName();          
           theQuery += theName + ',';
        }    
        
        theQuery += ' createdBy.Name';    
        theQuery += ' from Response_Line_Item__c where Response__c =: idfromController';         
        theQuery += ' order by Last_Number_RLI__c ASC';
        for(Response_Line_Item__c rli : Database.query(theQuery)) {
            listOfWrapper.add(new WrapperClass(rli));
        }
    }    
        
    public void saveRec() {        
        List<Response_Line_Item__c> listToUpdate = new List<Response_Line_Item__c>();        
        
        for(Integer i = 0; i < listOfWrapper.size(); i++) {             
            Response_Line_Item__c rliInstance = new Response_Line_Item__c(id = listOfWrapper[i].rli.id);
            
            rliInstance.iDealRunName__c = listOfWrapper[i].rli.iDealRunName__c;
            rliInstance.TG2__c = listOfWrapper[i].rli.TG2__c;
            rliInstance.Approved__c= listOfWrapper[i].rli.Approved__c;
            
            rliInstance.Type__c = listOfWrapper[i].rli.Type__c;
            rliInstance.Comments__c = listOfWrapper[i].rli.Comments__c;
            
            listToUpdate.add(rliInstance);
        }
        
        try {
            Boolean isDone;   
            Database.SaveResult[] srList = Database.update(listToUpdate);
                    
            for(Database.SaveResult sr : srList) {
                if(!sr.isSuccess()) {
                    isDone = false; 
                    break;     
                }
            }         
                
            if(isDone != false) {              
                refreshLineItem(); 
            }          
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
        }      
    }      

    public PageReference DeleteRec() {        
        if(idToBeDeleted != null) {
            try {  
                Response_Line_Item__c rliToBeDeleted = new Response_Line_Item__c(Id = idToBeDeleted);     
                delete rliToBeDeleted;
            }   
            catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
            } 
        }
        return null;        
    }    
}