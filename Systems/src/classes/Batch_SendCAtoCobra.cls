/*
 Last modfied by Abhishek on 12/21/2016 to use Cronos field of EPA to decide end point (US/UK) 
*/
global with sharing class Batch_SendCAtoCobra implements Database.Batchable<sObject>,Database.AllowsCallouts
{
    public string query;
    Public Static String SOURCE_SYSTEM,TARGET_SYSTEM,OPERATION_TYPE,MESSAGE_TYPE,SFSOAPHeader,SFSOAP_BODY_Start,SFSOAP_BODY_End,TIME_STAMP,SFSOAPHeader_UK,SFSOAP_BODY_Start_UK,SFSOAP_BODY_End_UK;
    Static
    {
        SOURCE_SYSTEM = 'AVISYSSFDC';
        TARGET_SYSTEM = 'AviationCobra';
        OPERATION_TYPE = 'CLOSE';
        MESSAGE_TYPE =  'Cost Authorization';
        DateTime TIME_STAMP_Date = System.now();
        TIME_STAMP = TIME_STAMP_Date.format('MM/dd/yyyy HH:mm:ss');
        // Below Added Custom Label as Values keep on changing per environment .
        SFSOAPHeader = Label.SOAP_HEADER;
        SFSOAP_BODY_Start = Label.SOAP_BODY_Start;
        SFSOAP_BODY_End = Label.SOAP_BODY_End;
        
        //Once we have UK location's WSDL and endpoints then first we need to setup values over those custom labels.
        SFSOAPHeader_UK = Label.SOAP_HEADER_UK_Location; 
        SFSOAP_BODY_Start_UK = Label.SOAP_BODY_Start_UK_Location;
        SFSOAP_BODY_End_UK = Label.SOAP_BODY_End_UK_Location;
    }
    //Execute the query.
    global database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scopeCA) 
    {
        for(sObject so : scopeCA)
        { 
            Cost_Authorization__c objCA = (Cost_Authorization__c)so;
            String Concat_EPA_Number = '';
            Decimal Percent_Complete  = 0;
            
            string CA_Id = objCA.Id;
            if(objCA.Concat_EPA_Number__c != null){
                Concat_EPA_Number = objCA.Concat_EPA_Number__c;
            }
            if(objCA.Percent_Complete__c != null){
                Percent_Complete = objCA.Percent_Complete__c;
            }
            string KronosInstance = objCA.EPA_Number__r.Kronos_Instance__c;
            //Run in case if CA have a location as "Cost Authorization US"
            if(KronosInstance == 'US'){
                    String envelope = SFSOAPHeader +   
                                        '<soapenv:Body>'+ 
                                            SFSOAP_BODY_Start+
                                            '<SERVICE_ENVELOPE>'+
                                                '<OPERATION>'+OPERATION_TYPE+'</OPERATION>'+
                                                '<SOURCE_SYSTEM>'+SOURCE_SYSTEM+'</SOURCE_SYSTEM>'+
                                                '<TARGET_SYSTEM>'+TARGET_SYSTEM+'</TARGET_SYSTEM>'+
                                                '<VERSION></VERSION>'+
                                                '<MESSAGE_TYPE>'+MESSAGE_TYPE+'</MESSAGE_TYPE>'+
                                                '<INTERFACE_NAME></INTERFACE_NAME>'+
                                                '<TIME_STAMP>'+TIME_STAMP+'</TIME_STAMP>'+
                                                '<UNIQUE_TXN_ID></UNIQUE_TXN_ID>'+
                                                '<SOURCE_BUSINESS></SOURCE_BUSINESS>'+
                                                '<TARGET_BUSINESS></TARGET_BUSINESS>'+
                                                '<TARGET_SYSTEM_IDENTIFIER></TARGET_SYSTEM_IDENTIFIER>'+
                                            '</SERVICE_ENVELOPE>'+
                                            '<CLOSED_COBRA_DETAILS>'+
                                                '<CLOSED_COBRA>'+
                                                      '<Concat_EPA_Number>'+Concat_EPA_Number+'</Concat_EPA_Number>'+
                                                      '<Percent_Complete>'+Percent_Complete+'</Percent_Complete>'+
                                                '</CLOSED_COBRA>'+
                                            '</CLOSED_COBRA_DETAILS>'+
                                            SFSOAP_BODY_End+ 
                                        '</soapenv:Body>'+
                                    '</soapenv:Envelope>' ;
                                                    
                System.Debug('envelope created' + envelope);
                
                //Sending data to Main_Header_Info
                String envelope_Conversion = envelope;    
                // Below logic is to convert SOAP Message, if contains '&' to '&amp;' 
                String Actual_SOAP_envelope =  envelope_Conversion.replace('&', '&amp;'); 
                HTTPResponse response =  Main_Header_Info(Actual_SOAP_envelope);  
                system.debug('CHECK FOR RESPONSE '+response.getStatusCode());
            }
            else if(KronosInstance == 'UK'){ 
                //Run in case if CA have a location as "Cost Authorization UK"
                    String envelope = SFSOAPHeader_UK +   
                                        '<soapenv:Body>'+ 
                                            SFSOAP_BODY_Start_UK+
                                            '<SERVICE_ENVELOPE>'+
                                                '<OPERATION>'+OPERATION_TYPE+'</OPERATION>'+
                                                '<SOURCE_SYSTEM>'+SOURCE_SYSTEM+'</SOURCE_SYSTEM>'+
                                                '<TARGET_SYSTEM>'+TARGET_SYSTEM+'</TARGET_SYSTEM>'+
                                                '<VERSION></VERSION>'+
                                                '<MESSAGE_TYPE>'+MESSAGE_TYPE+'</MESSAGE_TYPE>'+
                                                '<INTERFACE_NAME></INTERFACE_NAME>'+
                                                '<TIME_STAMP>'+TIME_STAMP+'</TIME_STAMP>'+
                                                '<UNIQUE_TXN_ID></UNIQUE_TXN_ID>'+
                                                '<SOURCE_BUSINESS></SOURCE_BUSINESS>'+
                                                '<TARGET_BUSINESS></TARGET_BUSINESS>'+
                                                '<TARGET_SYSTEM_IDENTIFIER></TARGET_SYSTEM_IDENTIFIER>'+
                                            '</SERVICE_ENVELOPE>'+
                     
                                            '<CLOSED_COBRA_DETAILS>'+
                                                '<CLOSED_COBRA>'+
                                                      '<Concat_EPA_Number>'+Concat_EPA_Number+'</Concat_EPA_Number>'+
                                                      '<Percent_Complete>'+Percent_Complete+'</Percent_Complete>'+
                                                '</CLOSED_COBRA>'+
                                            '</CLOSED_COBRA_DETAILS>'+
                                            SFSOAP_BODY_End_UK+ 
                                            '</soapenv:Body>'+
                                        '</soapenv:Envelope>';
                                                    
                    System.Debug('envelope created' + envelope);
                    String envelope_Conversion = envelope;    
                    String Actual_SOAP_envelope =  envelope_Conversion.replace('&', '&amp;'); 
                    HTTPResponse response =  Main_Header_Info_UKLocation(Actual_SOAP_envelope);  
                    System.debug('CHECK FOR RESPONSE '+response.getStatusCode()); 
            }
        } 
    }
    global void finish(Database.BatchableContext BC) {
    }
    
    //*********** BELOW IS MAIN HEADER CLASS -For US Location***********
    Public static HTTPResponse Main_Header_Info (String Actual_SOAP_envelope)
    {
        //Creating request object
        HttpRequest request = new HttpRequest();        

        // Below Added Custom Label as Values keep on changing per environment. 
        String username = Label.USERNAME;
        String password = Label.PASSWORD;
        String EndPoint = Label.Wm_Endpoint;    
    
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
        request.setMethod('POST');
        request.setTimeout(120000);
        
        Http http = new Http();
        request.setEndpoint(Label.Wm_Endpoint);
        
        //Setting content-type and SOAPAction
        request.setHeader('content-type', 'text/xml; charset=utf-8');  
        //request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_Provider_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse'); 
        
        request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse');
            
        //Setting the body
        request.setBody(Actual_SOAP_envelope);
            
        //Sending request
        HTTPResponse response = http.send(request);
        system.debug('-----&&&&&'+response.getBody());
    
        return response;
    } 
 
    //*********** BELOW IS MAIN HEADER CLASS - For UK Location***********
    
    Public static HTTPResponse Main_Header_Info_UKLocation (String Actual_SOAP_envelope)
    {
        //Creating request object
        HttpRequest request = new HttpRequest();        
        
        //Below Added Custom Label as Values keep on changing per environment . 
        String username = Label.USERNAME_UK_Location;
        String password = Label.PASSWORD_UK_Location;
        String EndPoint = Label.WM_EndPoint_UK_Location;    
         
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);
                
        request.setMethod('POST');
        request.setTimeout(120000);
        
        Http http = new Http();
        request.setEndpoint(Label.WM_EndPoint_UK_Location);
        
        //Setting content-type and SOAPAction
        request.setHeader('content-type', 'text/xml; charset=utf-8');  
        //request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_Provider_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse'); 
        
        request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse');
                
        //Setting the body
        request.setBody(Actual_SOAP_envelope);
        system.debug('-----&&&&&'+request.getBody());           
                
        //Sending request
        HTTPResponse response = http.send(request);
        system.debug('-----&&&&&'+response.getBody());
        system.debug('response---'+response);
        
        return response;
    }
}