/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - trgautoPopulateProductVariation_Clone
    
    Version     last modified By            last modified  Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar                     05 jan 2015             last modified to increase the test coverage.
            
*************************************************************************************************************************************************/      
  
@isTest
private class Test_trgautoPopulateProductVariation {
  /*  static testmethod void test_trgautoPopulateProductVariation() {
        Profile[] pro = [select id from profile where name = 'System Administrator' limit 1];

        User u = new User(alias = 'mkumar', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'mohan1.kumar@ge.com', emailencodingkey = 'UTF-8', lastname = 'kumar', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'mohan1.kumar@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(name = 'Hello');
        insert acc1; 
        
        system.runAs(u) {
            List<Platform_Master__c> listOfPM = new List<Platform_Master__c>();
            
            Platform_Master__c pm = new Platform_Master__c(Name = '17011111111111111111111111111111111111111111111111111111111111111', Civil_Military__c = 'Military',Segment__c = 'Regional Transports');
            listOfPM.add(pm);
            Platform_Master__c pm1 = new Platform_Master__c(Name = '171', Civil_Military__c = 'Military',Segment__c = 'Regional Transports');
            listOfPM.add(pm1);
            Platform_Master__c pm2 = new Platform_Master__c(Name = '172', Civil_Military__c = 'Military',Segment__c = 'Regional Transports');
            listOfPM.add(pm2);
            
            insert listOfPM;
            
            List<Forecast__c> listOfAP = new List<Forecast__c>();
            
            Forecast__c fore = new Forecast__c (Name = 'Test123451', Airframer__c = acc1.id,Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            listOfAP.add(fore);    
            Forecast__c fore1 = new Forecast__c(Name = 'Test123452', Airframer__c = acc1.id,Forecast_Platform__c = pm1.id, Is_Tokenized__c = false);    
            listOfAP.add(fore1);
            Forecast__c fore2 = new Forecast__c(Name = 'Test123453', Airframer__c = acc1.id,Forecast_Platform__c = pm2.id, Is_Tokenized__c = false);
            listOfAP.add(fore2);
            insert listOfAP;

            List<Product_Hiearchy_Object__c> listHier = new List<Product_Hiearchy_Object__c>();
            Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            listHier.add(hierarchy1);
            Product_Hiearchy_Object__c hierarchy2 = new Product_Hiearchy_Object__c(Name = 'Across Multiple Products', Product_Group__c = 'Avionics', Product_Area__c = 'Aviation Computing Systems', Product_Family__c = 'Tools', Product_Child__c = 'Across Multiple Products');
            listHier.add(hierarchy2);
            
            insert listHier; 
            
            List<Program__c> lstOfProg = new List<Program__c>();
            
            Program__c prog = new Program__c(Name = 'Program', Lookup_hiearchyname__c = hierarchy1.id, Product_Sales_Owner__c = u.id,
                                             Forecast__c = fore.id, Program_Probability__c = '50% - Potential funding');
            lstOfProg.add(prog);                                 
            Program__c prog1 = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy2.id, Product_Sales_Owner__c = u.id,
                                             Forecast__c = fore1.id, Program_Probability__c = '100% - Funded');
            lstOfProg.add(prog1);
            Program__c prog2 = new Program__c(Name = 'Program2', Lookup_hiearchyname__c = hierarchy2.id, Product_Sales_Owner__c = u.id,
                                             Forecast__c = fore2.id, Program_Probability__c = '0% - Not funded');
            lstOfProg.add(prog2);                                 
            insert lstOfProg;          
            
            Test.startTest();
            
            List<Opportunity> listOfOppy = new List<Opportunity>();
            
            opportunity opp = new opportunity(Name = 'Test opp 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', StageName = 'Contract Awarded', CloseDate = date.today(),
                                               Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                                               Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy1.id, Contract_Award_Date__c = date.today());
            
            listOfOppy.add(opp);
            opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = 'Contract Awarded', CloseDate = date.today(),
                                               Program__c = prog1.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                                               Probability__c = '75% - Program incumbent or selected supplier', Lookup_hiearchyname__c = hierarchy1.id, Contract_Award_Date__c = date.today());
            
            listOfOppy.add(opp1);
            opportunity opp2 = new opportunity(Name = 'Test opp2', StageName = 'Contract Awarded', CloseDate = date.today(),
                                               Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                                               Probability__c = '0% - Lost', Lookup_hiearchyname__c = hierarchy1.id, Contract_Award_Date__c = date.today());
            listOfOppy.add(opp2); 
            
            insert listOfOppy; 
                        
            opp1.name = 'Test opp22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222';
            opp1.Lookup_hiearchyname__c = hierarchy2.id;
            opp2.Program__c = prog1.id;
            Class_TriggerCheck.Var_StopProdVarMultipleExecution = true;
            
            try {
                update listOfOppy;
            }
            
            catch(Exception e) {
                System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
                System.Assert(e instanceOf system.Dmlexception);
                System.Assert(e.getMessage().contains('You cannot change program name'));
            }            
           
            Test.stopTest();
        }
    } */
}