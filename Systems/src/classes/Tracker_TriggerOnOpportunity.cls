/*************************************************
    Test Class to provide code coverage for the following Trigger/class:
    - TriggerOnOpportunity
    - OpportunityTrigger_Handler // handler class
    
    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Abhishek          01/04/2016        Initial Creation
                Abhishek          04/18/2016        Jira 511/537   
                Abhishek          05/24/2016         Created new test method "testPipelineOEServiceOpportunity"  for Jira 557
                Abhishek          07/19/2016      Custom contract changes
                Abhishek          09/18/2016      removed account platform
**************************************************************************************************************/    

@IsTest
public class Tracker_TriggerOnOpportunity
{

 private static testmethod void testOpportunity()
 {
   
   Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
   Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
   Class_TriggerCheck.Var_isProgam_Probability_change = true;
   Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
   Class_TriggerCheck.Var_StopUpdateRelationship = false;
   Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;   
   Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
   Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
   Class_TriggerCheck.var_TriggerOnMarginEscalator = false;
   Class_TriggerCheck.var_preventOppyNameUpdate_Recursion = true;
   
   Class_TriggerCheck.var_TriggerOnContract = false;
   Class_TriggerCheck.var_TriggerOnSchedule = false;
   Class_TriggerCheck.var_TriggerOnOrder = false;
   Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
   Class_TriggerCheck.var_TriggerOnResponse = false;
   Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
   Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
   Class_TriggerCheck.var_triggerOnUser = false;
   
   
   Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];   
   list<user> insertUser = new list<user>();
   
   User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
   localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
   insertUser.add(u) ; 

   User newUser = new User(alias = 'skaponew', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal1@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor1', languagelocalekey = 'en_US',
   localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal123@ge.com.sys.dev');
   insertUser.add(newUser)    ;
   
   insert insertUser ;
    System.runAs(insertUser[0]) {  
    
    //inserting acount
    Account acc = new Account(Name='GE GEAS');
    insert acc; 
    
   //inserting contact
   Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                            MailingCity='Delhi', MailingCountry='India', email = 'abhishek3.singh@ge.com');
   insert con;    
   //inserting Platform master
   Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Civil', Segment__c = 'Regional Transports');
   insert pm; 
         
   //inserting hierarchy.          
   Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Digital Solutions', Product_Group__c = 'Avionics', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
   insert hierarchy;
   
   //inserting hierarchy.          
   Product_Hiearchy_Object__c hierarchy_OE = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Avionics', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
   insert hierarchy_OE;
   
   //inserting product.
   Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True,IsActive = True);
   insert p2;  
   
   // Insert Ideation
   Ideation__c myIdea = new Ideation__c(name='Initial', Product_Group__c = 'Avionics', Status__c = 'Open', Civil_Military__c = 'Civil', Site__c = 'Austin');
   Insert myIdea;
   
   Ideation__c newIdea = new Ideation__c(name='New Idea', Product_Group__c = 'Avionics', Status__c = 'Open', Civil_Military__c = 'Civil', Site__c = 'Austin');
   Insert newIdea;
     
   // insert program
   Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = insertUser[0].id,
                Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
   insert prog;
   
 
   
   // Fetch all recordtypes based on label
   
   Id initialOppRec = OpportunityTrigger_Handler.getRecordTypeId(Opportunity.SObjectType, Label.Label_InitialOpportunity);
   Id poForecastOppRec = OpportunityTrigger_Handler.getRecordTypeId(Opportunity.SObjectType, Label.Label_POForecastOpportunity);
   Id pipelineOppRec = OpportunityTrigger_Handler.getRecordTypeId(Opportunity.SObjectType, Label.Label_PipelineOpportunity);
   Id renOppRec = OpportunityTrigger_Handler.getRecordTypeId(Opportunity.SObjectType, Label.Label_RenewalOpportunity);
   
   EPA__c epa = new EPA__c(Contract_Site__c = 'Brisbane');
   insert epa;   
   
           Test.StartTest();
           
               List<opportunity> InsertOpp = new list<opportunity>();
               
               
               opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                , CloseDate = date.today(), Program__c = prog.id
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc.id
                                , Ideation__c = myIdea.Id
                                , Offering_Type__c = 'Digital Solutions'
                                , Contract_Term__c = 5);
                                
               insert opp;
              // InsertOpp.add(opp);
               
                 // insert contract
                 id recContractId = [Select id from RecordType where IsActive = true and SobjectType = 'Contract_PO__c' and name = 'Digital Solutions'][0].id;
               Contract_PO__c ContrMonthly = new Contract_PO__c (Account_Name__c = acc.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
                                       , Legal__c = con.id, Program_Manager__c = insertUser[0].id
                                       , Finance__c = con.ID, Sales__c = insertUser[0].id , Comm_Ops__c = insertUser[0].id    
                                       , Number_of_PO_s__c = 2.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Monthly'
                                       , Revenue_Increment__c = 'Monthly', Pipeline_Opportunity__c = opp.id
                                       , RecordTypeId = recContractId); 
                insert ContrMonthly;
                
                
             
             // Create Opp with all recordtypes to provide test data for menthod "autoGenerateOppyname" 
             opportunity pipelineOpp= new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                 , CloseDate = date.today()
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc.id
                                , recordtypeId = pipelineOppRec
                                , Ideation__c = myIdea.Id
                                , Offering_Type__c = 'OE / Services');
                                
             InsertOpp.add(pipelineOpp)  ; 
              
             opportunity initialOp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                 , CloseDate = date.today(), Program__c = prog.id
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc.id
                                , recordtypeId = initialOppRec
                                , ContractID1__c=ContrMonthly.Id
                                , Ideation__c = myIdea.Id, Offering_Type__c = 'Digital Solutions' );
                                
              InsertOpp.add(initialOp);
            
             opportunity renOpp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                 , CloseDate = date.today(), Program__c = prog.id
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc.id
                                , recordtypeId = renOppRec
                                , ContractID1__c=ContrMonthly.Id
                                , Ideation__c = myIdea.Id, Offering_Type__c = 'Digital Solutions' );                   
              InsertOpp.add(renOpp)  ;  
              
              Insert InsertOpp;
              
              id oeContractId = [Select id from RecordType where IsActive = true and SobjectType = 'Contract_PO__c' and name = 'OE/Services'][0].id;
             
             Contract_PO__c PoContr = new Contract_PO__c (Account_Name__c = acc.id, 
                                          Status__c =   'Draft'  , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
                                            , Legal__c = con.id, Program_Manager__c = insertUser[0].id, Finance__c = con.ID, Sales__c = insertUser[0].id , Comm_Ops__c = insertUser[0].id    
                                            , Number_of_PO_s__c = 2.0, First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Monthly'
                                            , Revenue_Increment__c = 'Monthly', Pipeline_Opportunity__c = pipelineOpp.id, RecordTypeId = oeContractId); 
                insert PoContr;
   
                // insert Order
                Order__c ord = new order__c(name = 'test', Contract1__c = PoContr.id, Status__c='Open');
                insert ord; 
                
                opportunity poForecastOpp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                 , CloseDate = date.today()
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc.id
                                , recordtypeId = poForecastOppRec
                                , ContractID1__c=PoContr.Id
                                , Ideation__c = myIdea.Id, Offering_Type__c = 'OE / Services', Price_Escalation_COLA__c = 10, Price_Escalation_Start_Date__c = System.Today() );
               Insert poForecastOpp  ;  
              
        // ContrMonthly.Pipeline_Opportunity__c = InsertOpp[1].id;      
         ContrMonthly.Pipeline_Opportunity__c = opp.id;     
         // inserting OLIs to provide test data for Cola Update menthod and for tokenization method     
         Opportunity_Line_Item__c pli = new Opportunity_Line_Item__c(Opportunity__c=opp.id, Product__c=p2.Id, Probability__c= 20, Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false);
         insert pli;
         
         Opportunity_Line_Item__c Opli = new Opportunity_Line_Item__c(Opportunity__c=InsertOpp[1].id, Product__c=p2.Id, Probability__c= 20, Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false, Unit_Price__c = 50);
         insert Opli;  
         
         Opportunity_Line_Item__c oRenewal = new Opportunity_Line_Item__c(Opportunity__c=InsertOpp[2].id, Product__c=p2.Id, Probability__c= 20, Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false, Unit_Price__c = 50);
         insert oRenewal; 

         Opportunity_Line_Item__c Popli = new Opportunity_Line_Item__c(Opportunity__c=poForecastOpp.id, Product__c=p2.Id, Probability__c= 20, Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false, Unit_Price__c = 10);
         insert Popli;
         
         // insert Response
         list<Sales_Request__c > myResponseList = new list<Sales_Request__c >();
         for(integer respCount=0;respCount<3;respCount++){
             Sales_Request__c sr1 = new Sales_Request__c(Name = 'Test response5' + respCount,Pipeline_Opportunity__c = opp.id,Type__c ='test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = insertUser[0].id,Sales_Owner__c =insertUser[0].id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c ='Windchill UK',Response_Submitted__c = system.Today(),Cost_Estimator__c = con.id,Engineering_Lead__c = con.id,Pricing_Manager__c = con.id,Manufacturing_Cost_Estimator__c = con.id,Additional_Member__c = con.id,Program_Manager__c = insertUser[0].id,Finance_Manager_Analyst__c = con.id,Windchill_Folder_Structure__c = 'ksbdbs');
             if(respCount<2)
               sr1.Pipeline_Opportunity__c = opp.id; // for Digital
             else
               sr1.Pipeline_Opportunity__c = InsertOpp[0].id; // for OE
             
             myResponseList.add(sr1);
         }
         insert myResponseList;
         
         list<Cost_Authorization__c> insertCA = new list<Cost_Authorization__c>();
         Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = myResponseList[0].id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, FY_Spent_to_Date__c = NULL, Cost_Authorization_US_UK__c = 'Cost Authorization US', Close_Cost_Authorization__c = false);
         insertCA.add(ca);
         Cost_Authorization__c caOE = new Cost_Authorization__c(Sales_Request__c = myResponseList[2].id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, FY_Spent_to_Date__c = NULL, Cost_Authorization_US_UK__c = 'Cost Authorization US', Close_Cost_Authorization__c = false);
         insertCA.add(caOE);
         
         insert insertCA;
         
              List<Opportunity> UpdateOpp = new List<Opportunity>();
              InsertOpp.add(poForecastOpp);
              for(Opportunity o : InsertOpp)
              {
                o.Ideation__c = newIdea.Id;
                o.Name = 'UpdateName' + o.Name;
               // o.Is_Tokenized__c = true;
                o.Price_Escalation_COLA__c = 2;
                o.Price_Escalation_Start_Date__c = System.Today();
                o.Contract_Term__c = 7;
                o.StageName = Label.OpportunityStage7;
                o.Cancelled_Reason__c = 'Error-Duplicate Entry';
                if(o.Offering_Type__c == 'OE / Services' && o.recordtypeId==pipelineOppRec)
                    o.ContractID1__c = PoContr.Id;
                    
                UpdateOpp.add(o);
                
             }

             Opp.ownerId = insertUser[1].id; // to cover UpdateSalesOwnerOnResponse
             UpdateOpp.add(opp);
             
             List<Schedule__c> lstOfSch = new List<Schedule__c>();
             Date dateInstance  = Date.newInstance(System.Today().Year(),1,30); 
            for(Integer i=0;i<=18;i++)
            {
              Schedule__c sc = new Schedule__c(Opportunity_Line_Item__c = pli.id, Schedule_Date__c = dateInstance.addMonths(i), Quantity__c = 1, Escalated_Revenue__c = 3
              , Opportunity__c = opp.ID);
              lstOfSch.add(sc);
            } 
            
             for(Integer i=0;i<=18;i++)
            {
              Schedule__c osc = new Schedule__c(Opportunity_Line_Item__c = Opli.id, Schedule_Date__c  = dateInstance.addMonths(i), Quantity__c = 1, Escalated_Revenue__c = 3, Opportunity__c = InsertOpp[1].ID);
              lstOfSch.add(osc);
            } 
            
             for(Integer i=0;i<=12;i++)
            {
              Schedule__c osc1 = new Schedule__c(Opportunity_Line_Item__c = oRenewal.id, Schedule_Date__c  = dateInstance.addMonths(i), Quantity__c = 1, Escalated_Revenue__c = 3, Opportunity__c = InsertOpp.get(2).ID, Total_Actual_Amount__c = 100);
              lstOfSch.add(osc1);
            } 
            
             for(Integer i=0;i<=12;i++)
            {
              Schedule__c poSch = new Schedule__c(Opportunity_Line_Item__c = Popli.id, Schedule_Date__c  = dateInstance.addMonths(i), Quantity__c = 1, Escalated_Revenue__c = 3, Opportunity__c = poForecastOpp.ID, Total_Actual_Amount__c = 100);
              lstOfSch.add(poSch);
            } 
            
            insert lstOfSch;
             
             update UpdateOpp;
             try{
                Delete UpdateOpp;
                Undelete UpdateOpp;
             }catch (exception e) {
                boolean isException = e.getMessage().contains('opportunity cannot be deleted') ? true : false ;
                    system.assertEquals(isException , true);
             }
             
               
         Test.StopTest();
    
    } 
 
 
 } 
 
 private static testmethod void testPipelineOEServiceOpportunity()
 {
   
   Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
   Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
   Class_TriggerCheck.Var_isProgam_Probability_change = true;
   Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
   Class_TriggerCheck.Var_StopUpdateRelationship = false;
   Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;   
   Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
   Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
   Class_TriggerCheck.var_TriggerOnMarginEscalator = false;
   Class_TriggerCheck.var_preventOppyNameUpdate_Recursion = true;
   
   Class_TriggerCheck.var_TriggerOnContract = false;
   Class_TriggerCheck.var_TriggerOnSchedule = false;
   Class_TriggerCheck.var_TriggerOnOrder = false;
   Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
   Class_TriggerCheck.var_TriggerOnResponse = false;
   Class_TriggerCheck.var_triggerOnUser = false;
   
   Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];        
   User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
   localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
   insert u;  
       
    System.runAs(u) {  
    
    //inserting acount
    Account acc = new Account(Name='GE GEAS');
    insert acc; 
    
   //inserting contact
   Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                            MailingCity='Delhi', MailingCountry='India', email = 'abhishek3.singh@ge.com');
   insert con;    
   //inserting Platform master
   Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
   insert pm; 
         
   //inserting hierarchy.          
   Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Avionics', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
   insert hierarchy;
   
   //inserting product.
   Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True,IsActive = True);
   insert p2;  
   
   
   // Fetch all recordtypes based on label
    
   Id pipelineOppRec = OpportunityTrigger_Handler.getRecordTypeId(Opportunity.SObjectType, Label.Label_PipelineOpportunity);
  
  
    
            Test.StartTest();
           
               List<opportunity> InsertOpp = new list<opportunity>();
               // Create Opp with all recordtypes to provide test data for menthod "autoGenerateOppyname" 
               opportunity pipelineOpp= new opportunity(Name = 'Test opp2'
                                        , CloseDate = date.today() , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                        , GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                                        , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                        , Platform_New__c = pm.id, accountId= acc.id, recordtypeId = pipelineOppRec
                                        , Offering_Type__c = 'OE / Services', StageName=label.OpportunityStage2);
               InsertOpp.add(pipelineOpp)  ; 
               
               Insert InsertOpp;
         
               Opportunity_Line_Item__c Opli = new Opportunity_Line_Item__c(Opportunity__c=InsertOpp[0].id, Product__c=p2.Id, Probability__c= 20, Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false, Unit_Price__c = 50);
               insert Opli;  
         
       
               // insert Response
               list<Sales_Request__c > myResponseList = new list<Sales_Request__c >();
               for(integer respCount=0;respCount<2;respCount++){
                    Sales_Request__c sr1 = new Sales_Request__c(Name = 'Test response5' + respCount,Pipeline_Opportunity__c = InsertOpp[0].id,Type__c ='test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = u.id,Sales_Owner__c =u.id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c ='Windchill UK',Response_Submitted__c = system.Today(),Cost_Estimator__c = con.id,Engineering_Lead__c = con.id,Pricing_Manager__c = con.id,Manufacturing_Cost_Estimator__c = con.id,Additional_Member__c = con.id,Program_Manager__c = u.id,Finance_Manager_Analyst__c = con.id,Windchill_Folder_Structure__c = 'ksbdbs', SR_Status__c='Opened');
             
                    myResponseList.add(sr1);
                }
                insert myResponseList;
         
                List<Opportunity> UpdateOpp = new List<Opportunity>();
                for(Opportunity o : InsertOpp){
                    o.StageName = label.OpportunityStage5;
                    UpdateOpp.add(o);
                }
                update UpdateOpp;
            Test.StopTest();
    
    } 
 
 
 } 

}