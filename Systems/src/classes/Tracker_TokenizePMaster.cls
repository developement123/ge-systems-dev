/************************************************************************************************
This test class was created to provide code coverage for the following trigger:
- TokenizePMaster

Version     Author(s)               Date              Details 
-------------------------------------------------------------------------------------------------------
1.0         Mohan kumar          10/15/2014        Initial Creation
            Abhishek             10/6/2016         Rewrite Tesr class 
**************************************************************************************************/      
      
      
@istest
public class Tracker_TokenizePMaster {
      
      static testmethod void test_TokenizePMaster() {    
       Class_TriggerCheck.var_TriggerOnAccount = false;
       Class_TriggerCheck.var_triggerOnUser = false;
       
       Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

       User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS LIMITED', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev', Perspecsys_Access__c = true);
       insert u;
       
       Account acc1 = new Account(Name = 'Hello');
       insert acc1;
         
       
            
        System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports', Is_Tokenized__c = true);
            insert pm;
            
            Platform_Model__c PMO = new Platform_Model__c(name='platform model', Platform_Master__c=pm.id, Is_Tokenized__c = true);
            insert PMO;
            
            pm.name='test';
            update pm;
            
        }       
     }    
}