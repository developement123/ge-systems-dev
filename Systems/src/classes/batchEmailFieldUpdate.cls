/*
        Developer : Abhishek
        Date: 16/Feb/2017    
        Action : This class delete all UK records and being used in "runPostRefreshActivity" class
          
    */
global class batchEmailFieldUpdate implements Database.Batchable<sObject> {
 global Iterable<Sobject> start(Database.batchableContext info){ 
       Iterable<SObject> myIter = (Iterable<SObject>) new CustomIterable();
       return myIter; 
   }     
   
   global void execute(Database.batchableContext info, List<SObject> scope){ 
      Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
       Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
       Class_TriggerCheck.var_IsStopEPABatchTrg = true;
       Class_TriggerCheck.Var_ShouldEPA_update = false;
       Class_TriggerCheck.Var_StopTriggerOnEPA = false;
       Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
       Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
       Class_TriggerCheck.var_TriggerOnResponse = false;
       Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
       Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
       Class_TriggerCheck.var_TriggerOnContract = false;
       Class_TriggerCheck.var_TriggerOnSchedule = false;
       Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
       Class_TriggerCheck.Var_StopPlatMasterTrigger = false; 
       Class_TriggerCheck.var_TriggerOnIdeationTrg = false;
       Class_TriggerCheck.var_TriggerOnOrder = false;
       Class_TriggerCheck.var_triggerOnUser = false;    
       
       if(runPostRefreshActivity.isSandbox() || Test.isRunningTest() ){
          system.debug('deletion started');
          database.delete(scope,false);
          database.emptyRecycleBin(scope); 
          system.debug('deletion complete');
       }
   }     
   global void finish(Database.batchableContext info){     
       InvalidateEmails.updateAppEMails('QA');
       
   } 
 
 
 
 }