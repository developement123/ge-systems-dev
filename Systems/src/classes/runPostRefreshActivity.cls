/*
        Developer : Abhishek
        Date: 6/Feb/2017    
        Action : This class need to be executed post sandbox refrest to perfrom below operations
        - Delete all UKrecords from objects mentioned in custom hierarchy using batch class "batchEmailFieldUpdate" 
        - Update  user's Username having profile system admin & delegated admin and also change profile
          of delegated admin to system admin - we commented this functionality as they want to run this manually 
          
    */

    global Class runPostRefreshActivity implements SandboxPostCopy{
    list<string>listOfObjectTodelete;


    public runPostRefreshActivity(){
       
       // setting static vairables to false to avoid trigger execution
       Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
       Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
       Class_TriggerCheck.var_IsStopEPABatchTrg = true;
       Class_TriggerCheck.Var_ShouldEPA_update = false;
       Class_TriggerCheck.Var_StopTriggerOnEPA = false;
       Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
       Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
       Class_TriggerCheck.var_TriggerOnResponse = false;
       Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
       Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
       Class_TriggerCheck.var_TriggerOnContract = false;
       Class_TriggerCheck.var_TriggerOnSchedule = false;
       Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
       Class_TriggerCheck.Var_StopPlatMasterTrigger = false; 
       Class_TriggerCheck.var_TriggerOnIdeationTrg = false;
       Class_TriggerCheck.var_TriggerOnOrder = false;
       Class_TriggerCheck.var_triggerOnUser = false;    

       
        listOfObjectTodelete = new list<string>();
        // get all objects from custom setting 
        for(UKObjectList__c objStr:[select name,ObjectAPI__c,Order__c from UKObjectList__c where wantToDelete__c=true order by Order__c]){
            listOfObjectTodelete.add(objStr.ObjectAPI__c);
        }
    }

    // use this method for manual execution i.e from developerconsole
    public void flushUKRecord(string sbxName){
        if(isSandbox() || Test.isRunningTest()){
            updateEmails.updateUser(sbxName);
            // batch class to delete all UK records
            batchEmailFieldUpdate b = new batchEmailFieldUpdate(); 
            database.executebatch(b,500);
        }
    }


    global void runApexClass(SandboxContext context) {
        if(isSandbox() || Test.isRunningTest()){
            // below code commented on Feb,27 as they want it to run manually frm dev console
            //updateEmails.updateUser(context.sandboxName());
            // batch class to delete all UK records
            batchEmailFieldUpdate b = new batchEmailFieldUpdate(); 
            database.executebatch(b,500);
        }
    }
    
    //return true if sandbox     
    public static boolean isSandbox(){
        return [select IsSandbox from Organization limit 1][0].IsSandbox ;
        
    }

   

/*

    public void getRecordsByObject(list<string> objectList,string isDelete){
        List<SObject> ukRecords;
        string sql;
        for(string str : objectList){
            sql = 'select id from ' + str + ' where Is_Tokenized__c=true';
            system.debug(sql);
            ukRecords = Database.query(sql);
            if(isDelete=='true'){
                deleteRecord(ukRecords, str);
            }
        }
        
    }

    public void deleteRecord(list<sobject> recordTodelete, string objectAPIname){
        if(!recordTodelete.isEMpty()){
            try{
                database.delete(recordTodelete,false);
                system.debug('Records from object ' + objectAPIname + ' have been deleted.');
            }catch(exception ex){
                System.debug(' Following exception occured ' + ex.getMessage() + ' for object ' + objectAPIname);
            }
        }
    }

    public void updateOrder(){
    list<order__c> updateOrder = new list<order__c>();
    for(Order__c ukOrder : [select id, Status__c from Order__c where Is_Tokenized__c = true and Status__c in ('PO Booked','ERP Forecast')]){
            ukOrder.Status__c = 'Open';
            updateOrder.add(ukOrder);
      
       }
       if(!updateOrder.isEmpty()){
            database.update(updateOrder,false);
       }
    deleteOrder();

    }
 
    public void deleteOrder(){
    list<order__c> DeleteOrder = new list<order__c>();
    for(Order__c ukOrder : [select id, Status__c from Order__c where Is_Tokenized__c = true]){
            DeleteOrder.add(ukOrder);
    }  
       if(!DeleteOrder.isEmpty()){
            database.delete(DeleteOrder,false);
       }
      
     deletePoRenewalsOpp(); 
    }

    public void deletePoRenewalsOpp(){ 
     list<Opportunity> DeleteOpportunity = new list<Opportunity>();
     for(Opportunity ukOpp : [select id from Opportunity where Is_Tokenized__c = true and recordtype.name in ('PO forecast','Initial','Renewal') order by createddate desc]){
            DeleteOpportunity.add(ukOpp);
     }  
       if(!DeleteOpportunity.isEmpty()){
            database.delete(DeleteOpportunity,false);
       }
     
    } */

}