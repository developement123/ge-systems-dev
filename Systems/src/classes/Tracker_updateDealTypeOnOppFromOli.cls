//Comments added By Abhishek: Jan.- 2016:
//This test class is not required as corresponding trigger "updateME" is inactivated for Opportunity Optimization Requirement.
//End
/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following Trigger:
    - updateDealTypeOnOppFromOli
    
    Version      last modified by               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0           Mohan kumar                  10/16/2014        initial creation
    
    Last modified By : 15 April 2015.
            
*************************************************************************************************************************************************/      

   
@istest(seeAllData = true)
public class Tracker_updateDealTypeOnOppFromOli {    
   /* static testmethod void mytest1() {
    
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
        
        Account acc = new Account(Name = 'Test Account');
        insert acc;
    
        Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
        insert plat;

        Forecast__c fore2 = new Forecast__c(Airframer__c = acc.id, Forecast_Platform__c = plat.id);
        insert fore2;

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo', email = 'sunny.kapoor21@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor21', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor2111@birlasoft50.com.sys.dev');
        insert u;

        System.runAs(u) {
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore2.id, Program_Probability__c = '100% - Funded');
            insert prog;

            Opportunity opp = new opportunity(Name = 'Test opp', StageName = 'Prospecting', CloseDate = date.today(), Program__c = prog.id, Firm_Anticipated__c = 'Anticipated', Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id);
            insert opp;

            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True);
            insert p2;  
            
            Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true For Update];
            
            Pricebook2 pb = new Pricebook2(Name = 'Custom Price Book ', Description = 'Price Book 2009 Products', IsActive = true);
            insert pb;
            
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, 
                                           Product2Id = p2.Id, UnitPrice = 10000, 
                                           IsActive = true, UseStandardPrice = false);
            insert standardPrice;

            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = p2.Id, 
                                 UnitPrice = 99, isActive = true, UseStandardPrice = false);
            insert pbe;
            
            Test.startTest();
            
                List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();

                oliList.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id,
                    OpportunityId = opp.Id,Revenue_Stream_top_level__c = 'Engineering',
                    Revenue_Stream_child_level__c = 'Funded R&D', Quantity = 1, UnitPrice = 0));                
               
                oliList.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id,
                    OpportunityId = opp.Id,Revenue_Stream_top_level__c = 'Engineering',
                    Revenue_Stream_child_level__c = 'ECP/ECR', Quantity = 1, UnitPrice = 0));
                 
                oliList.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id,
                    OpportunityId = opp.Id,Revenue_Stream_top_level__c = 'Hardware/OE',
                    Revenue_Stream_child_level__c = 'New Product Development', Quantity = 1, UnitPrice = 0));
                
                oliList.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id,
                    OpportunityId = opp.Id,Revenue_Stream_top_level__c = 'Hardware/OE',
                    Revenue_Stream_child_level__c = 'Momentum Production', Quantity = 1, UnitPrice = 0));
                
                oliList.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id,
                    OpportunityId = opp.Id,Revenue_Stream_top_level__c = 'Services',
                    Revenue_Stream_child_level__c = 'Momentum Production', Quantity = 1, UnitPrice = 0));
                
                oliList.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id,
                    OpportunityId = opp.Id,Revenue_Stream_top_level__c = 'Royalties',
                    Revenue_Stream_child_level__c = 'Existing Product', Quantity = 1, UnitPrice = 0));                
                
                insert oliList;  
                delete oliList;         
            Test.stopTest();
        }
    }
    static testmethod void mytest2() {
        
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
    
        Account acc = new Account(Name = 'Test Account');
        insert acc;
        
        Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
        insert plat;  

        Forecast__c fore2 = new Forecast__c(Airframer__c = acc.id, Forecast_Platform__c = plat.id);
        insert fore2;

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo', email = 'sunny.kapoor21@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor21', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor2111@birlasoft50.com.sys.dev');
        insert u;

        System.runAs(u) {
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore2.id, Program_Probability__c = '100% - Funded');
            insert prog;

            Opportunity opp = new opportunity(Name = 'Test opp', StageName = 'Prospecting', CloseDate = date.today(), Program__c = prog.id, Firm_Anticipated__c = 'Anticipated', Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id);
            insert opp;

            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True);
            insert p2;

            Pricebook2 standardPB = [select id from Pricebook2 where isStandard = true For Update];
            
            Pricebook2 pb = new Pricebook2(Name = 'Custom Price Book ', Description = 'Price Book 2009 Products', IsActive = true);
            insert pb;
            
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, 
                                           Product2Id = p2.Id, UnitPrice = 10000, 
                                           IsActive = true, UseStandardPrice = false);
            insert standardPrice;

            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = p2.Id, 
                                 UnitPrice = 99, isActive = true, UseStandardPrice = false);
            insert pbe;
            
            Test.startTest();
            
                List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
              
                OpportunityLineItem ol2 = new OpportunityLineItem(PriceBookEntryId = pbe.Id,
                    OpportunityId = opp.Id, Revenue_Stream_top_level__c = 'Royalties',
                    Revenue_Stream_child_level__c = 'Existing Product', Quantity = 1, UnitPrice = 0);
                oliList.add(ol2);
                
                insert oliList;   
                
                ol2.Revenue_Stream_top_level__c = 'Services'; 
                    
                ol2.Revenue_Stream_top_level__c = 'Hardware/OE';            
                ol2.Revenue_Stream_child_level__c = 'Momentum Production';     
                update ol2; 
                
                ol2.Revenue_Stream_top_level__c = 'Engineering';            
                ol2.Revenue_Stream_child_level__c = 'ECP/ECR';   
                update ol2;
                
                ol2.Revenue_Stream_top_level__c = 'Services';
                update ol2;
            
            Test.stopTest();
        }
    } */
}