/*
        Developer : Abhishek
        Date: 14/Feb/2017    
        Action : This class updates user's record and being used in "runPostRefreshActivity" class
          
    */

public class updateEmails{


// updating users in future methods to avoid mixed DML error
@future 
// Update  user's Username having profile system admin & delegated admin and also change profile
    // of delegated admin to system admin
    public static void updateUser(string sandboxName){
        Class_TriggerCheck.var_triggerOnUser = false;  
        map<string,id>profileInfo = new map<string,id>();
        for(profile p : [select id, name from profile where name in ('Systems Delegated Administrator - BPSS Cleared','Systems Delegated Administrator','System Administrator')]){
          profileInfo.put(p.name,p.id);
        }
        
        list<user> listofUser = new list<user>();
        for(user u : [select id, Email, ProfileId, profile.name,username  from user where ProfileId=:profileInfo.values() and IsActive=true]){
            if( u.profile.name=='Systems Delegated Administrator - BPSS Cleared' || u.profile.name=='Systems Delegated Administrator'){
                u.ProfileId = profileInfo.get('System Administrator');
            }
            if(u.email.contains('@example.com'))
                u.email = u.username.substring(0, u.username.indexOf('.sys')) ;
            
            listofUser.add(u);
        }
        if(!listofUser.isEmpty()){
        try{
                update listofUser;
                System.debug(' users have been updated');
           }catch(exception ex){
                System.debug(' Following exception occured ' + ex.getMessage() + ' on updating User');
           }
        }
    }



}