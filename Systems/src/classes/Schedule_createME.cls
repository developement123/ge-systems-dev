/***************************************************************************************************************************************************
    This Schedule class was written to schedule the batch class 'createME_new'.    
    Written by : Mohan Kumar
    Date       : 3/11/2014
    Last modified by : Mohan Kumar(13 Jan 2015) for JIRA 364.
    Last modified by : Mohan Kumar(30 October 2015).
****************************************************************************************************************************************************/

global with sharing class Schedule_createME implements Schedulable {    
    global void execute(SchedulableContext sc) {
        if(Label.start_stop_oppylineitemschedule_batch == 'true') {
            createME_new obj = new createME_new(); //Calling new version of ME batch created as a part of Opportunity Optimization Requirement.
            Database.executeBatch(obj, 50);            
        }
    }
}