@isTest
private class AgreementDelete_Test{
 static testmethod void dodelete(){
 
    Test.starttest();
    //creating Account
    Account a=new Account(Name='Test Account');
     insert a;
   //Create agreement record
    
    Apttus__APTS_Agreement__c agr=new Apttus__APTS_Agreement__c();
    agr.Name='Agreement Name';
    recordtype rt = [select id from RecordType where SobjectType='Apttus__APTS_Agreement__c' and DeveloperName='GE_Digital_Services' Limit 1];
    agr.RecordTypeId = rt.id ;
    agr.Apttus__Contract_Start_Date__c=System.today();
    agr.Apttus__Account__c=a.id;
    agr.Apttus__Contract_End_Date__c=System.today();
    agr.Apttus__Description__c='Test Description';    
    insert agr;
    PageReference pageRef = Page.AgreementDelete;   
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('id',agr.Id);
    
    ApexPages.StandardController controller = new ApexPages.StandardController(agr);  
    AgreementDelete agrdel= new AgreementDelete(controller);
    agrdel.dodelete();
    
    
    Apttus__APTS_Agreement__c agr1=new Apttus__APTS_Agreement__c();
    agr.Name='Agreement Name';
    recordtype rt1 = [select id from RecordType where SobjectType='Apttus__APTS_Agreement__c' and DeveloperName='GE_Digital_Services' Limit 1];
    agr1.RecordTypeId = rt1.id ;
    agr1.Apttus__Contract_Start_Date__c=System.today();
    agr1.Apttus__Account__c=a.id;
    agr1.Apttus__Contract_End_Date__c=System.today();
    agr1.Apttus__Description__c='Test Description';    
    insert agr1;
    
    PageReference pageRef1 = Page.AgreementDelete;   
    Test.setCurrentPage(pageRef1);
    ApexPages.currentPage().getParameters().put('id',agr1.Id);
    ApexPages.StandardController controller1 = new ApexPages.StandardController(agr1);  
    AgreementDelete agrde2= new AgreementDelete(controller1);
    agrde2.doCancel();
    
 }
 }