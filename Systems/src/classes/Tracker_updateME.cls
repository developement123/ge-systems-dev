//Comments added By Abhishek: Jan.- 2016:
//This test class is not required as corresponding trigger "updateME" is inactivated for Opportunity Optimization Requirement.
//End
/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - updateME

    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          12/03/2014        Initial Creation
            
*************************************************************************************************************************************************/      
      
      
@istest(SeeAllData = true)
private class Tracker_updateME {
 /*
     static testmethod void test_updateME() {
     
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;

        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
                          localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;

            opportunity opp = new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),Margin_Escalator__c = false);

            insert opp;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True);
            insert p2;

            //Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true For Update];
            
            // Get standard price book ID.
            // This is available irrespective of the state of SeeAllData.
            Id standardPBId = Test.getStandardPricebookId(); //to fix error "Record Currently Unavailable:" & "Unable to obtain exclusive access" while run all tests
        
            Pricebook2 pb = new Pricebook2(Name = 'Custom Price Book', Description = 'Price Book 2009 Products', IsActive = true);
            insert pb;
            
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPBId , 
                                           Product2Id = p2.Id, UnitPrice = 10000, 
                                           IsActive = true, UseStandardPrice = false);
            insert standardPrice;

            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = p2.Id, 
                                 UnitPrice = 99, isActive = true, UseStandardPrice = false);
            insert pbe;

            OpportunityLineItem ol2 = new OpportunityLineItem(PriceBookEntryId = pbe.Id, OpportunityId = opp.Id,
                Quantity = 1, UnitPrice = 0,Probability__c=10);
            insert ol2;
            
            Date mydate = Date.newInstance(Integer.valueOf(System.Today().Year()), 1, 5);
            Date mydate1 = Date.newInstance(Integer.valueOf(System.Today().Year() +1), 10, 5);
            
            OpportunityLineItemSchedule olis1 = new OpportunityLineItemSchedule(OpportunityLineItemId = ol2.id, ScheduleDate = mydate, Quantity = 1, Revenue = 3, Type = 'both');
            insert olis1; 
            
        Test.startTest(); 
                            
            Sales_Target__c st = new Sales_Target__c(Sales_Person__c = u.id,Sales_Year__c=String.valueOf(System.Today().Year()));
            
            insert st; 
            
            List<OpportunityLineItemSchedule> listofSchedule = new List<OpportunityLineItemSchedule>();
           
            OpportunityLineItemSchedule olis2 = new OpportunityLineItemSchedule(OpportunityLineItemId = ol2.id, ScheduleDate = mydate1, Quantity = 1, Revenue = 100, Type = 'both');
            OpportunityLineItemSchedule olis3 = new OpportunityLineItemSchedule(OpportunityLineItemId = ol2.id, ScheduleDate = mydate1, Quantity = 2, Revenue = 300, Type = 'both');
            
            listofSchedule.add(olis2);
            listofSchedule.add(olis3);
            
            insert listofSchedule;
            
            delete ol2;
                                 
        Test.stopTest();   
        }
    }  
    
    static testmethod void test_updateME1() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;     
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;       

        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
            List<opportunity> listOfOppy = new List<opportunity>();
        
            listOfOppy.add(new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),
                Margin_Escalator__c = false));
            listOfOppy.add(new opportunity(Name = 'Test opp2', StageName = 'PO Booked', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),
                Margin_Escalator__c = false));
           
            insert listOfOppy;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True);
            insert p2;

            //Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true For Update];
            
            // Get standard price book ID.
            // This is available irrespective of the state of SeeAllData.
            Id standardPBId = Test.getStandardPricebookId(); //to fix error "Record Currently Unavailable:" & "Unable to obtain exclusive access" while run all tests
            
            Pricebook2 pb = new Pricebook2(Name = 'Custom Price Book', Description = 'Price Book 2009 Products', IsActive = true);
            insert pb;
            
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPBId , 
                                           Product2Id = p2.Id, UnitPrice = 10000, 
                                           IsActive = true, UseStandardPrice = false);
            insert standardPrice;

            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = p2.Id, 
                                 UnitPrice = 99, isActive = true, UseStandardPrice = false);
            insert pbe;
            
            List<OpportunityLineItem> listOfOli = new List<OpportunityLineItem>();

            listOfOli.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id, OpportunityId = listOfOppy[0].Id,
                Quantity = 1, UnitPrice = 0,Probability__c=10));
            listOfOli.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id, OpportunityId = listOfOppy[1].Id,
                Quantity = 1, UnitPrice = 0,Probability__c=10));
            
            insert listOfOli;            
            
            Date mydate = Date.newInstance(Integer.valueOf(System.Today().Year()), 1, 5);
            Date mydate1 = Date.newInstance(Integer.valueOf(System.Today().Year() + 1), 10, 5);
            
            List<OpportunityLineItemSchedule> listOfSchedules = new List<OpportunityLineItemSchedule>();
            
            listOfSchedules.add(new OpportunityLineItemSchedule(OpportunityLineItemId = listOfOli[0].id, ScheduleDate = mydate, Quantity = 1, Revenue = 3, Type = 'both'));
            listOfSchedules.add(new OpportunityLineItemSchedule(OpportunityLineItemId = listOfOli[1].id, ScheduleDate = mydate, Quantity = 1, Revenue = 3, Type = 'both'));
            
            insert listOfSchedules; 
            
        Test.startTest(); 
            List<Sales_Target__c> listOfST = new List<Sales_Target__c>();
            
            Sales_Target__c st  = new Sales_Target__c(Sales_Person__c = u.id, MTG_Achieved__c = 19, MTG_Pipeline__c = 15, Sales_Year__c=String.valueOf(System.Today().Year()));    listOfST.add(st);
            Sales_Target__c st1 = new Sales_Target__c(Sales_Person__c = u.id, MTG_Pipeline__c = 12, MTG_Achieved__c = 13, Sales_Year__c=String.valueOf(System.Today().Year() + 1));   listOfST.add(st1);
            
            insert listOfST; 
            
            List<opportunity> listOfClonedOppy = new List<opportunity>();
            
            listOfClonedOppy.add(new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(), clonedOppy__c = listOfOppy[0].id,
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),
                Margin_Escalator__c = false, MTG_Year__c = String.valueOf(System.Today().Year())));
            listOfClonedOppy.add(new opportunity(Name = 'Test opp2', StageName = 'PO Booked', CloseDate = date.today(), clonedOppy__c = listOfOppy[1].id,
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),
                Margin_Escalator__c = false, MTG_Year__c = String.valueOf(System.Today().Year())));
            insert listOfClonedOppy;
            
            List<OpportunityLineItem> listOfClonedOLi = new List<OpportunityLineItem>();

            listOfClonedOLi.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id, OpportunityId = listOfClonedOppy[0].Id,
                Quantity = 1, UnitPrice = 0, Probability__c=10));
            listOfClonedOLi.add(new OpportunityLineItem(PriceBookEntryId = pbe.Id, OpportunityId = listOfClonedOppy[1].Id,
                Quantity = 1, UnitPrice = 0, Probability__c=10));
            insert listOfClonedOLi;
            
            Class_TriggerCheck.var_updateMETrg=true;
            update listOfClonedOLi;
            
        Test.stopTest();   
        }
    }  */
}