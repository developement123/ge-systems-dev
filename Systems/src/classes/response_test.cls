/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following class:
    - Controller_Selected_Responses    
    
    Last modified By : Mohan Kumar(8 May 2015).
    Last modified By : Abhishek(2nd March 2016) for JIRA 498.
    Last modified By : Abhishek(18th sep 2017) to remove account platform
*************************************************************************************************************************************************/      

@istest
private class response_test {
    public static id platId {get; private set;}
    public static id accId {get; private set;}
    public static Sales_Request__c sr1;
    public static Sales_Request__c sr2;
    public static Sales_Request__c sr3;
    public static List<Sales_Request__c> lstforInsertSR;

    public static void loadTest() { 
    
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        
        Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnContract = false; 
        Class_TriggerCheck.var_TriggerOnResponse = false;
        Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
    
        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor@birlasoft50.com.sys.dev');
        insert u;       
        
        List<Account> listOfAcc = new List<Account>();
        
        Account acc1 = new Account(Name = 'Test Account');
        listOfAcc.add(acc1);
        Account acc2 = new Account(Name = 'Test Account2');
        listOfAcc.add(acc2);
        Account acc3 = new Account(Name = 'Test Account3');
        listOfAcc.add(acc3);
        
        insert listOfAcc;
        
        Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Civil', Segment__c = 'Regional Transports');
        insert plat;       
        platId = plat.id;
        accId = listOfAcc[1].id;
        Contact con = new Contact(Account = acc1, Email = 'neha.mittal@birlasoft.com', LastName = 'Test');         

        List<Product_Hiearchy_Object__c> lstForInsertPH = new List<Product_Hiearchy_Object__c>();
        
        Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child1');
        lstForInsertPH.add(hierarchy1);

        Product_Hiearchy_Object__c hierarchy2 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child2');
        lstForInsertPH.add(hierarchy2);

        Product_Hiearchy_Object__c hierarchy3 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child3');
        lstForInsertPH.add(hierarchy3);

        insert lstForInsertPH;

        List<Program__c> lstForInsertPrg = new List<Program__c>();
        
        Program__c prog1 = new Program__c(Lookup_hiearchyname__c = hierarchy1.id, Product_Sales_Owner__c = u.id,
            Program_Probability__c = '100% - Funded');
        lstForInsertPrg.add(prog1);

        Program__c prog2 = new Program__c(Lookup_hiearchyname__c = hierarchy2.id, Product_Sales_Owner__c = u.id,
             Program_Probability__c = '100% - Funded');
        lstForInsertPrg.add(prog2);

        Program__c prog3 = new Program__c(Lookup_hiearchyname__c = hierarchy3.id, Product_Sales_Owner__c = u.id,
             Program_Probability__c = '100% - Funded');
        lstForInsertPrg.add(prog3);
               
        insert lstForInsertPrg;
  
        // added Account_Platform__c for JIRA 498
        Opportunity opp = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1, CloseDate = date.today(), Program__c = prog1.id, Amount = 1000, Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, deal_type__c = 'Services', Lookup_hiearchyname__c = hierarchy1.id, Platform_New__c = plat.id, accountId= acc2.id, Is_Tokenized__c = false);
        insert opp;

        lstforInsertSR = new List<Sales_Request__c>();

        sr1 = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response', Program__c = prog1.id,  Proposal_Coordinator__c = con.Id, Pipeline_Opportunity__c = opp.Id);
        lstforInsertSR.add(sr1);        

        sr2 = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2', Program__c = prog2.id, Proposal_Coordinator__c = con.Id, Pipeline_Opportunity__c = opp.Id);
        lstforInsertSR.add(sr2); 
        
        sr3 = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2', Program__c = prog3.id, Proposal_Coordinator__c = con.Id, Pipeline_Opportunity__c = opp.Id);
        lstforInsertSR.add(sr3);
        
        for(Integer i=0; i < 20; i++) {
            lstforInsertSR.add(new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2', Program__c = prog3.id, Proposal_Coordinator__c = con.Id, Pipeline_Opportunity__c = opp.Id));
        }
        insert lstforInsertSR;  
        
        List<Major_Proposal__c> listOfMP = new List<Major_Proposal__c>();

        Major_Proposal__c mp = new Major_Proposal__c(Name = 'Test Proposal', Account__c = acc2.id, Platform__c=plat.id);
        listOfMP.add(mp);        

        Major_Proposal__c mp2 = new Major_Proposal__c(Name = 'Test Proposal', Account__c = acc3.id, Platform__c=plat.id);
        listOfMP.add(mp2);
                
        insert listOfMP;  
        
        sr3.Major_Proposals__c = mp2.id;       

      //  sr3.Estimated_Manufacturing_Cost__c = 300;
      //  sr3.Avg_Close_Probability2__c = 30;
      //  sr3.Total_Amount2__c = 200;
     //   sr3.CM2__c = 250;
     //   sr3.Estimated_Engineering_Cost__c = 300;
     //   sr3.Service_Cost__c = 200;
        update sr3;

        update mp2;
        
        sr1.Major_Proposals__c = mp.id;
        sr1.Response_Description__c = 'Changed desc';
     //   sr1.Estimated_Engineering_Cost__c = 500;
     //   sr1.Estimated_Manufacturing_Cost__c = 900;
     //   sr1.Service_Cost__c = 1000;
        sr1.Sales_Request_Date__c = System.Today();
        
        update sr1;
        sr2.Major_Proposals__c = mp.id;

        update sr2;
        
        Class_TriggerCheck.IsPreventing = True;
        CREATE_UPDATE_HEADER_CA_RESPONSE.firstRun = True;
        
        update mp;
        
    }
    
    static testmethod void mytest1() {   
    
  
   
                 
        loadTest();   
       
       Test.startTest();   
            
            Pagereference pageref1 = System.currentPageReference();
            
            pageref1.getParameters().put('platformid', platId);
            pageref1.getParameters().put('accid', accId);
            Test.setCurrentPageReference(pageref1);            

            Controller_Selected_Responses controller = new Controller_Selected_Responses();
           
            controller.wrapRespDataList = new List<Controller_Selected_Responses.cObject>();
           
                      
            
            controller.SearchResponses();
            for(Sales_Request__c sr : lstforInsertSR) {
                Controller_Selected_Responses.cObject testCoject = new Controller_Selected_Responses.cObject(sr);
                 testCoject.selected = true;
                controller.wrapRespDataList.add(testCoject);
               
            }  
            
            controller.processSelected();
            
            controller.GetFirst();
            controller.GetLast();
            controller.GetPrevious();

            controller.searchString = '1';
            controller.SearchResponses();  
            controller.DoCancel();
            
            Pagereference pageRef = controller.processSelected();

            Controller_Selected_Responses.cObject testCoject5 = new Controller_Selected_Responses.cObject(sr1);
            testCoject5.selected = true;
           
            Pagereference pageRef2 = controller.DoCancel();   
            
            try {
                Controller_Selected_Responses.cObject testCoject7 = new Controller_Selected_Responses.cObject(sr3);
                testCoject7.selected = false;
                controller.wrapRespDataList.add(testCoject7);
                controller.GetNext();
            }
            catch(exception ex) {
                System.Assert(ex.getMessage().contains('List index out of bounds:'), '');
            } 
          
        Test.stopTest();
 
      
    }    
}