/****************************************************************************
    Developer : Abhishek
    Date      : 29/June/2016    
    Action    : This handler class invoked by "TriggerOnEPA"

    Change desription:
    Developer         Date                  Description
*****************************************************************************/
public with sharing class EPATriggerHandler{
    
     /***********************************************************************************************************
        Method 1 :  To prevent Product Hierarchy deletion, if it is being used in Opportunity
    ************************************************************************************************************/
    public void ValidateEPATOnDelete(Map<id,EPA__c> oldMap, String eventType, String triggerType){
        if(Label.Label_ValidateEPATOnDelete== 'true'){   
            map<id,list<Cost_Authorization__c>> mapOfEPA = new map<id,list<Cost_Authorization__c>>();
            
            for(Cost_Authorization__c costAuth : [select id,EPA_Number__c from Cost_Authorization__c where EPA_Number__c=:oldMap.keyset()]){
                        if(mapOfEPA.containsKey(costAuth.EPA_Number__c))
                                mapOfEPA.get(costAuth.EPA_Number__c).add(costAuth);
                        else
                                mapOfEPA.put(costAuth.EPA_Number__c,new list<Cost_Authorization__c>{costAuth});
            }
            
           
            
            if(eventType == 'Before' && triggerType == 'Delete') {
                     for(EPA__c EPA : oldMap.values()){
                       if( (mapOfEPA.containskey(EPA.id))  ){
                          EPA.adderror(Label.ValidateEPATOnDelete);
                       }
                     }
            }
        }
    }

}