/*
  Author: Mayank Joshi
  Date: 3/14/2013 9:20 AM
  Action: Class is sending Outbound Message to Web Methods on Creation/Updation of
          Approved CA Fields Via Triggers Trg_sendCA .
  Logic Last Modified By: Mayank Joshi,  3/22/2013 7:25 AM .
  
  Last modfied by Abhishek on 12/02/2016 to use Cronos field of EPA to decide end point (US/UK) and also query change to get cronos field from EPA.
*/
Public with sharing Class Cobra_Create_Update_ApprovedCA
    {
      
       Public Static String SOURCE_SYSTEM,TARGET_SYSTEM,MESSAGE_TYPE,SFSOAPHeader,SFSOAP_BODY_Start,SFSOAP_BODY_End,TIME_STAMP,SFSOAPHeader_UK,SFSOAP_BODY_Start_UK,SFSOAP_BODY_End_UK;
       Public static boolean firstCheck = true; 
       Static
       {
        SOURCE_SYSTEM = 'AVISYSSFDC';
        TARGET_SYSTEM = 'AviationCobra';
        MESSAGE_TYPE =  'Cost Authorization';
        DateTime TIME_STAMP_Date = System.now();
        TIME_STAMP = TIME_STAMP_Date.format('MM/dd/yyyy HH:mm:ss');
        // Below Added Custom Label as Values keep on changing per environment .
        SFSOAPHeader = Label.SOAP_HEADER;
        SFSOAP_BODY_Start = Label.SOAP_BODY_Start;
        SFSOAP_BODY_End = Label.SOAP_BODY_End;
        
        //Once we have UK location's WSDL and endpoints then first we need to setup values over those custom labels.
        SFSOAPHeader_UK = Label.SOAP_HEADER_UK_Location; 
        SFSOAP_BODY_Start_UK = Label.SOAP_BODY_Start_UK_Location;
        SFSOAP_BODY_End_UK = Label.SOAP_BODY_End_UK_Location;
       }
      
     //** To Send Created Cost Authorization Fields with status as Approved  Only - Using for US/UK location**
     //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 
     @Future(callout=true)
     public static void Create_ApprovedCA(List<String> GetCreatedApproved_CA_id, String Cobra_Operation, String WP_Create )
     {
       For(Integer i = 0; i < GetCreatedApproved_CA_id.size() ; i++)
       {
       String SR_id = GetCreatedApproved_CA_id[i];
       
       String Concat_EPA_Number = '';
       String EPA_Number = '' ;
       String Description = '' ;
       String Approved_Budget_Backend = '' ;
       String Cost_Authorization_Opened_Date = '' ;
       String Cost_Authorization_Finish_Date = '';
       string KronosInstance = '';
       //String WP_Create = '';

          
   //List<Sales_Request__c> Lst_Response = New List<Sales_Request__c > ();
   List<Cost_Authorization__c> Lst_CA = New List<Cost_Authorization__c> ();
   
   For(Cost_Authorization__c Approved_CA:[
                                         Select ID,Concat_EPA_Number__c,EPA_Number__r.Name, Description__c, Approved_Budget_Backend__c,Cost_Authorization_Opened__c,EPA_Number__r.Kronos_Instance__c from Cost_Authorization__c where ID =:GetCreatedApproved_CA_id[i]
                                         ]
      )
   {
        Lst_CA.add(Approved_CA); 
   }
   
   
   Cost_Authorization__c CAList = Lst_CA[0];
   Concat_EPA_Number = CAList.Concat_EPA_Number__c;
   EPA_Number = CAList.EPA_Number__r.Name;
   Description = CAList.Description__c;
   //Approved_Budget_Backend = CAList.Approved_Budget_Backend__c.toPlainString();
   
   If(CAList.Approved_Budget_Backend__c == 0 || CAList.Approved_Budget_Backend__c == NULL ){
   System.debug('Check for Approved Budget Value'+ CAList.Approved_Budget_Backend__c);
   Approved_Budget_Backend = '0' ; //Modifield to accept '0' instead of 'Null' OR Blank value
   }
   Else{
    Approved_Budget_Backend = CAList.Approved_Budget_Backend__c.toPlainString();
   }
   
   Datetime Cost_Authorization_Opened_Date_CONVERSION = CAList.Cost_Authorization_Opened__c;
   Cost_Authorization_Opened_Date = Cost_Authorization_Opened_Date_CONVERSION.format('MM/dd/yyyy hh:mm:ss a');
   //Cost_Authorization_Opened_Date = Cost_Authorization_Opened_Date_CONVERSION.format();
   
   //Cost_Authorization_Finish_Date = CAList.Cost_Authorization_Finish_Date__c;
   //WP_Create = CAList.wp_create__c;
  
   // Using new field "Kronos Instance" of EPA, to decide end point - 12/2/2016
     if(CAList.EPA_Number__r.Kronos_Instance__c != null)
        KronosInstance = CAList.EPA_Number__r.Kronos_Instance__c;
     else
        KronosInstance = 'US';
   
   if(KronosInstance == 'US'){     
           String envelope = SFSOAPHeader +   '<soapenv:Body>'+ 
                    //'<ge:SalesRequestPayload>'+
                      SFSOAP_BODY_Start +
                    '<SERVICE_ENVELOPE>'+
                    
                    '<OPERATION>'+Cobra_Operation+'</OPERATION>'+
                    
                    '<SOURCE_SYSTEM>'+SOURCE_SYSTEM+'</SOURCE_SYSTEM>'+
                    
                    '<TARGET_SYSTEM>'+TARGET_SYSTEM+'</TARGET_SYSTEM>'+
                    
                    '<VERSION></VERSION>'+
                    
                    '<MESSAGE_TYPE>'+MESSAGE_TYPE+'</MESSAGE_TYPE>'+
                    
                    '<INTERFACE_NAME></INTERFACE_NAME>'+
                   
                    '<TIME_STAMP>'+TIME_STAMP+'</TIME_STAMP>'+
                    
                    '<UNIQUE_TXN_ID>123</UNIQUE_TXN_ID>'+
                    
                    '<SOURCE_BUSINESS></SOURCE_BUSINESS>'+
                    
                    '<TARGET_BUSINESS></TARGET_BUSINESS>'+
                    
                    '<TARGET_SYSTEM_IDENTIFIER></TARGET_SYSTEM_IDENTIFIER>'+
                 '</SERVICE_ENVELOPE>'+
                         
                 '<PROCESS_COBRA_DETAILS>'+
                    '<PROCESS_COBRA>'+
                          '<Concat_EPA_Number>'+Concat_EPA_Number+'</Concat_EPA_Number>'+
                          '<EPA_Number>'+EPA_Number+'</EPA_Number>'+
                          '<Description>'+Description+'</Description>'+
                          '<Approved_Budget_Backend>'+Approved_Budget_Backend+'</Approved_Budget_Backend>'+
                          '<Cost_Authorization_Opened_Date>'+Cost_Authorization_Opened_Date+'</Cost_Authorization_Opened_Date>'+
                          //'<Cost_Authorization_Finish_Date>'+Cost_Authorization_Opened_Date+'</Cost_Authorization_Finish_Date>'+
                          '<WP_Create>'+WP_Create+'</WP_Create>'+
                    '</PROCESS_COBRA>'+
                 '</PROCESS_COBRA_DETAILS>'+
                SFSOAP_BODY_End+ 
           '</soapenv:Body>'+
        '</soapenv:Envelope>' ;
                                                
            System.Debug('envelope created' + envelope);
            
            //Sending data to Main_Header_Info
            String envelope_Conversion = envelope;    
            //System.debug ('Converted SOAP value is' + envelope_Conversion); 
            
            // Below logic is to convert SOAP Message, if contains '&' to '&amp;' 
            String Actual_SOAP_envelope =  envelope_Conversion.replace('&', '&amp;'); 
            //System.debug ('Actual SOAP Envelope value is' + Actual_SOAP_envelope);            
                             
                             
            HTTPResponse response =  Main_Header_Info(Actual_SOAP_envelope);  
                             
            //HTTPResponse response =  Main_Header_Info(envelope);  
            System.debug('CHECK FOR RESPONSE '+response.getStatusCode());
        }
        else if(KronosInstance == 'UK'){// Run in the case if location on Response will be "Windchill UK"
        /*    //Implemented by Kashish on Dated:24-Oct-2013
            //for a Requirement of Disconnected "US/UK Location Integration"
            //Commented for now will be run once we have information for UK location.
             String envelope = SFSOAPHeader_UK +   '<soapenv:Body>'+ 
            SFSOAP_BODY_Start_UK +
            '<SERVICE_ENVELOPE>'+
            
            '<OPERATION>'+Cobra_Operation+'</OPERATION>'+
            
            '<SOURCE_SYSTEM>'+SOURCE_SYSTEM+'</SOURCE_SYSTEM>'+
            
            '<TARGET_SYSTEM>'+TARGET_SYSTEM+'</TARGET_SYSTEM>'+
            
            '<VERSION></VERSION>'+
            
            '<MESSAGE_TYPE>'+MESSAGE_TYPE+'</MESSAGE_TYPE>'+
            
            '<INTERFACE_NAME></INTERFACE_NAME>'+
           
            '<TIME_STAMP>'+TIME_STAMP+'</TIME_STAMP>'+
            
            '<UNIQUE_TXN_ID>123</UNIQUE_TXN_ID>'+
            
            '<SOURCE_BUSINESS></SOURCE_BUSINESS>'+
            
            '<TARGET_BUSINESS></TARGET_BUSINESS>'+
            
            '<TARGET_SYSTEM_IDENTIFIER></TARGET_SYSTEM_IDENTIFIER>'+
         '</SERVICE_ENVELOPE>'+
                 
             '<PROCESS_COBRA_DETAILS>'+
                '<PROCESS_COBRA>'+
                      '<Concat_EPA_Number>'+Concat_EPA_Number+'</Concat_EPA_Number>'+
                      '<EPA_Number>'+EPA_Number+'</EPA_Number>'+
                      '<Description>'+Description+'</Description>'+
                      '<Approved_Budget_Backend>'+Approved_Budget_Backend+'</Approved_Budget_Backend>'+
                      '<Cost_Authorization_Opened_Date>'+Cost_Authorization_Opened_Date+'</Cost_Authorization_Opened_Date>'+
                      //'<Cost_Authorization_Finish_Date>'+Cost_Authorization_Opened_Date+'</Cost_Authorization_Finish_Date>'+
                      '<WP_Create>'+WP_Create+'</WP_Create>'+
                '</PROCESS_COBRA>'+
             '</PROCESS_COBRA_DETAILS>'+
              SFSOAP_BODY_End_UK+ 
           '</soapenv:Body>'+
        '</soapenv:Envelope>' ;
                                        
            System.Debug('envelope created' + envelope);
            String envelope_Conversion = envelope;    
            String Actual_SOAP_envelope =  envelope_Conversion.replace('&', '&amp;'); 
            
            HTTPResponse response =  Main_Header_Info_UKLocation(Actual_SOAP_envelope);  
                     
            System.debug('CHECK FOR RESPONSE '+response.getStatusCode());
        */
        }   
 }   
 } //** END OF Create_ApprovedCA - For US/UK location**

// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


// ** To Send Closed CA Fields Only - Using for US/UK Location**
 @Future(callout=true) 
 public static void Closed_CA(List<String> CA_Closed_Id ,String Cobra_Closed_Operation)
 {
   //-- CA Attribute to pass into SOAP --
    For(Integer i = 0; i < CA_Closed_Id.size() ; i++)
       {
       string KronosInstance = '';
       String SR_id = CA_Closed_Id[i];
       
       String Concat_EPA_Number = '';
       Decimal Percent_Complete  = 0;

          
   //List<Sales_Request__c> Lst_Response = New List<Sales_Request__c > ();
   List<Cost_Authorization__c> Lst_CA = New List<Cost_Authorization__c> ();
   
   For(Cost_Authorization__c Approved_CA:[
                                         Select ID,Concat_EPA_Number__c,Percent_Complete__c,EPA_Number__r.Kronos_Instance__c from Cost_Authorization__c where ID =:CA_Closed_Id[i]
                                         ]
      )
   {
        Lst_CA.add(Approved_CA); 
   }
   
   
   Cost_Authorization__c CA_Closed_List = Lst_CA[0];
   Concat_EPA_Number = CA_Closed_List.Concat_EPA_Number__c;
   Percent_Complete = CA_Closed_List.Percent_Complete__c;
   
   // Using new field "Kronos Instance" of EPA, to decide end point - 12/2/2016
     if(CA_Closed_List.EPA_Number__r.Kronos_Instance__c != null)
        KronosInstance = CA_Closed_List.EPA_Number__r.Kronos_Instance__c;
     else
        KronosInstance = 'US';
   
      if(KronosInstance == 'US'){
          String envelope = SFSOAPHeader +   '<soapenv:Body>'+ 
            //'<ge:SalesRequestPayload>'+
                      SFSOAP_BODY_Start+
                    '<SERVICE_ENVELOPE>'+
                    
                    '<OPERATION>'+Cobra_Closed_Operation+'</OPERATION>'+
                                
                    '<SOURCE_SYSTEM>'+SOURCE_SYSTEM+'</SOURCE_SYSTEM>'+
                    
                    '<TARGET_SYSTEM>'+TARGET_SYSTEM+'</TARGET_SYSTEM>'+
                    
                    '<VERSION></VERSION>'+
                    
                    '<MESSAGE_TYPE>'+MESSAGE_TYPE+'</MESSAGE_TYPE>'+
                    
                    '<INTERFACE_NAME></INTERFACE_NAME>'+
                   
                    '<TIME_STAMP>'+TIME_STAMP+'</TIME_STAMP>'+
                    
                    '<UNIQUE_TXN_ID></UNIQUE_TXN_ID>'+
                    
                    '<SOURCE_BUSINESS></SOURCE_BUSINESS>'+
                    
                    '<TARGET_BUSINESS></TARGET_BUSINESS>'+
                    
                    '<TARGET_SYSTEM_IDENTIFIER></TARGET_SYSTEM_IDENTIFIER>'+
                 '</SERVICE_ENVELOPE>'+
                 '<CLOSED_COBRA_DETAILS>'+
                    '<CLOSED_COBRA>'+
                          '<Concat_EPA_Number>'+Concat_EPA_Number+'</Concat_EPA_Number>'+
                          '<Percent_Complete>'+Percent_Complete+'</Percent_Complete>'+
                    '</CLOSED_COBRA>'+
                 '</CLOSED_COBRA_DETAILS>'+
              
              SFSOAP_BODY_End+ 
           '</soapenv:Body>'+
        '</soapenv:Envelope>' ;
                                                
            System.Debug('envelope created' + envelope);
            
            //Sending data to Main_Header_Info
            String envelope_Conversion = envelope;    
            //System.debug ('Converted SOAP value is' + envelope_Conversion); 
            
            // Below logic is to convert SOAP Message, if contains '&' to '&amp;' 
            String Actual_SOAP_envelope =  envelope_Conversion.replace('&', '&amp;'); 
            //System.debug ('Actual SOAP Envelope value is' + Actual_SOAP_envelope);            
                             
                             
            HTTPResponse response =  Main_Header_Info(Actual_SOAP_envelope);  
                             
            //HTTPResponse response =  Main_Header_Info(envelope);  
            System.debug('CHECK FOR RESPONSE '+response.getStatusCode());
        }
        else if(KronosInstance == 'UK'){// Run in the case if location on Response will be "Windchill UK"
        /*    //Implemented by Kashish on Dated: 24-Oct-13
            //for a Requirement of Disconnected "US/UK Location Integration"
            // Commented for now will be run once we have information for UK location.
            
            String envelope = SFSOAPHeader_UK +   '<soapenv:Body>'+ 
            
                      SFSOAP_BODY_Start_UK+
                    '<SERVICE_ENVELOPE>'+
                    
                    '<OPERATION>'+Cobra_Closed_Operation+'</OPERATION>'+
                                
                    '<SOURCE_SYSTEM>'+SOURCE_SYSTEM+'</SOURCE_SYSTEM>'+
                    
                    '<TARGET_SYSTEM>'+TARGET_SYSTEM+'</TARGET_SYSTEM>'+
                    
                    '<VERSION></VERSION>'+
                    
                    '<MESSAGE_TYPE>'+MESSAGE_TYPE+'</MESSAGE_TYPE>'+
                    
                    '<INTERFACE_NAME></INTERFACE_NAME>'+
                   
                    '<TIME_STAMP>'+TIME_STAMP+'</TIME_STAMP>'+
                    
                    '<UNIQUE_TXN_ID></UNIQUE_TXN_ID>'+
                    
                    '<SOURCE_BUSINESS></SOURCE_BUSINESS>'+
                    
                    '<TARGET_BUSINESS></TARGET_BUSINESS>'+
                    
                    '<TARGET_SYSTEM_IDENTIFIER></TARGET_SYSTEM_IDENTIFIER>'+
                 '</SERVICE_ENVELOPE>'+
                 
                 '<CLOSED_COBRA_DETAILS>'+
                    '<CLOSED_COBRA>'+
                          '<Concat_EPA_Number>'+Concat_EPA_Number+'</Concat_EPA_Number>'+
                          '<Percent_Complete>'+Percent_Complete+'</Percent_Complete>'+
                    '</CLOSED_COBRA>'+
                 '</CLOSED_COBRA_DETAILS>'+
              SFSOAP_BODY_End_UK+ 
           '</soapenv:Body>'+
        '</soapenv:Envelope>' ;
                                                
            System.Debug('envelope created' + envelope);
            String envelope_Conversion = envelope;    
            
            String Actual_SOAP_envelope =  envelope_Conversion.replace('&', '&amp;'); 
            
            HTTPResponse response =  Main_Header_Info_UKLocation(Actual_SOAP_envelope);  
            System.debug('CHECK FOR RESPONSE '+response.getStatusCode()); 
        */    
        }    
 
  }
 } // ** END OF Closed_CA -For US/UK Location**

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 


//*********** BELOW IS MAIN HEADER CLASS -For US Location***********
 Public static HTTPResponse Main_Header_Info (String Actual_SOAP_envelope)
 {
    //Creating request object
    HttpRequest request = new HttpRequest();        
    
    // Below Added Custom Label as Values keep on changing per environment . 
    String username = Label.USERNAME;
    String password = Label.PASSWORD;
    String EndPoint = Label.Wm_Endpoint;    
        
    Blob headerValue = Blob.valueOf(username + ':' + password);
    String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
    request.setHeader('Authorization', authorizationHeader);
            
    request.setMethod('POST');
    request.setTimeout(120000);
            
    Http http = new Http();
            
    request.setEndpoint(Label.Wm_Endpoint);
            
    //Setting content-type and SOAPAction
    request.setHeader('content-type', 'text/xml; charset=utf-8');  
    //request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_Provider_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse'); 
    
    request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse');
            
    //Setting the body
    request.setBody(Actual_SOAP_envelope);
            
    //Sending request
    HTTPResponse response = http.send(request);
    System.debug('response---'+response);
    return response;

 } 
 
 //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 

//*********** BELOW IS MAIN HEADER CLASS - For UK Location***********
//Implemented by Kashish on Dated: 24-Oct-13
//for a Requirement of Disconnected "US/UK Location Integration"
 Public static HTTPResponse Main_Header_Info_UKLocation (String Actual_SOAP_envelope)
 {
    //Creating request object
    HttpRequest request = new HttpRequest();        
    
    // Below Added Custom Label as Values keep on changing per environment . 
    String username = Label.USERNAME_UK_Location;
    String password = Label.PASSWORD_UK_Location;
    String EndPoint = Label.WM_EndPoint_UK_Location;    
        
    Blob headerValue = Blob.valueOf(username + ':' + password);
    String authorizationHeader = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
    request.setHeader('Authorization', authorizationHeader);
            
    request.setMethod('POST');
    request.setTimeout(120000);
            
    Http http = new Http();
            
    request.setEndpoint(Label.WM_EndPoint_UK_Location);
    
    //Setting content-type and SOAPAction
    request.setHeader('content-type', 'text/xml; charset=utf-8');  
    //request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_Provider_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse'); 
    
    request.setHeader('SOAPAction', 'GE_AVISYS_SFDC_AviationSalesResponse_Source_WebServices_publishAviationSalesResponse_WSD_Binder_publishAviationSalesResponse');
            
    //Setting the body
    request.setBody(Actual_SOAP_envelope);
    System.debug('-----&&&&&'+request.getBody());           
            
    //Sending request
    HTTPResponse response = http.send(request);
    System.debug('response---'+response);
    return response;

 }
 
}