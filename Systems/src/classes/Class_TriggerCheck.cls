/***************************************************************************************************************************************************
    Author: Jeetesh Bahuguna
    Date: 2/26/2013 2:17 AM
    Action: This is a class which is having static variable only.It is used to prevant the recursion  and 
    set context variable values. 
    Logic Last Modified By: Jeetesh Bahuguna,  4/5/2013 7:22 AM.
  
    last modified by : Mohan Kumar(3 march 2015).
****************************************************************************************************************************************************/

public with sharing class Class_TriggerCheck {
    public static Boolean Var_TokenizeProg = true;
    public static Boolean Var_StopPlatMasterTrigger = true; // to stop platform master trigger execution on User Update
    
    public static Boolean Var_StopAccPlatUpdate = true; // to stop account platform to get updated when platform master trigger updates civil/Military on acc plat.
    
/*  Below Comment added by Suman : Dec. 15
    This static variable 'Var_StopProdVarMultipleExecution' is not required further 
    as inactivate the related trigger for Opp Optimization Requirement*/

    public static Boolean Var_StopProdVarMultipleExecution = true;// to stop recursion in product variation trigger while running test class.
 //End
    
    public static Boolean Var_StopUpdateOppTrgOnUserUpdate = true;// to stop exection of trigger auto update opportunity on program on USER update
    
/*  Below Comment added by Suman : Dec. 15
    This static variable 'Var_StopUpdateRelationship' is not required further 
    as inactivate the related trigger for Opp Optimization Requirement*/
    
    public static Boolean Var_StopUpdateRelationship = true; // to stop execution of Relationship update (trg_toupdate_relatedresponses).
//End

    public static boolean Var_StopOpportunityTrigger = true; // to stop recursion of trigger trgSetOLIAsIsTokenizeforOpportunity 
    
    public static Boolean Isclosed_CA = True;
    public static Boolean IsUpdated_CA = True;
    public static Boolean IsWindchill_UpdatedCA = True;
    public static Boolean IsWindchill_CreatedCA = True;
    public static Boolean IsPreventing = True;
    public static Boolean Var_ShouldEPA_update=true; 
    public static Boolean Var_ShouldCA_update=true; 
    public static Boolean Var_IsProgramUPdated=false;
    public static Boolean Var_Epa_OwnerChanged=false;
    
/*  Below Comment added by Suman : Dec. 15
    This static variable 'Var_isProgam_Probability_change' is not required further 
    as inactivate the related trigger for Opp Optimization Requirement*/  
    
    public static Boolean Var_isProgam_Probability_change=false; 
    
//End
    
    public static Boolean Var_SendBatch_ClosedCA = true;
    public static Integer Var_IsEmailsend=1;
    public static Boolean var_IsStopEPABatchTrg = false; //restrict to run the batch on EPA when CA will be updated
    public static Integer Var_IsEmailSendResponse = 1; //restrict the code to send the mails only once.---trigger - SendMailToRespTeam 
    
    public static boolean var_IsExecuteCAOutboundCallTrg = true;
    public static boolean var_IsExecuteResOutboundCallTrg = true;
    
/*  Below Comment added by Suman : Oct. 15
    This static variable 'var_execute_createMEmanuallyTrg' was used in old trigger on
    ME object and has been replaced by 'var_TriggerOnMarginEscalator' in new Trigger on ME. */
    
    public static Boolean var_execute_createMEmanuallyTrg = true;        // for JIRA 339
//End  
   
    public static Boolean var_setMTGcheckboxvalueinMETrg = true;         // for JIRA 339
    public static Boolean var_updateMETrg = true;                        // for JIRA 339
    
    public static Boolean var_preventUpdate_UpdateSalesTargetTrg = true; // for JIRA 340
    public static Boolean var_update_ST_onSplitupdateTrg = true;         // for JIRA 340
    public static Boolean var_updateSTonOppyOwnerUpdateTrg = true;       // for JIRA 340
    
    public static List<id> listofSalesTargetinClass = new List<Id>();    // for JIRA 340 
    public static List<id> listofSTIdinBeforeTrg = new List<Id>();       // for JIRA 340
    
/*  Below Comment added by Suman : Oct. 15
    This static variable 'var_clonedOppyId' is not required further as we have now Custom Clone 
    functionality created as a part of Opportunity Optimization Requirement.      */
   
    public static String var_clonedOppyId;                               // for JIRA 340
//End  

/*  Below Comment added by Suman : Dec. 15
    This static variable 'var_SendMailToRespTeamTrg ' is not required further 
    as inactivate the related trigger for Opp Optimization Requirement*/
    
    public static Boolean var_SendMailToRespTeamTrg = true;                 // for JIRA 296
//End

    public static Boolean var_UpdateMajorresponsefromResponseTrg = true;    // for JIRA 296
    public static Boolean var_Trg_Update_EPA_owner_To_ResponseTrg = true;   // for JIRA 296
    
/*  Below Comments added by Suman : Oct. 15
    This variable 'var_createMEmanually_AfterTrg' was used in old trigger on 
    ME object and has been replaced by 'var_TriggerOnMarginEscalator_AfterEvents'
    in new Trigger on ME.     */
    
    public static Boolean var_createMEmanually_AfterTrg = true;             // for JIRA 369
//End  
  
/*  Below Comment added by Suman : Dec. 15
    This static variable 'var_Trg_updateRelationshipTrg' is not required further 
    as inactivate the related trigger for Opp Optimization Requirement*/
    
    public static Boolean var_Trg_updateRelationshipTrg = true;             // for JIRA 296
//End
    public static Boolean var_trgRollupSummaryTriggerTrg = true;            // for JIRA 385
    
    public static Boolean var_AutoNameGenrationTrg = true;                  // for JIRA 384
    
    public static Boolean var_TriggerOnIdeationTrg = true;                  // for Ideation requirement(JIRA 426). 
    public static Boolean var_TriggerOnOpportunityTrg = true;               // for Ideation requirement(JIRA 426) and other new functionalities 
    public static Boolean var_TriggerOnAccount = true;  // for (Jira 474)
    
    public static Map<Id, String> Var_OppId_Name = new Map<Id, String>();
    public static Map<Id, String> Var_Pro_Name = new Map<Id, String>();
    public static Map<Id, Double> map_programToupdate = new map<Id, Double>();
    
//  Oct-15: New static variables created on new Triggers as a part of Opportunity Optimization Requirement.
    
    public static Boolean var_TriggerOnSchedule = true;
    public static Boolean var_TriggerOnAssociatedProduct = true;
    public static Boolean var_TriggerOnMarginEscalator = true; 
    public static Boolean var_TriggerOnMarginEscalator_AfterEvents = true;
    public static Boolean var_validateGrowthSplitsRecords = true;    // this variable is for opportunity split trigger : validateGrowthSplitsRecords
    public static Boolean var_TriggerOnContract = true;
    
    public static Boolean var_prevenUpdate_In_AfterInsertDSol = true; //this variable is used for generating/updating schedules
    public static Boolean var_TriggerOnOrder = true;
    
    public static Boolean var_preventOppyNameUpdate_Recursion = true;
    
    public static Boolean var_TriggerOnResponse = true;
    public static Boolean var_triggerOnUser = true;
    public static Boolean var_preventDuplicateCodeExecutionOnScheduleDelete = true; // we are using this in Opportunity handler and Product handler
    public static Boolean Var_StopProductHiearchy = true;
    public static Boolean Var_StopTriggerOnEPA = true;
    public static Boolean var_StopRecursiveOnHoldCalculation = true;
//End
}