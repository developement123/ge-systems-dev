/*****************************************************************
This test class was created to provide code coverage for the following trigger/Class:
- TriggerOnProductHiearchy/ProductHierarchyTriggerHandler

Version     Author(s)               Date              Details 
--------------------------------------------------------------------------------------
1.0         Abhishek          07/14/2016        Initial Creation
            Abhishek          09/18/2017        removed account platform
*******************************************************************/   

@isTest
private class Tracker_TriggerOnProductHiearchy{

    static testMethod void  testValidateProductHierarchyOnDelete(){
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
         System.runAs(u) {
            Account acc1 = new Account(Name = 'Hello');
            insert acc1;
            
            Account childacc = new Account(Name = 'childacc', Parentid = acc1.id);
            insert childacc;
         
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;
            
            //inserting hierarchy.          
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Digital Solutions', Product_Group__c = 'Avionics', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            List<opportunity> InsertOpp = new list<opportunity>();
               
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                , CloseDate = date.today(), Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc1.id
                                , Offering_Type__c = 'Digital Solutions'
                                , Contract_Term__c = 5);
            InsertOpp.add(opp);
            opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1
                                , CloseDate = date.today(), Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc1.id
                                , Offering_Type__c = 'Digital Solutions'
                                , Contract_Term__c = 5);
            InsertOpp.add(opp1);
            Insert InsertOpp;
            
            try{
                delete hierarchy;
           } catch(exception e) {
              boolean isException = e.getMessage().contains('product hierarchy') ? true : false ;
                    system.assertEquals(isException , true);
           }
            
         }

    }

}