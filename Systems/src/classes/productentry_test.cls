@istest(SeeAllData=true)
public class productentry_test
{   
    /*  static testmethod void mytest()
    {   
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u;
    
      Account acc = new Account(Name='Test Account');
      insert acc;
      
      Account acc2 = new Account(Name='Account2');
      insert acc2;
      
      Account acc3 = new Account(Name='Account3');
      insert acc3;
            
      Platform_Master__c pm=new Platform_Master__c(Name='170',Civil_Military__c='Military',
      Segment__c='Regional Transports');
      insert pm;
    
      Forecast__c fore=new Forecast__c(Forecast_Platform__c=pm.id,Airframer__c=acc.id);
      insert fore;
      
      Forecast__c fore2=new Forecast__c(Forecast_Platform__c=pm.id,Airframer__c=acc2.id);
      insert fore2;
     
      Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
      insert hierarchy;
      
      Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name='Test hierarchy1',Product_Group__c='Test group1',Product_Area__c='Test area1',Product_Family__c='Test family1',Product_Child__c='Test child1');
      insert hierarchy1;
      
      Product_Hiearchy_Object__c hierarchy2 = new Product_Hiearchy_Object__c(Name='Test hierarchy1',Product_Group__c='Test group1',Product_Area__c='Across Multiple Products',Product_Family__c='Across Multiple Products',Product_Child__c='Across Multiple Products');
      insert hierarchy2;
      
      Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,Forecast__c=fore.id,Program_Probability__c='50% - Potential funding');
      insert prog;   
      
      Program__c prog2=new Program__c(Lookup_hiearchyname__c=hierarchy1.id,Product_Sales_Owner__c=u.id,Forecast__c=fore.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
       
      insert prog2;
          
      Opportunity opp=new opportunity(Name='Test opp',StageName='Prospecting',
      CloseDate=date.today(),Program__c=prog.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp;
      
      Opportunity opp2=new opportunity(Name='OpportunityTest2',StageName='Prospecting',
      CloseDate=date.today(),Program__c=prog2.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp2;
     
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog2;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore.Status__c ='Active';
      update fore;
      System.Debug('Hello 123');
      System.Debug('Opportunity Name '+ opp2.Name);
      System.Debug('Program Name '+ prog2.Name);
      
      Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product');
      insert p2;
      
      Product2 p3 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product3');
      insert p3;
      
      Product2 p4 = new Product2(Lookup_hiearchyname__c=hierarchy2.id,Name='Test Product3');
      insert p4;
     
      list<Pricebook2> list_pricebook= new list<Pricebook2>();
      
      Pricebook2 pb = new Pricebook2(Name = 'Test Price Book 2009', Description = 'This is the test Price Book ',  IsActive = true);
      list_pricebook.add(pb);
    
      Pricebook2 pb2 = new Pricebook2(Name = 'Test Price Book 2013', Description = 'This is the testPrice Book', IsActive = true);
      list_pricebook.add(pb2);
      
      insert list_pricebook;
      
      id stn_price_id;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id=pb2Standard.id;
        system.debug('stn_price_id>>>>'+stn_price_id);
      }
    

      list<PricebookEntry> list_priceEntry= new list<PricebookEntry>();
    
      PricebookEntry pbe_s1 = new PricebookEntry(Pricebook2Id=stn_price_id,UseStandardPrice=false, 
      Product2Id=p2.Id, UnitPrice=99, isActive=true);
      insert pbe_s1;
      
      PricebookEntry pbe_s2 = new PricebookEntry(Pricebook2Id=stn_price_id,UseStandardPrice=false, 
      Product2Id=p3.Id, UnitPrice=99, isActive=true);
      insert pbe_s2;
       
      PricebookEntry pbe = new PricebookEntry(Pricebook2Id=pb.Id,UseStandardPrice=false, 
      Product2Id=p2.Id, UnitPrice=99, isActive=true);
      insert pbe;
      
      PricebookEntry pbe12 = new PricebookEntry(Pricebook2Id=pb.Id,UseStandardPrice=false, 
      Product2Id=p3.Id, UnitPrice=99, isActive=true);
      insert pbe12;
   
  

      OpportunityLineItem ol = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=opp.Id,
      Quantity=1, TotalPrice=99);
      insert ol;
      
      OpportunityLineItem ol12 = new OpportunityLineItem(PriceBookEntryId=pbe12.Id, OpportunityId=opp.Id,
      Quantity=1, TotalPrice=99);
      insert ol12;
      
      System.Debug('Platfrom For Line Item : '+ ol.Platform__c);
     
      OpportunityLineItem oli = [select Id,Product_Group1__c,Product_Area1__c,Product_Family1__c,Product_Child1__c,PricebookEntryId, PricebookEntry.Pricebook2Id, PricebookEntry.Name, PriceBookEntry.Product2Id, OpportunityId, Opportunity.AccountId from OpportunityLineItem 
                                   where Id=:ol.Id];
      OpportunityLineItem oli12 = [select Id,Product_Group1__c,Product_Area1__c,Product_Family1__c,Product_Child1__c,PricebookEntryId, PricebookEntry.Pricebook2Id, PricebookEntry.Name, PriceBookEntry.Product2Id, OpportunityId, Opportunity.AccountId from OpportunityLineItem 
                                    where Id=:ol12.Id];
      
       test.startTest();   
       
       // load the page       
       PageReference pageRef = Page.OpportunityProductLineItems;
       pageRef.getParameters().put('Id',oli.OpportunityId);
       Test.setCurrentPageReference(pageRef);
        
       // load the extension
       ApexPages.StandardController sc1 = new ApexPages.standardController(opp2);
       OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
       oPEE .setString('Testing');
       PageReference page1= oPEE.priceBookCheck();
       PageReference page2= oPEE.onCancel();
       PageReference page3= oPEE.changePricebook();
         
       PricebookEntry pbe2 = new PricebookEntry(Pricebook2Id=pb2.Id, 
       Product2Id=p2.Id, UnitPrice=99,UseStandardPrice=false, isActive=true);
       list_priceEntry.add(pbe2);
       insert pbe2;
    
       ApexPages.StandardController sc = new ApexPages.standardController(oli.Opportunity);
       OpportunityProductLineItemsExt oPEE1 = new OpportunityProductLineItemsExt(sc);
       oPEE1.setString('Testing');
       
       PageReference page11= oPEE1.priceBookCheck();
       PageReference page21= oPEE1.onCancel();
       PageReference page31= oPEE1.changePricebook();
       PageReference page41= oPEE1.onSave();
       PageReference page51= oPEE1.removeFromShoppingCart();
       
       ApexPages.StandardController sc2 = new ApexPages.standardController(oli12.Opportunity);
       OpportunityProductLineItemsExt oPEE2 = new OpportunityProductLineItemsExt(sc2);
        
        // test 'getChosenCurrency' method
        if(UserInfo.isMultiCurrencyOrganization())
            System.assert(oPEE2.getChosenCurrency()!='');
        else
            System.assertEquals(oPEE2.getChosenCurrency(),'');

        // we know that there is at least one line item, so we confirm
        Integer startCount = oPEE2.ShoppingCart.size();
       
        system.assert(startCount==0);
        oPEE2.searchString = 'hip cat';
        oPEE2.updateAvailableList();
        
        //test search functionality without finding anything
        
        system.assert(oPEE2.AvailableProducts.size()==0);
        oPEE2.searchString = 'Test Product';
        oPEE2.updateAvailableList();

        oPEE2.toUnselect = oli.PricebookEntryId;
        oPEE2.removeFromShoppingCart();
        system.assert(oPEE2.shoppingCart.size()==startCount);
        
        //test save and reload extension
        oPEE2.onSave();
        oPEE2= new OpportunityProductLineItemsExt(new ApexPages.StandardController(oli.Opportunity));
        system.assert(oPEE2.shoppingCart.size()==startCount);
        
        // test search again, this time we will find something
        oPEE2.searchString = oli.PricebookEntry.Name;
        oPEE2.updateAvailableList();
        
        // test add to Shopping Cart function
        try {
             oPEE2.toSelect = oPEE2.AvailableProducts[0].Id;
             oPEE2.addToShoppingCart();
             
            }
        catch (ListException ex)
         {
            system.debug('List Exception'+ ex);
         } 
                  
        // test save method - WITHOUT quanitities and amounts entered and confirm that error message is displayed
        oPEE2.onSave();
        
        // add required info and try save again
        for(OpportunityLineItem o : oPEE2.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
        }
        oPEE2.onSave();
        
        // query line items to confirm that the save worked
        opportunityLineItem[] oli2 = [select Id from opportunityLineItem where OpportunityId = :oli.OpportunityId];
        
        // test on new Opp (no pricebook selected) to make sure redirect is happening
        Opportunity newOpp = new Opportunity(Name='New Opp',stageName='Pipeline',Amount=10,closeDate=System.Today()+30,AccountId=oli.Opportunity.AccountId,
                                                Program__c=prog.id);
        Class_TriggerCheck.Var_IsProgramUPdated=false;
        insert(newOpp);
        oPEE2 = new OpportunityProductLineItemsExt(new ApexPages.StandardController(newOpp));
        oPEE2.setString('Test');
        oPEE2.getString();
        
        test.stopTest();
        
        ////////////////////////////////////////
        //  test redirect page
        ////////////////////////////////////////
        
    }
    
    static testmethod void mytest1(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u2 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u2;
      
      Account acc11 = new Account(Name='Account3');
      insert acc11;
            
      Platform_Master__c pm11 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm11;
    
      Forecast__c fore11 = new Forecast__c(Forecast_Platform__c=pm11.id,Airframer__c=acc11.id);
      insert fore11;

      Product_Hiearchy_Object__c hierarchy4 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Test Family4',Product_Child__c='Across Multiple Products');
      insert hierarchy4;
      system.debug('');
      
      Program__c prog11 =new Program__c(Lookup_hiearchyname__c=hierarchy4.id,Product_Sales_Owner__c=u2.id,Forecast__c=fore11.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog11;
      
      Opportunity opp11 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog11.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp11;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog11;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore11.Status__c ='Active';
      update fore11;
      
      Product2 p5 = new Product2(Lookup_hiearchyname__c=hierarchy4.id,Name='Test Product5');
      insert p5;  
      
      id stn_price_id1;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id1 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s11 = new PricebookEntry(Pricebook2Id=stn_price_id1,UseStandardPrice=false, 
                              Product2Id=p5.Id, UnitPrice=99, isActive=true);
      insert pbe_s11;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp11.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp11);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = 'Across Multiple Products';
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest1_1(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u2 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u2;
      
      Account acc11 = new Account(Name='Account3');
      insert acc11;
            
      Platform_Master__c pm11 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm11;
    
      Forecast__c fore11 = new Forecast__c(Forecast_Platform__c=pm11.id,Airframer__c=acc11.id);
      insert fore11;

      Product_Hiearchy_Object__c hierarchy4 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Test Family4',Product_Child__c='Across Multiple Products');
      insert hierarchy4;
      system.debug('');
      
      Program__c prog11 =new Program__c(Lookup_hiearchyname__c=hierarchy4.id,Product_Sales_Owner__c=u2.id,Forecast__c=fore11.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog11;
      
      Opportunity opp11 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog11.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp11;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog11;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore11.Status__c ='Active';
      update fore11;
      
      Product2 p5 = new Product2(Lookup_hiearchyname__c=hierarchy4.id,Name='Test Product5');
      insert p5;  
      
      id stn_price_id1;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id1 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s11 = new PricebookEntry(Pricebook2Id=stn_price_id1,UseStandardPrice=false, 
                              Product2Id=p5.Id, UnitPrice=99, isActive=true);
      insert pbe_s11;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp11.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp11);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = 'Test Child';
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest1_2(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u2 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u2;
      
      Account acc11 = new Account(Name='Account3');
      insert acc11;
            
      Platform_Master__c pm11 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm11;
    
      Forecast__c fore11 = new Forecast__c(Forecast_Platform__c=pm11.id,Airframer__c=acc11.id);
      insert fore11;

      Product_Hiearchy_Object__c hierarchy4 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Test Family4',Product_Child__c='Across Multiple Products');
      insert hierarchy4;
      system.debug('');
      
      Program__c prog11 =new Program__c(Lookup_hiearchyname__c=hierarchy4.id,Product_Sales_Owner__c=u2.id,Forecast__c=fore11.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog11;
      
      Opportunity opp11 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog11.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp11;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog11;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore11.Status__c ='Active';
      update fore11;
      
      Product2 p5 = new Product2(Lookup_hiearchyname__c=hierarchy4.id,Name='Test Product5');
      insert p5;  
      
      id stn_price_id1;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id1 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s11 = new PricebookEntry(Pricebook2Id=stn_price_id1,UseStandardPrice=false, 
                              Product2Id=p5.Id, UnitPrice=99, isActive=true);
      insert pbe_s11;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp11.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp11);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest2(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u3 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u3;
      
      Account acc12 = new Account(Name='Account3');
      insert acc12;
            
      Platform_Master__c pm12 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm12;
    
      Forecast__c fore12 = new Forecast__c(Forecast_Platform__c=pm12.id,Airframer__c=acc12.id);
      insert fore12;

      Product_Hiearchy_Object__c hierarchy5 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Across Multiple Products',Product_Child__c='Across Multiple Products');
      insert hierarchy5;
      
      Program__c prog12 =new Program__c(Lookup_hiearchyname__c=hierarchy5.id,Product_Sales_Owner__c=u3.id,Forecast__c=fore12.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog12;
      
      Opportunity opp12 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog12.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp12;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog12;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore12.Status__c ='Active';
      update fore12;
      
      Product2 p6 = new Product2(Lookup_hiearchyname__c=hierarchy5.id,Name='Test Product5');
      insert p6;  
      
      id stn_price_id2;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id2 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s12 = new PricebookEntry(Pricebook2Id=stn_price_id2,UseStandardPrice=false, 
                              Product2Id=p6.Id, UnitPrice=99, isActive=true);
      insert pbe_s12;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp12.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp12);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = 'Across Multiple Products';
      oPEE.valPrdFamily = 'Across Multiple Products';
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest2_1(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u3 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u3;
      
      Account acc12 = new Account(Name='Account3');
      insert acc12;
            
      Platform_Master__c pm12 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm12;
    
      Forecast__c fore12 = new Forecast__c(Forecast_Platform__c=pm12.id,Airframer__c=acc12.id);
      insert fore12;

      Product_Hiearchy_Object__c hierarchy5 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Across Multiple Products',Product_Child__c='Across Multiple Products');
      insert hierarchy5;
      
      Program__c prog12 =new Program__c(Lookup_hiearchyname__c=hierarchy5.id,Product_Sales_Owner__c=u3.id,Forecast__c=fore12.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog12;
      
      Opportunity opp12 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog12.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp12;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog12;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore12.Status__c ='Active';
      update fore12;
      
      Product2 p6 = new Product2(Lookup_hiearchyname__c=hierarchy5.id,Name='Test Product5');
      insert p6;  
      
      id stn_price_id2;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id2 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s12 = new PricebookEntry(Pricebook2Id=stn_price_id2,UseStandardPrice=false, 
                              Product2Id=p6.Id, UnitPrice=99, isActive=true);
      insert pbe_s12;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp12.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp12);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = 'Test Child';
      oPEE.valPrdFamily = 'Test Family';
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest2_2(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u3 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u3;
      
      Account acc12 = new Account(Name='Account3');
      insert acc12;
            
      Platform_Master__c pm12 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm12;
    
      Forecast__c fore12 = new Forecast__c(Forecast_Platform__c=pm12.id,Airframer__c=acc12.id);
      insert fore12;

      Product_Hiearchy_Object__c hierarchy5 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Across Multiple Products',Product_Child__c='Across Multiple Products');
      insert hierarchy5;
      
      Program__c prog12 =new Program__c(Lookup_hiearchyname__c=hierarchy5.id,Product_Sales_Owner__c=u3.id,Forecast__c=fore12.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog12;
      
      Opportunity opp12 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog12.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp12;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog12;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore12.Status__c ='Active';
      update fore12;
      
      Product2 p6 = new Product2(Lookup_hiearchyname__c=hierarchy5.id,Name='Test Product5');
      insert p6;  
      
      id stn_price_id2;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id2 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s12 = new PricebookEntry(Pricebook2Id=stn_price_id2,UseStandardPrice=false, 
                              Product2Id=p6.Id, UnitPrice=99, isActive=true);
      insert pbe_s12;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp12.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp12);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = '';
      oPEE.valPrdFamily = 'Test Family';
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest2_3(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u3 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u3;
      
      Account acc12 = new Account(Name='Account3');
      insert acc12;
            
      Platform_Master__c pm12 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm12;
    
      Forecast__c fore12 = new Forecast__c(Forecast_Platform__c=pm12.id,Airframer__c=acc12.id);
      insert fore12;

      Product_Hiearchy_Object__c hierarchy5 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Across Multiple Products',Product_Child__c='Across Multiple Products');
      insert hierarchy5;
      
      Program__c prog12 =new Program__c(Lookup_hiearchyname__c=hierarchy5.id,Product_Sales_Owner__c=u3.id,Forecast__c=fore12.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog12;
      
      Opportunity opp12 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog12.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp12;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog12;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore12.Status__c ='Active';
      update fore12;
      
      Product2 p6 = new Product2(Lookup_hiearchyname__c=hierarchy5.id,Name='Test Product5');
      insert p6;  
      
      id stn_price_id2;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id2 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s12 = new PricebookEntry(Pricebook2Id=stn_price_id2,UseStandardPrice=false, 
                              Product2Id=p6.Id, UnitPrice=99, isActive=true);
      insert pbe_s12;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp12.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp12);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = '';
      oPEE.valPrdFamily = 'Test Family';
      oPEE.valPrdArea = 'Test Area';
      oPEE.valPrdGroup = 'Test Group';
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest2_4(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u3 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u3;
      
      Account acc12 = new Account(Name='Account3');
      insert acc12;
            
      Platform_Master__c pm12 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm12;
    
      Forecast__c fore12 = new Forecast__c(Forecast_Platform__c=pm12.id,Airframer__c=acc12.id);
      insert fore12;

      Product_Hiearchy_Object__c hierarchy5 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Test Area4',Product_Family__c='Across Multiple Products',Product_Child__c='Across Multiple Products');
      insert hierarchy5;
      
      Program__c prog12 =new Program__c(Lookup_hiearchyname__c=hierarchy5.id,Product_Sales_Owner__c=u3.id,Forecast__c=fore12.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog12;
      
      Opportunity opp12 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog12.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp12;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog12;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore12.Status__c ='Active';
      update fore12;
      
      Product2 p6 = new Product2(Lookup_hiearchyname__c=hierarchy5.id,Name='Test Product5');
      insert p6;  
      
      id stn_price_id2;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id2 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s12 = new PricebookEntry(Pricebook2Id=stn_price_id2,UseStandardPrice=false, 
                              Product2Id=p6.Id, UnitPrice=99, isActive=true);
      insert pbe_s12;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp12.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp12);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = '';
      oPEE.valPrdFamily = '';
      oPEE.valPrdArea = '';
      oPEE.valPrdGroup = 'Test Group';
      oPEE.updateAvailableList();
    
    }
    
    static testmethod void mytest3(){
      Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
      System.Debug('Id of administrator = '+ pro[0].id);
      user u3 = new user(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                    localesidkey = 'en_US',country = 'India',profileid=pro[0].id,timezonesidkey = 'Europe/London', username = 'sunny.kapoor@ge.com.sys');
      insert u3;
      
      Account acc12 = new Account(Name='Account3');
      insert acc12;
            
      Platform_Master__c pm12 = new Platform_Master__c(Name='170',Civil_Military__c='Military',
                                                      Segment__c='Regional Transports');
      insert pm12;
    
      Forecast__c fore12 = new Forecast__c(Forecast_Platform__c=pm12.id,Airframer__c=acc12.id);
      insert fore12;

      Product_Hiearchy_Object__c hierarchy5 = new Product_Hiearchy_Object__c(Name='Test hierarchy4',Product_Group__c='Test group4',Product_Area__c='Across Multiple Products',Product_Family__c='Across Multiple Products',Product_Child__c='Across Multiple Products');
      insert hierarchy5;
      
      Program__c prog12 =new Program__c(Lookup_hiearchyname__c=hierarchy5.id,Product_Sales_Owner__c=u3.id,Forecast__c=fore12.id,Program_Probability__c='50% - Potential funding');
      Class_TriggerCheck.Var_isProgam_Probability_change=false; 
      insert prog12;
      
      Opportunity opp12 = new opportunity(Name='Test opp',StageName='Prospecting',
                                           CloseDate=date.today(),Program__c=prog12.id);
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      insert opp12;
      
      // Updating Program
      try
      {
          Class_TriggerCheck.Var_isProgam_Probability_change=false;
          update prog12;
      }
      
      catch(DmlException de){}
      
      Class_TriggerCheck.Var_isProgam_Probability_change=false;
      fore12.Status__c ='Active';
      update fore12;
      
      Product2 p6 = new Product2(Lookup_hiearchyname__c=hierarchy5.id,Name='Test Product5');
      insert p6;  
      
      id stn_price_id2;
      for(PriceBook2 pb2Standard :[select Id from Pricebook2 where isStandard=true limit 1])
      {
        stn_price_id2 = pb2Standard.id;
      }
      
      PricebookEntry pbe_s12 = new PricebookEntry(Pricebook2Id=stn_price_id2,UseStandardPrice=false, 
                              Product2Id=p6.Id, UnitPrice=99, isActive=true);
      insert pbe_s12;
      
      PageReference pageRef = Page.OpportunityProductLineItems;
      pageRef.getParameters().put('Id',opp12.Id);
      Test.setCurrentPageReference(pageRef);
      
      ApexPages.StandardController sc1 = new ApexPages.standardController(opp12);
      OpportunityProductLineItemsExt oPEE = new OpportunityProductLineItemsExt(sc1);
      oPEE.valPrdChild = 'Across Multiple Products';
      oPEE.valPrdFamily = 'Across Multiple Products';
      oPEE.valPrdArea = 'Across Multiple Products';
      oPEE.updateAvailableList();
    
    } */
 }