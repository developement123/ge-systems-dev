/*
        Developer : Abhishek
        Date: 16/Feb/2017    
        Action : This class is a test class to cover runPostRefreshActivity
        Version     Written by        last modified Date              
        ---------------------------------- ------------------
        1.0         Abhishek            18 sep 2017 removed account platform         
    */
@IsTest
public class Tracker_runPostRefreshActivity
{

 private static testmethod void testflushUKRecord()
 {
      // setting static vairables to false to avoid trigger execution
       Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
       Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
       Class_TriggerCheck.var_IsStopEPABatchTrg = true;
       Class_TriggerCheck.Var_ShouldEPA_update = false;
       Class_TriggerCheck.Var_StopTriggerOnEPA = false;
       Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
       Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
       Class_TriggerCheck.var_TriggerOnResponse = false;
       Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
       Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
       Class_TriggerCheck.var_TriggerOnContract = false;
       Class_TriggerCheck.var_TriggerOnSchedule = false;
       Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
       Class_TriggerCheck.Var_StopPlatMasterTrigger = false; 
       Class_TriggerCheck.var_TriggerOnIdeationTrg = false;
       Class_TriggerCheck.var_TriggerOnOrder = false;
       Class_TriggerCheck.var_triggerOnUser = false;    
       
       Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];   
       UserRole[] UR = [select id, DeveloperName from UserRole where DeveloperName = 'Delegated_Systems_Administration_UKM' limit 1];
        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',localesidkey = 'en_GB', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev', Perspecsys_Access__c=true,UserRoleId=UR[0].id);
        insert u;
        
        Profile[] proDelegated = [select Id from Profile where Name = 'Systems Delegated Administrator' limit 1];
        
        User delegatedUser = new User(alias = 'skapo121', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@example.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',localesidkey = 'en_GB', profileid = proDelegated[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal123@ge.com.sys.dev');
        insert delegatedUser;
        
        
        system.runAs(u){
            //inserting acount
            Account acc = new Account(Name='GE GEAS');
            insert acc; 
        
            //inserting contact
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                                MailingCity='Delhi', MailingCountry='India', email = 'abhishek3.singh@ge.com');
            insert con;    
            
            //inserting Platform master
            Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports',Is_Tokenized__c=true);
            insert pm; 
            
            //inserting hierarchy.          
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'test12', Product_Group__c = 'Avionics', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child', Is_Tokenized__c = true);
            insert hierarchy;

       
            //inserting product.
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True,IsActive = True);
            insert p2;  
       
            // Insert Ideation
            Ideation__c myIdea = new Ideation__c(name='Initial', Product_Group__c = 'Avionics', Status__c = 'Open', Civil_Military__c = 'Military', Site__c = 'Austin', Is_Tokenized__c = true);
            Insert myIdea;
            
            // Create Opp 
            Id pipelineOppRec = OpportunityTrigger_Handler.getRecordTypeId(Opportunity.SObjectType, Label.Label_PipelineOpportunity);
            opportunity pipelineOpp= new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                    , CloseDate = date.today(), Amount = 500, Firm_Anticipated__c = 'Anticipated'
                    , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                    , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true, Platform_New__c = pm.id, accountId= acc.id, recordtypeId = pipelineOppRec, Ideation__c = myIdea.Id
                    , Offering_Type__c = 'OE / Services', Is_Tokenized__c=true);
            insert pipelineOpp;
            
            // inserting OLIs 
             Opportunity_Line_Item__c pli = new Opportunity_Line_Item__c(Opportunity__c=pipelineOpp.id, Product__c=p2.Id, Probability__c= 20, Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false, Is_Tokenized__c = true);
             insert pli;
            
            // inserting Schedules
            List<Schedule__c> lstOfSch = new List<Schedule__c>();
            Date dateInstance  = Date.newInstance(System.Today().Year(),1,30); 
            for(Integer i=0;i<=18;i++){
                  Schedule__c sc = new Schedule__c(Opportunity_Line_Item__c = pli.id, Schedule_Date__c = dateInstance.addMonths(i), Quantity__c = 1, Escalated_Revenue__c = 3
                  , Opportunity__c = pipelineOpp.ID, Is_Tokenized__c = true);
                  lstOfSch.add(sc);
            }
            insert lstOfSch;
            
            // insert custom setting 
            UKObjectList__c cs = new UKObjectList__c();
            cs.Name='Ideation';
            cs.ObjectAPI__c = 'Ideation__c';
            cs.Order__c = 11;
            cs.wantToDelete__c = true;
            //cs.Other fiels values
            upsert cs;
            
            Test.StartTest();
                runPostRefreshActivity runPost = new runPostRefreshActivity();
                runPost.flushUKRecord('True');
            Test.Stoptest();
      }
 }
 
 private static testmethod void testrunApexClass()
 {
      // setting static vairables to false to avoid trigger execution
       Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
       Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
       Class_TriggerCheck.var_IsStopEPABatchTrg = true;
       Class_TriggerCheck.Var_ShouldEPA_update = false;
       Class_TriggerCheck.Var_StopTriggerOnEPA = false;
       Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
       Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
       Class_TriggerCheck.var_TriggerOnResponse = false;
       Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
       Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
       Class_TriggerCheck.var_TriggerOnContract = false;
       Class_TriggerCheck.var_TriggerOnSchedule = false;
       Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
       Class_TriggerCheck.Var_StopPlatMasterTrigger = false; 
       Class_TriggerCheck.var_TriggerOnIdeationTrg = false;
       Class_TriggerCheck.var_TriggerOnOrder = false;
       Class_TriggerCheck.var_triggerOnUser = false;    
       
       Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];   
       UserRole[] UR = [select id, DeveloperName from UserRole where DeveloperName = 'Delegated_Systems_Administration_UKM' limit 1];
        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',localesidkey = 'en_GB', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev', Perspecsys_Access__c=true,UserRoleId=UR[0].id);
        insert u;
        
        Profile[] proDelegated = [select Id from Profile where Name = 'Systems Delegated Administrator' limit 1];
        
        User delegatedUser = new User(alias = 'skapo121', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@example.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',localesidkey = 'en_GB', profileid = proDelegated[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal123@ge.com.sys.dev');
        insert delegatedUser;
        
        
        system.runAs(u){
            //inserting acount
            Account acc = new Account(Name='GE GEAS');
            insert acc; 
        
            //inserting contact
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                                MailingCity='Delhi', MailingCountry='India', email = 'abhishek3.singh@ge.com');
            insert con;    
            
            //inserting Platform master
            Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports',Is_Tokenized__c=true);
            insert pm; 
            
            //inserting hierarchy.          
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'test12', Product_Group__c = 'Avionics', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child', Is_Tokenized__c = true);
            insert hierarchy;

       
            //inserting product.
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True,IsActive = True);
            insert p2;  
       
            // Insert Ideation
            Ideation__c myIdea = new Ideation__c(name='Initial', Product_Group__c = 'Avionics', Status__c = 'Open', Civil_Military__c = 'Military', Site__c = 'Austin', Is_Tokenized__c = true);
            Insert myIdea;
            
            // Create Opp 
            Id pipelineOppRec = OpportunityTrigger_Handler.getRecordTypeId(Opportunity.SObjectType, Label.Label_PipelineOpportunity);
            opportunity pipelineOpp= new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                    , CloseDate = date.today(), Amount = 500, Firm_Anticipated__c = 'Anticipated'
                    , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                    , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true, Platform_New__c = pm.id, accountId= acc.id, recordtypeId = pipelineOppRec, Ideation__c = myIdea.Id
                    , Offering_Type__c = 'OE / Services', Is_Tokenized__c=true);
            insert pipelineOpp;
            
            // inserting OLIs 
             Opportunity_Line_Item__c pli = new Opportunity_Line_Item__c(Opportunity__c=pipelineOpp.id, Product__c=p2.Id, Probability__c= 20, Revenue_Stream_top_level__c='Engineering', Revenue_Stream_child_level__c='Aviall', First_Ship_Date__c = system.today(), Last_Ship_Date__c = system.today(), Execution_Site__c='Austin', Installment_Periods__c = 'Annually',  Declined__c = false, Is_Tokenized__c = true);
             insert pli;
            
            // inserting Schedules
            List<Schedule__c> lstOfSch = new List<Schedule__c>();
            Date dateInstance  = Date.newInstance(System.Today().Year(),1,30); 
            for(Integer i=0;i<=18;i++){
                  Schedule__c sc = new Schedule__c(Opportunity_Line_Item__c = pli.id, Schedule_Date__c = dateInstance.addMonths(i), Quantity__c = 1, Escalated_Revenue__c = 3
                  , Opportunity__c = pipelineOpp.ID, Is_Tokenized__c = true);
                  lstOfSch.add(sc);
            }
            insert lstOfSch;
            
            // insert custom setting 
            UKObjectList__c cs = new UKObjectList__c();
            cs.Name='Ideation';
            cs.ObjectAPI__c = 'Ideation__c';
            cs.Order__c = 11;
            cs.wantToDelete__c = true;
            //cs.Other fiels values
            upsert cs;
            
            Test.StartTest();
                runPostRefreshActivity sandboxScriptclass = new runPostRefreshActivity();
                Test.testSandboxPostCopyScript(sandboxScriptclass, UserInfo.getOrganizationId(), UserInfo.getOrganizationId(), 'sandboxName');
            Test.Stoptest();
      }
 }
 
  public Id getRecordTypeId(SObjectType entity, String recordTypeName) {
    try {
            return entity.getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        } 
    catch(Exception e) {
         return null;
    }
  }
 
}