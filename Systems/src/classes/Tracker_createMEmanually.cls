//Comments added By Abhishek: Jan.- 2016:
//This test class is not required as corresponding trigger is inactivated for Opportunity Optimization Requirement.
//End
/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - createMEmanually
    
    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          12/03/2014        Initial Creation
    
    last modified by : Mohan Kumar(13 Jan 2015) for JIRA 364. 
    last modified by : Mohan Kumar(15 April 2015) for JIRA 364. 
               
*************************************************************************************************************************************************/      
      
      
@istest(SeeAllData = true)
private class Tracker_createMEmanually {

  /*   static testmethod void test_createMEmanually() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;

            opportunity opp = new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(), Margin_Escalator__c = true);
            insert opp;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True);
            insert p2;     

            Pricebook2 standardPB = [select id from Pricebook2 where isStandard = true For Update];
            
            Pricebook2 pb = new Pricebook2(Name = 'Custom Price Book', 
                    Description = 'Price Book 2009 Products', IsActive = true);
            insert pb;
            
            PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, 
                                           Product2Id = p2.Id, UnitPrice = 10000, 
                                           IsActive = true, UseStandardPrice = false);
            insert standardPrice;

            PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = p2.Id, 
                                 UnitPrice = 99, isActive = true, UseStandardPrice = false);
            insert pbe;

            OpportunityLineItem ol2 = new OpportunityLineItem(PriceBookEntryId = pbe.Id, OpportunityId = opp.Id,
                Quantity = 1, UnitPrice = 0);
            insert ol2;
            
            Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
            Date mydate1 = Date.newInstance(System.Today().Year(), 10, 5);
           
            List<OpportunityLineItemSchedule> lstOfSch = new List<OpportunityLineItemSchedule>();
           
            lstOfSch.add(new OpportunityLineItemSchedule(OpportunityLineItemId = ol2.id, ScheduleDate = mydate, Quantity = 1, Revenue = 3, Type = 'both'));
            lstOfSch.add(new OpportunityLineItemSchedule(OpportunityLineItemId = ol2.id, ScheduleDate = mydate1, Quantity = 1, Revenue = 100, Type = 'both'));
            
            insert lstOfSch;
            
        Test.startTest(); 
            Class_TriggerCheck.var_execute_createMEmanuallyTrg = true;            
            List<Margin_Escalator__c> listOfME = new List<Margin_Escalator__c>();
            
            listOfME.add(new Margin_Escalator__c(Opportunity__c=opp.Id, Year__c = String.valueOf(System.Today().year()), Out_of_Sync__c = true));
            insert listOfME;
            
            listOfME[0].Out_of_Sync__c = false;
            update listOfME; 

            delete listOfME;
         Test.stopTest();   
        }
    }   */ 
}