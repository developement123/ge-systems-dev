/*************************************************************************************************************************************************
    Following test class was created to provide code coverage for the following class:
    - DisplayResponseIdOnTaskController
     
    Version       Author(s)             Date                Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
      1.0         Mohan kumar          8/July/2015        Initial Creation
                  Abhishek             18/sep/2017        removed account platform
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_DisplayResponseIdOnTask {
  
    public static Sales_Request__c sr;
    
    public static void loadTest() {  
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false; 
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(Name = 'Hello');
        insert acc1;        

        System.runAs(u) {            

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
            opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
            
            sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response', Program__c = prog.id, Pipeline_Opportunity__c = opp1.id);
            insert sr;
        }
    }
    static testMethod void test_DisplayResponseIdOnTask() {
        loadTest();
        
        Test.startTest();
            List<task> t = new List<task> { new task(WhatID = sr.id,
                                            Subject = 'Donni',
                                            Status = 'Completed',
                                            Priority = 'Normal')
                                          };

            insert t; 
            
            ApexPages.StandardController obj = new ApexPages.StandardController(t[0]);
            DisplayResponseIdOnTaskController controller = new DisplayResponseIdOnTaskController(obj);
            
        Test.stopTest();
    } 
}