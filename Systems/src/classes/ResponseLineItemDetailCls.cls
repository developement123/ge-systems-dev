/***********************************************************************************************************************************************************
    Author : Mohan Kumar
    Date   : 19 March 2015
    
    Action : This class is used as extensions in vf page : 
                    ResponseLineItemDetail

***********************************************************************************************************************************************************/

public with sharing class ResponseLineItemDetailCls {

    public Response_Line_Item__c rliInstance{get;set;}
    
    public Boolean toShowEditPage{get;set;}
    public Boolean toShowDetailPage{get;set;}    
        
    public Id currentRecordIdFrmUrl{get;set;} 
    public Id respIdFrmUrl{get;set;}
    public Id parentIdFrmUrl{get;set;}
    
    public String identifierFrmURL{get;set;}
    
    public ResponseLineItemDetailCls(ApexPages.StandardController controller) {
        currentRecordIdFrmUrl = ApexPages.CurrentPage().getparameters().get('id');   
        parentIdFrmUrl = ApexPages.CurrentPage().getparameters().get('respId'); 
        identifierFrmURL = ApexPages.CurrentPage().getparameters().get('identifier');  
        
        rliInstance = new Response_Line_Item__c();  
       
        if(controller.getId() != null) {  
            if(identifierFrmURL == 'edit') {  // showing edit page only.
                toShowDetailPage = false; 
                toShowEditPage = true;  
            }
            else if(identifierFrmURL != 'edit'){  // showing detail page only.
                toShowDetailPage = true; 
                toShowEditPage = false;
            }
           
            String query = 'Select ';    
            Map<String, Schema.SObjectField> rliFields = Schema.getGlobalDescribe().get('Response_Line_Item__c').getDescribe().fields.getMap();
            
            for(String fieldAPI : rliFields.keySet()) {
                query += fieldAPI + ',';          
            }
            query = query.subString(0, query.length() - 1);
            query += ' from Response_Line_Item__c where id=\'' + controller.getId() + '\'';
           
            rliInstance = Database.query(query);  
           
            respIdFrmUrl = rliInstance.Response__c;  
        }   
        else {
            toShowDetailPage = false;
            toShowEditPage = true;
        }
    }
    
    public void ShowEditPage() {    
        toShowDetailPage = false;
        toShowEditPage = true;        
    }
    
    public void Cancel() { 
        toShowDetailPage = true;
        toShowEditPage = false;        
    }
    
    public pagereference DeleteRec() { 
           
        pagereference pr = new pagereference('/'+ respIdFrmUrl);
        try {         
            delete rliInstance;
        }   
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
        }       
        pr.setRedirect(true);
        return pr;       
    }
    
    public pagereference customSave() { 
        if(currentRecordIdFrmUrl == null) {
            try {
                rliInstance.Response__c = parentIdFrmUrl;
                insert rliInstance;
                pagereference pr = new pagereference('/apex/ResponseLineItemDetail?id='+ rliInstance.id); 
                pr.setRedirect(true);
                return pr; 
            }
            catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
            }
        }
        else if(currentRecordIdFrmUrl != null) {
            try { 
                update rliInstance;  
                pagereference pr = new pagereference('/apex/ResponseLineItemDetail?id='+ rliInstance.id); 
                pr.setRedirect(true);
                return pr;
            }
            catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
            }
        }
        return null;
    }    
}