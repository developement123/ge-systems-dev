/*************************************************************************************************************************************************
This test class was created to provide code coverage for the following trigger:
- Trg_sendCA

Version     Author(s)               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0         Mohan kumar          10/20/2014        Initial Creation
            Abhishek             02/29/2016        Modified for JIRA 498
            Abhishek             09/18/2017        removed account platform            
*************************************************************************************************************************************************/      
      
      
@istest
public class Tracker_Trg_sendCA {
      static testmethod void test_sendCA() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_triggerOnUser = false;

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');

        Test.setMock(HttpCalloutMock.class, mock);        

        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        Account acc1 = new Account(NAME = 'Hello');
        insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            // added Account_Platform__c for JIRA 498
            opportunity opp2 = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Platform_New__c = pm.id, accountId= acc1.id);
            insert opp2;

            Sales_Request__c sr = new Sales_Request__c(Name = 'Test response5', Program__c = prog.id, Response_Description__c = 'Test desc', Pipeline_Opportunity__c = opp2.id);
            insert sr;
            EPA__c epa = new EPA__c( Closed__c = False);
            insert epa;
            
         Test.startTest(); 
            Class_TriggerCheck.IsPreventing = true;            
            Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = NULL,
                FY_Spent_to_Date__c = NULL, Status__c = 'Approved', Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open') );
            insert ca;    
        Test.stopTest();   
            ca.Status__c = 'Rejected';
            update ca;                
            
        }
    }  
    
     static testmethod void test_sendCA1() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        
        Test.setMock(HttpCalloutMock.class, mock);        
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'Systems Integration User' limit 1];

        User u = new User(alias = 'rinte', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'User', firstName = 'Integration',  languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        Account acc1 = new Account(NAME = 'Hello');
        insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                 Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            // added Account_Platform__c for JIRA 498
            opportunity opp2 = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Platform_New__c = pm.id, accountId= acc1.id);
            insert opp2;

            Sales_Request__c sr = new Sales_Request__c(Name = 'Test response5', Program__c = prog.id, Response_Description__c = 'Test desc', Pipeline_Opportunity__c = opp2.id, Active_CA_Status__c = 'NOt Approved');
            insert sr;
            EPA__c epa = new EPA__c( Closed__c = False);
            insert epa;
            
            Test.startTest(); 
            Class_TriggerCheck.IsPreventing = false;
            Cobra_Create_Update_ApprovedCA.firstCheck = true;  
              
            Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = NULL,
                FY_Spent_to_Date__c = NULL, Status__c = 'Approved', Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open') );
            insert ca;   
            
            Class_TriggerCheck.IsPreventing = true;
            Class_TriggerCheck.IsUpdated_CA = true;
            ca.Status__c = 'Closed';
            update ca;   
            Test.stopTest();
        }
    }  
    
     static testmethod void test_sendCA2() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');

        Test.setMock(HttpCalloutMock.class, mock);        

        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'Systems Integration User' limit 1];

        User u = new User(alias = 'rinte', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'User', firstName = 'Integration',  languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        Account acc1 = new Account(NAME = 'Hello');
        insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                 Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            // added Account_Platform__c for JIRA 498
            opportunity opp2 = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Platform_New__c = pm.id, accountId= acc1.id);
            insert opp2;

            Sales_Request__c sr = new Sales_Request__c(Name = 'Test response5', Program__c = prog.id, Response_Description__c = 'Test desc', Pipeline_Opportunity__c = opp2.id);
            insert sr;
            EPA__c epa = new EPA__c(Closed__c = False);
            insert epa;
            
         Test.startTest(); 
            Class_TriggerCheck.IsPreventing = true; 
            Cobra_Create_Update_ApprovedCA.firstCheck = true;          
            
            Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = NULL,
                FY_Spent_to_Date__c = NULL, Status__c = 'closed', Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open') );
            insert ca;
            
         Test.stopTest();   
        }
    }  
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    } 
}