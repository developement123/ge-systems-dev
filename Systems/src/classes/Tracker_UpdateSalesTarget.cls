/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - UpdateSalesTarget

    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          12/02/2014        Initial Creation
    
    Last modified By : Mohan Kumar(2 April 2015).
    Last modified by : Abhishek(28 April 2015) for JIRA 553.       
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_UpdateSalesTarget {
    /*
    static testmethod void test_UpdateSalesTarget() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false; 

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
            List<Opportunity> listOfOppy = new List<Opportunity>();
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(), MTG_Year__c = String.valueOf(System.Today().Year()), Growth__c = true, Account_Platform__c = fore.Id);

            listOfOppy.add(opp);
            // added Account_Platform__c for JIRA 498
            opportunity opp1 = new opportunity(Name = 'Test opp2', StageName = 'Contract Awarded', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),
                MTG_Year__c = String.valueOf(System.Today().Year()), Growth__c = true, Account_Platform__c = fore.Id);
            
            listOfOppy.add(opp1);
            insert listOfOppy;
            
            Product_Group_Team__c pgt = new Product_Group_Team__c(Name = 'Core Avionics');
            insert pgt;
            
            Product_Area_Team__c pat = new Product_Area_Team__c(Name = 'XYZ');
            insert pat; 
                                      
            Test.startTest(); 
                List<Sales_Target__c> lstOfST = new List<Sales_Target__c>();
                     
                Sales_Target__c st = new Sales_Target__c(Sales_Person__c = u.id, Sales_Year__c=String.valueOf(System.Today().Year()), Product_Area_Team__c = pat.id, Product_Group_Team__c = pgt.id);
                lstofST.add(st);

                Sales_Target__c st1 = new Sales_Target__c(Sales_Person__c = u.id, Sales_Year__c=String.valueOf(System.Today().Year() +1), Product_Area_Team__c = pat.id, Product_Group_Team__c = pgt.id);
                lstofST.add(st1);

                insert lstofST; 

                Class_TriggerCheck.var_preventUpdate_UpdateSalesTargetTrg = true;   

                st1.Strategic_Win_Target__c = 4;
                st1.Growth_Target__c = 20;
                st1.MTG_Target__c = 10;
                update lstofST;

                delete lstofST;
                 
                
            Test.stopTest();   
        }
    } 
    
    static testmethod void test_UpdateSalesTarget1() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
            List<Opportunity> listOfOppy = new List<Opportunity>();
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(), MTG_Year__c = String.valueOf(System.Today().Year()), Growth__c = true, Account_Platform__c = fore.Id);

            listOfOppy.add(opp);
            // added Account_Platform__c for JIRA 498
            opportunity opp1 = new opportunity(Name = 'Test opp2', StageName = 'Contract Awarded', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(), MTG_Year__c = String.valueOf(System.Today().Year()), Growth__c = true, Account_Platform__c = fore.Id);
            
            listOfOppy.add(opp1);
            insert listOfOppy;
            
            Product_Group_Team__c pgt = new Product_Group_Team__c(Name = 'Core Avionics');
            insert pgt;
            
            Product_Area_Team__c pat = new Product_Area_Team__c(Name = 'XYZ');
            insert pat; 
                                      
            Test.startTest(); 
                List<Sales_Target__c> lstOfST = new List<Sales_Target__c>();
                     
                Sales_Target__c st = new Sales_Target__c(Sales_Person__c = u.id, Sales_Year__c=String.valueOf(System.Today().Year()), Product_Area_Team__c = pat.id, Product_Group_Team__c = pgt.id);
                lstofST.add(st);

                Sales_Target__c st1 = new Sales_Target__c(Sales_Person__c = u.id, Sales_Year__c=String.valueOf(System.Today().Year()), Product_Area_Team__c = pat.id, Product_Group_Team__c = pgt.id);
                lstofST.add(st1);
                
                try {
                    insert lstofST; 
                }
                catch(DmlException e) {
                     System.assert( e.getMessage().contains('DUPLICATE_VALUE, duplicate value found: '));
                }
                
                lstofST.remove(1);
                Sales_Target__c st2 = new Sales_Target__c(Sales_Person__c = u.id, Sales_Year__c=String.valueOf(System.Today().Year() +1), Product_Area_Team__c = pat.id, Product_Group_Team__c = pgt.id);
                lstofST.add(st2);
                
                //insert lstofST;
                upsert lstofST;

                Class_TriggerCheck.var_preventUpdate_UpdateSalesTargetTrg = true;   
                st2.Sales_Year__c=String.valueOf(System.Today().Year());
                st2.Strategic_Win_Target__c = 4;
                st2.Growth_Target__c = 20;
                st2.MTG_Target__c = 10;
                
                try {
                    update lstofST;
                }
                catch(DmlException e) {
                     System.assert( e.getMessage().contains('DUPLICATE_VALUE, duplicate value found: '));
                }

                delete lstofST; 
                Class_TriggerCheck.var_preventUpdate_UpdateSalesTargetTrg = true;
                undelete lstofST;            
                
            Test.stopTest();   
        }
    }    */
}