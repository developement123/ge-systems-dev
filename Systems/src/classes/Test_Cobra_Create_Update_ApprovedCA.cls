/*****************************************************************************************************
This test class was created to provide code coverage for the following class:
- Cobra_Create_Update_ApprovedCA
Version     Author(s)               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0        Abhishek             04/29/2015        created a new method "myUnitTest_FORUK" to increase code coverage as per Jora 325. Code coverage got increased from 65%-99%.            
           Abhishek             18/09/2017        removed account platform
*****************************************************************************************************/    
@isTest
private class Test_Cobra_Create_Update_ApprovedCA {

    static testMethod void myUnitTest() { 
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);
        HTTPResponse response = Cobra_Create_Update_ApprovedCA.Main_Header_Info('Check');
        //test.stoptest();
        
        Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
        insert plat;
        
        Account acc = new Account(Name='Test Account');
        insert acc; 
        
        Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
        
        User u =new User( alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=pro[0].id,country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor13@birlasoft.com.sys.dev');
               insert u; 
       
       Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
       insert hierarchy;
     
       
       Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
       Program_Probability__c='100% - Funded');
       insert prog;
       Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
       opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()), Offering_Type__c = 'OE / Services');
            insert opp1;
       
       //Class_TriggerCheck.IsPreventing = false;
       Sales_Request__c sr=new Sales_Request__c(Name='Test response5',Program__c=prog.id,Response_Description__c='Test desc',Windchill_US_UK__c='Windchill US', Pipeline_Opportunity__c = opp1.id);
       insert sr;
       
       Class_TriggerCheck.Var_SendBatch_ClosedCA = false;
       EPA__c epa=new EPA__c(Closed__c=False);
       insert epa;
      
       Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Approved_Budget_Backend__c=NULL,Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open'));
       insert ca; 
       
       test.starttest();
           List<String> List_ca=new list<String>();
           List_ca.add(ca.id);
           Cobra_Create_Update_ApprovedCA.Create_ApprovedCA(List_ca,'Create','wp');
           Cobra_Create_Update_ApprovedCA.Closed_CA(List_ca,'Closed');
       test.stoptest();
    }
    
     static testMethod void myUnitTest_FORUK() { 
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);
        HTTPResponse response = Cobra_Create_Update_ApprovedCA.Main_Header_Info('Check');
        //test.stoptest();
        
        Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
        insert plat;
        
        Account acc = new Account(Name='Test Account');
        insert acc; 
        
        Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
        
        User u =new User( alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=pro[0].id,country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor13@birlasoft.com.sys.dev');
               insert u;
       
       Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
       insert hierarchy;
       
       Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
       Program_Probability__c='100% - Funded');
       insert prog;
       
       Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
       
       opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
       
       //Class_TriggerCheck.IsPreventing = false;
       Sales_Request__c sr=new Sales_Request__c(Name='Test response5',Program__c=prog.id,Response_Description__c='Test desc',Windchill_US_UK__c='Windchill US', Pipeline_Opportunity__c = opp1.id);
       insert sr;
       
       Class_TriggerCheck.Var_SendBatch_ClosedCA = false;
       EPA__c epa=new EPA__c(Closed__c=False);
       insert epa;
     
       Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Approved_Budget_Backend__c=NULL,Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open'));
       insert ca; 
       
     test.starttest();  
       List<String> List_ca=new list<String>();
       List_ca.add(ca.id);
       
       //String op;
       //String wp;
       
       Cobra_Create_Update_ApprovedCA.Create_ApprovedCA(List_ca,'Create','wp');
       
       Cobra_Create_Update_ApprovedCA.Closed_CA(List_ca,'Closed');
       
       test.stoptest();
    }
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }    
}