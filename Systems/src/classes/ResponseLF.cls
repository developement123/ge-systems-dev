/******************************************************************************************************************************************
    Response LeadForm V2.0 Developed by Phillip Hughes.
    
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        09/13/2017      Removed Account_Platform_Name__c from SOQL
                                       Removed account platform with platform master lookup to get "Platform" name
                                       Removed below field from SOQL
                                       Pipeline_Opportunity__r.Account_Platform__r.Airframer__r.Name
*****************************************************************************************************************************************/

public with sharing class ResponseLF {            
   
   public string header{get;set;}
   public List<Sales_Request__c> cs{get;set;}               
         
    public ResponseLF(ApexPages.StandardController stdController) { 
         cs = new List<Sales_Request__c>(); 
    // Program_Name__c, removed from query
        for(Sales_Request__c sr :  [Select Id,  Response_ID_18_Chars__c, Response_ID2__c, Name, Internal_Yes_No__c,
                                                        Type__c, Deal_Type__c, Pricing_Type__c, Civil_Military__c,  Deal_Lane__c, Sales_Owner__r.Name, Response_Owner_Name__c,
                                                        Agreed_Customer_Due_Date__c, TG2_Schedule_Date__c, Estimate_Type__c, Manufacturing_Estimate__c, Engineering_Estimate__c,
                                                        Service_Estimate__c, RecordType.Name, Program__r.Platfrom__c
                                                        ,Pipeline_Opportunity__r.Account.Name,  Pipeline_Opportunity__r.Platform_New__r.Name
                                                        from Sales_Request__c
                                                        where id =: ApexPages.currentPage().getParameters().get('id')])  {       
           cs.add(sr);
       }
        if(!cs.isEmpty()){
            exportToCSV(cs[0]);
        }
    }

  public void exportToCSV(Sales_Request__c srCSV) {
       string ptype='';
       if(srCSV.Pricing_Type__c !=null){
        ptype = srCSV.Pricing_Type__c.replaceAll(',', ';');
       }
       header = 'Response Lead Form \n' ;
       header = header + 'Account Platform,' +srCSV.Pipeline_Opportunity__r.Account.Name.replaceAll(',', ';')+' '+ srCSV.Pipeline_Opportunity__r.Platform_New__r.Name.replaceAll(',', ';')  + '\n';
       header = header + 'Response ID,' + srCSV.Response_ID2__c + '\n'; 
       header = header + 'Platform,' + srCSV.Pipeline_Opportunity__r.Platform_New__r.Name.replaceAll(',', ';')  + '\n';
       header = header + 'Response Name,' + srCSV.Name + '\n';
       header = header + 'Response Type,' + srCSV.Type__c + '\n';
       header = header + 'Deal Type,' + srCSV.Deal_Type__c + '\n';
       header = header + 'System Timestamp,' + datetime.NOW() + '\n';
       header = header + 'Pricing Type,' + ptype + '\n';
       header = header + 'Civil/Military,' + srCSV.Civil_Military__c + '\n';
       header = header + 'Deal Lane,' + srCSV.Deal_Lane__c + '\n';
       header = header + 'Sales Owner,' + srCSV.Sales_Owner__r.Name + '\n';
       header = header + 'Response Owner,' + srCSV.Response_Owner_Name__c + '\n';
       header = header + 'Agreed Response Due (date),' + srCSV.Agreed_Customer_Due_Date__c + '\n';
       header = header + 'TG2 Functional Schedule Date,' + srCSV.TG2_Schedule_Date__c + '\n';
       header = header + 'Estimate Type (Manufacturing; Engineering; or Service),' + srCSV.Estimate_Type__c + '\n';
       header = header + 'Manufacturing Estimate,' + srCSV.Manufacturing_Estimate__c + '\n';
       header = header + 'Engineering Estimate,' + srCSV.Engineering_Estimate__c + '\n';
       header = header + 'Services Estimate,' + srCSV.Service_Estimate__c + '\n';
       header = header + 'Internal (Yes/No),' + srCSV.Internal_Yes_No__c + '\n';
       header = header + 'Account Name,'+srCSV.Pipeline_Opportunity__r.Account.Name.replaceAll(',', ';') ;
       
    }   
}