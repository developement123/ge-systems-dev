/*****************************************************************************************************
This test class was created to provide code coverage for the following class:
- CLONE_Batch_CA_Processing
Version     Author(s)               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0        Abhishek             04/29/2015        created a new method "myUnitTest_FORUK" to increase code coverage as per Jira 325. Code coverage got increased from 72%-97%         
           Abhishek             09/18/2017        removed account platform  
*****************************************************************************************************/    
@isTest
private class Test_CLONE_Batch_CA_Processing {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
         
      
        //HTTPResponse response =CREATE_UPDATE_HEADER_CA_RESPONSE.Main_Header_Info('Check');
    Class_TriggerCheck.IsPreventing = false;
    Class_TriggerCheck.IsWindchill_UpdatedCA = false;
    Class_TriggerCheck.Isclosed_CA = false;
    Class_TriggerCheck.Var_ShouldEPA_update = false;
    
    Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; // response callout
    Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false; // response callout
    Class_TriggerCheck.var_triggerOnUser = false; // user triggers
    Class_TriggerCheck.var_IsStopEPABatchTrg = false;
    Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
    Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
    Class_TriggerCheck.var_TriggerOnAccount = false;
    Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
    Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
    Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
    
    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
       
     Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
     insert plat;
     
     Account acc = new Account(Name='Test Account SSSSSSSSSSSSS');
     insert acc; 
     id bA_Profile_id;
     id Sys_admin_Profile_id;
     
   list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
   for(Profile p: pro)
   {
     if(p.name=='Systems Business Admin')
     {
        bA_Profile_id=p.Id;
     }
     else
     {
        Sys_admin_Profile_id=p.id;
     }
   }
     User u =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
               insert u;

     Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
     insert hierarchy;
    
     Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
     Program_Probability__c='100% - Funded');
     insert prog;
    
     opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
    
    Sales_Request__c sr=new Sales_Request__c(Windchill_US_UK__c ='Windchill US',Name='Test response5',Program__c=prog.id,Response_Description__c='Test desc', Pipeline_Opportunity__c = opp1.id);
    insert sr;
    
    EPA__c epa=new EPA__c(Closed__c=False);
    insert epa;
    
    List<Cost_Authorization__c> lstCA = new List<Cost_Authorization__c>();
   /*Description field not writable
    Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Description__c='Test desc',Approved_Budget_Backend__c=NULL,
                                                        FY_Spent_to_Date__c=NULL);*/
                                                        
    Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Approved_Budget_Backend__c=NULL,
                                                        FY_Spent_to_Date__c=NULL,Cost_Authorization_US_UK__c='Cost Authorization US');                                                  
    lstCA.add(ca);
    insert lstCA;
    
    string tempQuery = 'Select id from Cost_Authorization__c where Id = \''+ lstCA[0].id +'\'';
    
    test.starttest();
       
        Test.setMock(HttpCalloutMock.class, mock);
    
    CLONE_Batch_CA_Processing objCls = new CLONE_Batch_CA_Processing();
    objCls.query = tempQuery;
    objCls.start(null);
    objCls.execute(null, lstCA);
    objCls.finish(null);
    
    test.stoptest(); 
    
    }
    
    static testMethod void myUnitTest_FORUK() {
        // TO DO: implement unit test
      
        //HTTPResponse response =CREATE_UPDATE_HEADER_CA_RESPONSE.Main_Header_Info('Check');
        
     //HTTPResponse response =CREATE_UPDATE_HEADER_CA_RESPONSE.Main_Header_Info('Check');
    Class_TriggerCheck.IsPreventing = false;
    Class_TriggerCheck.IsWindchill_UpdatedCA = false;
    Class_TriggerCheck.Isclosed_CA = false;
    Class_TriggerCheck.Var_ShouldEPA_update = false;
    
    Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; // response callout
    Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false; // response callout
    Class_TriggerCheck.var_triggerOnUser = false; // user triggers
    Class_TriggerCheck.var_IsStopEPABatchTrg = false;
    Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
    Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
    Class_TriggerCheck.var_TriggerOnAccount = false;
    Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
    Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
    Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;

    
     Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
     insert plat;
     
     Account acc = new Account(Name='Test Account SSSSSSSSSSSSS');
     insert acc; 
     id bA_Profile_id;
     id Sys_admin_Profile_id;
     
   list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
   for(Profile p: pro)
   {
     if(p.name=='Systems Business Admin')
     {
        bA_Profile_id=p.Id;
     }
     else
     {
        Sys_admin_Profile_id=p.id;
     }
   }
     User u =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
               insert u;
          
     Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
     insert hierarchy;
    
     
    opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
   
    Sales_Request__c sr=new Sales_Request__c(Windchill_US_UK__c ='Windchill UK',Name='Test response5',Response_Description__c='Test desc', Pipeline_Opportunity__c = opp1.id);
    insert sr;
    
    EPA__c epa=new EPA__c(Closed__c=False);
    insert epa;
    
    List<Cost_Authorization__c> lstCA = new List<Cost_Authorization__c>();
 
                                                        
    Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Approved_Budget_Backend__c=NULL,
                                                        FY_Spent_to_Date__c=NULL,Cost_Authorization_US_UK__c='Cost Authorization US');                                                  
    lstCA.add(ca);
    insert lstCA;
    
    string tempQuery = 'Select id from Cost_Authorization__c where Id = \''+ lstCA[0].id +'\'';
    
    test.starttest();
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);
    
    CLONE_Batch_CA_Processing objCls = new CLONE_Batch_CA_Processing();
    objCls.query = tempQuery;
    objCls.start(null);
    objCls.execute(null, lstCA);
    objCls.finish(null);
    
    test.stoptest(); 
    
    }
}