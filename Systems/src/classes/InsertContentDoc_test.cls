@isTest
private class InsertContentDoc_test{
static testmethod void NewContentUpdate(){
 
    Test.starttest();
    recordtype rt = [select id from RecordType where SobjectType='Apttus__APTS_Agreement__c' and DeveloperName='GE_Digital_Services' Limit 1];
    Apttus__APTS_Agreement__c agr = new Apttus__APTS_Agreement__c(Name = 'Test Agreement100', RecordTypeId=rt.Id, Apttus__Contract_Start_Date__c=System.today(), Apttus__Contract_End_Date__c=System.today(), Apttus__Description__c='Test Description');
    insert agr;
   // Apttus__APTS_Agreement__c agr1 = [select id from Apttus__APTS_Agreement__c where name='Test Agreement100' Limit 1];

    ContentVersion content=new ContentVersion(); 
            content.Title='Header_Picture1'; 
            content.PathOnClient='/' + content.Title + '.jpg'; 
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob; 
            //content.LinkedEntityId=sub.id;
            content.origin = 'H';
          //  content.Agreement__c=agr1.Id;
        insert content;
        
       // ContentVersion content1 = [select id from ContentVersion where title='Header_Picture1' limit 1];
    content.agreement__c=agr.id;
    update content;
    Test.stoptest();
}
}