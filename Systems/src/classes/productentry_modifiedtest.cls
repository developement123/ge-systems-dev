/*************************************************************************************************************************************************
This test class has been modified to provide code coverage for the following new class:
    - OpportunityProductLineItemsExt_new
    
- Version     Author(s)    Date          
-----------------------------------------------------------------------------------------------------------------
    1.0         Jhansi      12/30/2015   
               
*************************************************************************************************************************************************/          
@istest
public class productentry_modifiedtest {
         
    static testmethod void mytest() {   
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;  
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;
        Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
     
      Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
        insert plat;

        Account acc = new Account(Name = 'Test Account');
        insert acc;

        Contact con = new Contact(Account = acc, MailingCity = 'Gurgaon', MailingCountry = 'India', Email = 'neha.mittal@birlasoft.com', LastName = 'Test1', FirstName = 'FirstName1');
        insert con;

        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo', email = 'sunny.kapoor21@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor21', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor2111@birlasoft50.com.sys.dev');
        insert u;

        Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Digital Solutions', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
        insert hierarchy;
        
        Product_Hiearchy_Object__c hierarchy_OE = new Product_Hiearchy_Object__c(Name='OE / Services', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
        insert hierarchy_OE;

        opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(),  Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id, Offering_Type__c ='OE / Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id,Contract_Term__c =3);
        Class_TriggerCheck.Var_isProgam_Probability_change=false;
        insert opp;
        
        opportunity opp2 = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(),  Firm_Anticipated__c = 'Anticipated',Offering_Type__c = 'Digital Solutions', Platform_New__c = plat.id, accountId= acc.id, GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Term__c = 3);
        Class_TriggerCheck.Var_isProgam_Probability_change=false;
        insert opp2;
             
        Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
        insert p2;
        
        Product2 p3 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
        insert p3;
        
        Id standardPBId = Test.getStandardPricebookId(); 
           
        Pricebook2 pb = new Pricebook2(Name = 'Custom Price Book', Description = 'Price Book 2009 Products', IsActive = true);
        insert pb;

      Opportunity_Line_Item__c ol2 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
      Probability__c = 111,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D');
      insert ol2;
      
      
      Opportunity_Line_Item__c ol3 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
      Probability__c = 111,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=11,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D');
      insert ol3;
       Test.startTest();   
       
       
       PageReference pageRef = Page.OpportunityProductLineItems_New;                           
       Test.setCurrentPage(pageRef );           
       ApexPages.CurrentPage().getparameters().put('id', ol2.Opportunity__c);
       ApexPages.StandardController sc = new ApexPages.StandardController(opp);
       OpportunityProductLineItemsExt_new controller = new OpportunityProductLineItemsExt_new(sc);
       ApexPages.CurrentPage().getparameters().put('id', ol3.Opportunity__c);
       ApexPages.StandardController sc1 = new ApexPages.StandardController(opp);
       OpportunityProductLineItemsExt_new controller1 = new OpportunityProductLineItemsExt_new(sc1);
      

       controller.setString('Testing');
       PageReference page1= controller.removeFromShoppingCart();
       PageReference page2= controller.onSave();
       PageReference page3= controller.onCancel();
       PageReference page34 = controller.customSave();
 
        if(UserInfo.isMultiCurrencyOrganization())
            System.assert(controller.getChosenCurrency()!='');
        else
            System.assertEquals(controller.getChosenCurrency(),'');

        Integer startCount = controller.ShoppingCart.size();
        controller.ShoppingCart.add(ol2);
        controller.ShoppingCart.add(ol3);
        system.assert(startCount==0);
        controller.TempStr = '';
        controller.showAvailableProductList();
 
        system.assert(controller.AvailableFilterProducts.size()==0);
        
        controller.TempStr = p2.Id + ';' + p2.Name + ';' + p2.ProductCode + ';' +
                        p2.Description + ';' + p2.Product_Area__c + ';' +
                        p2.Product_Family__c + ';' + p2.Product_Family_Child__c + ';' + p2.Lead_Time__c + ';' + p2.Is_Tokenized__c;
        controller.showAvailableProductList();
   
        controller.removeFromShoppingCart();
      
        controller.onSave();
        controller= new OpportunityProductLineItemsExt_new(new ApexPages.StandardController(opp));       
        controller = new OpportunityProductLineItemsExt_new(new ApexPages.StandardController(opp2));
       
        controller.TempStr = p2.Id + ';' + p2.Name + ';' + p2.ProductCode + ';' +
                        p2.Description + ';' + p2.Product_Area__c + ';' +
                        p2.Product_Family__c + ';' + p2.Product_Family_Child__c + ';' + p2.Lead_Time__c + ';' + p2.Is_Tokenized__c;
        controller.showAvailableProductList();
       
        try {
             controller.toSelect = controller.AvailableFilterProducts[0].prdId;
             controller.addToShoppingCart1();
             
            }
        catch (ListException ex)
         {
            system.debug('List Exception'+ ex);
         } 
                       
        controller.onSave();
        
        // add required info and try save again
        for(Opportunity_Line_Item__c o : controller.ShoppingCart){
            o.Quantity__c = 5;
            o.Unit_Price__c=1;
                    
        }
        controller.onSave();
        
        // query line items to confirm that the save worked
        Opportunity_Line_Item__c[] oli2 = [select Id from Opportunity_Line_Item__c where Opportunity__c = :opp.id];
        
        // test on new Opp (no pricebook selected) to make sure redirect is happening
        
        opportunity newOpp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(),  Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id, GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id);
        Class_TriggerCheck.Var_isProgam_Probability_change=false;
        Class_TriggerCheck.Var_IsProgramUPdated=false;
        insert(newOpp);
        controller = new OpportunityProductLineItemsExt_new(new ApexPages.StandardController(newOpp));
        controller.setString('Test');
        controller.getString();
        
        test.stopTest();
        
        ////////////////////////////////////////
        //  test redirect page
        ////////////////////////////////////////
    }
    
    static testmethod void mytest2() {   
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;  
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;
        Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
     
      Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
        insert plat;

        Account acc = new Account(Name = 'Test Account');
        insert acc;

        Contact con = new Contact(Account = acc, MailingCity = 'Gurgaon', MailingCountry = 'India', Email = 'neha.mittal@birlasoft.com', LastName = 'Test1', FirstName = 'FirstName1');
        insert con;

        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo', email = 'sunny.kapoor21@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor21', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor2111@birlasoft50.com.sys.dev');
        insert u;
                
        Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Digital Solutions', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
        insert hierarchy;
        
        Product_Hiearchy_Object__c hierarchy_OE = new Product_Hiearchy_Object__c(Name='OE / Services', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
        insert hierarchy_OE;

        opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(),  Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id, Offering_Type__c ='OE / Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id,Contract_Term__c =3);
        Class_TriggerCheck.Var_isProgam_Probability_change=false;
        insert opp;
        
        opportunity opp2 = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(),  Firm_Anticipated__c = 'Anticipated',Offering_Type__c = 'Digital Solutions', Platform_New__c = plat.id, accountId= acc.id, GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Term__c = 3);
        Class_TriggerCheck.Var_isProgam_Probability_change=false;
        insert opp2;
        
        Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
        insert p2;
        
        Product2 p3 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
        insert p3;
        
        Id standardPBId = Test.getStandardPricebookId(); 
           
        Pricebook2 pb = new Pricebook2(Name = 'Custom Price Book', Description = 'Price Book 2009 Products', IsActive = true);
        insert pb;
      
      Opportunity_Line_Item__c ol2 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
      Probability__c = 111,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D');
      insert ol2;
      
      
      Opportunity_Line_Item__c ol3 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
      Probability__c = 111,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=11,of_Installments__c =111,Installment_Periods__c ='Annually', Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D');
      insert ol3;
     system.runAs(u){ 
      Test.startTest();   
 
        PageReference pageRef = Page.OpportunityProductLineItems_New;                           
       Test.setCurrentPage(pageRef );           
       ApexPages.CurrentPage().getparameters().put('id', ol2.Opportunity__c);
       ApexPages.StandardController sc = new ApexPages.StandardController(opp);
       OpportunityProductLineItemsExt_new controller = new OpportunityProductLineItemsExt_new(sc);
       ApexPages.CurrentPage().getparameters().put('id', ol3.Opportunity__c);
       ApexPages.StandardController sc1 = new ApexPages.StandardController(opp);
       OpportunityProductLineItemsExt_new controller1 = new OpportunityProductLineItemsExt_new(sc1);

      
       try{
       controller.GetFirst();
       }
       catch(exception ex){
           }
    try{
       controller.GetLast();
        }
       catch(exception ex){
           }
        try{   
       controller.GetPrevious();
        }
       catch(exception ex){
           }
    try{   
       controller.GetNext();
        }
       catch(exception ex){
           }
    try{   
       controller.RedirectOnPage();
        }
       catch(exception ex){
           }
           
           
            
        Test.StopTest();   
     }
     
    }
}