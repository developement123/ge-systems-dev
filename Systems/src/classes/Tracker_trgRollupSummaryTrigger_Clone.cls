/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - trgRollupSummaryTrigger_Clone

    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          13/April/2014        Initial Creation
            
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_trgRollupSummaryTrigger_Clone {
/*
    static testmethod void test_trgRollupSummaryTrigger() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;        
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;     

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;
            
            List<Opportunity> listOfOppy = new List<Opportunity>();

            opportunity opp = new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(), MTG_Year__c = String.valueOf(System.Today().Year()), Growth__c = true);

            listOfOppy.add(opp);
            
            opportunity opp1 = new opportunity(Name = 'Test opp2', StageName = 'Contract Awarded', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(), MTG_Year__c = String.valueOf(System.Today().Year()), Growth__c = true);
            
            listOfOppy.add(opp1);                 
                                      
            Test.startTest(); 
                Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = true;
                insert listOfOppy;
                
                opp.amount = 100;
                Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = true; 
                update opp; 
                
                Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = true; 
                delete listOfOppy;
                
            Test.stopTest();   
        }
    } */
}