/*************************************************************************************************************************************************
This test class was created to provide code coverage for the following Trigger:
- Trg_SendBatchOf_Outbound_ClosedCA

Version      last modified by               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0           Mohan kumar                  10/17/2014        initial creation
              Abhishek                     04/18/2016        because of USER trigger(Jira 546), this test class was throwing "uncommitted work pending" error, to avoid this we set "var_triggerOnUser" to false
              Abhishek                     09/18/2016        removed account platform 
*************************************************************************************************************************************************/  

@isTest
public class Tracker_Trg_SendBatchOf_Outbound_CA {
    static testMethod void myUnitTest_US() {
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_TriggerOnResponse = false;
        Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);
        

     //   HTTPResponse response = CREATE_UPDATE_HEADER_CA_RESPONSE.Main_Header_Info('Check');

        Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
        insert plat;

        Account acc = new Account(Name = 'Test Account');
        insert acc;
        id bA_Profile_id;
        id Sys_admin_Profile_id;

        list < Profile > pro = [SELECT ID, name FROM PROFILE WHERE NAME = 'System Administrator'
            or Name = 'Systems Business Admin'];
        for (Profile p: pro) {
            if (p.name == 'Systems Business Admin') {
                bA_Profile_id = p.Id;
            } else {
                Sys_admin_Profile_id = p.id;
            }
        }
        User u = new User(alias = 'skapo', email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = Sys_admin_Profile_id, country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
        insert u;

        User u2 = new User(alias = 'skapo', email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = bA_Profile_id, country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12453.com.sys.dev');
        insert u2;

        Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
        insert hierarchy;

        Program__c prog = new Program__c(Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
            Program_Probability__c = '100% - Funded');
        insert prog; 
        
        opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
        
        Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response5', Program__c = prog.id, Response_Description__c = 'Test desc', Pipeline_Opportunity__c = opp1.id);
        insert sr;

        EPA__c epa = new EPA__c(Closed__c = False, Kronos_Instance__c = 'US');
        insert epa;

        List < Cost_Authorization__c > lstCA = new List < Cost_Authorization__c > ();

        Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = NULL,
            FY_Spent_to_Date__c = NULL, Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open') );
        lstCA.add(ca);
        
        insert lstCA;        
        
        Test.startTest();
            Class_TriggerCheck.var_IsStopEPABatchTrg = false;
            epa.Closed__c = true;
            update epa;
        Test.stopTest();
        
    }
    static testMethod void myUnitTest_UK() {
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_TriggerOnResponse = false;
        Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);
        

     //   HTTPResponse response = CREATE_UPDATE_HEADER_CA_RESPONSE.Main_Header_Info('Check');

        Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
        insert plat;

        Account acc = new Account(Name = 'Test Account');
        insert acc;
        id bA_Profile_id;
        id Sys_admin_Profile_id;

        list < Profile > pro = [SELECT ID, name FROM PROFILE WHERE NAME = 'System Administrator'
            or Name = 'Systems Business Admin'];
        for (Profile p: pro) {
            if (p.name == 'Systems Business Admin') {
                bA_Profile_id = p.Id;
            } else {
                Sys_admin_Profile_id = p.id;
            }
        }
        User u = new User(alias = 'skapo', email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = Sys_admin_Profile_id, country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
        insert u;

        User u2 = new User(alias = 'skapo', email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = bA_Profile_id, country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12453.com.sys.dev');
        insert u2;

        Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
        insert hierarchy;
        
        opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
        
        Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill UK', Name = 'Test response5',  Response_Description__c = 'Test desc', Pipeline_Opportunity__c = opp1.id);
        insert sr;

        EPA__c epa = new EPA__c(Closed__c = False, Kronos_Instance__c = 'UK');
        insert epa;

        List < Cost_Authorization__c > lstCA = new List < Cost_Authorization__c > ();

        Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = NULL,
            FY_Spent_to_Date__c = NULL, Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open') );
        lstCA.add(ca);
        
        insert lstCA;        
        
        Test.startTest();
            Class_TriggerCheck.var_IsStopEPABatchTrg = false;
            epa.Closed__c = true;
            update epa;
        Test.stopTest();
    }
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }
}