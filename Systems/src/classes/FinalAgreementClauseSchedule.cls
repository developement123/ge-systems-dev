global class FinalAgreementClauseSchedule implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
        
        // We now call the batch class to be scheduled
        FinalAgreementClause f = new FinalAgreementClause ();
        
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(f,1000);
    }
    
}