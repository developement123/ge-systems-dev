/*************************************************************************************************************************************************
    This test class was created to provide code coverage for Trigger "TriggeronOrder" on Order object and 
    handler class "OrderTriggerHandler"

    Version     Written by     last modified Date          
    -------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------
    1.0         Jhansi           5/11/2015   
                Abhishek         07/19/2016   COntrac change 
                Abhishek         09/18/2016   removed account platform
*************************************************************************************************************************************************/
@isTest
public class Tracker_TriggerOnOrder {
    
            private static testmethod void testautoGenerateOrderName() {
            
            Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
           User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
             insert u;

            Account acc = new Account(Name = 'GE GEAS');
            insert acc;
         
            System.runAs(u) {  
            Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
            insert pm;
                
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
            insert hierarchy;

            Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
            insert p2;
             
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                            MailingCity='Delhi',
                            MailingCountry='India');
            insert con;
            
            opportunity opp = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services');
            insert opp;
                            
            Contract_PO__c Cont = new Contract_PO__c(Account_Name__c = acc.id,Pipeline_Opportunity__c=opp.id,Legal__c= con.id,Program_Manager__c = u.id,Finance__c = con.id,Status__c = 'Draft',Last_Order_Number__c = 111,Number_of_PO_s__c = 111,First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual',Revenue_Increment__c = 'Annually', First_PO_Ship_Date__c = system.today());
            insert Cont;

            Order__c order = new Order__c(Name = 'Test12345',Contract1__c = Cont.id,PO_Schedule_Opportunity__c = opp.id);
            insert order;
            

            }
    
        }
        
            private static testmethod void testautoGenerateOrderName1() {
            
            Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
           User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
             insert u;

            Account acc = new Account(Name = 'GE GEAS');
            insert acc;
         
            System.runAs(u) {  
            Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
            insert pm;
                
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
            insert hierarchy;

            Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
            insert p2;
             
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                            MailingCity='Delhi',
                            MailingCountry='India');
            insert con;
            
            opportunity opp = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services');
            insert opp;
                            
          
            
            Contract_PO__c Cont1 = new Contract_PO__c(Account_Name__c = acc.id,Pipeline_Opportunity__c=opp.id,Legal__c= con.id,Program_Manager__c = u.id,Finance__c = con.id,Status__c = 'Draft',Last_Order_Number__c = null,Number_of_PO_s__c = 12,First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual',Revenue_Increment__c = 'Annually', First_PO_Ship_Date__c = system.today());
            insert Cont1;
            
            Order__c order1 = new Order__c(Name = 'Test12345',Contract1__c = Cont1 .id,PO_Schedule_Opportunity__c = opp.id, status__c='PO Booked');
            insert order1;
            
                try{
                    //delete reupsertconlist;
                    delete order1;
                 }
                 catch(exception e){
                    boolean isException = e.getMessage().contains('PO') ? true : false ;
                    system.assertEquals(isException , true);
                 }
            
            
            }
    
        }
        

    }