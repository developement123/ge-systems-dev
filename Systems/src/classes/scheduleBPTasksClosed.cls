/*
        Developer : Abhishek
        Date: December 2016   
        Action : This is a scheduled class to close B&P task on every Monday and recall approval records if any. 
*/

global class scheduleBPTasksClosed implements Schedulable {
global void execute(SchedulableContext SC) {
date myDate = date.today(); 
//date myDate = date.newinstance(2016, 12, 29);
date weekStart = myDate.toStartofWeek();
system.debug(UserInfo.getLocale());
date myWeekdate;
datetime myCurrentWeekdateInGMT;
if(UserInfo.getLocale()=='en_US')
     myWeekdate = weekStart.addDays(1);
else
    myWeekdate = weekStart;

system.debug(myWeekdate);

Time myTime = Time.newInstance(23, 0, 0, 0);

myCurrentWeekdateInGMT = DateTime.newInstanceGmt(myWeekdate,myTime);
system.debug('in GMT ' + myCurrentWeekdateInGMT);
Date responseSubmitDate = myCurrentWeekdateInGMT.date();

set<id>pipelineOppids = new set<id>();
set<id>responseIds = new set<id>();
set<id>bpIds = new set<id>();
for(sales_request__c responseLastweek : [select id, Pipeline_Opportunity__c,Response_Submitted__c from sales_request__c where createddate<=:myCurrentWeekdateInGMT]){
    if(responseLastweek.Pipeline_Opportunity__c!=null)
        pipelineOppids.add(responseLastweek.Pipeline_Opportunity__c);
}

for(sales_request__c responseSubmitted : [select id, Pipeline_Opportunity__c,Response_Submitted__c from sales_request__c where Response_Submitted__c<=:responseSubmitDate]){
    if(responseSubmitted.Response_Submitted__c!=null)
        responseIds.add(responseSubmitted.id);
}

list<Cost_Authorization__c> listOfBPToUpdate = new list<Cost_Authorization__c>();
if( !pipelineOppids.isEMpty()){
    for(Cost_Authorization__c costAUth : [Select id, Close_Cost_Authorization__c from Cost_Authorization__c where Pipeline_Opportunity__c =: pipelineOppids and Status__c!='Closed' and Phase__c = 'Phase 1' ]){
        costAUth.Close_Cost_Authorization__c = true;
        listOfBPToUpdate.add(costAUth);
        bpIds.add(costAUth.id);
    }
}
if(!responseIds.isEMpty()){
    for(Cost_Authorization__c costAUthres : [Select id, Close_Cost_Authorization__c from Cost_Authorization__c where Sales_Request__c =:responseIds and Phase__c = 'Phase 2' and Status__c!='Closed' ]){
        costAUthres.Close_Cost_Authorization__c = true;
        listOfBPToUpdate.add(costAUthres);
        bpIds.add(costAUthres.id);
    }
}

if(!listOfBPToUpdate.isEmpty()){
        list<Approval.ProcessWorkitemRequest> reqList = new list<Approval.ProcessWorkitemRequest>();
     for(ProcessInstanceWorkitem pwi : [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: bpIds]){
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setAction('Removed');        
            req.setWorkitemId(pwi.Id);
            reqList.add(req);
       }
      if(!reqList.isEmpty()) 
          Approval.process(reqList,false);
          
    update listOfBPToUpdate;      
  }
}
}