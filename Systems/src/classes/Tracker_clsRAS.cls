/*************************************************************************************************************************************************
    This test class was created to provide code coverage to the following classes:
    - clsRAS
    - RedirectToRASController

    Version        createdBy                      Date                            Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
     1.0            Mohan kumar                  14/April/2015                   initial creation
                    Abhishek                     18/Sep/2017                     removed account platform
     last modified by: Mohan Kumar(13 august 2015 for JIRA 441).   
     last modified by: Abhishek (02/29/2016 for JIRA 498 )
*************************************************************************************************************************************************/     

@istest
private class Tracker_clsRAS {  

    public static Sales_Request__c sr;  
    public static Sales_Request__c srContractWithoutPo;  
    
     //   following method 'testData' is to create the test data that can be used in following methods.    
    
    public static void testData() {  
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;  
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;
        Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
        
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        Class_TriggerCheck.var_TriggerOnContract = false;
        Class_TriggerCheck.var_TriggerOnResponse = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
        Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
        
        
        Profile[] pro = [select Id from profile where Name ='System Administrator' limit 1];
         
        User u = new User( alias = 'skapo12', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', 
                        lastname =  'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', 
                        profileid=pro[0].id, country = 'India', timezonesidkey = 'Europe/London', 
                        username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc = new Account(Name = 'GE GEAS');
        insert acc;
        
        //inserting contact
       Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                                MailingCity='Delhi', MailingCountry='India', email = 'abhishek3.singh@ge.com');
       insert con; 
        
        System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military',
                                                           Segment__c='Regional Transports');
            insert pm;
                
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', 
                                                   Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Program__c prog = new Program__c(Lookup_hiearchyname__c = hierarchy.id,Product_Sales_Owner__c = u.id, 
                                            Program_Probability__c = '100% - Funded');
            insert prog;                
            
            
            
            List<Opportunity> listOfOppy = new List<Opportunity>();
            for(integer i = 0; i < 20; i++) {
            // added Account_Platform__c for JIRA 498
                opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                            Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                            GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,
                            Contract_Award_Date__c = date.today(), Margin_Escalator__c = true, Platform_New__c = pm.id, accountId= acc.id, Offering_Type__c = 'OE / Services');
                listOfOppy.add(opp);
            }
            insert listOfOppy; 
            
            // insert Response
            list<Sales_Request__c> allSalesRequest = new list<Sales_Request__c>();
            for(integer m=0;m<=2;m++){
                Sales_Request__c sr1 = new Sales_Request__c(Windchill_US_UK__c ='Windchill US', Name = 'Testresponse5', Response_Description__c = 'Test desc',Pipeline_Opportunity__c = listOfOppy[m].id);
                allSalesRequest.add(sr1);
            }
            insert allSalesRequest;
            
            sr = [select id, Name, Response_Description__c, Pipeline_Opportunity__c from Sales_Request__c where id=:allSalesRequest[0].id][0];
            
            srContractWithoutPo = [select id, Name, Response_Description__c, Pipeline_Opportunity__c from Sales_Request__c where id=:allSalesRequest[1].id][0];
            
            list<Contract_PO__c> listOfContractNeedToInsert = new list<Contract_PO__c>();
            list<Opportunity> opportunityToUpdate = new list<Opportunity>();
            Contract_PO__c ContrWithOutPoForecast = new Contract_PO__c (Account_Name__c = acc.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = listOfOppy[1].id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual'
                                       , First_PO_Ship_Date__c  = system.today()
                                       , Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false
                                       );
             listOfContractNeedToInsert.add(ContrWithOutPoForecast);
            
             
             Contract_PO__c ContrWitForecast = new Contract_PO__c (Account_Name__c = acc.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = listOfOppy[2].id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual'
                                       , First_PO_Ship_Date__c  = system.today()
                                       , Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false 
                                       );   
             listOfContractNeedToInsert.add(ContrWitForecast);  
             
             insert listOfContractNeedToInsert;
             
             
             for(integer k=1;k<=2;k++){
                 Opportunity myOpp = new Opportunity(id=listOfOppy[k].id);
                 myOpp.ContractID1__c = listOfContractNeedToInsert[k-1].id;
                 opportunityToUpdate.add(myOpp);
             }
             update opportunityToUpdate;
             
            
                                       
        }    
    }   
   
     //   following test method 'myTest_clsRAS' is used to provide the test coverage to 2 classes. 'RedirectToRASController' and 'clsRAS'.    
    
    static testMethod void myTest_clsRAS_WithoutContract() {     
        testData();                 
             
        Test.startTest();            
            Pagereference pageref = System.currentPageReference();
            pageref.getParameters().put('id', sr.id); 
            pageref.getParameters().put('isTokenizeVariable', 'true');  
            Test.setCurrentPageReference(pageref);
            
            ApexPages.StandardController obj2 = new ApexPages.StandardController(sr);
            RedirectToRASController controller2 = new RedirectToRASController(obj2);
            
            ApexPages.StandardController obj = new ApexPages.StandardController(sr);
            clsRAS controller = new clsRAS(obj); 
            
            Pagereference pageref1 = System.currentPageReference();
            pageref1.getParameters().put('id', sr.id); 
            pageref1.getParameters().put('isTokenizeVariable', 'false');  
            pageref1.getParameters().put('var6', 'XYZ,ewrw,435sdfsd');  
            Test.setCurrentPageReference(pageref1);
            
            ApexPages.StandardController obj1 = new ApexPages.StandardController(sr);
            clsRAS controller1 = new clsRAS(obj1);
            
            Pagereference pageref2 = System.currentPageReference();
            pageref2.getParameters().put('id', srContractWithoutPo.id); 
            pageref2.getParameters().put('isTokenizeVariable', 'false');  
            pageref2.getParameters().put('var6', 'XYZ,ewrw,435sdfsd');  
            Test.setCurrentPageReference(pageref2);
            
            ApexPages.StandardController obj3 = new ApexPages.StandardController(sr);
            clsRAS controller3 = new clsRAS(obj3);
            
            

            
        Test.stopTest();        
    }
    
     static testMethod void myTest_clsRAS_WithContractNoPO() {     
        testData();                 
             
        Test.startTest();            
            
            
            Pagereference pageref2 = System.currentPageReference();
            pageref2.getParameters().put('id', srContractWithoutPo.id); 
            pageref2.getParameters().put('isTokenizeVariable', 'false');  
            pageref2.getParameters().put('var6', 'XYZ,ewrw,435sdfsd');  
            Test.setCurrentPageReference(pageref2);
            
            ApexPages.StandardController obj3 = new ApexPages.StandardController(srContractWithoutPo);
            clsRAS controller3 = new clsRAS(obj3);
            
            

            
        Test.stopTest();        
    } 
}