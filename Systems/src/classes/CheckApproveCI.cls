/*
* ClassName: CheckApproveCI
* CreatedBy: Venkata Krishna Koppula
* LastModifiedBy: sairam
* LastModifiedOn: 29 November 2012
* CalledFrom: SubmitCI Button
* Description: This class is copying the Record from Submit CI to Approve CI object including Attachments
* ---------------------------------------
* Revision History
* 21 May 2012 – Initial Creation
* changes modified: added a OCI__c (Owner of competitive Information field) in SOQL
*changes modified: added a Submitter_Email_ID__C (Submitter Email Id) in SOQL. This Field is added in SOQL because, If Approver 
*                    wants more information from Submitter he can get in touch through this email
* Lastmodified Date : 30-09-2015
* Reason : Added 2 fields ( Is_Export_Control_Data__c, Confirm_do_not_have_Export_Control_Data__c ) in SOQL quieries
*/

Global class CheckApproveCI{

    WebService static Integer CopySubmitCIToApproveCI(Id idSubmitCI)
    {
        User usr=UserData.getUserInfo();
            //create instance of Attachment Object
        List<Attachment> listNewAttachment=new List<Attachment>();
            //pulling the values from Submit CI record
        Submit_CI__c objSubmitCI = [SELECT Id, Name, Is_Export_Control_Data__c,Confirm_do_not_have_Export_Control_Data__c,Business_Relevant__c, OwnerId, Submitted_By__c, CIS__c, Status__c, AONP__c, DACPM__c, DCIRCBP__c, DCIRGMSSP__c, DCIRO__c, 
                                    DCIRUSFGMP__c, DGMCA__c, WCIRPF__c , LCIRO__c, SOCI__c, IsEmailServiceRecord__c, Submitter_SSO__c, SOCOR__c, OCI__c, Submitter_Email_ID__c
                                    FROM Submit_CI__c WHERE Id =: idSubmitCI];
                                    
            //Checking the condition in Submit CI record
            // Can insert record into Approve CI if status is null or Submitted
            if(objSubmitCI.Status__c == null || objSubmitCI.Status__c == 'Submitted')
            {
                //Checking the condition if this record is created through email services
                if( objSubmitCI.IsEmailServiceRecord__c ==false && (objSubmitCI.Submitter_SSO__c == null || objSubmitCI.Submitter_SSO__c == ''))
                {
                    //setting the flag to True  
                    if(utility.SCIupdateFlag==true)
                    {
                        //insering details received from email 
                        objSubmitCI.Status__c = 'Submitted';
                        objSubmitCI.Submitted_By__c = usr.Name;
                        objSubmitCI.Submitter_Email_ID__c= usr.email;
                        objSubmitCI.Submitter_SSO__c = usr.FederationIdentifier;
                        objSubmitCI.Submission_Date__c = System.today();
                        utility.SCIupdateFlag=false; 
                    }
                    //Update record
                    update objSubmitCI;
                }
                
                //create new instance of Approve CI object to copy the Submit CI record
                //below SOQL is ensuring that whether a record is exsisting with the SubmitCI record
                List<Approve_CI__c> listExistAppCI = [SELECT Id FROM Approve_CI__c WHERE Name =:objSubmitCI.Name];
                
                //declaring a variable
                Approve_CI__c objExistAppCI;
                List<Attachment> listAttachmentExist;
                
                //checking if any record is available 
                    if(listExistAppCI!=null && listExistAppCI.size()>0)
                       objExistAppCI = [SELECT Id 
                                        FROM Approve_CI__c 
                                        WHERE Name =:objSubmitCI.Name];
                    
                    //declaring a variable                  
                    Approve_CI__c objAppCI;
                    //Checking condition
                    if(listExistAppCI!=null && listExistAppCI.size()>0)
                    //if record exsists it will update the exsisting record
                       objAppCI = new Approve_CI__c(id=objExistAppCI.Id,
                                                    Name=objSubmitCI.Name,
                                                    Business_information__c=objSubmitCI.Business_Relevant__c,
                                                    Submitted_By__c=objSubmitCI.Submitted_By__c,
                                                    CIS__c=objSubmitCI.CIS__C,
                                                    Submission_Date__c=System.today(),
                                                    Status__c='Submitted',
                                                    AONP__c=objSubmitCI.AONP__c,
                                                    DACPM__c=objSubmitCI.DACPM__c,
                                                    DCIRCBP__c=objSubmitCI.DCIRCBP__c,
                                                    DCIRGMSSP__c=objSubmitCI.DCIRGMSSP__c,
                                                    DCIRO__c=objSubmitCI.DCIRO__c,
                                                    DCIRUSFGMP__c=objSubmitCI.DCIRUSFGMP__c,
                                                    DGMCA__c=objSubmitCI.DGMCA__c,
                                                    WCIRPF__c=objSubmitCI.WCIRPF__c,
                                                    LCIRO__c=objSubmitCI.LCIRO__c,
                                                    SOCI__c=objSubmitCI.SOCI__c,
                                                    //for SSo Validation
                                                    SSSO__c=objSubmitCI.Submitter_SSO__c,
                                                    SOCOR__c=objSubmitCI.SOCOR__c,
                                                    OCI__c=objSubmitCI.OCI__c,
                                                    Submitter_Email_ID__c=objSubmitCI.Submitter_Email_ID__c,
                                                    Is_Export_Control_Data__c = objSubmitCI.Is_Export_Control_Data__c ,
                                                    Confirm_do_not_have_Export_Control_Data__c = objSubmitCI.Confirm_do_not_have_Export_Control_Data__c ); //Sai- Added Submitter Email ID 
                    else
                        //else create a new record
                           objAppCI = new Approve_CI__c(Name=objSubmitCI.Name,
                                                        Business_information__c=objSubmitCI.Business_Relevant__c,
                                                        Submitted_By__c=objSubmitCI.Submitted_By__c,
                                                        CIS__c=objSubmitCI.CIS__C,
                                                        Submission_Date__c=System.today(),
                                                        Status__c='Submitted',
                                                        AONP__c=objSubmitCI.AONP__c,
                                                        DACPM__c=objSubmitCI.DACPM__c,
                                                        DCIRCBP__c=objSubmitCI.DCIRCBP__c,
                                                        DCIRGMSSP__c=objSubmitCI.DCIRGMSSP__c,
                                                        DCIRO__c=objSubmitCI.DCIRO__c,
                                                        DCIRUSFGMP__c=objSubmitCI.DCIRUSFGMP__c,
                                                        DGMCA__c=objSubmitCI.DGMCA__c,
                                                        WCIRPF__c=objSubmitCI.WCIRPF__c,
                                                        LCIRO__c=objSubmitCI.LCIRO__c,
                                                        SOCI__c=objSubmitCI.SOCI__c,
                                                        //for SSo Validation
                                                        SSSO__c=objSubmitCI.Submitter_SSO__c,
                                                        SOCOR__c=objSubmitCI.SOCOR__c,
                                                        OCI__c=objSubmitCI.OCI__c,
                                                        Submitter_Email_ID__c=objSubmitCI.Submitter_Email_ID__c,
                                                        Is_Export_Control_Data__c = objSubmitCI.Is_Export_Control_Data__c ,
                                                        Confirm_do_not_have_Export_Control_Data__c = objSubmitCI.Confirm_do_not_have_Export_Control_Data__c); //Sai- Added Submitter Email ID
                        Upsert objAppCI;
                
                    //finding the attachmements
                    if(listExistAppCI!=null && listExistAppCI.size()>0)
                        listAttachmentExist = [SELECT Id,
                                                      ParentId, 
                                                      Name, 
                                                      IsPrivate, 
                                                      ContentType, 
                                                      BodyLength, 
                                                      Body, 
                                                      OwnerId,
                                                      CreatedDate, 
                                                      CreatedById, 
                                                      LastModifiedDate, 
                                                      LastModifiedById, 
                                                      SystemModstamp, 
                                                      Description 
                                               FROM Attachment 
                                               WHERE ParentId=:objExistAppCI.Id];
                    List<Attachment> listAttachment = [SELECT Id,
                                                              ParentId, 
                                                              Name, 
                                                              IsPrivate, 
                                                              ContentType, 
                                                              BodyLength,
                                                              Body, 
                                                              OwnerId,
                                                              CreatedDate, 
                                                              CreatedById, 
                                                              LastModifiedDate, 
                                                              LastModifiedById, 
                                                              SystemModstamp, 
                                                              Description 
                                                       FROM Attachment 
                                                       WHERE ParentId=:idSubmitCI];
                                                   
                    if(listAttachmentExist!=null && listAttachmentExist.size()>0)
                    {
                        utility.CIAttachmentDelFlag=true;
                        delete listAttachmentExist;
                    }
                    //inserting attachments
                    if(listAttachment!=null && listAttachment.size()>0)
                    {    
                        for(Attachment recAtt:listAttachment)
                        {
                            Attachment newAtt=new Attachment(ParentId=objAppCI.Id,
                                                             Name=recAtt.Name,
                                                             IsPrivate = recAtt.IsPrivate,
                                                             ContentType=recAtt.ContentType,
                                                             Body=recAtt.Body,
                                                             OwnerId=recAtt.OwnerId,
                                                             Description=recAtt.Description);
                            listNewAttachment.add(newAtt);
                        }
                    }
                    if(listNewAttachment!=null && listNewAttachment.size()>0)
                    {
                        utility.CIAttachmentFlag=true;
                        insert listNewAttachment;

                    } 
                return 0;
            }
        else
            return 1;
    }//CopySubmitCIToApproveCI
}//CheckApproveCI