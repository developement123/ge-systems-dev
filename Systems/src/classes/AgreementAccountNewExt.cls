/********************************************************************************************************
 * Class Name       :   AgreementAccountNewExt
 * Created By       :   Manjari Singh
 * LastModifiedBy   :   
 * LastmodifiedOn   :   
 * Description      :   AgreementAccountNewExt
 * --------------------------------------------------------------------------------
 * Revision History
 * January 2017 - Initial creation
 
 ****************************************************************************************************/

public class AgreementAccountNewExt {    
    
    public Apttus__APTS_Agreement__c Agr {get;set;}
    //private list<recordtype> lstRec {get;set;}
    private list<account> lstAcc {get;set;}
    private list<Contract_PO__c> lstContr {get;set;}
    private list<recordtype> recdevelopername{get; set;}
    private String accName{get; set;}

    public AgreementAccountNewExt(ApexPages.StandardController controller) {
    
    this.Agr =  (Apttus__APTS_Agreement__c )controller.getrecord();
   
    recdevelopername = [select Name from recordtype where id =:agr.recordtypeid]; 
  
    if(Agr.Related_Contract_PO__c != null)
    {
        lstContr = [select Contract_Start_Date__c, Contract_End_Date__c,Account_Name__c from Contract_PO__c where id =:Agr.Related_Contract_PO__c];
        Agr.Apttus__Contract_Start_Date__c = lstContr[0].Contract_Start_Date__c;
        Agr.Apttus__Contract_End_Date__c = lstContr[0].Contract_End_Date__c;
        Agr.Apttus__Account__c = lstContr[0].Account_Name__c;        
    }
    else
    Agr.Apttus__Contract_Start_Date__c=System.Today();
    if (Agr.Apttus__Account__c != null) 
    {
        lstAcc = [select name from account where id =:Agr.Apttus__Account__c];
        accName = lstAcc[0].name;
        if (accName.length() >25) {accName = accName.substring (0,25 );}
    }
  
    else
    {
        accName = '';
    }
    String strDate = string.valueof(System.today());
    recdevelopername = [select Name from recordtype where id =:agr.recordtypeid]; 
   
  // Agr.Name = accName + ' - ' + recdevelopername[0].name +  ' - '+ strdate ;
   Agr.Name = accName + ' - GE Digital - Services - '+ strdate ;
   
    
    }
   
   
   public PageReference saveRec() {
    try{
        
        insert agr;
        if(agr.id != null)
        return new PageReference ('/' + agr.id + '/e?cancelURL=%2Fapex%2Fapttus__cancelactioninterceptor%3FactionName%3Dcreate_new_agreement%26agreementId%3D'+ agr.id + '%26objectType%3Da0x%26rollbackId%3D' + agr.id + '&retURL=' + agr.id );
        
    }
    catch(Exception e){
    }
        return null;

    }
}