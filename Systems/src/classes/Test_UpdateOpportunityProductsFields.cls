@istest(SeeAllData=true)
private class Test_UpdateOpportunityProductsFields
{
    static testMethod void UpdateOpportunityProductsFields1()
    {
        Application__c app = new Application__c();
        app.name = 'TestApplication';
        insert app;
        /*Insert Account*/
        Account accnt = new Account(name='TestAccount');
        insert accnt;
        
         List<Custom_Setting_NewVsSustaininf__c> customStng1 = Custom_Setting_NewVsSustaininf__c.getall().values();
        boolean chekbool1 = true;
        for(Custom_Setting_NewVsSustaininf__c cslist:customStng1)
         {
                if(cslist.Name == 'value')
                {
                        chekbool1 = false;
                }
                else
                {
                }
         }
        if(chekbool1 == true)
        {
            Custom_Setting_NewVsSustaininf__c NewVsSustaininf = new Custom_Setting_NewVsSustaininf__c();
            NewVsSustaininf.name = 'value';
            NewVsSustaininf.Both__c = 'Both';
            insert NewVsSustaininf;
        }
        else
        {
        }
        List<Opportunity_stage_value__c> customStng2 = Opportunity_stage_value__c.getall().values();
        boolean chekbool2 = true;
        for(Opportunity_stage_value__c cslist:customStng2)
         {
                if(cslist.Name == 'status')
                {
                        chekbool2 = false;
                        break;
                }
                else
                {
                }
         }
        if(chekbool2 == true)
        {
            Opportunity_stage_value__c OppntyStgVal = new Opportunity_stage_value__c();
            OppntyStgVal.name = 'status';
            OppntyStgVal.bidsubmit__c = Label.OpportunitySTage3;
            OppntyStgVal.Lost__c = Label.OpportunitySTage6;
            OppntyStgVal.new__c = Label.OpportunitySTage1;
            OppntyStgVal.nobid__c = Label.OpportunitySTage7;
            OppntyStgVal.pursuit__c = Label.OpportunitySTage2;
            OppntyStgVal.Win__c = Label.OpportunitySTage5;
            insert OppntyStgVal;
        }
        else
        {
        }
        List<OpportunityProbability__c> customStng3 = OpportunityProbability__c.getall().values();
        boolean chekbool3 = true;
        for(OpportunityProbability__c cslist:customStng3)
         {
                if(cslist.Name == 'Probability')
                {
                        chekbool3 = false;
                        break;
                }
                else
                {
                }
         }
        if(chekbool3 == true)
        {
            OpportunityProbability__c oppprob = new OpportunityProbability__c();
            oppprob.name = 'Probability';
            oppprob.Lost__c = 10;
            oppprob.New__c = 10;
            oppprob.Pursuit__c = 10;
            oppprob.Submitted__c = 10;
            oppprob.Won__c = 10;
            insert oppprob;
        }
        else
        {
        }
        List<OpportunityRecordType__c> customStng4 = OpportunityRecordType__c.getall().values();
        boolean chekbool4 = true;
        for(OpportunityRecordType__c cslist:customStng4)
         {
                if(cslist.Name == 'opp_recd_typ')
                {
                        chekbool4 = false;
                        break;
                }
                else
                {
                }
         }
        if(chekbool4 == true)
        {
            OpportunityRecordType__c OpptyRecrdType = new OpportunityRecordType__c();
            OpptyRecrdType.name = 'opp_recd_typ';
            OpptyRecrdType.typename1__c = 'OEM > 500K';
            OpptyRecrdType.typename2__c = '< 500K';
            OpptyRecrdType.typename3__c = 'Services > 500K';
            insert OpptyRecrdType;
        }
        else
        {
        }
        
        /*Insert Opportunity*/
        opportunity opprtnity = new opportunity();
        opprtnity.Name = 'test Opp';
        opprtnity.UDF_Oppty_Name__c = 'test789';
        opprtnity.AccountId = accnt.id;
        opprtnity.Amount = 30;
        opprtnity.StageName = Label.OpportunitySTage1;
        opprtnity.Actual_Margin__c = 20;
        opprtnity.Customer_Request_Type__c = 'RFI/ROM';
        opprtnity.Market_Sector__c = 'OEM-Ext';
        opprtnity.Market__c = 'Civil';
        opprtnity.L3_Platform_Lookup__c = app.id;
        opprtnity.Engineering_Required__c = 'No';
        opprtnity.closedate = date.today();
        opprtnity.probability = 50;
        opprtnity.Execution_Site__c = 'Dayton';
        opprtnity.Risky_Terms__c = 'yes';
        opprtnity.Walkaway_Margin__c = 34;
        opprtnity.Incremental_vs_Sustaining_Revenue__c = 'New';
        opprtnity.Requested_650_10_CEO_Review_Date__c = date.today();
        opprtnity.Requested_65010_GM_Review_Date__c = date.today();
        opprtnity.Requested_65010_VP_Review_Date__c = date.today();      
        opprtnity.Services_Channel__c = 'Airline';
        opprtnity.GE_Standard_Probability__c = Label.GEStandardProb3;
        opprtnity.RFI_RFP_Recieved__c = date.today();
        opprtnity.Proposal_Commit_Date__c = date.today();
        opprtnity.Submission_Date__c = date.today();
        opprtnity.Contract_Start_Date__c = date.today();
        opprtnity.Link_to_Contract_Document__c = 'testLink';
        opprtnity.reason_won__c = 'testLink';
        opprtnity.reason_won_comments__c = 'ddasd';
        insert opprtnity;
        
        Product2 product = new Product2();
        product.L1_Product__c = 'Electrical';
        product.IsActive = true;
        product.L2_Product__c = 'test';
        product.name = 'test';
        product.ProductCode = 'test';
        insert product;
        
        Product2 product2 = new Product2();
        product2.L1_Product__c = 'Electrical';
        product2.IsActive = true;
        product2.L2_Product__c = 'test2';
        product2.name = 'test';
        product2.ProductCode = 'test2';
        insert product2;
        
        Pricebook2 pb= [select Id from PriceBook2 where IsStandard=true limit 1];
        pricebook2 pricebook = new pricebook2();
        //pricebook.IsStandard = true;
        pricebook.name = 'test';
        pricebook.IsActive = true;
        insert pricebook;
        
        pricebookentry prbkentry = new pricebookentry();
        prbkentry.IsActive = true;
        //prbkentry.Pricebook2Id = pricebook.id;
        prbkentry.Pricebook2Id = pb.id;
        prbkentry.Product2Id = product.id;
        prbkentry.UnitPrice = 1000;
        prbkentry.UseStandardPrice = false;
        //prbkentry.Name = 'test';
        insert prbkentry;
        
        pricebookentry prbkentry2 = new pricebookentry();
        prbkentry2.IsActive = true;
        //prbkentry.Pricebook2Id = pricebook.id;
        prbkentry2.Pricebook2Id = pb.id;
        prbkentry2.Product2Id = product2.id;
        prbkentry2.UnitPrice = 1000;
        prbkentry2.UseStandardPrice = false;
        //prbkentry.Name = 'test';
        insert prbkentry2;
        
        list<OpportunityLineItem> objOppLineItemlist = new list<OpportunityLineItem>();
        OpportunityLineItem objOppLineItem = new OpportunityLineItem();
        objOppLineItem.OpportunityId = opprtnity.id;
        objOppLineItem.PricebookEntryId = prbkentry.id;
        objOppLineItem.Quantity = 2;
        objOppLineItem.ServiceDate= date.today();
        objOppLineItem.Type__c= 'Electronics';
        objOppLineItem.UnitPrice= 1000;
        objOppLineItemlist.add(objOppLineItem);
        OpportunityLineItem objOppLineItem2 = new OpportunityLineItem();
        objOppLineItem2.OpportunityId = opprtnity.id;
        objOppLineItem2.PricebookEntryId = prbkentry2.id;
        objOppLineItem2.Quantity = 2;
        objOppLineItem2.ServiceDate= date.today();
        objOppLineItem2.Type__c= 'Electronics';
        objOppLineItem2.UnitPrice= 1000;
        objOppLineItemlist.add(objOppLineItem2);
        insert objOppLineItemlist;
        
        OpportunityLineItem objOppLineItem3 = new OpportunityLineItem(id=objOppLineItemlist[0].id);
        objOppLineItem3.Quantity = 3;
        update objOppLineItem3;
        
        OpportunityLineItem objOppLineItem4 = new OpportunityLineItem(id=objOppLineItemlist[1].id);
        delete objOppLineItem4;
    }
}