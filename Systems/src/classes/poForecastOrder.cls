/**************************************************************************************
    Author   : Abhishek
    Date     : 26/September/2016
    Action   : This class is being call from custom button "Regenrate order" on contract - US18859
                we are reusing an existing methods of hadnler classes with a little modification to reuse code.
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        November 2016   Added @auraEnabled on method to use in lightening action "Regenerate_Order" - US42014
******************************************************************************************/

global with sharing class poForecastOrder{
@AuraEnabled
webservice static integer regeneratePoOrder(id conId){
    integer iSuccess ;
    list<order__c> orderToDelete = new list<order__c>();
    id poForecastId ;
    // Create a savepoint before any DML
    Savepoint sp = Database.setSavepoint();

    list<Schedule__c> listOfScheduleToUpdate = new list<Schedule__c>();
    for(order__c ord : [select id from Order__c where Contract1__c =:conId and (Status__c !='PO Booked' and Status__c!='ERP Forecast') ] ){
        orderToDelete.add(ord);
    }
    
    delete orderToDelete; // Delete order first
    
    list<Contract_PO__c> listofContract = [select id,Pipeline_Opportunity__c, First_PO_Ship_Date__c,First_PO_Award_Date__c, Number_of_PO_s__c , PO_Increment__c, Contract_End_Date__c, PO_Forecast_Opportunity__c from Contract_PO__c where id=: conId];
    CustomContractTriggerHandler customTriggerHandler = new CustomContractTriggerHandler();
    customTriggerHandler.generateOrdersFromContract(listofContract);
    iSuccess = 1;
    // list will always have 1 schedule 
    poForecastId = listofContract[0].PO_Forecast_Opportunity__c;
    // query schedule 
    for (Schedule__c mySch : [select id, Opportunity__c, Schedule_Date__c, Order__c from Schedule__c where  Opportunity__c =:poForecastId and Order__c=null order by id asc]){
        listOfScheduleToUpdate.add(mySch);
    }
    if(!listOfScheduleToUpdate.isEmpty()){
    ScheduleTriggerHandler trgSchHandler = new ScheduleTriggerHandler();
    iSuccess = trgSchHandler.updateOrderIdOnSchedule(listOfScheduleToUpdate, null, 'Update');
        if(iSuccess!=1){
            // Rollback to previous state
            Database.rollback(sp);
        }
    }
    return iSuccess;
}

}