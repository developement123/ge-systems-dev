/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following class:
    - AutoGenerateOpportunitiesController

    Version     Written by                last modified Date              
    ---------------------------------- --------------------------------------------------------------------------------------------------------------
    1.0         Jhansi                         12/16/2015   
                Abhishek                       09/18/2017  removed account platform
*************************************************************************************************************************************************/
@isTest
public class Tracker_AutoGenerateOppController {

        private static testmethod void testAutoGenerateOppController() {
            
            Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
            Class_TriggerCheck.var_TriggerOnSchedule = false;
            Class_TriggerCheck.var_triggerOnUser = false;
            Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
            Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
            
            Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
            User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
             insert u;
             
            Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Opportunity; 
            Map<String,Schema.RecordTypeInfo> OpportunityRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();

            Account acc = new Account(Name = 'GE GEAS');
            insert acc;
         
            System.runAs(u) {  
            Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
            insert pm;
                
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
            insert hierarchy;

            Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
            insert p2;
             
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',AccountId = acc.id,
                            MailingCity='Delhi',
                            MailingCountry='India');
            insert con;
            
            opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services');
            test.startTest();
             insert opp;
            test.stopTest();
            
            Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
            Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='test', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =51,Installment_Periods__c = 'Annually',Schedule_Start_Date__c =system.today());
            insert oli;
                
            Contract_PO__c Cont = new Contract_PO__c(Account_Name__c = acc.id,Pipeline_Opportunity__c=opp.id,Legal__c= con.id,Program_Manager__c = u.id,Finance__c = con.id,Status__c = 'Draft',Last_Order_Number__c = 111,Number_of_PO_s__c = 111,First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Weekly',Revenue_Increment__c = 'Weekly', First_PO_Ship_Date__c = system.today());
            insert Cont;
            
            //Cont.PO_Increment__c = 'Bi-annual';
           // Cont.Revenue_Increment__c = 'Bi-annually';
           // update Cont;
            
           // Cont.PO_Increment__c = 'Quarterly';
           // Cont.Revenue_Increment__c = 'Quarterly';
           // update Cont;
            
           // Cont.PO_Increment__c = 'Monthly';
           // Cont.Revenue_Increment__c = 'Monthly';
           // update Cont; 
            
           // Cont.PO_Increment__c = 'Weekly';
          // Cont.Revenue_Increment__c = 'Weekly';
           // update Cont; 
            
            
            PageReference pageRef = Page.AutoGenerateOpportunitiesThroughContract;
            Test.setCurrentPage(pageRef ); 
            ApexPages.CurrentPage().getparameters().put('id', Cont.Id);
             ApexPages.StandardController sc = new ApexPages.StandardController(Cont);
            AutoGenerateOpportunitiesController AutoController = new AutoGenerateOpportunitiesController(sc);

            
            List<Opportunity_Line_Item__c> listOfOli = new List<Opportunity_Line_Item__c>();
 
             listOfOli.add(new Opportunity_Line_Item__c(Opportunity__c = opp.id,Product__c = p2.id,Installment_Periods__c = 'Annually', of_Installments__c =51, Quantity__c=51, Schedule_Start_Date__c =system.today()));
             listOfOli.add(new Opportunity_Line_Item__c(Opportunity__c = opp.id,Product__c = p2.id,Installment_Periods__c = 'Annually', of_Installments__c =51, Quantity__c=51, Schedule_Start_Date__c =system.today()));
             listOfOli.add(new Opportunity_Line_Item__c(Opportunity__c = opp.id,Product__c = p2.id,Installment_Periods__c = 'Annually', of_Installments__c =51, Quantity__c=51, Schedule_Start_Date__c =system.today()));
                        
             insert listOfOli;
             
            // AutoGenerateOpportunitiesController.wrapperOli newWrap = new AutoGenerateOpportunitiesController.wrapperOli(true,listOfOli[0]);
             
             AutoController.wrap_Oli= new List<AutoGenerateOpportunitiesController.wrapperOli>();
             AutoController.wrap_Oli.add(new AutoGenerateOpportunitiesController.wrapperOli(true,listOfOli[0]));
             AutoController.wrap_Oli.add(new AutoGenerateOpportunitiesController.wrapperOli(true,listOfOli[1]));
             AutoController.wrap_Oli.add(new AutoGenerateOpportunitiesController.wrapperOli(true,listOfOli[2]));
                        
            
            AutoController.Generate();
            AutoController.cancel();
}

} 
}