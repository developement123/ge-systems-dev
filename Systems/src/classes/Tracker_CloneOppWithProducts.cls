/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following class:
    - CloneOppWithProducts

    Version     Written by                last modified Date              
    ---------------------------------- --------------------------------------------------------------------------------------------------------------
    1.0         Jhansi                         5/11/2015    
                Abhishek                       09/18/2017  removed account platform
*************************************************************************************************************************************************/
@isTest
public class Tracker_CloneOppWithProducts {
    
    private static testmethod void testInitialiseObjectsForCloning() {
        
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_triggerOnUser = true;
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc = new Account(Name = 'Hello');
        insert acc;
         
         System.runAs(u) {  
            Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
            insert pm;
                
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
            insert hierarchy;
            
            Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
            Program_Probability__c='100% - Funded');
            insert prog;
            
            Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
            insert p2;
             
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            Email='test1@duptest.com',MailingCity='Delhi',
                            MailingCountry='India');
             opportunity opp = new opportunity(Name = 'Test opp', StageName =Label.OpportunityStage1, CloseDate = date.today(), Contract_Term__c = 3,
                              Program__c = prog.id, Amount = 1000, Firm_Anticipated__c = 'Anticipated', deal_type__c = 'Services',
                          GE_Standard_Probability__c=Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Platform_New__c = pm.id, accountId= acc.id);
           
            insert opp; 
           
            id recType = [select id from recordtype where SobjectType='Game_Film__c' and DeveloperName='Won' and isActive=true][0].id;
            
            Game_Film__c gm = new Game_Film__c( recordTypeId = recType, Opportunity__c = opp.id);
            insert gm;
            
            opp.stagename= Label.OpportunityStage5;
            update opp;

            Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
                                 Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='test',Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1);
            insert oli;
            
            List<Schedule__c> Sch_List = new List<Schedule__c>{};
            Sch_List.add(new Schedule__c(Opportunity__c = opp.id, Opportunity_Line_Item__c = oli.id));
           
            insert Sch_List;
            
            CloneOppWithProducts.Schedules_Webservice(opp.id);
        }
    }
       
}