/*************************************************************************************************************************************************
    Following test class was created to provide code coverage for the following classes and triggers:

    - Triggers : TriggerOnIdeation
                 
     Version       Author(s)             Date                Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
      1.0         Mohan kumar          27/July/2015        Initial Creation
                  Abhishek             18/Seo/2017         removed account platform
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_ideation {

    public static Program__c prog;
    public static Product_Hiearchy_Object__c hierarchy;  
    
    /* following method is to create test data */  

    public static void loadTest() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false; 
                
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal@ge.com.system.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;
        
        System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;    

            hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            prog = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                 Program_Probability__c = '100% - Funded', Is_Tokenized__c = false);
            insert prog;     
        }  
    }
    
    /* following testMethod is to cover 'TriggerOnIdeation' trigger */

    static testMethod void test_TriggerOnIdeation() {        
        Test.startTest();           
            Ideation__c idea = new Ideation__c(Name = 'xyz', Civil_Military__c = 'Civil', Site__c = 'Sterling');
            insert idea;
            
            System.assertEquals(idea.Name, 'xyz');
            
            idea.Civil_Military__c = 'Military';
            update idea;            
            
            System.assertEquals(idea.Civil_Military__c, 'Military');            
        Test.stopTest();
    }
}