/*************************************************************************

    Version     Written by        last modified Date              
    ---------------------------------- --------------
    1.0         Abhishek            18 sep 2017 removed account platform 
************************************************************************/
@isTest
private class Test_Trg_Update_CA_status {

    static testMethod void myUnitTest() {
     test.starttest();   
     Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
     insert plat;
     
     Account acc = new Account(Name='Test Account');
     insert acc; 
     id bA_Profile_id;
     id Sys_admin_Profile_id;
     
   list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
   for(Profile p: pro)
   {
     if(p.name=='Systems Business Admin')
     {
        bA_Profile_id=p.Id;
     }
     else
     {
        Sys_admin_Profile_id=p.id;
     }
   }
     User u =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
               insert u;
               
               
    User u2 =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=bA_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12453.com.sys.dev');
    insert u2;
    
     Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
     insert hierarchy;
    
     Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
     Program_Probability__c='100% - Funded');
     insert prog;
    
    Class_TriggerCheck.IsPreventing = false;
    Class_TriggerCheck.IsWindchill_UpdatedCA = false;
    Class_TriggerCheck.Isclosed_CA = false;
    Class_TriggerCheck.Var_SendBatch_ClosedCA = false;
    Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
    
    opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
            
    Sales_Request__c sr=new Sales_Request__c(Windchill_US_UK__c ='Windchill US',Name='Test response5',Program__c=prog.id,Response_Description__c='Test desc', Pipeline_Opportunity__c = opp1.id);
    insert sr;
    
    EPA__c epa=new EPA__c(Closed__c=False);
    insert epa;
    
    List<Cost_Authorization__c> lstCA = new List<Cost_Authorization__c>();
   
    
    Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Approved_Budget_Backend__c=NULL,
                                                        FY_Spent_to_Date__c=NULL, Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open'));
    lstCA.add(ca);
    insert lstCA;
    
    epa.Closed__c = true;
    update epa;
    test.stoptest(); 
    
    }
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }
}