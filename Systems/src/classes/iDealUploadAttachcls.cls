/***********************************************************************************************************************************************************
    Author : Mohan Kumar
    Date   : 22 April 2015
    
    Action : This class is used as controller in VF page : iDealUploadAttach
    
    Last modified by : Mohan Kumar(7 July 2015) for JIRA 436

***********************************************************************************************************************************************************/

public with sharing class iDealUploadAttachcls {

    public Blob body{get;set;} 
    public Id respIdFromUrl{get;set;}
    public Sales_Request__c respInstance;
    
    public List<Response_Line_Item__c> listOfRLI = new List<Response_Line_Item__c>();
    
    public iDealUploadAttachcls() {    
        respIdFromUrl = apexPages.currentPage().getParameters().get('respId');
        String theQuery = 'select Id, Response_ID2__c from Sales_Request__c where Id =: respIdFromUrl';
        respInstance = Database.query(theQuery);   
    }  
    
    public void iDealSubmit() {
    
        String errMsg = Label.iDealUploadAttach_valid_xml_error;      
        String errMsg_valid_RespId = Label.iDealUploadAttach_valid_ResponseId;
        
        if(body == null) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.Error, errMsg));
        }
        
        else if(body != null) {
            try {
                DOM.Document xmlDOC = new DOM.Document(); 
                String xmlString = body.toString();
                xmlDOC.load(xmlString);
                
                Integer iterId;
                String idealRunName;
                
                for(DOM.XMLNode aNode : xmlDOC.getRootElement().getChildElements() ) {                
                    if(aNode.getName() == 'responseID') {
                        if(String.valueof(aNode.getText()).trim() != respInstance.Response_ID2__c) {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg_valid_RespId));    
                            break;
                        }                 
                    }                    
                    
                    if(aNode.getName() == 'iteration_id') 
                        iterId = Integer.valueof(aNode.getText());  
                                
                    if(aNode.getName() == 'iDealRunName')  
                        idealRunName = String.valueof(aNode.getText());
                                 
                    if(aNode.getName() == 'Current') {
                        Response_Line_Item__c rli = new Response_Line_Item__c(iDeal_Flag__c = true, Response__c = respIdFromUrl);
                        
                        for(DOM.XMLNode bNode : aNode.getChildElements()) {
                        
                            rli.Iteration_Id__c = iterId;
                            rli.iDealRunName__c = idealRunName;
                            
                            if(bNode.getName() == 'SalesOE') 
                                rli.SalesOE__c = decimal.valueof(bNode.getText());                                
                           
                            if(bNode.getName() == 'ContributionMarginOE') 
                                rli.ContributionMarginOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginOEPER') 
                                rli.ContributionMarginOEPER__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostCFDOE') 
                                rli.BaseCostCFDOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostRTSOE') 
                                rli.BaseCostRTSOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMOE') 
                                rli.OMOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMPERCOE') 
                                rli.OMPERCOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceEscalationOE')
                                rli.PriceEscalationOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaterialInflationOE') 
                                rli.MaterialInflationOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'LaborInflationOE') 
                                rli.LaborInflationOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'CurrencyFx') 
                                rli.CurrencyFx__c = String.valueof(bNode.getText());
                               
                            if(bNode.getName() == 'LearningCurveProductivity') 
                                rli.LearningCurveProductivity__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SGARateOE') 
                                rli.SGARateOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SalesAM') 
                                rli.SalesAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginAM') 
                                rli.ContributionMarginAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginAMPER') 
                                rli.ContributionMarginAMPER__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostRTSAM') 
                                rli.BaseCostRTSAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMAM') 
                                rli.OMAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMPERCAM') 
                                rli.OMPERCAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceEscalationAM') 
                                rli.PriceEscalationAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaterialInflationAM') 
                                rli.MaterialInflationAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'LaborInflationAM') 
                                rli.LaborInflationAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'AMMarketshare') 
                                rli.AMMarketshare__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SGARateAM') 
                                rli.SGARateAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'Volume') 
                                rli.Volume__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceBaseyr') 
                                rli.PriceBaseyr__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceAvg') 
                                rli.PriceAvg__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SalesTotal') 
                                rli.SalesTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginTotal') 
                                rli.ContributionMarginTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginTotalPER') 
                                rli.ContributionMarginTotalPER__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostCFDTotal') 
                                rli.BaseCostCFDTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostRTSTotal') 
                                rli.BaseCostRTSTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMTotal') 
                                rli.OMTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMPERCTOTAL') 
                                rli.OMPERCTOTAL__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaxCashInvestment') 
                                rli.MaxCashInvestment__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'IRR') 
                                rli.IRR__c = String.valueof(bNode.getText());
                                               
                            if(bNode.getName() == 'NPV') 
                                rli.NPV__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'Payback') 
                                rli.Payback__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'HeadroomTotal') 
                                rli.HeadroomTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PricingStrategyTotal') 
                                rli.PricingStrategyTotal__c = String.valueof(bNode.getText());
                           
                            if(bNode.getName() == 'WACCTotal') 
                                rli.WACCTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SGARateTotal') 
                                rli.SGARateTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'NRERates') 
                                rli.NRERates__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'NRERiskReserve') 
                                rli.NRERiskReserve__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'CapitalizationNRE') 
                                rli.CapitalizationNRE__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OtherOE')                      // added for JIRA 436
                                rli.OtherOE__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OtherAM')                      // added for JIRA 436
                                rli.OtherAM__c = String.valueof(bNode.getText());
                                
                            if(bNode.getName() == 'OtherTotal')                  // added for JIRA 436
                                rli.OtherTotal__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaxDeferredTotal') 
                                rli.MaxDeferredTotal__c = decimal.valueof(bNode.getText());
                            
                            rli.Type__c = 'Offer';
                            
                        }
                        listOfRLI.add(rli);             
                    }
                    
                    if(aNode.getName() == 'Walkaway') {
                        Response_Line_Item__c rli = new Response_Line_Item__c(iDeal_Flag__c = true, Response__c = apexPages.currentPage().getParameters().get('respId'));
                        for(DOM.XMLNode bNode : aNode.getChildElements()) {
                            
                            rli.Iteration_Id__c = iterId;
                            rli.iDealRunName__c = idealRunName;
                            
                            if(bNode.getName() == 'SalesOE') 
                                rli.SalesOE__c = decimal.valueof(bNode.getText());        
                            
                            if(bNode.getName() == 'ContributionMarginOE') 
                                rli.ContributionMarginOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginOEPER') 
                                rli.ContributionMarginOEPER__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostCFDOE') 
                                rli.BaseCostCFDOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostRTSOE') 
                                rli.BaseCostRTSOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMOE') 
                                rli.OMOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMPERCOE') 
                                rli.OMPERCOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceEscalationOE') 
                                rli.PriceEscalationOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaterialInflationOE') 
                                rli.MaterialInflationOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'LaborInflationOE') 
                                rli.LaborInflationOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'CurrencyFx') 
                                rli.CurrencyFx__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'LearningCurveProductivity') 
                                rli.LearningCurveProductivity__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SGARateOE') 
                                rli.SGARateOE__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SalesAM') 
                                rli.SalesAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginAM') 
                                rli.ContributionMarginAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginAMPER') 
                                rli.ContributionMarginAMPER__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostRTSAM') 
                                rli.BaseCostRTSAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMAM') 
                                rli.OMAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMPERCAM') 
                                rli.OMPERCAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceEscalationAM') 
                                rli.PriceEscalationAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaterialInflationAM') 
                                rli.MaterialInflationAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'LaborInflationAM') 
                                rli.LaborInflationAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'AMMarketshare') 
                                rli.AMMarketshare__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SGARateAM') 
                                rli.SGARateAM__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'Volume')
                                rli.Volume__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceBaseyr')
                                rli.PriceBaseyr__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PriceAvg') 
                                rli.PriceAvg__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SalesTotal')
                                rli.SalesTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginTotal') 
                                rli.ContributionMarginTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'ContributionMarginTotalPER') 
                                rli.ContributionMarginTotalPER__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostCFDTotal') 
                                rli.BaseCostCFDTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'BaseCostRTSTotal') 
                                rli.BaseCostRTSTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMTotal') 
                                rli.OMTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OMPERCTOTAL') 
                                rli.OMPERCTOTAL__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaxCashInvestment') 
                                rli.MaxCashInvestment__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'IRR') 
                                rli.IRR__c = String.valueof(bNode.getText());
                                                
                            if(bNode.getName() == 'NPV') 
                                rli.NPV__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'Payback') 
                                rli.Payback__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'HeadroomTotal') 
                                rli.HeadroomTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'PricingStrategyTotal') 
                                rli.PricingStrategyTotal__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'WACCTotal') 
                                rli.WACCTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'SGARateTotal') 
                                rli.SGARateTotal__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'NRERates') 
                                rli.NRERates__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'NRERiskReserve') 
                                rli.NRERiskReserve__c = decimal.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'CapitalizationNRE') 
                                rli.CapitalizationNRE__c = String.valueof(bNode.getText());
                                
                            if(bNode.getName() == 'OtherOE')                 // added for JIRA 436
                                rli.OtherOE__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'OtherAM')                 // added for JIRA 436
                                rli.OtherAM__c = String.valueof(bNode.getText());
                                
                            if(bNode.getName() == 'OtherTotal')               // added for JIRA 436
                                rli.OtherTotal__c = String.valueof(bNode.getText());
                            
                            if(bNode.getName() == 'MaxDeferredTotal') 
                                rli.MaxDeferredTotal__c = decimal.valueof(bNode.getText());
                            
                            rli.Type__c = 'Walk';
                        }
                        listOfRLI.add(rli);             
                    }                  
                }
            }
            catch(Exception ex) {                              
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
            }
            
            if(!listOfRLI.isEmpty()) {
                try {
                    Boolean isDone;                    
                    Database.SaveResult[] srList = Database.insert(listOfRLI);
                    
                    for(Database.SaveResult sr : srList) {
                        if(!sr.isSuccess()) {
                            isDone = false; 
                            break;     
                        }
                    }                    
                    
                    if(isDone != false) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,' Upload process completed successfully.'));
                    }               
                }
                catch(Exception ex) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
                }
            }
        }
    }
}