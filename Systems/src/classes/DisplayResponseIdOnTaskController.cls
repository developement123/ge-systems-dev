/***********************************************************************************************************************************************************
    Author : Suman Gupta
    Date   : 06 July 2015
    
    Action : This class is used as extension of vf page : DisplayResponseIdOnTask
             This class is used to display Response Id on related Task
             This is related to JIRA: ASSFDC-434
***********************************************************************************************************************************************************/
public with sharing class DisplayResponseIdOnTaskController {

    public Id TaskId;
    public Sales_Request__c objResponse {get;set;}
    
    public DisplayResponseIdOnTaskController(ApexPages.StandardController controller) {
        
        TaskId = controller.getid();
        
        Task objTask;
        
        If(TaskId != null)
        {
            objTask = [Select WhatId from Task where id = :TaskId];
        }
        
        Schema.SObjectType sobjType;
        
        if(objTask != null)
        {
            sobjType = objTask.WhatId.getSObjectType();
        }
        
        //checking if related object of Task is 'Response'
        If(sobjType == Sales_Request__c.SObjectType)
        {
            objResponse  = [Select Auto__c from Sales_Request__c where id = :objTask.Whatid];
        }
        
    }

}