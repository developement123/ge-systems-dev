@isTest
private class TestFinalAgreementClause{
 static testmethod void AgreementClauseFinal(){
 
    Test.starttest();
    //creating Account
    Account a=new Account(Name='Test Account');
     insert a;
   //Create agreement record
    Apttus__APTS_Agreement__c agr=new Apttus__APTS_Agreement__c();
    agr.Name='Agreement Name';
    recordtype rt = [select id from RecordType where SobjectType='Apttus__APTS_Agreement__c' and DeveloperName='GE_Digital_Services' Limit 1];
    agr.RecordTypeId = rt.id ;
    agr.Apttus__Contract_Start_Date__c=System.today();
    agr.Apttus__Account__c=a.id;
    agr.Apttus__Contract_End_Date__c=System.today();
    agr.Apttus__Description__c='Test Description';
    agr.clauses_processed__c=false;
    agr.Apttus__Status__c='Activated';    
    insert agr;
    
    Apttus__Agreement_Clause__c agrcl1=new Apttus__Agreement_Clause__c();
    agrcl1.Apttus__Action__c = 'Original';
    agrcl1.Apttus__Clause__c = 'Clause 1';
    agrcl1.Apttus__Agreement__c = agr.id;
    agrcl1.In_Final__c = false;
    agrcl1.Apttus__Text__c = 'sample text';
    insert agrcl1;
    
    Apttus__Agreement_Clause__c agrcl2=new Apttus__Agreement_Clause__c();
    agrcl2.Apttus__Action__c = 'Deleted';
    agrcl2.Apttus__Clause__c = 'Clause 2';
    agrcl2.Apttus__Agreement__c = agr.id;
    agrcl2.In_Final__c = false;
    agrcl2.Apttus__Text__c = 'sample text';
    insert agrcl2;
    
    FinalAgreementClause obj = new FinalAgreementClause();
    DataBase.executeBatch(obj);

    
    Test.stoptest();
    }
    

}