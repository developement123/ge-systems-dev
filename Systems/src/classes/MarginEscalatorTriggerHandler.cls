/******************************************************************************************************************************************
    Author   : Jhansi B
    Date     : 15/October/2015
    Action   : This is the handler class for trigger 'TriggerOnMarginEscalator'.    
******************************************************************************************************************************************/

public with sharing class MarginEscalatorTriggerHandler {

    /******************************************************************************************************************
          Method 1: following method 'createUpdateMarginEscalator' 
              1. Calculate the Margin Escalator field values when Margin Escalator was created/updated manually.
              2. updates 'CY Margin Escalator1' field in opportunity when ME is inserted/updated/deleted/Undeleted.
    *******************************************************************************************************************/
    
    public void createUpdateMarginEscalator(List<Margin_Escalator__c> newList, List<Margin_Escalator__c> oldList, Map<Id, Margin_Escalator__c> oldMap, Map<Id, Margin_Escalator__c> newMap, String eventType, String triggerType) {
        
        if(Label.Label_createUpdateMarginEscalator == 'true') {
            
            Set<Id> setofoppyId = new Set<Id>();
            Set<Id> setOfUpdatedId = new Set<Id>();
            Set<Id> setOfOppyIds = new Set<Id>();
            
            List<Opportunity> listOfOpportunity = new List<Opportunity>();  
            List<Opportunity> listOfOppToUpdate = new List<Opportunity>();
            
            Map<Id, Opportunity> mapOfIdToOppy = new Map<Id, Opportunity>();    // map of opportunity id vs opportunity.
            Map<Id, List<Schedule__c>> mapOls = new Map<Id, List<Schedule__c>>();
            Map<Id, List<Schedule__c>> mapOlsinRec = new Map<Id, List<Schedule__c>>();
            Map<Id, Double> oppyCYMap = new Map<Id, Double>();            
    
            if(eventType == 'Before' && triggerType == 'Insert' && Class_TriggerCheck.var_TriggerOnMarginEscalator) {  // in before insert event, we are calculating ME field values based on available schedules.
                            
                                                                                                                       //  this static variable 'var_TriggerOnMarginEscalator' is used to stop firing of this portion of code when ME is getting inserted because of Schedules and Batch (setting this to false in class 'Systems_CommonClass' & createME_new)
                Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
                Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
                Class_TriggerCheck.Var_isProgam_Probability_change = true;
                Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
                Class_TriggerCheck.Var_StopOpportunityTrigger = false;                
                Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
                            
                for(Margin_Escalator__c menew: newList)
                    setofoppyId.add(menew.Opportunity__c);
                
                for(Opportunity oppy: [select id, closeDate, Margin_Escalator__c 
                                           from Opportunity 
                                           where id IN: setofoppyId])
                    mapOfIdToOppy.put(oppy.id, oppy);                   
                 
                for(Schedule__c ols: [select Id, Quantity__c, Schedule_Date__c, Escalated_Revenue__c, Opportunity_Line_Item__r.Probability__c, Opportunity__c
                                         from Schedule__c 
                                         where Opportunity__c IN: setofoppyId 
                                         AND Schedule_Date__c != null]) {
                    if(mapOls.containsKey(ols.Opportunity__c))
                        mapOls.get(ols.Opportunity__c).add(ols);
                    else
                        mapOls.put(ols.Opportunity__c, new List<Schedule__c>{ols});
                }
    
                for(Margin_Escalator__c meRec: newList) {
                    Systems_CommonClass calculateMEInstance = new Systems_CommonClass(); 
                    if(!mapOfIdToOppy.isEmpty()
                        && mapOfIdToOppy.get(meRec.Opportunity__c) != null 
                        && mapOfIdToOppy.get(meRec.Opportunity__c).Margin_Escalator__c
                        && mapOfIdToOppy.get(meRec.Opportunity__c).CloseDate.Year() == System.Today().Year() 
                        && meRec.Year__c == String.valueOf(System.Today().Year())) {    
                            meRec.MTG__c = true;
                            listOfOpportunity.add(new Opportunity(id = meRec.Opportunity__c, MTG_Year__c = meRec.Year__c));
                    }
                    
                    List<Schedule__c> listOfFilteredSchedules = new List<Schedule__c>();
                    
                    if(mapOls.get(meRec.Opportunity__c) != null && !mapOls.get(meRec.Opportunity__c).isEmpty()) {
                        for(Schedule__c sch: mapOls.get(meRec.Opportunity__c)) {                    
                            if(meRec.Year__c != null && sch.Schedule_Date__c.Year() == Integer.valueOf(meRec.Year__c))
                                listOfFilteredSchedules.add(sch);                           
                        } 
                    } 
                    
                    if(!listOfFilteredSchedules.isEmpty())      // all filtered schedules based on ME year.               
                        meRec = calculateMEInstance.calculatingME(meRec, listOfFilteredSchedules);
                }            
                        
                if(!listOfOpportunity.isEmpty()) {      // if this list is not empty then it will update MTG year in Opportunity.   
                    try {
                        update listOfOpportunity;
                    }
                    catch(DmlException ex) {
                        System.debug('The following exception has occurred while updating opportunities in class MarginEscalatorTriggerHandler : ' + ex.getMessage());
                    }
                }                    
            }
            
            /************************************************************************************************************************************
                in before update event, trigger will not allow user to update ME field values manually if Out_of_Sync__c checkbox is false.
                    if Out_of_Sync__c is changed to false from true, then trigger will again calculate the values based on available schedules.
            ************************************************************************************************************************************/
            if(eventType == 'Before' && triggerType == 'Update' && Class_TriggerCheck.var_TriggerOnMarginEscalator) {  //  this static variable 'var_TriggerOnMarginEscalator' is used to stop firing of this portion of code when ME is getting updated because of Schedules and Batch (setting this to false in class 'Systems_CommonClass' & createME_new)
                for(Margin_Escalator__c me: newList) {
                    if((me.Jan__c != oldMap.get(me.id).Jan__c
                        || me.Feb__c != oldMap.get(me.id).Feb__c
                        || me.Mar__c != oldMap.get(me.id).Mar__c
                        || me.Apr__c != oldMap.get(me.id).Apr__c 
                        || me.May__c != oldMap.get(me.id).May__c 
                        || me.Jun__c != oldMap.get(me.id).Jun__c 
                        || me.Jul__c != oldMap.get(me.id).Jul__c 
                        || me.Aug__c != oldMap.get(me.id).Aug__c  
                        || me.Sept__c != oldMap.get(me.id).Sept__c  
                        || me.Oct__c != oldMap.get(me.id).Oct__c  
                        || me.Nov__c != oldMap.get(me.id).Nov__c  
                        || me.Dec__c != oldMap.get(me.id).Dec__c) 
                            && me.Out_of_Sync__c == false) {                        
                                                 
                        me.addError(Label.Me_Manual_Update_Alert_Message);   
                    }
        
                    if(oldMap.get(me.id).Out_of_Sync__c && !newMap.get(me.id).Out_of_Sync__c)
                        setOfUpdatedId.add(me.Opportunity__c);
                                       
                    for(Schedule__c ols: [select Id, Quantity__c, Schedule_Date__c, Escalated_Revenue__c, Opportunity_Line_Item__r.Probability__c, Opportunity__c 
                                              from Schedule__c 
                                              where Opportunity__c IN: setOfUpdatedId 
                                              AND Schedule_Date__c != null]) {
                        if(mapOlsinRec.containsKey(ols.Opportunity__c))
                            mapOlsinRec.get(ols.Opportunity__c).add(ols);
                        else
                            mapOlsinRec.put(ols.Opportunity__c, new List<Schedule__c>{ols});
                    } 
            
                    for(Margin_Escalator__c meRec: newList) {
                        Systems_CommonClass calculateMEInstance = new Systems_CommonClass();
                       
                        List<Schedule__c> listOfFilteredSchedules = new List<Schedule__c>();
                        
                        if(mapOlsinRec.get(meRec.Opportunity__c) != null && !mapOlsinRec.get(meRec.Opportunity__c).isEmpty()){
                            for(Schedule__c sch: mapOlsinRec.get(meRec.Opportunity__c)) {                                
                                if(meRec.Year__c != null && sch.Schedule_Date__c.Year() == Integer.valueOf(meRec.Year__c))
                                    listOfFilteredSchedules.add(sch);                           
                            }
                        }
                        
                        if(!listOfFilteredSchedules.isEmpty()) // passing only filtered schedules.
                            meRec = calculateMEInstance.calculatingME(meRec, listOfFilteredSchedules);
                    }               
                }   
            }
            
             /************************************************************************************************************************************
                in after events, trigger is updating 'CY_Margin_Escalator1__c' field in opportunity after insertion/updation/deletion/undeletion
                    of Margin Escalators.                
            ************************************************************************************************************************************/
            
            if(eventType == 'After' && Class_TriggerCheck.var_TriggerOnMarginEscalator_AfterEvents) {   //this static variable 'var_TriggerOnMarginEscalator_AfterEvents' is not getting false anywhere in code currently             
                
                Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
                Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
                Class_TriggerCheck.Var_isProgam_Probability_change = true;
                Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
                Class_TriggerCheck.Var_StopOpportunityTrigger = false;
                Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;           
                
                if(triggerType == 'Insert' || triggerType == 'Undelete') {
                    for(Margin_Escalator__c me: newList)
                        setOfOppyIds.add(me.Opportunity__c);                   
                }
        
                if(triggerType == 'Update') {
                    for(Margin_Escalator__c me: newList) {                      
                        if(oldMap.get(me.id).CY_Margin_Escalator__c != newMap.get(me.id).CY_Margin_Escalator__c) 
                            setOfOppyIds.add(me.Opportunity__c);                        
                    }
                }
                
                if(triggerType == 'Delete') {
                    for(Margin_Escalator__c me: oldList)
                        setOfOppyIds.add(me.Opportunity__c);
                }
                
                if(Label.CY_Margin_Escalator_Year != null && !setOfOppyIds.isEmpty()) {       
                    String yearfromLabel = Label.CY_Margin_Escalator_Year.trim();
                        
                    for(Margin_Escalator__c me : [select Opportunity__c, CY_Margin_Escalator__c 
                                                    from Margin_Escalator__c 
                                                    where Opportunity__c IN : setOfOppyIds 
                                                    AND Year__c =: yearfromLabel]) {
                        oppyCYMap.put(me.Opportunity__c, me.CY_Margin_Escalator__c);            
                    }   
                }
                
                if(!setOfOppyIds.isEmpty()) {
                    for(id oppy_id : setOfOppyIds) {
                        Opportunity opp = new Opportunity(id = oppy_id); 
                        
                        if(oppyCYMap.get(oppy_id) != null) 
                            opp.CY_Margin_Escalator1__c = oppyCYMap.get(oppy_id);
                        else
                            opp.CY_Margin_Escalator1__c = 0.0;
                            
                        listOfOppToUpdate.add(opp);
                    }
                }
                
                if(!listOfOppToUpdate.isEmpty()) {   // in this list we are updating CY_Margin_Escalator1__c value in opportunity. 
                    try {
                        update listOfOppToUpdate;
                    }
                    catch(DmlException ex) {
                        System.debug('The following exception has occurred while updating opportunities in class: MarginEscalatorTriggerHandler and in method: createUpdateMarginEscalator: ' + ex.getMessage());
                    }
                } 
            } 
        }  
    }         
}