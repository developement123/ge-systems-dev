/****************************************************************************************************
    Author   : Abhishek
    Date     : March/2016
    Action   : This is a class to generate renewals on contract generation. This is being called in "ContractTriggerHandler" 
               Also, it contains utility methods that might be related to other functionality. Use it as a Utility class.
               
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        03/23/2016      Created a new field "Legal_Contracts__c" to be use in a workflow. Populating this field with contract's Legel in method "Generate"
    Abhishek        04/13/2016      Created a new Method "AddUserToPermissionSetAndGroups". - Jira 546
    Abhishek        06/30/2016      Added code to fillup contact "Contract__c" lookup on Associated product JIRA 587
     Abhishek        03/08/2017     "Persuade / Negotiate" replaced with "4 - Negotiation"
     Abhishek        04/17/2017     replaced platform&win probability with GE standard Probability
     Abhishek        06/07/2017      Added GE_X_Sell__Opportunity_External_ID__c for lead share issue
    Abhishek         07/20/2017      set field Corporate_Sharing__c to false
*******************************************************************************************************/
public with sharing class ScheduleUtility{
   
   public static void Generate(Contract_PO__c con, Opportunity OldOpp, Double InitialAMT) {
        List<Opportunity> renewalOpp = new List<Opportunity> ();
        // For TOV calculation
        map<id, double> mapOfContractToRenAmt = new map<id, double>(); 
        mapOfContractToRenAmt.put(con.Id, InitialAMT);

        renewalOpp = [Select id from Opportunity where ContractID1__c =: con.id and RecordType.Name = :Label.Label_RenewalOpportunity ];
        
        if(renewalOpp.size()>0){ // if Renewal is already generated
            con.addError(Label.AutoGenerateOpportunitiesThroughContract_RenewalValidation);
        }
        
        if(con.no_automatic_renewals__c ==0 || con.Automatic_Renewal_Term_months__c ==0 ){
            con.addError(Label.Alert_Message_Renewal);
        }
            if(con.no_automatic_renewals__c >0 && con.Automatic_Renewal_Term_months__c>0){
        
                try{
                    // initialize variables
                    Map<Id, Opportunity> mapOfInsertedFreshOppy = new Map<Id, Opportunity>();
                    List<Opportunity_Line_Item__c> listOfNewOli = new List<Opportunity_Line_Item__c>();
                    List<Schedule__c> listOfNewSchedules = new List<Schedule__c>();
                    Map<Id, Id> mapOfOliIdToOppyId = new Map<Id, Id>();
                    Map<Id, Opportunity_Line_Item__c> mapOfOliIdToOli = new Map<Id, Opportunity_Line_Item__c>();
                    Map<Id, List<Schedule__c>> mapOfFreshOliIdToSchedules = new Map<Id, List<Schedule__c>>();
                    List<Schedule__c> listOfFreshSchedules_To_Insert = new List<Schedule__c>();
                    
  
                    list<Opportunity> renewal_oppty = new list<Opportunity>();
                    Date d;
                   
                    if(con.no_automatic_renewals__c > 0){
                        for(Integer i=0; i<con.no_automatic_renewals__c; i++) {
                            Opportunity RenewalRecordTypeOppy = new Opportunity();
                            RenewalRecordTypeOppy = oldOpp.clone(false,true);
                            RenewalRecordTypeOppy.GE_X_Sell__Opportunity_External_ID__c = null;
                            RenewalRecordTypeOppy.Corporate_Sharing__c = false;
                            RenewalRecordTypeOppy.Reason_Won_Lost__c = '';
                            
                            if(i==0)
                                d = con.Initial_Term_End_Date__c.adddays(1);
                            else
                                d = d.addMonths(Integer.valueOf(con.Automatic_Renewal_Term_months__c));
                            
                            RenewalRecordTypeOppy.recordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get(Label.Label_RenewalOpportunity).getRecordTypeId();
                            RenewalRecordTypeOppy.closeDate = d;
                            RenewalRecordTypeOppy.ContractID1__c = con.Id;
                            RenewalRecordTypeOppy.Contract_Term__c = con.Automatic_Renewal_Term_months__c;
                            RenewalRecordTypeOppy.stageName = label.OpportunityStage4; // 4 = 4 - Negotiation
                            RenewalRecordTypeOppy.ownerId = con.Sales__c;
                            RenewalRecordTypeOppy.GE_Standard_Probability__c = label.GEStandardProb2; // 2= medium
                            
                            RenewalRecordTypeOppy.Price_Escalation_COLA__c = con.Price_Escalation_COLA__c;
                            RenewalRecordTypeOppy.Price_Escalation_Start_Date__c = con.Price_Escalation_Start_Date__c;
                            RenewalRecordTypeOppy.Legal_Contracts__c = con.Legal__c;
                            
                            renewal_oppty.add(RenewalRecordTypeOppy);
                        }
                        
                    }
                    
                    //inserting renewal opportunities
                    if(!renewal_oppty.isEmpty())
                        insert renewal_oppty;  
                        
                     for(Opportunity oppy : renewal_oppty)
                        mapOfInsertedFreshOppy.put(oppy.Id, oppy); 
                    
                    if(!oldOpp.Opportunity_Line_Items__r.isEmpty()) {
                        if(!renewal_oppty.isEmpty()) {
                            for(Opportunity opty : renewal_oppty) {  
                                Date RenewalEndDate = opty.CloseDate.addmonths(Integer.valueOf(opty.Contract_Term__c)); 
                                //getting old AP    
                                for(Opportunity_Line_Item__c assPro : oldOpp.Opportunity_Line_Items__r) {
                                    Opportunity_Line_Item__c newAssProd = assPro.clone(false);
                                    newAssProd.Opportunity__c = opty.Id; 
                                // Service Start Date
                                newAssProd.Services_Start_Date_New__c = opty.closeDate.toStartOfMonth().addmonths(Integer.valueof(assPro.Services_Lag_months__c + 1));
                                // Service End Date
                                newAssProd.Services_End_Date_New__c = newAssProd.Services_Start_Date_New__c.addDays(date.daysInMonth(newAssProd.Services_Start_Date_New__c.year() , newAssProd.Services_Start_Date_New__c.month())  - 1).addmonths(Integer.valueof(assPro.Contract_Term_from_Oppy__c - 1)); 
                                // fillup contract lookup JIRA 587
                                newAssProd.Contract1__c = con.Id;
                                
                                listOfNewOli.add(newAssProd);
                                    
                                    //getting old schedules
                                    if(!oldOpp.Schedules__r.isEmpty()) {                    
                                        for(Schedule__c oldSchedule : oldOpp.Schedules__r) {    
                                            Schedule__c newSchedules = new Schedule__c();
                                            if(oldSchedule.Schedule_Date__c >= opty.CloseDate && oldSchedule.Schedule_Date__c < RenewalEndDate){
                                                newSchedules = oldSchedule.clone(false);                        
                                                newSchedules.Opportunity__c = opty.Id;      
                                                if(oldSchedule.Opportunity_Line_Item__c == assPro.id) {
                                                    newSchedules.Opportunity_Line_Item__r = newAssProd;
                                                    listOfNewSchedules.add(newSchedules);
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }    
                    } 
                    
                    if(!listOfNewOli.isEmpty())
                        insert listOfNewOli;
                        
                    for(Opportunity_Line_Item__c oli : listOfNewOli) {
                        mapOfOliIdToOppyId.put(oli.Id, oli.Opportunity__c);
                        mapOfOliIdToOli.put(oli.Id, oli);    
                    }
                        
                    if(!listOfNewSchedules.isEmpty()) {
                        for(Schedule__c schObj: listOfNewSchedules) {                        
                            if(schObj.Opportunity_Line_Item__r != null) {    // is there a relationship to a parent?
                                schObj.Opportunity_Line_Item__c = schObj.Opportunity_Line_Item__r.Id;   // reach through the relationship and get the ID from the parent and set that id value on the child.
                            }
                        }
                        
                        for(Schedule__c newSchedules : listOfNewSchedules) {
                            if(mapOfFreshOliIdToSchedules.containsKey(newSchedules.Opportunity_Line_Item__c)) 
                                mapOfFreshOliIdToSchedules.get(newSchedules.Opportunity_Line_Item__c).add(newSchedules);               
                            else         
                                mapOfFreshOliIdToSchedules.put(newSchedules.Opportunity_Line_Item__c, new List<Schedule__c>{newSchedules});
                        }
                        
                        for(Id ids : mapOfFreshOliIdToSchedules.keySet()) {
                            Double colaAmount = 0;
                            
                            Double power = 1;            
                            Integer countOfStartingYear = 0;     
                            Date my_Check_Date = mapOfInsertedFreshOppy.get(mapOfOliIdToOppyId.get(ids)).Price_Escalation_Start_Date__c;
                                
                            if(mapOfInsertedFreshOppy.get(mapOfOliIdToOppyId.get(ids)).Price_Escalation_COLA__c != 0)
                                colaAmount =  1 + mapOfInsertedFreshOppy.get(mapOfOliIdToOppyId.get(ids)).Price_Escalation_COLA__c/100; 
                        
                            for(Schedule__c freshSch : mapOfFreshOliIdToSchedules.get(ids)) {
                                if(freshSch.Type__c == 'Services') { 
                                
                                    integer noOfDays;
                                    if(mapOfInsertedFreshOppy.get(mapOfOliIdToOppyId.get(ids)).Price_Escalation_COLA__c != 0)    
                                    {       
                                        if(freshSch.Schedule_Date__c > my_Check_Date)
                                            noOfDays = my_Check_Date.daysBetween(freshSch.Schedule_Date__c);
                                        else
                                            noOfDays = freshSch.Schedule_Date__c.daysBetween(my_Check_Date);
                                    }
                                        
                                    //Price Escalation Start Date can be greater than, equal to and less than First Schedule Record date           
                                    while(mapOfInsertedFreshOppy.get(mapOfOliIdToOppyId.get(ids)).Price_Escalation_COLA__c != 0 && freshSch.Schedule_Date__c >= my_Check_Date.addYears(1) && freshSch.Schedule_Date__c <= my_Check_Date.addDays(noOfDays)) {
                                        power++;                                        
                                        my_Check_Date = my_Check_Date.addYears(1);
                                    }
                                 
                                    if(freshSch.Schedule_Date__c >= mapOfInsertedFreshOppy.get(mapOfOliIdToOppyId.get(ids)).Price_Escalation_Start_Date__c && mapOfInsertedFreshOppy.get(mapOfOliIdToOppyId.get(ids)).Price_Escalation_COLA__c != 0)
                                        freshSch.Anticipated_Service_Amount__c = (mapOfOliIdToOli.get(ids).Services_Amount_Annual__c) * Math.pow(colaAmount, power);
                                    else
                                        freshSch.Anticipated_Service_Amount__c = mapOfOliIdToOli.get(ids).Services_Amount_Annual__c;
                                }
                                InitialAMT = InitialAMT + freshSch.Escalated_Revenue__c;
                                mapOfContractToRenAmt.put(Con.Id, InitialAMT);
                                listOfFreshSchedules_To_Insert.add(freshSch);                        
                            }
                        }
                        
                        if(!listOfFreshSchedules_To_Insert.isEmpty())    
                            insert listOfFreshSchedules_To_Insert;
                    }
                 
                }
                    catch(Exception ex) {
                        System.debug(' Following exception occured while inserting Schedules in opportunity in method: Generate and class: ScheduleUtility ' + ex.getMessage());
                    }
                    //Calculate TOV
                    validatePipelineTOV(con, OldOpp, mapOfContractToRenAmt.get(con.Id));
                    
                    
                }
                
            }   
  
    public static void validatePipelineTOV(Contract_PO__c con, Opportunity PipelineOpp, Double InitialRenAmount){
            double pipelineAmt = PipelineOpp.Total_Order_Value__c;
            if(InitialRenAmount.longValue()!=pipelineAmt.longValue() && Label.Label_PipelineTOVValidationRequired=='true'){
               //system.assertEquals(pipelineAmt.longValue(),InitialRenAmount.longValue(),Label.Label_PipelineTOVValidationRequired);
               con.addError(Label.validatePipelineTOV);
                
            }
            
    }
    
    // This future method is being called inside handler class "userTriggerHandler".To avoid mixed DML exception
    //   we used this future methods that assign permission set/groups/Managed package to user. 
       
    @future
    public static void AddUserToPermissionSetAndGroups(Set<Id> BusinessUserIds, set<Id> newUserIds )
    {
      list<GroupMember> GroupMembers = new list<GroupMember>();
      list<PermissionSetAssignment> listOfPSAs = new list<PermissionSetAssignment>();
      list<PermissionSet> permissionSets = new list<PermissionSet>();
      List<PackageLicense> gridBuddyLicense = new List<PackageLicense>();
      List<UserPackageLicense> firstUPLs = new List<UserPackageLicense>();
      
      set<id> AllGroupId = new set<id>();
      map<string, set<id>> GroupUsers =   new map<string, set<id>>();
      set<id> AllPermissionSetId = new set<id>();
      set<id> managePackageId = new set<id>();
      map<string, set<id>> PermissionSetUsers =   new map<string, set<id>>();
      map<string, set<id>> managePackageUsers =   new map<string, set<id>>();
      
      list<Group> listGroupToAdd = [select id, Name from Group where DeveloperName  = 'Opp_Sharing_Rule'];
      permissionSets =  [select id, Name from PermissionSet where Name in ('API_Enable','Enable_Single_Sign_On')];
      gridBuddyLicense = [select AllowedLicenses, UsedLicenses, Id from PackageLicense where NamespacePrefix = 'GBLite']; 
      
      if(!listGroupToAdd.isEmpty()){
        AllGroupId = (new Map<Id,SObject>(listGroupToAdd)).keySet();
      }
      
      if(!permissionSets.isEmpty()){
        AllPermissionSetId = (new Map<Id,SObject>(permissionSets)).keySet();
      }
      
      if(!gridBuddyLicense.isEmpty()){
        managePackageId = (new Map<Id,SObject>(gridBuddyLicense)).keySet();
      }
      
      for(GroupMember groupUser : [select id, GroupId, UserOrGroupId from GroupMember where UserOrGroupId=: BusinessUserIds and GroupId=: AllGroupId]){
              if(GroupUsers.containsKey(groupUser.GroupId))
                  GroupUsers.get(groupUser.GroupId).add(groupUser.UserOrGroupId);
              else
                  GroupUsers.put(groupUser.GroupId,new set<id>{groupUser.UserOrGroupId});
         }
         
      
      set<id> AssignedUsersToGroup= new set<id>();
      
      for(id uid : BusinessUserIds){
         // insert into Group
         for(group g : listGroupToAdd){
                if(GroupUsers.containsKey(g.Id)){
                    AssignedUsersToGroup = GroupUsers.get(g.Id);
                    if(!AssignedUsersToGroup.isEmpty()){
                         if(!(AssignedUsersToGroup.contains(uid))){
                             GroupMember GM = new GroupMember( GroupId=g.id,UserOrGroupId = uid);
                             GroupMembers.add(GM);
                         }
                     
                    }
                }else{
                        GroupMember GM = new GroupMember( GroupId=g.id,UserOrGroupId = uid);
                       GroupMembers.add(GM);
                }
             }
         
      }
      if(!GroupMembers.isEmpty()){
         try{
            insert GroupMembers;
         }Catch (exception ex){
             System.debug(' Following exception occured on adding users to group in method: AddToGroups and class : ScheduleUtility ' + ex.getMessage());
         }
       }
       
       for(PermissionSetAssignment PermUser : [select id, PermissionSetId, AssigneeId from PermissionSetAssignment  where AssigneeId=: newUserIds and PermissionSetId=: AllPermissionSetId]){
              if(PermissionSetUsers.containsKey(PermUser.PermissionSetId))
                  PermissionSetUsers.get(PermUser.PermissionSetId).add(PermUser.AssigneeId);
              else
                  PermissionSetUsers.put(PermUser.PermissionSetId,new set<id>{PermUser.AssigneeId});
         }
       
        for(UserPackageLicense PackageUser : [select id, PackageLicenseId, UserId from UserPackageLicense  where UserId=: newUserIds and PackageLicenseId=: managePackageId]){
              if(managePackageUsers.containsKey(PackageUser.PackageLicenseId))
                  managePackageUsers.get(PackageUser.PackageLicenseId).add(PackageUser.UserId);
              else
                  managePackageUsers.put(PackageUser.PackageLicenseId,new set<id>{PackageUser.UserId});
         }
       
        set<id> AssignedUsersToPermissionSet= new set<id>();
        set<id> AssignedUsersToPackage= new set<id>();
        
       if(!newUserIds.isEmpty()){
           for(id newUserId : newUserIds){
                // insert into Permissionsets
                if(!permissionSets.isEmpty()){
                    for(PermissionSet pSet : permissionSets){
                        if(managePackageUsers.containsKey(pSet.Id)){
                            AssignedUsersToPermissionSet = managePackageUsers.get(pSet.Id);
                            if(!AssignedUsersToPermissionSet.isEmpty()){
                                if(!(AssignedUsersToPermissionSet.contains(newUserId))){
                                    PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = pSet.id, AssigneeId = newUserId);
                                    listOfPSAs.add(psa);
                                }
                     
                            }
                        }else{
                            PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = pSet.id, AssigneeId = newUserId);
                            listOfPSAs.add(psa);
                         }
                    }
                }
                
                 
                 // To assign Gridbuddy license
                if(!gridBuddyLicense.isEmpty()){
                    
                    for(PackageLicense ManageLicense : gridBuddyLicense){
                        if(managePackageUsers.containsKey(ManageLicense.Id)){
                            AssignedUsersToPackage = managePackageUsers.get(ManageLicense.Id);
                            if(!AssignedUsersToPackage.isEmpty()){
                                if(!(AssignedUsersToPackage.contains(newUserId))){
                                    UserPackageLicense upl = new UserPackageLicense(PackageLicenseId = gridBuddyLicense[0].Id, UserId = newUserId);
                                    firstUPLs.add(upl);
                                }
                     
                            }
                        }else{
                            UserPackageLicense upl = new UserPackageLicense(PackageLicenseId = gridBuddyLicense[0].Id, UserId = newUserId);
                            firstUPLs.add(upl);
                         }
                    }
                    
                    
                }
             }
         }
         
         if(!listOfPSAs.isEmpty()){
             try{
                insert listOfPSAs;
             }Catch (exception ex){
                 System.debug(' Following exception occured on adding users to permission set: AddToGroups and class : ScheduleUtility ' + ex.getMessage());
             }
         }
         
         if(!firstUPLs.isEmpty()){
             try{
                insert firstUPLs;
             }Catch (exception ex){
                 System.debug(' Following exception occured on adding users to gridBuddy: AddToGroups and class : ScheduleUtility ' + ex.getMessage());
             }
         }
            
    }
    
  
  /* @future
    public static void futureContactMethod(string jsonlistofContactstoInsert){
        List<Contact> futureContacts = (JSON.deserialize(jsonlistofContactstoInsert));
        insert futureContacts;
 
    }
    
        @future
    public static void insertChatterGroupMembers( list<CollaborationGroupMember> chatterGroupMembers){
        insert chatterGroupMembers;
 
    } */
 

}