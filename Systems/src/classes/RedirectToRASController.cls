/*************************************************************************************************
    Author : Suman Gupta
    Date   : 13 Aug 2015
    
    Action : This class is used as controller of vf page : RedirectToRAS
             This class is used to get Opp data for RAS template PDF and display regular text in Perspecsys URL.
             This is related to JIRA: ASSFDC-441
    Change desription:
    Developer         Date                  Description
    Abhishek          7/7/2016        Removed the reference of "Oppty_SR_Intersection__c" to delete the same object "Jira 564"
***********************************************************************************************/

public with sharing class RedirectToRASController {

    public Sales_Request__c objResponse;    
    public RedirectToRASController(ApexPages.StandardController controller) {
    objResponse = new Sales_Request__c();
        if(Apexpages.currentpage().getparameters().get('Id') != null) {
            objResponse = [select Id, Response_Description__c
                          from Sales_Request__c 
                          where Id =: Apexpages.currentpage().getparameters().get('Id')];
          
        }    
    }

}