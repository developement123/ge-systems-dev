/*
* ClassName: UserInfo
* CreatedBy: Venkata Krishna Koppula
* LastModifiedBy: Venkata Krishna Koppula
* LastModifiedOn: 02 Apr 2012
* CalledFrom: AccountMasterHandler,...
* Description: 
* ---------------------------------------
* Revision History
* 02 Apr 2012 – Initial Creation
*/
public class UserData {

    public static User usr;
    
    public static User getUserInfo() {
        
        if( usr == null ) 
            usr=[select user.Username, UserRoleId,UserRole.name, Name,profile.name,Email,FederationIdentifier from User where id=:userinfo.getUserid()];
        return usr;    
    }
}