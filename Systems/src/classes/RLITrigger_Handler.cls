/***************************************************************************************************************************************************************************
    Developer : Mohan Kumar
    Date      : 04/May/2015
    Action    : This class is handler class for the trigger : RLI_AutoNameGenrationtrigger 
    
**************************************************************************************************************************************************************************/

public with sharing class RLITrigger_Handler {     
      
    public void RLIHandler_method(List<Response_Line_Item__c> newList, List<Response_Line_Item__c> oldList, Map<Id, Response_Line_Item__c> newMap, Map<Id, Response_Line_Item__c> oldMap, String eventType, String triggerType) {
        
        if(eventType == 'Before' && triggerType == 'Insert') {
        
            Set<Id> setOfRespIds = new Set<Id>();
            List<Sales_Request__c> listOfRespToBeUpdated = new List<Sales_Request__c>();
            
            for(Response_Line_Item__c rli : newList) {
                setOfRespIds.add(rli.Response__c);        // this set will contain Response Id's.
            }
                
            Map<Id, Sales_Request__c> mapOfSR = new Map<Id, Sales_Request__c>();    
            
            for(Sales_Request__c sr : [select Id, Name, Response_ID2__c, Max_RLI_Number__c from Sales_Request__c where Id IN : setOfRespIds]) {       
                mapOfSR.put(sr.Id, sr);        
            }
                
            for(Response_Line_Item__c rli : newList) {
                
                if(mapOfSR.get(rli.Response__c).Max_RLI_Number__c != null) {
                    Integer numberToAdd = Integer.valueOf(mapOfSR.get(rli.Response__c).Max_RLI_Number__c) + 1;
                    
                    mapOfSR.get(rli.Response__c).Max_RLI_Number__c += 1; 
                    
                    if(rli.Type__c == 'Walk' || rli.Type__c == 'Offer')    // if RLI type is Offer/Walk then, trigger will update TG2 as true. 
                        rli.TG2__c = true;     
                    
                    rli.Name = mapOfSR.get(rli.Response__c).Response_ID2__c  + '-RLI-' + numberToAdd; 
                    rli.Last_Number_RLI__c = numberToAdd;
                }
                else if(mapOfSR.get(rli.Response__c).Max_RLI_Number__c == null) {
                    Integer numberToAdd = 1;
                    
                    mapOfSR.get(rli.Response__c).Max_RLI_Number__c = 0;
                    
                    mapOfSR.get(rli.Response__c).Max_RLI_Number__c += 1;
                    
                    if(rli.Type__c == 'Walk' || rli.Type__c == 'Offer')
                        rli.TG2__c = true;  
                        
                    rli.Name = mapOfSR.get(rli.Response__c).Response_ID2__c  + '-RLI-' + numberToAdd;    
                    rli.Last_Number_RLI__c = numberToAdd;      
                }   
            }
            
            if(!mapOfSR.values().isEmpty()) {        // this map.values will update 'max rli number' field in response object.
                try {
                    update mapOfSR.values();        
                }
                catch(Exception ex) {
                    System.debug(' Following exception occured while updating responses in Class : RLITrigger_Handler ' + ex.getMessage());
                }    
            }
        }       
        
        if(eventType == 'Before' && triggerType == 'Update') {    // if RLI type is Offer/Walk then, trigger will update TG2 as true. 
            for(Response_Line_Item__c rli : newList) {
                if(rli.Type__c == 'Walk' || rli.Type__c == 'Offer')
                    rli.TG2__c = true;                  
            }
        }
        
        List<Response_Line_Item_History__c> listOfRLIHis = new List<Response_Line_Item_History__c>();
        
        if(eventType == 'After' && triggerType == 'Insert') {
        
            for(Response_Line_Item__c rli : newList) {
            
                Response_Line_Item_History__c rliHis = new Response_Line_Item_History__c(Date__c = System.now(), Response_Line_Item__c = rli.Id, Response_Id__c = rli.Response__c, User__c = UserInfo.getUserId(), Response_Line_Item_Text__c = rli.name);
                
                if(!rli.iDeal_Flag__c)
                    rliHis.Desciption__c = 'Created by Manual Entry.';
                else if(rli.iDeal_Flag__c)
                    rliHis.Desciption__c = 'Created by iDeal Upload.';               
                listOfRLIHis.add(rliHis);
            }
        }
        
        if(eventType == 'After' && triggerType == 'Update') {  
            for(Response_Line_Item__c rli : newList) {
                if(oldMap.get(rli.id).iDealRunName__c != newMap.get(rli.id).iDealRunName__c) {
                   
                    Response_Line_Item_History__c rliHis = new Response_Line_Item_History__c(Date__c = System.now(), Response_Line_Item__c = rli.Id, Response_Id__c = rli.Response__c, User__c = UserInfo.getUserId(), Response_Line_Item_Text__c = rli.name, Is_RLI_updated__c = true);
                    rliHis.Field_updated__c = 'iDeal Run Name';
                    rliHis.Old_Description__c = oldMap.get(rli.id).iDealRunName__c;
                    rliHis.Desciption__c = newMap.get(rli.id).iDealRunName__c;
                    listOfRLIHis.add(rliHis);
                }
                if(oldMap.get(rli.id).TG2__c != newMap.get(rli.id).TG2__c) {
                   
                    Response_Line_Item_History__c rliHis = new Response_Line_Item_History__c(Date__c = System.now(), Response_Line_Item__c = rli.Id, Response_Id__c = rli.Response__c, User__c = UserInfo.getUserId(), Response_Line_Item_Text__c = rli.name, Is_RLI_updated__c = true);
                    rliHis.Field_updated__c = 'TG2';
                    rliHis.Old_Description__c = String.valueOf(oldMap.get(rli.id).TG2__c);
                    rliHis.Desciption__c = String.valueOf(newMap.get(rli.id).TG2__c); 
                    
                    listOfRLIHis.add(rliHis);
                }
                if(oldMap.get(rli.id).Approved__c != newMap.get(rli.id).Approved__c) {
                   
                    Response_Line_Item_History__c rliHis = new Response_Line_Item_History__c(Date__c = System.now(), Response_Line_Item__c = rli.Id, Response_Id__c = rli.Response__c, User__c = UserInfo.getUserId(), Response_Line_Item_Text__c = rli.name, Is_RLI_updated__c = true);
                    rliHis.Field_updated__c = 'Approved';
                    rliHis.Old_Description__c = String.valueOf(oldMap.get(rli.id).Approved__c);
                    rliHis.Desciption__c = String.valueOf(newMap.get(rli.id).Approved__c);
                    
                    listOfRLIHis.add(rliHis);
                }
                if(oldMap.get(rli.id).Type__c != newMap.get(rli.id).Type__c) {
                   
                    Response_Line_Item_History__c rliHis = new Response_Line_Item_History__c(Date__c = System.now(), Response_Line_Item__c = rli.Id, Response_Id__c = rli.Response__c, User__c = UserInfo.getUserId(), Response_Line_Item_Text__c = rli.name, Is_RLI_updated__c = true);
                    rliHis.Field_updated__c = 'Type';
                
                    rliHis.Old_Description__c = oldMap.get(rli.id).Type__c;
                    rliHis.Desciption__c = newMap.get(rli.id).Type__c;
                        
                    listOfRLIHis.add(rliHis);
                }
            }    
        }        
        
        if(eventType == 'After' && triggerType == 'Delete') {
            
            for(Response_Line_Item__c rli : oldList) {
                Response_Line_Item_History__c rliHis = new Response_Line_Item_History__c(Date__c = System.now(), Response_Id__c = rli.Response__c, User__c = UserInfo.getUserId(), Response_Line_Item_Text__c = rli.name);
                
                rliHis.Desciption__c = 'Deleted.';                
                listOfRLIHis.add(rliHis);
            }
        }
        
        if(!listOfRLIHis.isEmpty()) {
            try {
                insert listOfRLIHis;        
            }
            catch(Exception ex) {
                System.debug(' Following exception occured while inserting RLI History in class : RLITrigger_Handler ' + ex.getMessage());
            }    
        }
    } 
}