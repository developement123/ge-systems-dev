/**********************************************************************************************************************************************************
    Author       :   Mohan Kumar 
    Created Date :   15/April/2015
    Action       :   This class is used as extensions in vf page : 
                         displayResponseLineItemHistory

**********************************************************************************************************************************************************/


public with sharing class extDisplayRliHistory { 
    
    public List<Response_Line_Item_History__c> listOfRLIHis{get;set;}    
    public Id idfromController{get;set;}
    
    public extDisplayRliHistory(ApexPages.StandardController controller) {
        listOfRLIHis = new List<Response_Line_Item_History__c>();        
        idfromController = controller.getId();
         
        String theQuery = 'select Id, Name, Field_updated__c, Is_RLI_updated__c, Old_Description__c, Response_Line_Item_Text__c, Date__c, Desciption__c, Response_Line_Item__c, User__c, User__r.Name from Response_Line_Item_History__c where Response_Id__c =: idfromController Order by Date__c DESC';
    
        listOfRLIHis = Database.query(theQuery);
    }
}