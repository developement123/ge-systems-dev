/******************************************************************************************************************
    Author   : Abhishek
    Date     : 2016
    Action   : This is the handler class for trigger 'TriggerOnAccount'.
    
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        09/12/2017      remove account platform add Opportunity or major response on Account deletion
**************************************************************************************************************/
public with sharing class AccountTriggerHandler{
 // Method 1: To prevent account deletion, if it has related Opportunity or major response
public void AccountValidationOnDelete(map<id, Account> oldMap,  String eventType, String triggerType){
    if(label.Label_AccountValidationOnDelete=='true'){
        set<id>accid = new set<id>();
        for( Opportunity Opp : [select id, AccountId, Child_Account__c from Opportunity where (AccountId=:oldMap.keyset() or Child_Account__c=:oldMap.keyset() )] ){
            accid.add(Opp.AccountId); 
            accid.add(Opp.Child_Account__c);
        }
        
        for(Major_Proposal__c mp : [select id, Account__c from Major_Proposal__c where id=:oldMap.keyset()]){
            accid.add(mp.Account__c); 
        }
        
        if(eventType == 'Before' && triggerType == 'Delete'){
            for( Account acc : oldMap.values() ){
                 if(accid.contains(acc.id)){
                  acc.adderror(Label.AccountValidationOnDelete);
               }
            }
        }
    }
 }
}