/*************************************************************************************************************************************************
This test class was created to provide code coverage for the following trigger:
- updateSTonOppyOwnerUpdate

Version     Author(s)               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0         Mohan kumar          12/05/2014        Initial Creation
            Abhishek             04/28/2016        Commented all as per JIRA 553.            
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_updateSTonOppyOwnerUpdate {
/*
     static testmethod void test_updateSTonOppyOwnerUpdate() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; 
        
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;

        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        User u2 = new User(alias = 'mkumar', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'mohan.kumar@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'mohan.kumar@ge.com.sys.dev');
        insert u2;
        
        Account acc1 = new Account(NAME = 'Hello');
        insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;

            opportunity opp = new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),Margin_Escalator__c = false,MTG_Year__c = '2014');

            insert opp;
            
            Sales_Target__c st = new Sales_Target__c(Sales_Person__c = u.id,Sales_Year__c='2014');
            insert st;
            Sales_Target__c st1 = new Sales_Target__c(Sales_Person__c = u2.id,Sales_Year__c='2014');
            insert st1;
           
         Test.startTest(); 
            opp.ownerId = u2.Id;
            update opp;  
         Test.stopTest();   
        }
    }   */ 
}