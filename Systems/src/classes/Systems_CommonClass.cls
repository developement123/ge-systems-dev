/******************************************************************************************************************************************
    Author : Mohan Kumar
    Date     : 08/October/2015
    Action   : This class has methods which are common for triggers/batch/classes. 
    
******************************************************************************************************************************************/

public With sharing class Systems_CommonClass { 

    /************************************************************************************************************************************
        Method 1:
            following method 'updateCurrentYear_ME_And_MTG_Year' is to 
                1. create/update current year 'Margin Escalator' based on available schedules.
                2. update MTG Year in corresponding opportunity
    ************************************************************************************************************************************/

    public void updateCurrentYear_ME_And_MTG_Year(Set<Id> setOfFiltredOppyIds) {
    
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;    // these variables will prevent others triggers from firing.
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;      
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        
        List<Margin_Escalator__c> updateMEList = new List<Margin_Escalator__c>();    // this list will insert/update the ME records.
        Map<Id, List<Schedule__c>> mapOfOppyIdToSchedules = new Map<Id, List<Schedule__c>>();    // Map of Opportunity Id vs all available schedules.
        Map<Id, List<Margin_Escalator__c>> mapOfOppyIdToMEList = new Map<Id, List<Margin_Escalator__c>>();    // // Map of Opportunity Id vs current Year ME.
        
        Map<Id, Opportunity> mapOfIdToOppy = new Map<Id, Opportunity>();    // Map of Opportunity Id vs Opportunity.
        
        Date fromdate = Date.newInstance(Integer.valueOf(date.today().Year()), 1, 1);
        Date todate   = Date.newInstance(Integer.valueOf(date.today().Year()), 12, 31);
        String currentYear = String.valueOf(date.today().Year());
        
        for(Schedule__c ols: [Select Id, Quantity__c, Schedule_Date__c , Opportunity_Line_Item__r.Probability__c, Escalated_Revenue__c, Opportunity__c 
                                    from Schedule__c
                                    where Opportunity__c IN: setOfFiltredOppyIds
                                    AND Schedule_Date__c >= : fromdate 
                                    AND Schedule_Date__c <= : todate]) {
            if(mapOfOppyIdToSchedules.containsKey(ols.Opportunity__c)) 
                mapOfOppyIdToSchedules.get(ols.Opportunity__c).add(ols);               
            else         
                mapOfOppyIdToSchedules.put(ols.Opportunity__c, new List<Schedule__c>{ols});                    
        }
        
        for(Opportunity o : [Select Id, closeDate, Margin_Escalator__c, (Select Id, Year__c, Out_of_Sync__c, Opportunity__c from Margin_Escalator__r where Year__c =: currentYear)
                                from Opportunity
                                where Id IN: setOfFiltredOppyIds]) {
            mapOfIdToOppy.put(o.id, o);
            for(Margin_Escalator__c me : o.Margin_Escalator__r) {
                if(mapOfOppyIdToMEList.containsKey(o.Id)) 
                    mapOfOppyIdToMEList.get(o.Id).add(me);               
                else         
                    mapOfOppyIdToMEList.put(o.Id, new List<Margin_Escalator__c>{me}); 
            } 
        }
        
        Set<Id> processedOppyIds = new Set<Id>();
        
        for(Id ids: setOfFiltredOppyIds) {
            
            if((mapOfOppyIdToSchedules.get(ids) == null || mapOfOppyIdToSchedules.get(ids).isEmpty()) 
                && (mapOfOppyIdToMEList.get(ids) != null && !mapOfOppyIdToMEList.get(ids).isEmpty())) {
               
                processedOppyIds.add(ids);               
                Margin_Escalator__c updateExistingME = mapOfOppyIdToMEList.get(ids)[0];
                
                if(!updateExistingME.Out_of_Sync__c) {
                    updateExistingME.Jan__c = 0.0;
                    updateExistingME.Feb__c = 0.0;
                    updateExistingME.Mar__c = 0.0;
                    updateExistingME.Apr__c = 0.0;
                    updateExistingME.May__c = 0.0;
                    updateExistingME.Jun__c = 0.0;
                    updateExistingME.Jul__c = 0.0;
                    updateExistingME.Aug__c = 0.0;
                    updateExistingME.Sept__c = 0.0;
                    updateExistingME.Oct__c = 0.0;
                    updateExistingME.Nov__c = 0.0;
                    updateExistingME.Dec__c = 0.0;
                    updateMEList.add(updateExistingME);                
                }
            }
            
            if(!mapOfOppyIdToMEList.isEmpty() && mapOfOppyIdToMEList.get(ids) != null && !mapOfOppyIdToMEList.get(ids).isEmpty() && !processedOppyIds.contains(ids)) {
           
                processedOppyIds.add(ids);
                Margin_Escalator__c updateExistingME = mapOfOppyIdToMEList.get(ids)[0];
                
                if(!updateExistingME.Out_of_Sync__c) {
                    if(mapOfOppyIdToSchedules.get(ids) != null && !mapOfOppyIdToSchedules.get(ids).isEmpty())                        
                        updateMEList.add(calculatingME(updateExistingME, mapOfOppyIdToSchedules.get(ids))); ////calling "calculatingME" method
                    else {
                        updateExistingME.Jan__c = 0.0;
                        updateExistingME.Feb__c = 0.0;
                        updateExistingME.Mar__c = 0.0;
                        updateExistingME.Apr__c = 0.0;
                        updateExistingME.May__c = 0.0;
                        updateExistingME.Jun__c = 0.0;
                        updateExistingME.Jul__c = 0.0;
                        updateExistingME.Aug__c = 0.0;
                        updateExistingME.Sept__c = 0.0;
                        updateExistingME.Oct__c = 0.0;
                        updateExistingME.Nov__c = 0.0;
                        updateExistingME.Dec__c = 0.0;
                        updateMEList.add(updateExistingME);
                    }
                }
            }            
            
        }
        
        List<Opportunity> listOfOpportunityToUpdate = new List<Opportunity>();
        
        for(Id ids: setOfFiltredOppyIds) {
            if(!processedOppyIds.contains(ids)) {
                Margin_Escalator__c meNew = new Margin_Escalator__c(Opportunity__c = ids, Year__c = String.valueOf(date.today().Year()));
                
                if(!mapOfIdToOppy.isEmpty()
                     && mapOfIdToOppy.get(ids) != null
                     && mapOfIdToOppy.get(ids).Margin_Escalator__c
                     && mapOfIdToOppy.get(ids).CloseDate.Year() == System.Today().Year()) {
                        meNew.MTG__c = true;
                        listOfOpportunityToUpdate.add(new Opportunity(id = ids, MTG_Year__c = String.valueOf(System.Today().Year())));
                }
                updateMEList.add(calculatingME(meNew, mapOfOppyIdToSchedules.get(ids)));  //calling "calculatingME" method
            }
        }
        
        if(!updateMEList.isEmpty()) {    // if this list is not empty then it will insert/update ME records.
            try {
                Class_TriggerCheck.var_TriggerOnMarginEscalator= false;    // this static variable will prevent ME trigger (before Insert & before Update event) from firing.
                upsert updateMEList;
            }
            catch(Exception e) {
                System.debug('Following exception has occurred while updating Margin Escalators in method: updateCurrentYear_ME_And_MTG_Year in Class : Systems_CommonClass: ' + e.getMessage());
            }
        } 
        
        if(!listOfOpportunityToUpdate.isEmpty()) {    // this list will update MTG Year in opportunity.
            try {
                update listOfOpportunityToUpdate;
            }
            catch(Exception e) {
                System.debug('Following exception has occurred while updating MTG Year in opportunity in method: updateCurrentYear_ME_And_MTG_Year in Class : Systems_CommonClass: ' + e.getMessage());
            }
        }
    }
    
    /************************************************************************************************************************************
        Method 2:
            following method 'calculatingME' is to 
                1. calculate Margin Escalator field values and return the Margin Escalator to the calling method.
    ************************************************************************************************************************************/
    
    public Margin_Escalator__c calculatingME(Margin_Escalator__c me, List<Schedule__c> listOfOlis) {
       
       Double janValue = 0;
       Double febValue = 0;
       Double marValue = 0;
       Double aprValue = 0;
       Double mayValue = 0;
       Double juneValue = 0;
       Double julyValue = 0;
       Double augValue = 0;
       Double septValue = 0;
       Double octValue = 0;
       Double novValue = 0;
       Double decValue = 0;
       
       if(listOfOlis != null && !listOfOlis.isEmpty()) {     
       
           for(Schedule__c ols : listOfOlis) {    
                            
               if(ols.Opportunity_Line_Item__r.Probability__c != null && ols.Escalated_Revenue__c != null) { 
                    if(ols.Schedule_Date__c.Month() == 1) 
                        janValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 2) 
                        febValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 3) 
                        marValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 4) 
                        aprValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 5) 
                        mayValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 6) 
                        juneValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 7) 
                        julyValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 8) 
                        augValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 9) 
                        septValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                                        
                    else if(ols.Schedule_Date__c.Month() == 10) 
                        octValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 11) 
                        novValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                    
                    else if(ols.Schedule_Date__c.Month() == 12) 
                        decValue += (ols.Escalated_Revenue__c*ols.Opportunity_Line_Item__r.Probability__c)/100;
                }                
            }
        }
        
        me.Jan__c = janValue;
        me.Feb__c = febValue;
        me.Mar__c = marValue;
        me.Apr__c = aprValue;
        me.May__c = mayValue;
        me.Jun__c = juneValue;  
        me.Jul__c = julyValue;
        me.Aug__c = augValue;
        me.Sept__c= septValue;
        me.Oct__c = octValue;
        me.Nov__c = novValue;
        me.Dec__c = decValue;
        
        return me;
    }
}