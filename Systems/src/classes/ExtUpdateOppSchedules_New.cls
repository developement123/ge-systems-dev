/********************************************************************************************
    Author : Mohan Kumar
    Date   : 08 October 2015
    
    Action : This class is used as extensions in vf page: 'UpdateOppSchedules_New'.
             This new version of class has been created as a part of Opportunity Optimization Requirement.
    
    Change History
    =============
    Developer       Modified On          Change description
    Abhishek        05/20/2016           Changed as per requirement mentioned in JIRA 523, Modified to show 2nd screen on page load and added functionality to move orders if user want
****************************************************************************************************/

public with sharing class ExtUpdateOppSchedules_New {

    public final Opportunity opp{get;set;}
    public String oppId{get;set;}
    
    public Boolean hasRecords{get;set;}
    public Boolean isFirstScreen{get;set;}    // if this variable is true then first screen will be rendered.
    public Boolean isSecondScreen{get;set;}   // if this variable is true then second screen will be rendered.
    public Boolean isThirdScreen{get;set;}    // if this variable is true then third screen will be rendered.
    
    public Integer addNoOfDays{get;set;}
    public Boolean isOrderDateMoveRequired{get;set;} // whether order need to move 
    
    public List<wrapperOLI> lstwrapperOLI{get;set;}
    private List<Opportunity_Line_Item__c> lstOLI{get;set;}
    private List<Schedule__c> lstOLS{get;set;}
    
    public ExtUpdateOppSchedules_New(ApexPages.StandardController controller) {        
        hasRecords = false;
        isFirstScreen = false; // changed as per Jira 523 - no need to show first screen
        isSecondScreen = true; //  show 2nd screen on page load
        isThirdScreen = false;
        lstwrapperOLI = new List<wrapperOLI>();
        this.opp = (Opportunity)controller.getRecord();
        
        oppId = ApexPages.CurrentPage().getParameters().get('id');
        
        lstOLI = [select Id, Is_Tokenized__c, Opportunity__c, Product__c, Product__r.Name, Product_Code__c, Product_Child1__c, Total_Price__c, Probability__c,
                        Quantity__c, Internal__c, Revenue_Stream_child_level__c, Execution_Site__c, First_Ship_Date__c
                    from Opportunity_Line_Item__c 
                    where Opportunity__c =: oppId];
        
        if(!lstOLI.isEmpty()) {
            hasRecords = true;
            lstOLS = [select Id, Opportunity_Line_Item__c, Schedule_Date__c 
                        from Schedule__c 
                        where Opportunity_Line_Item__c IN: lstOLI];
            showSecondScreen();         
        }
    }
    
    public class wrapperOLI {    // this class is to bind Associated Product and Boolean variable in single object.
        public Opportunity_Line_Item__c objOLI{get;set;}
        public Boolean isProcess{get;set;}
         
        
        public wrapperOLI(Opportunity_Line_Item__c objTempOLI) {
            objOLI = objTempOLI;
            isProcess = false;
            
        }
    }
    
    /*****************************************************************************************************************************************************
        following method 'doUpdateSchedules' gets invoked when Done button is clicked on
        third screen. 
        This method updates Schedules. 
        Also it updates Has_Date_Changed__c field in opportunity to false.            
        Note: No need to update Associated Products in this new version of class (as per logic written in old version of class) 
              as now, all trigger logic is there on 'Schedule' Custom Object instaed of 'Associated Product' object
    *****************************************************************************************************************************************************/
    
    public void doUpdateSchedules() {
        
        List<Schedule__c> listOfSchedulesToUpdate = new List<Schedule__c>();
        List<Opportunity_Line_Item__c> listOfAssoProducts = new List<Opportunity_Line_Item__c>();
        
        set<id> setOfOrderToUpdate = new set<id>();
        List<order__c> listOfOrdersToUpdate = new List<order__c>();
        
        if(!lstwrapperOLI.isEmpty()) {
            for (wrapperOLI eachWrp : lstwrapperOLI) {
                if(eachWrp.isProcess == true)
                    listOfAssoProducts.add(eachWrp.objOLI);
            }
        }    
            
        if(!listOfAssoProducts.isEmpty()) {
           for(Schedule__c eachOLS : [select Id, Opportunity_Line_Item__c, Schedule_Date__c, Opportunity__c
                                                       FROM Schedule__c
                                                       where Opportunity_Line_Item__c IN: listOfAssoProducts and Order__r.Status__c = 'Open']){
                Schedule__c schTemp = new Schedule__c(id=eachOLS.id, Schedule_Date__c = eachOLS.Schedule_Date__c);
                schTemp.Schedule_Date__c = schTemp.Schedule_Date__c.addDays(addNoOfDays);
                listOfSchedulesToUpdate.add(schTemp);
                setOfOrderToUpdate.add(eachOLS.Opportunity__c);
           }
           // added as per Jira 523
           if((!setOfOrderToUpdate.isEmpty()) && isOrderDateMoveRequired){
             for(order__c ord : [select id, PO_Award_Date__c, PO_Award_Start_Date__c, PO_Award_End_Date__c from order__c where PO_Schedule_Opportunity__c =:setOfOrderToUpdate and Status__c='Open']){
                ord.PO_Award_Date__c = ord.PO_Award_Date__c.addDays(addNoOfDays);
                ord.PO_Award_Start_Date__c = ord.PO_Award_Start_Date__c.addDays(addNoOfDays);
                ord.PO_Award_End_Date__c = ord.PO_Award_End_Date__c.addDays(addNoOfDays);
                listOfOrdersToUpdate.add(ord);
             }
           }
          
        }
        
        if(!listOfSchedulesToUpdate.isEmpty()) {
            try {
                update listOfOrdersToUpdate;
                update listOfSchedulesToUpdate;
            }
            catch(DmlException ex) {
                System.debug('The following exception has occurred while updating Schedules in class ExtUpdateOppSchedules_New: ' + ex.getMessage());
            }
        }           
        opp.Has_Date_Changed__c = false;
        
        try {
            update opp;
        }        
        catch(DmlException ex) {
            System.debug('The following exception has occurred while updating opportunities in class ExtUpdateOppSchedules_New: ' + ex.getMessage());    
        }
        
        isThirdScreen = false;
    }    
    
    public void showSecondScreen() {    // this method is invoked when 'Next' button is clicked on the first screen.
        isFirstScreen = false;
        isSecondScreen = true;
        
        if(lstwrapperOLI == null || lstwrapperOLI.size() == 0) {
            lstwrapperOLI = new List<wrapperOLI>();
           
            if(!lstOLI.isEmpty()) {
                for(Opportunity_Line_Item__c eachOLI: lstOLI) {
                    wrapperOLI objWrp = new wrapperOLI(eachOLI);
                    lstwrapperOLI.add(objWrp);
                }
            }
        }       
    }    
    
    public void backToFirstScreen() {    // this method is invoked when 'Previous' button is clicked on the second screen.
        isFirstScreen = true;
        isSecondScreen = false;
    }    
    
    public void showThirdScreen() {    // this method is invoked when 'Next' button is clicked on the screen.
        isSecondScreen = false;
        isThirdScreen = true;
        isOrderDateMoveRequired = true; // As per JIRA 523, checkbox should be true
    }
        
    public void backToSecondScreen() {     // this method is invoked when 'Previous' button is clicked on the third screen.
        isSecondScreen = true;
        isThirdScreen = false;
    }        
}