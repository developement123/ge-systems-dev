/****************************************************************************
    Developer : Abhishek
    Date      : 29/June/2016    
    Action    : This handler class invoked by "TriggerOnProductHierarchy"

    Change desription:
    Developer         Date                  Description
*****************************************************************************/
public with sharing class ProductHierarchyTriggerHandler{
    
     /***********************************************************************************************************
        Method 1 :  To prevent Product Hierarchy deletion, if it is being used in Opportunity
    ************************************************************************************************************/
    public void ValidateProductHierarchyOnDelete(Map<id,Product_Hiearchy_Object__c> oldMap, String eventType, String triggerType){
        if(Label.Label_ValidateProductHierarchyOnDelete == 'true' && eventType == 'Before' && triggerType == 'Delete'){
            map<id,list<Opportunity>> mapOfProductHierarchy = new map<id,list<Opportunity>>();
            for(Opportunity myOpp : [select id,Lookup_hiearchyname__c from Opportunity where Lookup_hiearchyname__c=:oldMap.keyset()]){
                        if(mapOfProductHierarchy.containsKey(myOpp.Lookup_hiearchyname__c))
                                mapOfProductHierarchy.get(myOpp.Lookup_hiearchyname__c).add(myOpp);
                        else
                                mapOfProductHierarchy.put(myOpp.Lookup_hiearchyname__c,new list<Opportunity>{myOpp});
            }

            if(!mapOfProductHierarchy.isEmpty()) {
                     for(Product_Hiearchy_Object__c pHiearchy : oldMap.values()){
                       if( (mapOfProductHierarchy.containskey(pHiearchy.id)) ) {
                          pHiearchy.adderror(Label.ValidateProductHierarchyOnDelete);
                       }
                     }
            }
        }
    }

}