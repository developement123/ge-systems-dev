/******************************************************************************************************************************************
    Author   : Jhansi Bondalapu
    Date     : 29/october/2015
    Action   : This is the Webservice class 'CloneOppWithProducts' which creates 
               a new cloned Opportunity with same cloned Associated Products and 
               Schedules.  

   Modified By      Date                Description
   Abhishek         March, 31           To resolve the issue with button "CLone product" - Jira 542
   Abhishek         April, 25           Jira 542 reopened again due to game film validation was throwing error for "no award" stage. To overcome this, we used a label where system admin can enter the stage. We used this approach to make code more dynamic. Also there was some improvement in javascript("clone with products") to show proper message in case of exception.
   Abhishek         November, 2016       Added auraenabled on method "Schedules_Webservice" and created 1 more method "getOpportunity" for lightening action "Clone_With_Products" - US42014
   Abhishek        03/08/2017     "Pursuit" replaced with "2 - Strategy/Proposal Generation"
   Abhishek         June, 2017          added GE_X_Sell__Opportunity_External_ID__c for lead sharing issue    
******************************************************************************************************************************************/

global with sharing class CloneOppWithProducts {

    /***********************************************************************************************************
        Method 1 :        
            following method 'Schedules_Webservice' creates a new cloned Opportunity with same cloned
             Associated Products and Schedules. 
    ************************************************************************************************************/
    @AuraEnabled
    webService static Id Schedules_Webservice(Id OppId) {     
        
        List<Opportunity> listOfOldOppy = new List<Opportunity>();
        List<Opportunity_Line_Item__c> listOfOldOli = new List<Opportunity_Line_Item__c>();
        List<Schedule__c> listOfOldSchedules = new List<Schedule__c>();
        List<Schedule__c> newSchedulesList = new List<Schedule__c>();
        List<Game_Film__c> ListOfOldGameFilm = new List<Game_Film__c>();
        List<Game_Film__c> ListOfNewGameFilm = new List<Game_Film__c>();
        Boolean isUpdateRequired = false;
        
        Map<id, Opportunity_Line_Item__c> mapOfNewAssProd = new Map<id, Opportunity_Line_Item__c>();      // this map will contain old Associated products id's  and  new Associated products records.
        
        String theQuery = 'select ';   
        theQuery += fetchFieldsApiNames('Opportunity');        
        theQuery += '(select ' + fetchFieldsApiNames('Opportunity_Line_Item__c');        
        theQuery = theQuery.subString(0, theQuery.length()-1);        
        theQuery += ' from Opportunity_Line_Items__r), ';        
        theQuery += '(select ' + fetchFieldsApiNames('Schedule__c');        
        theQuery = theQuery.subString(0, theQuery.length()-1);        
        theQuery += ' from Schedules__r), ';
        theQuery += '(select ' + fetchFieldsApiNames('Game_Film__c');
        theQuery = theQuery.subString(0, theQuery.length()-1);
        theQuery += ' from Game_Film__r) ';
        theQuery += ' from Opportunity where Id = \'' + OppId +'\'';
        
        listOfOldOppy = Database.query(theQuery);        
        listOfOldOli = listOfOldOppy[0].Opportunity_Line_Items__r;         
        listOfOldSchedules = listOfOldOppy[0].Schedules__r;   
        ListOfOldGameFilm = listOfOldOppy[0].Game_Film__r; 
        
        Opportunity oldOpp = listOfOldOppy[0];
        Opportunity newOppy = oldOpp.clone(false);
        
        
        try {
            newOppy.Name = 'Enter Free Text';
            newOppy.GE_X_Sell__Opportunity_External_ID__c = null;
            newOppy.ContractID1__c = null;
            // we used label that contains stages for which Game film validation required. If such stage found, we create opportunity with stage "2 - Strategy/Proposal Generation"(Just to avoid validation error) and after insertion of game film , we will update the opportunity with original stage(oldOpp.stagename). 
            if(system.label.Stage_Game_Film_Validation.containsIgnoreCase(oldOpp.stagename)){
                newOppy.StageName = label.OpportunityStage2; // added to avoid error, 2= Strategy/Proposal Generation
                newOppy.GE_Standard_Probability__c = label.GEStandardProb2;
                isUpdateRequired = true;
               }
               
            insert newOppy;
                
                // Added this code to create Game film if Original Opp has game film. Also creting new opp with stage '2 - Strategy/Proposal Generation' and updating latter.
                if(!ListOfOldGameFilm.isEmpty()) {
                        for(Game_Film__c gameFilm : ListOfOldGameFilm) {
                            Game_Film__c newGameFilm = gameFilm.clone(false);
                            newGameFilm.Opportunity__c = newOppy.Id;
                            ListOfNewGameFilm.add(newGameFilm);
                        }
                        
                        try {
                            insert ListOfNewGameFilm ;    //inserting new game film.
                        }            
                        catch(Exception ex) {
                            System.debug(' Following exception occured while inserting Associated Product in opportunity in method: Schedules_Webservice and class: CloneOppWithProducts ' + ex.getMessage());
                        }
                   }
                If(isUpdateRequired){
                    newOppy.StageName = oldOpp.stagename;
                    newOppy.GE_Standard_Probability__c = oldOpp.GE_Standard_Probability__c;
                    update newOppy;
                }  
            
           
        }
        catch(Exception ex) {
            System.debug(' Following exception occured while inserting/updating new opportunity in method: Schedules_Webservice and class: CloneOppWithProducts ' + ex.getMessage());
        }
        
        if(!listOfOldOli.isEmpty()) {
            for(Opportunity_Line_Item__c assPro : listOfOldOli) {
                Opportunity_Line_Item__c newAssProd = assPro.clone(false);
                newAssProd.Opportunity__c = newOppy.Id;
                mapOfNewAssProd.put(assPro.id, newAssProd);
            }
            
            try {
                insert mapOfNewAssProd.values();     //inserting new Associated Product records.
            }            
            catch(Exception ex) {
                System.debug(' Following exception occured while inserting Associated Product in opportunity in method: Schedules_Webservice and class: CloneOppWithProducts ' + ex.getMessage());
            }
        }
        
     
        return newOppy.Id;
    }
    
    public static String fetchFieldsApiNames(String SobjectApiName) {
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String queryString = '';
        for(String fieldName : fieldMap.keyset()) {
            queryString += fieldName + ',';    
        }        
        return queryString;
    }
    // Being used in lightening action "Clone with products"
    @AuraEnabled
    public static Opportunity getOpportunity(Id id) {
        Opportunity opportunity = [
                SELECT Id,  Name, StageName, 
                       Count_Game_Films__c, Growth__c
            FROM Opportunity
            WHERE Id = :id
         ];

        // Perform isAccessible() check here 
        return opportunity;
    }
}