/***************************************************************************************************************************************************
    Author : Mohan Kumar
    Date    : 16 July 2015    
    Note    : This is the handler class for trigger 'TriggerOnIdeation' and will:
                 tokenize Ideation records when 'Civil_Military__c' value is Military and 
                 Site__c is Cheltenham/Dowty/Southampton/Hamble/UKLG.
                  
***************************************************************************************************************************************************/

public with sharing class IdeationTriggerHandler {
    public void IdeationHandler_method(List<Ideation__c> newList, 
                                       Map<id, Ideation__c> oldMap, 
                                       Map<id, Ideation__c> newMap, 
                                       String eventType, 
                                       String triggerType) {
        
        if(eventType == 'Before' && triggerType == 'Insert') {
            for(Ideation__c idea : newList) {
                if(idea.Civil_Military__c == 'Military' && 
                   (idea.Site__c == 'Cheltenham' 
                    || idea.Site__c == 'Dowty' 
                    || idea.Site__c == 'Southampton' 
                    || idea.Site__c == 'Hamble'
                    || idea.Site__c == 'UKLG')) 
                    idea.Is_Tokenized__c = true;        
            }
        } 
        
        if(eventType == 'Before' && triggerType == 'Update') {
            for(Ideation__c idea : newList) {
                if(oldMap.get(idea.Id).Civil_Military__c != newMap.get(idea.Id).Civil_Military__c 
                    || oldMap.get(idea.Id).Site__c != newMap.get(idea.Id).Site__c ) {
                        if(idea.Civil_Military__c == 'Military' && (idea.Site__c == 'Cheltenham' || idea.Site__c == 'Dowty' || idea.Site__c == 'Southampton' || idea.Site__c == 'Hamble' || idea.Site__c == 'UKLG')) 
                            idea.Is_Tokenized__c = true;    
                }
            }
        }           
    }
}