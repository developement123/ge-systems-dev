/*********************************************************************************************************************************************
    Author :   Mohan Kumar
    Date   :   25 Sept. 2015
    Action :   This class is used in vf page 'OpportunityProductLineItems_New' as extension.
                       This new version of Class has been created as a part of Opportunity Optimization Requirement.
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        02/18/2016      added parent and child account field in query at line number 60,63.Assigned     parent and child account to associated product
*********************************************************************************************************************************************/

public with sharing class OpportunityProductLineItemsExt_new {
    
    public Opportunity theOpp{get;set;}
    public Opportunity objOpp{get;set;}
    public String searchString{get;set;}
    public Opportunity_Line_Item__c[] shoppingCart{get;set;}        // this list will be used for inserting the oli.
    public List<wrapperProductList> AvailableFilterProducts{get;set;}
    public List<wrapperProductList> AvailableFilterProductsList{get;set;}
    public String toSelect{get;set;}
    public String toUnselect{get; set;}
    public Decimal Total{get;set;}
    public Boolean multipleCurrencies{get; set;}
    public Boolean forcePricebookSelection{get;set;}
    public String idOpp{get;set;}
    public string currencyCode{get;set;} 
    public string priceBookId{get;set;}
    public string TempStr{get;set;}
    public string valProductHierarchyLabel{get;set;}
    public boolean isTokenizeOpp{get;set;}
    public integer hasPageNumber{get;set;}
    public integer intTotalPages{get;set;}
    private Opportunity_Line_Item__c[] forDeletion = new Opportunity_Line_Item__c[]{};
    public String productNameDisplay{get;set;}
    public String pole{get;set;}
    String s;
    public String poNumber {get; set;}
    public List<Selectoption> poNumberList {get; set;}
    
    
    /*  following 4 varibales are added for OE/Service and Digital solution offering type. */
    public Opportunity_Line_Item__c newAssoProduct{get;set;}    
    public Boolean showOEServiceFields{get;set;}
    public Boolean showDigitalSolutions{get;set;}     
    public Boolean hideBottomSection{get;set;}
    
    /*follow variable used to set default values for 'Pipeline PO' Pole and Owner  */
    //public Account assAccount{get;set;}
    
    public String getString() {
        return s;
    }
            
    public void setString(String s) {
        this.s = s;
    }
    
    public OpportunityProductLineItemsExt_new(ApexPages.StandardController controller) {  
       forcePricebookSelection = false;
       hideBottomSection = false;
       valProductHierarchyLabel = Label.ProductHierarchyMultiple;
       oppProductVariation();
       
       multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
        
        if(multipleCurrencies) {
            theOpp = database.query('select Id, Is_Tokenized__c, Product_Group__c, Product_Area__c, Product_Family__c, Product_Family_Child__c, CurrencyIsoCode, Offering_Type__c, AccountId, Child_Account__c, OwnerId, GE_Standard_Probability__c, Opportunity_Id__c, CloseDate, Contract_Site__c from Opportunity where Id = \'' + controller.getRecord().Id + '\' limit 1');
            //assAccount = database.query('select Id, Pole__c, Name from Account where Id = \''+ theOpp.AccountId +'\'limit 1');
            
        }
        else {
            theOpp = [select Id, Is_Tokenized__c, Product_Group__c, Product_Area__c, Product_Family__c, Product_Family_Child__c, Offering_Type__c, AccountId, Child_Account__c, OwnerId, GE_Standard_Probability__c, Opportunity_Id__c, CloseDate, Contract_Site__c from Opportunity where Id = :controller.getRecord().Id limit 1];
            //assAccount = [select Id, Pole__c, Name from Account where Id = :theOpp.AccountId limit 1];
        }
        
        shoppingCart = new Opportunity_Line_Item__c[] {
        
        };
        poNumber = '01';
        poNumberList = new List<selectoption>();
        For(Integer i = 0; i < 100; i++){
            if(i==0){
                poNumberList.add(new selectoption('None','None'));
            }else if(i<10 && i != 0){
                poNumberList.add(new selectoption('0'+String.valueOf(i),'0'+String.valueOf(i)));
            }
            else {
                poNumberList.add(new selectoption(String.valueOf(i),String.valueOf(i)));
            }
        }  
             
        currencyCode = getChosenCurrency();
        isTokenizeOpp = theOpp.Is_Tokenized__c;
    }    
       
    public String getChosenCurrency() {    
        if(multipleCurrencies)
            return (String)theOpp.get('CurrencyIsoCode');
        else
            return '';
    }

    public void oppProductVariation() {
        idOpp = System.currentPageReference().getParameters().get('id');
        
        objOpp = [select Id, Name, Product_Group__c, Product_Area__c, Product_Family__c, Product_Family_Child__c, Pricebook2Id, PriceBook2.Name,Lookup_hiearchyname__c, Lookup_hiearchyname__r.Name, Lookup_hiearchyname__r.Product_Group__c,Lookup_hiearchyname__r.Product_Area__c,Lookup_hiearchyname__r.Product_Family__c,Lookup_hiearchyname__r.Product_Child__c
                    from Opportunity 
                    where Id = :idOpp 
                    Limit 1];
    }
    
    public void showAvailableProductList() {
        AvailableFilterProducts = new List<wrapperProductList>();
        AvailableFilterProductsList = new List<wrapperProductList>();
        
        if(TempStr != null && TempStr != '') {
            String[] eachProductRow = TempStr.split('//');
            if(eachProductRow.size() > 0) {
                for(integer i=0; i < eachProductRow.size(); i++) {
                    String[] eachProductColumn = eachProductRow[i].split(';'); 
                                       
                    wrapperProductList objCls = new wrapperProductList(eachProductColumn[0], 
                                                                       eachProductColumn[1], 
                                                                       eachProductColumn[2], 
                                                                       eachProductColumn[3], 
                                                                       eachProductColumn[4], 
                                                                       eachProductColumn[5], 
                                                                       eachProductColumn[6], 
                                                                       eachProductColumn[7], 
                                                                       eachProductColumn[8]);
                    AvailableFilterProducts.add(objCls);
                }
            }
        }
        
        if(!AvailableFilterProducts.isEmpty()) {
            intSelectedPageNum = 1;
            hasPageNumber = 1;
            PropertyListPagiNation();
            GetPropertyListForSelectedPageNumber();
        }
    }
    
    public class wrapperProductList{
        public string prdId {get;set;}
        public string prdName {get;set;}
        public string prdCode {get;set;}
        public string prdDescription {get;set;}
        public string prdArea {get;set;}
        public string prdFamily {get;set;}
        public string prdChild {get;set;}
        public string prdLeadTime {get;set;}
        public boolean prdTokenize {get;set;}
        
        public wrapperProductList(string p_Id, string p_Name, string p_Code, string p_Desc, string p_Area, string p_Family, string p_Child, string p_LeadTime, string p_Istoken){
            prdId           = p_Id;
            prdName         = p_Name;
            prdCode         = p_Code;
            prdDescription  = p_Desc;
            prdArea         = p_Area;
            prdFamily       = p_Family;
            prdChild        = p_Child;
            prdLeadTime     = p_LeadTime;
            prdTokenize     = boolean.valueOf(p_Istoken);
        }
    }
    
    public Id selectedProductId;
    
    /**********************************************************************************
        Following method is to set default values in Associated product and to set 
        Boolean variable T/F to render the corresponding sections. 
    ***********************************************************************************/ 
    
    public void addToShoppingCart1() {
        hideBottomSection = true;
        newAssoProduct = new Opportunity_Line_Item__c(Opportunity__c = theOpp.Id, Parent_Account__c = theOpp.AccountId, Child_Account__c = theOpp.Child_Account__c);
        
        
        if(theOpp.Offering_Type__c == 'OE / Services') {
            newAssoProduct.Execution_Site__c = theOpp.Contract_Site__c;
            newAssoProduct.Installment_Periods__c = 'Annually';
            newAssoProduct.Pipeline_PO_Status__c = 'Open';
            newAssoProduct.Pipeline_PO_Owner__c = theOpp.OwnerId;
            newAssoProduct.Pipeline_PO_Probability__c = theOpp.GE_Standard_Probability__c;
            showOEServiceFields = true;
        }
        else if(theOpp.Offering_Type__c == 'Digital Solutions') {            
            showDigitalSolutions = true;
            newAssoProduct.Execution_Site__c = 'Austin';
            newAssoProduct.Implementation_Length__c = 3; 
            newAssoProduct.Implementation_Amount__c = 0.00;
            newAssoProduct.Implementation_Lag__c = 0; 
            
            newAssoProduct.Services_Lag_months__c = 0;
            newAssoProduct.Services_Amount_Annual__c = 0.00;
            
            newAssoProduct.Unit_Price__c = 0;
            newAssoProduct.Quantity__c = 0;
        }      
        
        Set<Id> setIds = new Set<Id>();
        
        if(shoppingCart.size() > 0)  
            Apexpages.addMessage(new Apexpages.Message(ApexPages.severity.INFO, 'Only 1 Opportunity Line Item can be added at a time.'));
        else {
            for(wrapperProductList d : AvailableFilterProductsList) {                
                if((String)d.prdId == toSelect) {
                    setIds.add(d.prdId);                    
                    break;
                }
            }
        }  
        
        if(setIds.size() > 0) {
            for(Product2 p : [Select Id, Name, Family, IsActive, Description, Lead_Time__c,
                                ProductCode, Product_Area__c,
                                Product_Family__c, Product_Family_Child__c, 
                                CanUseQuantitySchedule, CanUseRevenueSchedule
                                from Product2
                                where Id IN: setIds]) {
                selectedProductId = p.Id;
                productNameDisplay = p.Name;
            }
        }
    }

    public PageReference removeFromShoppingCart() {    
        Integer count = 0;
    
        for(Opportunity_Line_Item__c d : shoppingCart) {
            if(d.Product__c != null)
                forDeletion.add(d);
        
            shoppingCart.remove(count);
            break;
       
            count++;    
        }
        return null;
    }
    
    public PageReference onSave() {        
        try {
            if(shoppingCart.size() > 0)
                upsert(shoppingCart);
        }
        catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    public PageReference onCancel() {        
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }  
    
    public integer intSelectedPageNum;
    public integer intItemsPerPage = 20;
    public integer intRemainingItemsInLastPage;
    
    public boolean hasPrevious{get;set;}
    public boolean hasNext{get;set;}
    
    public void PropertyListPagiNation() {   
        if(AvailableFilterProducts.size() >= intItemsPerPage ) {
            intTotalPages = AvailableFilterProducts.size() / intItemsPerPage;
            
            intRemainingItemsInLastPage = AvailableFilterProducts.size() - (intTotalPages * intItemsPerPage);
            if(AvailableFilterProducts.size() >= intItemsPerPage) {
                if(intRemainingItemsInLastPage > 0) 
                    intTotalPages += 1;                
            }
        } 
        else {
            intTotalPages = 1;
            intRemainingItemsInLastPage = AvailableFilterProducts.size();
        }
    }    
    
    public void GetPropertyListForSelectedPageNumber() {   
        integer intStartIndex = intItemsPerPage * (intSelectedPageNum - 1);
        integer intEndIndex = (intItemsPerPage * intSelectedPageNum) -1;
        
        if(intEndIndex >= AvailableFilterProducts.size()) 
            intEndIndex = intStartIndex + intRemainingItemsInLastPage -1;        
        
        if(intEndIndex < AvailableFilterProducts.size()-1)
            hasNext = true;
        else
            hasNext = false;
            
        if(intStartIndex <= 0)
            hasPrevious = false;
        else
            hasPrevious = true;
            
        AvailableFilterProductsList = new List<wrapperProductList>();
        
        for(integer i = intStartIndex; i <= intEndIndex ; i++) 
            AvailableFilterProductsList.add(AvailableFilterProducts[i]);
    }
    
    public void GetFirst() {
        intSelectedPageNum = 1;
        GetPropertyListForSelectedPageNumber();
    }
    
    public void GetLast() {
        intSelectedPageNum = intTotalPages;
        GetPropertyListForSelectedPageNumber();
    }
    
    public void GetPrevious() {
        intSelectedPageNum -= 1;
        GetPropertyListForSelectedPageNumber();
    }
   
    public void GetNext() {
        intSelectedPageNum += 1;
        GetPropertyListForSelectedPageNumber();
    }
    
    public void RedirectOnPage() {
        intSelectedPageNum = hasPageNumber;
        GetPropertyListForSelectedPageNumber();
    }
    
    // following method invokes when Save is clicked on Associated product.
    public pagereference customSave() {
        try {
            if(poNumber == 'None'){
                newAssoProduct.Pipeline_PO_Group__c = null;
            } else{
                newAssoProduct.Pipeline_PO_Group__c = 'P'+theOpp.Opportunity_Id__c+'-'+poNumber;
            } 
            newAssoProduct.Product__c = selectedProductId;
            insert newAssoProduct;
            
            pagereference pr = new pagereference('/' + theOpp.Id); 
            pr.setRedirect(true);
            return pr; 
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
        }
        return null;
    }
}