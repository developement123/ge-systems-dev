/*************************************************************************************************************************************************
    This test class was created to provide code coverage for Trigger "TriggerOnAssociatedProduct" on 
    AssociatedProduct and handler class "AssociatedProductTriggerHandler"
    Version     Written by                last modified Date              
  --------------------------------------------------------------------------------------------------------------
    1.0         Jhansi                         12/29/2015    
                Abhishek                       04/18/2016      JIRA 511 
                Abhishek                       07/19/2016      Custom contract changes
                Abhishek                       09/18/2017      removed account platform 
*************************************************************************************************************************************************/
@isTest
public class Tracker_TriggerOnAssociatedProduct{

   public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }
    
    private static testmethod void testAssociatedProdcut(){
        
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        
         Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
         User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
         localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London',username ='neha.mittal12@ge.com.sys.dev');
         
          
         insert u;
        System.runAs(u) {
         Account acc = new Account(Name = 'GE GEAS');
         insert acc;
         
         Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
         insert pm;  
                
         Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Digital Solutions', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
         insert hierarchy;

         Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
         insert p2;
         
         opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='Digital Solution',Margin_Escalator__c= true,Price_Escalation_Start_Date__c = system.today()-1);
         insert opp;
         
         opportunity opp1 = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='Digital Solutions',Margin_Escalator__c= true,CY_Q1_Revenue__c = 100,CY_Q2_Revenue__c = 100,CY_Q3_Revenue__c = 100,CY_Q4_Revenue__c = 100,Price_Escalation_Start_Date__c = system.today()-1);
         insert opp1;
        test.startTest(); 
         Opportunity_Line_Item__c oli1 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='test', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'test');
         insert oli1;
         
         Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp1.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='test', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Bi-Annually',Schedule_Start_Date__c =system.today(),Revenue_Stream_child_level__c = 'test');
         insert oli;
         
         Margin_Escalator__c  MagEsc =  new Margin_Escalator__c(Year__c = '2013',Out_of_Sync__c = true,Opportunity__c  = opp1.id );
         insert MagEsc;
         
         List<Schedule__c> Sch_List = new List<Schedule__c>{};
            Sch_List.add(new Schedule__c(Opportunity__c = opp1.id,Quantity__c = 5,  Opportunity_Line_Item__c = oli.id,Schedule_Date__c= system.today(),Escalated_Revenue__c = 100));

            insert Sch_List;
            
           //delete oli;
         test.StopTest() ;  
      
        }
    
    }
    
    
    private static testmethod void testAssociatedProdcut2(){
     
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        //Class_TriggerCheck.var_update_ST_onSplitupdateTrg
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false; 
        Class_TriggerCheck.var_triggerOnUser = false;
        
         Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
         User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
         localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London',username ='neha.mittal12@ge.com.sys.dev');
         insert u;
        System.runAs(u) {
         Account acc = new Account(Name = 'GE GEAS');
         insert acc;
         
         Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
         insert pm;  
                
         Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Digital Solution', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
         insert hierarchy;

         Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
         insert p2;
         
         opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='Digital Solution',CY_Q1_Revenue__c = 111,CY_Q2_Revenue__c = 111,CY_Q3_Revenue__c = 111,CY_Q4_Revenue__c = 111,Price_Escalation_Start_Date__c = system.today()-1);
         insert opp;
     
         List<Opportunity_Line_Item__c> newOlis = new List<Opportunity_Line_Item__c>();
         
         Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D');
         //insert oli;
         newOlis.add(oli);

         Opportunity_Line_Item__c oli1 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'New Product Development');
         //insert oli1;
         newOlis.add(oli1);
         
         Opportunity_Line_Item__c oli2 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'ECP/ECR');        
         //insert oli2;
         newOlis.add(oli2);
         
         Opportunity_Line_Item__c oli21 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded Development of  Retrofits,Modifications & Upgrades');      
         //insert oli21;
         newOlis.add(oli21);
         
         Opportunity_Line_Item__c oli22 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'License');        
         //insert oli22;
         newOlis.add(oli22);
         
         Opportunity_Line_Item__c oli23 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Deferred Engineering');       
         //insert oli23;
         newOlis.add(oli23);
         
         opp.Deal_Type__c = 'Momentum with NRE';
         update opp;
         
         
         Opportunity_Line_Item__c oli3 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Hardware/OE', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'New Product Development');
        // insert oli3;
         newOlis.add(oli3);
         
         Opportunity_Line_Item__c oli4 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Hardware/OE', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Momentum Production');
         //insert oli4;
         newOlis.add(oli4);
         
         
         Opportunity_Line_Item__c oli5 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Services', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Momentum Production');
         //insert oli5;
         newOlis.add(oli5);
         
         
         Opportunity_Line_Item__c oli6 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Royalties', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Momentum Production');
         //insert oli6;
         newOlis.add(oli6);
         
         insert newOlis;
         
          List<Schedule__c> Sch_List = new List<Schedule__c>{};
          for(integer i=0;i<12;i++){
          Sch_List.add(new Schedule__c(Opportunity__c = opp.id, Opportunity_Line_Item__c = newOlis.get(9).Id,Schedule_Date__c= system.today().addMonths(i),No_Actual_Revenue__c = true,Escalated_Revenue__c = 100));
          }
         insert Sch_List;
         Sch_List[0].Escalated_Revenue__c = 200;
         update Sch_List;
         
         newOlis[8].Declined__c = true;
         newOlis[8].Decline_Reason__c ='Test';
         update newOlis;
         
         delete newOlis.get(8);
        }
    
    }
    
    private static testmethod void testGenerateSchedulesOEServices(){
     
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        //Class_TriggerCheck.var_update_ST_onSplitupdateTrg
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false; 
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnContract = false;
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        
         Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
         User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
         localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London',username ='neha.mittal12@ge.com.sys.dev');
         insert u;
        System.runAs(u) {
         Account acc = new Account(Name = 'GE GEAS');
         insert acc;
         
         Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India');
         
         Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
         insert pm;  
                
         Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test12345', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
         insert hierarchy;

         Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
         insert p2;
         
         opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Offering_Type__c = 'OE / Services', Price_Escalation_COLA__c = 2,Price_Escalation_Start_Date__c = System.Today().addMonths(5));
         insert opp;
         test.StartTest();
             List<Opportunity_Line_Item__c> newOlis = new List<Opportunity_Line_Item__c>();
             Date lastSchedule = Date.newInstance(System.Today().Year(), 1, 5).addMonths(6);
             Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
             Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Last_Ship_Date__c=lastSchedule);
             insert oli;
             Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
             List<Schedule__c> lstOfSch = new List<Schedule__c>();
            for(Integer i=0;i<=5;i++)
            {
              Schedule__c sc = new Schedule__c();
              sc.Opportunity_Line_Item__c = oli.id;
              sc.Schedule_Date__c  = mydate.addMonths(i);
              sc.Quantity__c = 1;
              sc.Escalated_Revenue__c = 3;
              sc.Opportunity__c = opp.ID;
              lstOfSch.add(sc);
            } 
            insert lstOfSch;
            
             
             Contract_PO__c ContrAnnual = new Contract_PO__c (Account_Name__c = acc.id, Status__c = 'Draft' , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
             , Legal__c = con.ID, Program_Manager__c = u.Id, Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id, Pipeline_Opportunity__c = opp.id, Number_of_PO_s__c = 50.0
             , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual'
             , First_PO_Ship_Date__c  = system.today(), Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false  );
             
             insert ContrAnnual;
             
             opportunity poForecastOpp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                                 , CloseDate = date.today()
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc.id
                                , recordtypeId = findRecordtypeId('Opportunity', 'POForecast')
                                , ContractID1__c=ContrAnnual.Id
                                , Offering_Type__c = 'OE / Services');
               Insert poForecastOpp  ;
               
               Opportunity_Line_Item__c pOoli = new Opportunity_Line_Item__c(Opportunity__c = poForecastOpp.id, Product__c = p2.id, 
             Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =2,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Pipeline_Associated_Product__c = oli.id);
             newOlis.add(pOoli);
             
             Opportunity_Line_Item__c BiAnnualoli = new Opportunity_Line_Item__c(Opportunity__c = poForecastOpp.id, Product__c = p2.id, 
             Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =2,Installment_Periods__c ='Bi-Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Pipeline_Associated_Product__c = oli.id);
             newOlis.add(BiAnnualoli);
             
             Opportunity_Line_Item__c Quarterlyoli = new Opportunity_Line_Item__c(Opportunity__c = poForecastOpp.id, Product__c = p2.id, 
             Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =2,Installment_Periods__c ='Quarterly',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Pipeline_Associated_Product__c = oli.id);
             newOlis.add(Quarterlyoli);
             
             Opportunity_Line_Item__c Monthlyoli = new Opportunity_Line_Item__c(Opportunity__c = poForecastOpp.id, Product__c = p2.id, 
             Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =2,Installment_Periods__c ='Monthly',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Pipeline_Associated_Product__c = oli.id);
             newOlis.add(Monthlyoli);
             
             Opportunity_Line_Item__c Weeklyoli = new Opportunity_Line_Item__c(Opportunity__c = poForecastOpp.id, Product__c = p2.id, 
             Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =2,Installment_Periods__c ='Weekly',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Pipeline_Associated_Product__c = oli.id);
             newOlis.add(Weeklyoli);
             
             Opportunity_Line_Item__c Dailyoli = new Opportunity_Line_Item__c(Opportunity__c = poForecastOpp.id, Product__c = p2.id, 
             Parent_Account__c = acc.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =2,Installment_Periods__c ='Daily',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Pipeline_Associated_Product__c = oli.id);
             newOlis.add(Dailyoli);
             
             insert newOlis;
             
         test.StopTest();
         
        }
    
    }
    
     private static testmethod void testRebuildInitialRenewalSchedule(){
        
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_TriggerOnContract = false ;
        Class_TriggerCheck.var_TriggerOnSchedule = false ;
        Class_TriggerCheck.var_triggerOnUser = false;
        
         Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];
    
         User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
         localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London',username ='neha.mittal12@ge.com.sys.dev');
         
          
         insert u;
        System.runAs(u) {
         Account acc = new Account(Name = 'GE GEAS');
         insert acc;
         
         Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India');
                            
         Platform_Master__c pm = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports');
         insert pm;  

                
         Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Digital Solutions', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child');
         insert hierarchy;

         Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy.id,Name='Test Product',IsActive=true);
         insert p2;
         
         date priceEsclationStart = system.today().addmonths(4);
         opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 36,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='Digital Solutions',Margin_Escalator__c= true,Price_Escalation_Start_Date__c = priceEsclationStart,
          Price_Escalation_COLA__c = 10);
         insert opp;
         
         Opportunity_Line_Item__c oli1 = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
         Probability__c = 111,Platform__c = pm.id,Parent_Account__c = acc.id,Revenue_Stream_top_level__c='test', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=100,of_Installments__c =20,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'test',
         Implementation_Length__c = 3);
         insert oli1;
        
         List<Schedule__c> Sch_List = new List<Schedule__c>{};
            Sch_List.add(new Schedule__c(Opportunity__c = opp.id,Quantity__c = 5,  Opportunity_Line_Item__c = oli1.id,Schedule_Date__c= system.today(),Escalated_Revenue__c = 100));

         insert Sch_List;
         
         
          Contract_PO__c ContrAnnual = new Contract_PO__c (Account_Name__c = acc.id, Status__c = 'Draft'
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today().addMonths(36)
                                       , Legal__c = con.ID, Program_Manager__c = u.Id
                                       , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
                                       , Pipeline_Opportunity__c = opp.id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual'
                                       , First_PO_Ship_Date__c  = system.today()
                                       , Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false 
                                       , Initial_Term_End_Date__c = system.today().addMonths(24)
                                       , Price_Escalation_COLA__c = 10
                                       , Price_Escalation_Start_Date__c = system.today()
                                       , RecordTypeID = findRecordtypeId('Contract_PO__c', 'Digital_Solutions')
                                       );
         insert ContrAnnual;
         
         opp.ContractID1__c = ContrAnnual.id;
         Update opp;
         
         RecordType InitialRecordType = [select id from recordType where Name =:Label.Label_InitialOpportunity];
            opportunity oppInitial = new opportunity(Name = 'Test opprenewal', StageName = label.OpportunityStage4
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc.id, Offering_Type__c = 'Digital Solutions'
                             , RecordTypeID =InitialRecordType.Id, ContractID1__c  = ContrAnnual.id
                             , Price_Escalation_COLA__c = 10, Price_Escalation_Start_Date__c = priceEsclationStart, Contract_Term__c = 12);
            insert oppInitial;
         
         Opportunity_Line_Item__c oliInitial = new Opportunity_Line_Item__c(Opportunity__c =  oppInitial.Id
                    , Quantity__c = 1, Unit_Price__c = 100, Probability__c = 50, Product__c = p2.id
                    , Installment_Periods__c = 'Annually', Implementation_Length__c = 4) ;       
            insert oliInitial;
            
            Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
            Date mydate1 = Date.newInstance(System.Today().Year(), 10, 5);
            
            List<Schedule__c> lstOfSch = new List<Schedule__c>();
            for(Integer i=0;i<=49;i++)
            {
              Schedule__c sc = new Schedule__c();
              sc.Opportunity_Line_Item__c = oliInitial.id;
              sc.Schedule_Date__c  = mydate.addMonths(i);
              sc.Quantity__c = 1;
              sc.Escalated_Revenue__c = 3;
              sc.Opportunity__c = oppInitial.ID;
              lstOfSch.add(sc);
            } 
            insert lstOfSch;
         
        test.startTest(); 
         
         List<Opportunity_Line_Item__c> oliTOUpdate = new List<Opportunity_Line_Item__c>();
         oliInitial.Implementation_Length__c = 5;
         oliTOUpdate.add(oliInitial);
         oli1.Implementation_Length__c = 2;
         oliTOUpdate.add(oli1);
         update oliTOUpdate;
        
         Delete oliInitial;
         
         test.StopTest() ;  
      
        }
    
    }
    
    private static testmethod void testTokenizeProduct(){
     
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false; 
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_StopProductHiearchy = false;
        Class_TriggerCheck.var_TriggerOnSchedule = false;
        Class_TriggerCheck.var_TriggerOnMarginEscalator = false;
         Profile[] pro = [select Id from Profile where Name = 'Systems Integration User' limit 1];
    
         User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
         localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London',username ='neha.mittal12@ge.com.sys.dev');
         insert u;
        System.runAs(u) {
         Account acc = new Account(Name = 'GE GEAS');
         insert acc;
         
         list<Platform_Master__c> PMs = new list<Platform_Master__c>();
         Platform_Master__c pmUK = new Platform_Master__c(Name='170', Civil_Military__c='Military', Segment__c = 'Regional Transports', Is_Tokenized__c = true);
         PMs.add(pmUK);
         Platform_Master__c pmNONUK = new Platform_Master__c(Name='180', Civil_Military__c='Civil', Segment__c = 'Regional Transports');
         PMs.add(pmNONUK);
         
         insert PMs;
         
         
         list<Product_Hiearchy_Object__c> PHOs = new list<Product_Hiearchy_Object__c>();
         
         Product_Hiearchy_Object__c hierarchyUK = new Product_Hiearchy_Object__c(Name='Test hierarchy', Product_Group__c='Test group', Product_Area__c='Test area',Product_Family__c='Test family', Product_Child__c='Test child', Is_Tokenized__c = true);
         PHOs.add(hierarchyUK);
         Product_Hiearchy_Object__c hierarchyNONUK = new Product_Hiearchy_Object__c(Name='hierarchyNONUK', Product_Group__c='Test NONUKgroup', Product_Area__c='Test NONUKarea',Product_Family__c='Test NONUKfamily', Product_Child__c='Test NONUKchild');
         PHOs.add(hierarchyNONUK);
         
         Insert PHOs;

         Product2 p2 = new Product2(Lookup_hiearchyname__c=PHOs[0].id,Name='Test Product',IsActive=true);
         insert p2;
         
         Product2 p3 = new Product2(Lookup_hiearchyname__c=PHOs[1].id,Name='Test NONUKProduct',IsActive=true);
         insert p3;
         
         opportunity opp = new opportunity(Name = 'Test UK opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = PMs[0].id, accountId= acc.id , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = PHOs[0].id, Offering_Type__c = 'OE / Services', Price_Escalation_COLA__c = 5,Price_Escalation_Start_Date__c = System.Today(), Is_Tokenized__c = true);
         insert opp;
         
         opportunity oppNONUK = new opportunity(Name = 'Test UK opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = PMs[1].id, accountId= acc.id , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = PHOs[1].id, Offering_Type__c = 'OE / Services', Price_Escalation_COLA__c = 5,Price_Escalation_Start_Date__c = System.Today());
         insert oppNONUK;
         
         List<Opportunity_Line_Item__c> newOlis = new List<Opportunity_Line_Item__c>();
             
             Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, Parent_Account__c = acc.id, Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test', Quantity__c = 5, Unit_Price__c=1, of_Installments__c =111, Installment_Periods__c ='Annually', Schedule_Start_Date__c = system.today(), Revenue_Stream_child_level__c = 'Funded R&D', Is_Tokenized__c = true);
             
             newOlis.add(oli);
             
             Opportunity_Line_Item__c oliNONUK = new Opportunity_Line_Item__c(Opportunity__c = oppNONUK.id, Product__c = p3.id, Parent_Account__c = acc.id, Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test', Quantity__c = 5, Unit_Price__c=1, of_Installments__c =111, Installment_Periods__c ='Annually', Schedule_Start_Date__c = system.today(), Revenue_Stream_child_level__c = 'Funded R&D');
             newOlis.add(oliNONUK);
             insert newOlis;
             Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
             List<Schedule__c> lstOfSch = new List<Schedule__c>();
            for(Integer i=0;i<=49;i++)
            {
              Schedule__c sc = new Schedule__c();
              sc.Opportunity_Line_Item__c = oliNONUK.id;
              sc.Schedule_Date__c  = mydate.addMonths(i);
              sc.Quantity__c = 1;
              sc.Escalated_Revenue__c = 3;
              sc.Opportunity__c = opp.ID;
              lstOfSch.add(sc);
            } 
            insert lstOfSch;
            
         test.StartTest();
             
            
             oliNONUK.Opportunity__c = opp.id;
             oliNONUK.Is_Tokenized__c = true;
             update oliNONUK;
             
         test.StopTest();
         
        }
    
    }
    
}