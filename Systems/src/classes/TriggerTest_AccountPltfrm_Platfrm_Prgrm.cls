/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - Trg_Update_Account_Platform_CivilMilitary

    Version      Last modified By             Date                  Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
      1.0          Mohan kumar              12/05/2015           Initial Creation
    
                
*************************************************************************************************************************************************/      


@istest
private class TriggerTest_AccountPltfrm_Platfrm_Prgrm {

    static testMethod void myTest() {
        
        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u1 = new User(alias = 'skapoor1', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor1', languagelocalekey = 'en_US',
                                localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal1@ge.com.sys.dev');
        insert u1;
        
        List<Account> listOfAcc = new List<Account>();
        
        Account acc = new Account(Name = 'Testing Account');
        listOfAcc.add(acc);     
        Account acc1 = new Account(Name = 'Testing Account 1');
        listOfAcc.add(acc1);
        
        insert listOfAcc;
        
        System.runAs(u1) {
            Platform_Master__c plat = new Platform_Master__c(Name = 'P.Master', Civil_Military__c = 'Military', Segment__c = 'Test segment');
            insert plat;
                  
            Forecast__c fore = new Forecast__c(Airframer__c = acc1.id, Forecast_Platform__c = plat.id);
            insert fore;
               
            Test.startTest();            
                plat.Civil_Military__c = 'Civil';
                update plat;        
            Test.stopTest();    
        }   
    }
}