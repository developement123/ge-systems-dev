// Modified by Abhishek on March,2nd 2016  for JIRA 498
// Modified by Abhishek on Sep,18 2017 : removed account platform
@istest
public class program_test
{   
    static testmethod void mytest()
    {
        Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
        
        User u =new User( alias = 'skapo12', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                localesidkey = 'en_US',profileid=pro[0].id,country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
                insert u;
                
        
                
        ACCOUNT acc1 = new ACCOUNT(NAME='Hello');
        insert acc1;
        
        ACCOUNT acc2 = new ACCOUNT(NAME='Bye');
        insert acc2;
        
        system.runAs(u){
       
        Platform_Master__c pm=new Platform_Master__c(Name='170',Civil_Military__c='Military',
        Segment__c='Regional Transports');
        insert pm;
        
        
        u.Legal_Entity__c = 'GE Aviations Systems';
        update u ;
       
        
        Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
        insert hierarchy;
        
       /*deployment Program__c prog=new Program__c(Name='Test prog',Product_Group__c='Avionics',
        Product_Area__c='Mission Systems',Product__c='Avionics',
        Product_Family_Child__c='Autoflight',Product_Sales_Owner__c=u.id,
        Program_Probability__c='100% - Funded');
       deployment*/ 
        Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
        Program_Probability__c='100% - Funded');
        insert prog;
        // Added  Account_Platform__c for JIRA 498      
        opportunity opp=new opportunity(Name='Test opp',StageName=label.OpportunityStage1,CloseDate=date.today(),
        Program__c=prog.id,Amount =1000,Firm_Anticipated__c='Anticipated',
        GE_Standard_Probability__c=label.GEStandardProb3,Lookup_hiearchyname__c = hierarchy.id, Platform_New__c = pm.id, accountId= acc1.id);
        
      /*deployment  opp.Product_Group__c=prog.Product_Group__c;
        opp.Product_Area__c=prog.Product_Area__c;
        opp.Product_Family_Child__c=prog.Product_Family_Child__c;
        opp.Product_Family__c=prog.Product__c;       
        deployment*/
        insert opp;
        
        // Added  Account_Platform__c for JIRA 498
        opportunity opp2=new opportunity(Name='Test opp2',StageName=label.OpportunityStage1,CloseDate=date.today(),
        Program__c=prog.id,Amount =500,Firm_Anticipated__c='Anticipated',
        GE_Standard_Probability__c=label.GEStandardProb3,Lookup_hiearchyname__c = hierarchy.id, Platform_New__c = pm.id, accountId= acc1.id);
        
        
      /*deployment  opp2.Product_Group__c=prog.Product_Group__c;
        opp2.Product_Area__c=prog.Product_Area__c;
        opp2.Product_Family_Child__c=prog.Product_Family_Child__c;
        opp2.Product_Family__c=prog.Product__c; 
        deployment*/
        insert opp2;
        
     /*deployment   system.assertEquals(opp2.Product_Area__c,'Mission Systems');
        system.assertEquals(opp2.Product_Family_Child__c,'Autoflight');
        system.assertEquals(opp2.Product_Family__c,'Avionics');
        system.assertEquals(opp2.Product_Group__c,'Avionics');
        
             
        System.Debug('Product Test Area : '+ opp.Product_Area__c);
        system.assertEquals(opp.Product_Area__c,'Mission Systems');
        system.assertEquals(opp.Product_Family_Child__c,'Autoflight');
        system.assertEquals(opp.Product_Family__c,'Avionics');
        system.assertEquals(opp.Product_Group__c,'Avionics');
        deployment*/
        delete opp2;
        }
    }
         
    static testmethod void mytest1(){
    
    Profile[] pro=[SELECT ID FROM PROFILE WHERE NAME='System Administrator' limit 1];
        
        User u =new User( alias = 'skapo12', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
                localesidkey = 'en_US',profileid=pro[0].id,country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
                insert u;
        
        system.runAs(u){
            
            Platform_Master__c plat1=new Platform_Master__c(Name='birlasoft',Civil_Military__c='Military',Segment__c='Test segment');
            insert plat1;
         
            Account acc = new Account(Name='Testing Account');
            insert acc; 
            
            u.Legal_Entity__c = 'GE AVIATION SYSTEMS LIMITED';
            try{
            update u;
            }catch(exception ex){
                system.debug('=========='); 
            }
            
        }
    }   
         
             
  }