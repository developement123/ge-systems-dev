public with sharing class AgreementDelete {
    
    public ID qid;
    private Apttus__APTS_Agreement__c ObjAgr;
    public list<Apttus__APTS_Agreement__c> lstAgr;
    public AgreementDelete(ApexPages.StandardController controller) {
    qid= apexpages.currentpage().getparameters().get('id');        
    lstAgr = new list<Apttus__APTS_Agreement__c>();
    }
    
    public pagereference doCancel(){
    pagereference pgc = new pagereference ('/'+qid);
    pgc.setredirect(true);
    return pgc;
    }
    
    public pagereference doDelete()
    {
    ObjAgr = [select id, name from Apttus__APTS_Agreement__c where id = : qid];
    lstAgr.add (ObjAgr );
    delete lstAgr;
    pagereference pg = new pagereference ('/a0e');
    pg.setredirect(true);
    return pg;
    }
  }