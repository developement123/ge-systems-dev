/*************************************************************************************************************************************************
    This test class has been modified to provide code coverage for the following new Batch class and modified Scheduler class:
    - createME_new and Schedule_createME

    Version     Author(s)             Date                 Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          12/03/2014        Initial Creation
 
    Last modified by : Mohan Kumar(13 Jan 2015) for JIRA 364.   
    Last modified by : Mohan Kumar(14 April 2015).
    Last modified by : Abhishek(24 Dec 2015).
    Last modified by : Abhishek(18 sep 2017) : removed account platform 
*************************************************************************************************************************************************/      
      
@istest(seeAllData = false)
private class Tracker_Schedule_createME {

    static testmethod void test_Schedule_createME() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;  
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;
 
        
        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                  Amount = 500, Firm_Anticipated__c = 'Anticipated',
                  GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,
                  Contract_Award_Date__c = date.today(),Margin_Escalator__c = false, Platform_New__c = pm.id, accountId= acc1.id);
            insert opp1;
            
           // Margin_Escalator__c me = new Margin_Escalator__c(Opportunity__c=opp1.Id,Year__c = String.valueOf(System.Today().Year()));
            Margin_Escalator__c me = new Margin_Escalator__c(Opportunity__c=opp1.Id,Year__c =Label.ME_setup_year);
            insert me;
            
            List<Opportunity> listOfOppy = new List<Opportunity>();
            opportunity opp2 = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated',GE_Standard_Probability__c = label.GEStandardProb3, 
                 Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(), 
                 Margin_Escalator__c = false, Platform_New__c = pm.id, accountId= acc1.id);
            listOfOppy.add(opp2);   
            
            opportunity opp3 = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                  Amount = 500, Firm_Anticipated__c = 'Anticipated',
                GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,
                Contract_Award_Date__c = date.today(), Margin_Escalator__c = true, Platform_New__c = pm.id, accountId= acc1.id);
            listOfOppy.add(opp3);
            insert listOfOppy;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True, IsActive = true);
            insert p2;

            
            Date dateInstance  = Date.newInstance(Integer.valueOf(Label.ME_setup_year),1,30); 
            Date dateInstance1 = Date.newInstance(Integer.valueOf(Label.ME_setup_year),7,30);  
            
            List<Opportunity_Line_Item__c> listOfOlis = new List<Opportunity_Line_Item__c>();
            Opportunity_Line_Item__c oli1 = new Opportunity_Line_Item__c(Opportunity__c =  opp2.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id) ;              
            
           listOfOlis.add(oli1);
           
            Opportunity_Line_Item__c oli2 = new Opportunity_Line_Item__c(Opportunity__c =  opp3.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id) ;
            
            listOfOlis.add(oli2); 
            
            insert listOfOlis;        
            
            List<Schedule__c> listOfScheduletoBeInsert = new List<Schedule__c>();
            for(Integer i=0; i<24;i++) {
                Schedule__c olis = new Schedule__c(Opportunity_Line_Item__c = oli1.id, 
                Schedule_Date__c = dateInstance+i, Quantity__c = 1+i, Escalated_Revenue__c = 3+i, Opportunity__c=opp2.ID);
                listOfScheduletoBeInsert.add(olis);
            }  

            for(Integer i=0; i<24;i++) {
                 Schedule__c olis = new Schedule__c(Opportunity_Line_Item__c = oli2.id, 
                Schedule_Date__c = dateInstance+i, Quantity__c = 1+i, Escalated_Revenue__c = 3+i, Opportunity__c=opp3.ID);
                listOfScheduletoBeInsert.add(olis);
            } 
            insert listOfScheduletoBeInsert; 
            
                     
         
        
            Test.startTest();
                String CRON_EXP = '0 0 0 1 1 ? 2025';  
                String jobId = System.schedule('createME_new', CRON_EXP, new Schedule_createME());
            
                CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
            
                System.assertEquals(CRON_EXP, ct.CronExpression); 
                System.assertEquals(0, ct.TimesTriggered);
                System.assertEquals('2025-01-01 00:00:00', String.valueOf(ct.NextFireTime));
            Test.stopTest();
        }
    }
}