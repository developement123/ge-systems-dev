/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - TriggerOnMarginEscalator
    - MarginEscalatorTriggerHandler // handler class
    
    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Abhishek          12/28/2015        Initial Creation
                Abhishek          09/18/2017        removed account platform
*************************************************************************************************************************************************/      
      
      
@istest(SeeAllData = false)
private class Tracker_TriggerOnMarginEscalator {

     static testmethod void test_createMEmanually() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500, Firm_Anticipated__c = 'Anticipated'
                             , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id
                             ,Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id );
            insert opp;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True, IsActive = true);
            insert p2;     
            
            Opportunity_Line_Item__c ol2 = new Opportunity_Line_Item__c(Opportunity__c =  opp.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id) ;       
            insert ol2;
            
            Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
            Date mydate1 = Date.newInstance(System.Today().Year(), 10, 5);
            
            List<Schedule__c> lstOfSch = new List<Schedule__c>();
            lstOfSch.add(new Schedule__c(Opportunity_Line_Item__c = ol2.id, Schedule_Date__c = mydate, 
                         Quantity__c = 1, Escalated_Revenue__c = 3, Opportunity__c=opp.ID));
            lstOfSch.add(new Schedule__c(Opportunity_Line_Item__c = ol2.id, Schedule_Date__c = mydate, 
                         Quantity__c = 1, Escalated_Revenue__c = 100, Opportunity__c=opp.ID));
                        
            insert lstOfSch;
            
        Test.startTest(); 
          //  Class_TriggerCheck.var_execute_createMEmanuallyTrg = true; 
                       
            List<Margin_Escalator__c> listOfME = new List<Margin_Escalator__c>();
            
            listOfME.add(new Margin_Escalator__c(Opportunity__c=opp.Id, Year__c = String.valueOf(System.Today().year()), Out_of_Sync__c = true));
            insert listOfME;
            
            listOfME[0].Out_of_Sync__c = false;
            update listOfME; 

            delete listOfME;
            undelete listOfME;
         Test.stopTest();   
        }
    }    
}