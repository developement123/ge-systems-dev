//Comments added By Abhishek: Jan.- 2016:
//This test class is not required as corresponding trigger is inactivated for Opportunity Optimization Requirement.
//End
/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following Trigger:
    - Trg_updateRelationship_Clone
    
    Version      last modified by               Date               
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.1           Mohan kumar                  10/15/2014        

    Last modified by : Mohan Kumar(25 Feb 2015). 
    Last modified by : Mohan Kumar(14 April 2015).
             
*************************************************************************************************************************************************/      

@istest
private class Tracker_Trg_updateRelationship {    
 /*   static testmethod void mytest1() {
    
        Profile[] pro = [Select Id from profile where name = 'System Administrator' limit 1];
        
        User u = new User( alias = 'skapo12', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US',profileid=pro[0].id,country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc = new Account(Name = 'Hello');
        insert acc;
        
        Platform_Master__c pm=new Platform_Master__c(Name='170', Civil_Military__c='Civil',
                                                     Segment__c='Regional Transports');
        insert pm;
                
        Forecast__c fore = new Forecast__c(Name='Test12345', Airframer__c=acc.id,
                                         Forecast_Platform__c=pm.id);
        insert fore;
            
        Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',
                                                                              Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
        insert hierarchy;
        
        Program__c prog = new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
                                         Forecast__c=fore.id, Program_Probability__c='100% - Funded');
        insert prog;    
       
        List<Opportunity> lstOfOppy = new List<Opportunity>();        
        
        opportunity opp=new opportunity(Name='Test opp', StageName='Prospecting', CloseDate=date.today(),
                                        Program__c=prog.id, Amount =1000, Firm_Anticipated__c='Anticipated', Deal_Type__c = 'NPI',
                                        Probability__c='100% - Won', Lookup_hiearchyname__c = hierarchy.id);
        lstOfOppy.add(opp);  
        
        opportunity opp1=new opportunity(Name='Test opp',StageName='Withdrawn by GE',CloseDate=date.today(),
                                         Program__c=prog.id, Amount =1000,Firm_Anticipated__c='Anticipated', Deal_Type__c = 'Services',
                                         Probability__c='100% - Won', Lookup_hiearchyname__c = hierarchy.id);
        lstOfOppy.add(opp1); 
        
        opportunity opp2=new opportunity(Name='Test opp', StageName='Withdrawn by GE', CloseDate=date.today(),
                                         Program__c=prog.id, Amount =1000,Firm_Anticipated__c='Anticipated', Deal_Type__c = 'Momentum with NRE',
                                         Probability__c='100% - Won', Lookup_hiearchyname__c = hierarchy.id);
        lstOfOppy.add(opp2);
        
        insert lstOfOppy;         
       
        Sales_Request__c sr=new Sales_Request__c(Windchill_US_UK__c ='Windchill US', Name='Testresponse5', Program__c=prog.id, Response_Description__c='Test desc');
        insert sr;
        
        Test.startTest();        
            List<Oppty_SR_Intersection__c> listSR = new List<Oppty_SR_Intersection__c>();
            
            listSR.add(new Oppty_SR_Intersection__c(Opportunities__c=opp.id, Sales_Request__c=sr.id));        
            
            listSR.add(new Oppty_SR_Intersection__c(Opportunities__c=opp1.id, Sales_Request__c=sr.id));
            
            listSR.add(new Oppty_SR_Intersection__c(Opportunities__c=opp2.id, Sales_Request__c=sr.id));
            
            Class_TriggerCheck.var_Trg_updateRelationshipTrg = true;
            insert listSR;    
           
            Class_TriggerCheck.var_Trg_updateRelationshipTrg = true;
            update listSR;
           
            Class_TriggerCheck.var_Trg_updateRelationshipTrg = true;
            delete listSR;       
        Test.stopTest();     
    } */
}