/*************************************************************************************************************************************************
This test class was created to provide code coverage for the following trigger:
- CostAuthTrigger

Version     Author(s)               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0         Mohan kumar          02/02/2015        Initial Creation
            Abhishek             02/29/2016        Modfied for JIRA 498
            bhishek              09/18/2017        removed account platform     
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_CostAuthTrigger {
 
   static testmethod void test_CostAuthTrigger() {
      
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);    
        
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
        
        Contact con = new Contact(AccountId = acc1.Id, Email = 'neha.mittal@birlasoft.com', FirstName = 'Abc', LastName = 'Test', MailingCity = 'Abc', MailingCountry = 'Test Country', Function__c = 'Legal/Contracts');
        Insert con;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', User__c = u.id, Contract_Manager__c = con.id, Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id, Program_Probability__c = '100% - Funded', Is_Tokenized__c = false);
            insert prog;
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp;
            
            Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2', Program__c = prog.id, Proposal_Coordinator__c = con.Id,    Proposal_Manager__c = u.id, Engineering_Lead__c = con.id, Cost_Estimator__c = con.id, Additional_Member__c = con.id, Pipeline_Opportunity__c = opp.Id);
            
            insert sr;
            
            EPA__c epa = new EPA__c(Contract_Site__c = 'Brisbane');
            insert epa;            
            
            Test.startTest(); 
               
            Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, FY_Spent_to_Date__c = NULL, Close_Cost_Authorization__c = false, Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open'));
            Class_TriggerCheck.IsPreventing = true;
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = true;
            insert ca;
            
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = true;
            ca.Close_Cost_Authorization__c = true;
            update ca;
                
            Test.stopTest();
                              
               
        }
    }    
    
    
    static testmethod void test_CostAuthTrigger_Callout() {
      
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = true;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);    
        
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
        
        Contact con = new Contact(AccountId = acc1.Id, Email = 'neha.mittal@birlasoft.com', FirstName = 'Abc', LastName = 'Test', MailingCity = 'Abc', MailingCountry = 'Test Country', Function__c = 'Legal/Contracts');
        Insert con;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1', User__c = u.id, Contract_Manager__c = con.id, Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id, Program_Probability__c = '100% - Funded', Is_Tokenized__c = false);
            insert prog;
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp;
            
            Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2', Program__c = prog.id, Proposal_Coordinator__c = con.Id,    Proposal_Manager__c = u.id, Engineering_Lead__c = con.id, Cost_Estimator__c = con.id, Additional_Member__c = con.id, Pipeline_Opportunity__c = opp.Id);
            
            insert sr;
            
            EPA__c epa = new EPA__c(Contract_Site__c = 'Brisbane');
            insert epa;            
            
            Test.startTest(); 
               
            Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, FY_Spent_to_Date__c = NULL, Close_Cost_Authorization__c = false, Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open'));
            Class_TriggerCheck.IsPreventing = true;
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
            insert ca;
            
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
            ca.Close_Cost_Authorization__c = true;
            ca.Status__c = 'Approved';
            update ca;
                
            Test.stopTest();
                              
               
        }
    }
    
    static testmethod void test_validateBPunderPipeline() {
      
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);    
        
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
        
        Contact con = new Contact(AccountId = acc1.Id, Email = 'neha.mittal@birlasoft.com', FirstName = 'Abc', LastName = 'Test', MailingCity = 'Abc', MailingCountry = 'Test Country', Function__c = 'Legal/Contracts');
        Insert con;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

     
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp;
            
            Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2',  Proposal_Coordinator__c = con.Id,    Proposal_Manager__c = u.id, Engineering_Lead__c = con.id, Cost_Estimator__c = con.id, Additional_Member__c = con.id, Pipeline_Opportunity__c = opp.Id);
            
            insert sr;
            
            EPA__c epa = new EPA__c(Contract_Site__c = 'Brisbane');
            insert epa;            
            
            Test.startTest(); 
               
            Cost_Authorization__c ca = new Cost_Authorization__c(
            EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, 
            FY_Spent_to_Date__c = NULL, Close_Cost_Authorization__c = false, Phase__c = 'Phase 1', 
            recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase1_B_P_Task_Open')
            , Pipeline_Opportunity__c = opp.id );
            
            Class_TriggerCheck.IsPreventing = true;
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = true;
            try{
                insert ca;
            }catch(exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('create B&P task on associated Response') ? true : false ;
                System.AssertEquals(expectedExceptionThrown, true);
            }
                
            Test.stopTest();
                              
               
        }
    }
    
    static testmethod void test_validateBPTaskPerEPA() {
      
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);    
        
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
        
        Contact con = new Contact(AccountId = acc1.Id, Email = 'neha.mittal@birlasoft.com', FirstName = 'Abc', LastName = 'Test', MailingCity = 'Abc', MailingCountry = 'Test Country', Function__c = 'Legal/Contracts');
        Insert con;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

     
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp;
            
            
            opportunity Newopp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert Newopp;
            
            Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2',Proposal_Coordinator__c = con.Id,    Proposal_Manager__c = u.id, Engineering_Lead__c = con.id, Cost_Estimator__c = con.id, Additional_Member__c = con.id, Pipeline_Opportunity__c = opp.Id);
            
            insert sr;
            
            EPA__c epa = new EPA__c(Contract_Site__c = 'Brisbane');
            insert epa;      
            list<Cost_Authorization__c> listOfCostAuth = new list<Cost_Authorization__c>();
            for(integer i=0;i<2;i++){   
                Cost_Authorization__c caOpp = new Cost_Authorization__c(
                EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, 
                FY_Spent_to_Date__c = NULL, Close_Cost_Authorization__c = false, Phase__c = 'Phase 1', 
                recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase1_B_P_Task_Open')
                ,Pipeline_Opportunity__c = Newopp.id );
                listOfCostAuth.add(caOpp);
            }
            for(integer j=0;j<2;j++){   
                Cost_Authorization__c caRes = new Cost_Authorization__c(
                EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, 
                FY_Spent_to_Date__c = NULL, Close_Cost_Authorization__c = false, Phase__c = 'Phase 2', 
                recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open')
                ,Sales_Request__c = sr.id );
                listOfCostAuth.add(caRes);
            }
            
            Test.startTest(); 
            
            
            Class_TriggerCheck.IsPreventing = true;
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = true;
            try{
                insert listOfCostAuth;
            }catch(exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('with selected EPA') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
              system.debug('test error');
            }
            
                
            Test.stopTest();
                              
               
        }
    }
    
    static testmethod void test_calculatePipelineSpendTodate() {
      
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        Test.setMock(HttpCalloutMock.class, mock);    
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
        
        Contact con = new Contact(AccountId = acc1.Id, Email = 'neha.mittal@birlasoft.com', FirstName = 'Abc', LastName = 'Test', MailingCity = 'Abc', MailingCountry = 'Test Country', Function__c = 'Legal/Contracts');
        Insert con;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

     
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp;
            
            
            opportunity Newopp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert Newopp;
            
            Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2',Proposal_Coordinator__c = con.Id,    Proposal_Manager__c = u.id, Engineering_Lead__c = con.id, Cost_Estimator__c = con.id, Additional_Member__c = con.id, Pipeline_Opportunity__c = opp.Id);
            
            insert sr;
            
            EPA__c epa = new EPA__c(Contract_Site__c = 'Brisbane');
            insert epa;      
            list<Cost_Authorization__c> listOfCostAuth = new list<Cost_Authorization__c>();
        
                Cost_Authorization__c caOpp = new Cost_Authorization__c(
                EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, 
                 Close_Cost_Authorization__c = false, Phase__c = 'Phase 1', 
                recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase1_B_P_Task_Open')
                ,Pipeline_Opportunity__c = Newopp.id , FY_Spent_to_Date__c = 10);
                listOfCostAuth.add(caOpp);
                
                Cost_Authorization__c caRes = new Cost_Authorization__c(
                EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, 
                 Close_Cost_Authorization__c = false, Phase__c = 'Phase 2', 
                recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open')
                ,Sales_Request__c = sr.id, FY_Spent_to_Date__c = 10 );
                listOfCostAuth.add(caRes);
        
            Test.startTest(); 
            
            
            Class_TriggerCheck.IsPreventing = true;
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = true;
            
            insert listOfCostAuth;
            Opportunity oppSpendTodate = [select id, Pipeline_Spend_to_Date__c from Opportunity where id=:Newopp.id][0];
                system.assertEquals(10, oppSpendTodate.Pipeline_Spend_to_Date__c, 'assertionfailed');
            Test.stopTest();
                              
               
        }
    }
    
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }   
}