/*
 Created By Shravan Godha 
*/
public class GEAS_trgContactDuplicatePreventer_Helper {
    public void Contactduplicate(List<Contact> newList,List<Contact> oldList,Map<Id, Contact> newMap, Map<Id, Contact> oldMap,String eventType, String inserttriggerType) {
        Map<String, Contact> contactMapEmail = new Map<String, Contact>();// This is for Email
        Map<string, Contact> contactMapLN = new Map<String, Contact>(); // This is for Last Name
        Map<string, Contact> contactMapFN = new Map<String, Contact>(); // This is for First Name
        system.debug('inserttriggerType@@@' + inserttriggerType);
        for (Contact cont : newList)
        {
            
            // This logic will get bypassed when email address, firstname, last name are not changing.
            
            if ((cont.Email != null && cont.LastName !=null && cont.FirstName!=null) &&((inserttriggerType=='true') ||
                                                                                        ((cont.Email != oldMap.get(cont.Id).Email)||(cont.LastName != oldMap.get(cont.Id).LastName)||(cont.FirstName != oldMap.get(cont.Id).FirstName))))
            {
                // Logic to check newly added contact isn't a duplicate.  
                if (contactMapEmail.containsKey(cont.Email)&& contactMapLN.containsKey(cont.LastName) && contactMapFN .containsKey(cont.FirstName))
                {
                    cont.Email.addError('Duplicate contact exists with'+ 'same email address, LastName, FirstName');
                } 
                else
                {
                    contactMapEmail.put(cont.Email, cont);
                    contactMapLN.put(cont.LastName, cont);
                    contactMapFN.put(cont.FirstName, cont);
                }
            }
        }
        
        // Using a single database query, find all the Contacts in 
        // the database that have the same email address, last name, first name  as any  
        // of the Contacts being inserted or updated.  
        
        for (Contact cont : [SELECT Email, LastName, FirstName FROM Contact WHERE Email IN :contactMapEmail.KeySet() 
                             	And LastName IN :contactMapLN.KeySet() And FirstName IN :contactMapFN.KeySet()]) {
                                 Contact newContact = contactMapEmail.get(cont.Email);
                                 newContact.addError('A Contact with this Email, '+ 'First Name and Last Name already exists.');
                             }
    }
    
}