/*************************************************************************************************************************************************
This test class was created to provide code coverage for the following trigger:
-    trgAutoUpdateOpportunity_Clone

Version     Author(s)               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0         Mohan kumar          26 dec 2014        Initial Creation
            Abhishek             29 Feb 2016        modified for   JIRA 498
            Abhishek             18 sep 2017        removed account platform
*************************************************************************************************************************************************/      
      
@istest
private class Tracker_trgAutoUpdateOpportunity {

    static testmethod void test_trgAutoUpdateOpportunity() {
        Profile[] pro = [select id from profile where name = 'System Administrator' limit 1];

        User u = new User(alias = 'mkumar', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'mohan1.kumar@ge.com', emailencodingkey = 'UTF-8', lastname = 'kumar', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'mohan1.kumar@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(name = 'Hello');
        insert acc1; 
        
        system.runAs(u) {
        
            List<Platform_Master__c> listOfPM = new List<Platform_Master__c>();
            
            Platform_Master__c pm = new Platform_Master__c(Name = '17011111111111111111111111111111111111111111111111111111111111111', Civil_Military__c = 'Military',Segment__c = 'Regional Transports');
            listOfPM.add(pm);
            Platform_Master__c pm1 = new Platform_Master__c(Name = '171', Civil_Military__c = 'Military',Segment__c = 'Regional Transports');
            listOfPM.add(pm1);
            Platform_Master__c pm2 = new Platform_Master__c(Name = '172', Civil_Military__c = 'Military',Segment__c = 'Regional Transports');
            listOfPM.add(pm2);
            
            insert listOfPM;
           
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;           
            
            Test.startTest();
            
            List<Program__c> lstOfProg = new List<Program__c>();
            
            Program__c prog = new Program__c(Name = 'Program', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                                             Program_Probability__c = '50% - Potential funding');
            lstOfProg.add(prog);                                 
            Program__c prog1 = new Program__c(Name = 'Program1', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                                             Program_Probability__c = '100% - Funded');
            lstOfProg.add(prog1);
            Program__c prog2 = new Program__c(Name = 'Program2', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                                             Program_Probability__c = '0% - Not funded');
            lstOfProg.add(prog2);                                 
            insert lstOfProg;
            
            List<Opportunity> listOfOppy = new List<Opportunity>();
            
            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp', StageName = Label.OpportunityStage5, CloseDate = date.today(),
                                               Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                                               GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Contract_Award_Date__c = date.today(), Platform_New__c = pm.id, accountId= acc1.id);
            
            listOfOppy.add(opp);
            // added Account_Platform__c for JIRA 498
            opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = Label.OpportunityStage5, CloseDate = date.today(),
                                               Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                                               GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Contract_Award_Date__c = date.today(),  Platform_New__c = pm.id, accountId= acc1.id);
            
            listOfOppy.add(opp1);
            // added Account_Platform__c for JIRA 498
            opportunity opp2 = new opportunity(Name = 'Test opp2', StageName = Label.OpportunityStage5, CloseDate = date.today(),
                                               Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                                               GE_Standard_Probability__c = Label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id, Contract_Award_Date__c = date.today(), Platform_New__c = pm.id, accountId= acc1.id);
            listOfOppy.add(opp2);              
           
            insert listOfOppy;            
            
            Class_TriggerCheck.Var_isProgam_Probability_change = false;
            
            prog.Hidden_Account_Platform_Name__c = 'not null';
            update lstOfProg;
                
            Test.stopTest();
        }
    }
}