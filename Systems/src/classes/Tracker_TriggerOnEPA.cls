/*****************************************************************
This test class was created to provide code coverage for the following trigger/Class:
- TriggerOnEPA/EPATriggerHandler

Version     Author(s)               Date              Details 
--------------------------------------------------------------------------------------
1.0         Abhishek          07/14/2016        Initial Creation
            Abhishek          18/09/2017        removed account platform
*******************************************************************/       
      
@istest
private class Tracker_TriggerOnEPA
 {
 
   static testmethod void test_ValidateEPATOnDelete() {
        
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_StopProductHiearchy = false;
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        Class_TriggerCheck.var_TriggerOnResponse = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
        Class_TriggerCheck.IsPreventing = true;
        Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;  
        
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US', localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;

        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
        
        Contact con = new Contact(AccountId = acc1.Id, Email = 'neha.mittal@birlasoft.com', FirstName = 'Abc', LastName = 'Test', MailingCity = 'Abc', MailingCountry = 'Test Country', Function__c = 'Legal/Contracts');
        Insert con;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military', Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            // added Account_Platform__c for JIRA 498
            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp;
            
            opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                 Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id
                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
            insert opp1;
            
            Sales_Request__c sr = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response2',  Proposal_Coordinator__c = con.Id,    Proposal_Manager__c = u.id, Engineering_Lead__c = con.id, Cost_Estimator__c = con.id, Additional_Member__c = con.id, Pipeline_Opportunity__c = opp.id);
            
            insert sr;
            
            Sales_Request__c sr1 = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response',  Proposal_Coordinator__c = con.Id,    Proposal_Manager__c = u.id, Engineering_Lead__c = con.id, Cost_Estimator__c = con.id, Additional_Member__c = con.id, Pipeline_Opportunity__c = opp1.id);
            
            insert sr1;
            
            EPA__c epa = new EPA__c(Contract_Site__c = 'Brisbane');
            insert epa;            
            
            list<Cost_Authorization__c> listOfCuastAuthToInsert = new list<Cost_Authorization__c>();
            Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, FY_Spent_to_Date__c = NULL, Cost_Authorization_US_UK__c = 'Cost Authorization US', Close_Cost_Authorization__c = false);
            listOfCuastAuthToInsert.add(ca) ;
            Cost_Authorization__c ca1 = new Cost_Authorization__c(Sales_Request__c = sr1.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = 1000, FY_Spent_to_Date__c = NULL, Cost_Authorization_US_UK__c = 'Cost Authorization US', Close_Cost_Authorization__c = false);
            listOfCuastAuthToInsert.add(ca1) ;
            Insert listOfCuastAuthToInsert;
            
            Test.startTest(); 
             try{
                delete epa;
             } catch(exception e) {
               boolean isException = e.getMessage().contains('EPA') ? true : false ;
                    system.assertEquals(isException , true);
               }
                
            Test.stopTest();
                              
               
        }
    }    
    
}