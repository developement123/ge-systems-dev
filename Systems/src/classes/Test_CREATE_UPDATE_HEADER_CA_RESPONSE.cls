/**
     * This class contains unit tests for validating the behavior of Apex classes
     * and triggers.
     *
     * Unit tests are class methods that verify whether a particular piece
     * of code is working properly. Unit test methods take no arguments,
     * commit no data to the database, and are flagged with the testMethod
     * keyword in the method definition.
     *
     * All test methods in an organization are executed whenever Apex code is deployed
     * to a production organization to confirm correctness, ensure code
     * coverage, and prevent regressions. All Apex classes are
     * required to have at least 75% code coverage in order to be deployed
     * to a production organization. In addition, all triggers must have some code coverage.
     * 
     * The @isTest class annotation indicates this class only contains test
     * methods. Classes defined with the @isTest annotation do not count against
     * the organization size limit for all Apex scripts.
     *
     * See the Apex Language Reference for more information about Testing and Code Coverage.
       Modified by Abhishek on 18 sep 2017 to remove account platform
     */
    @isTest
    private class Test_CREATE_UPDATE_HEADER_CA_RESPONSE {

        static testMethod void myUnitTest() { // NON UK Callout
            // TO DO: implement unit test
            Class_TriggerCheck.var_triggerOnUser = false;
            Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
            Class_TriggerCheck.var_TriggerOnAccount = false;
            Class_TriggerCheck.Var_StopProductHiearchy = false;
            Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
            Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
            Class_TriggerCheck.var_TriggerOnResponse = false;
            Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
            Class_TriggerCheck.var_IsStopEPABatchTrg = false;
            Class_TriggerCheck.Var_StopTriggerOnEPA = false;
            Class_TriggerCheck.Var_ShouldEPA_update = false;
            
             
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('mockresponse');
            mock.setStatusCode(200);
            mock.setHeader('Content-Type', 'text/xml');
            Test.setMock(HttpCalloutMock.class, mock);
            HTTPResponse response =CREATE_UPDATE_HEADER_CA_RESPONSE.Main_Header_Info('Check');
            
           
             Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
         insert plat;
         
         Account acc = new Account(Name='Test Account SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS');
         insert acc; 
         id bA_Profile_id;
         id Sys_admin_Profile_id;
         
       list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
       for(Profile p: pro)
       {
         if(p.name=='Systems Business Admin')
         {
            bA_Profile_id=p.Id;
         }
         else
         {
            Sys_admin_Profile_id=p.id;
         }
       }
         User u =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
                   localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
                   insert u;
                   
                   
         User u2 =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
                   localesidkey = 'en_US',profileid=bA_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12453.com.sys.dev');
             insert u2;
             
         Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
         insert hierarchy;
        
         Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
         Program_Probability__c='100% - Funded');
         insert prog;
         
         opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(),
                     Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id
                    , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
                insert opp1;
           
        Sales_Request__c sr=new Sales_Request__c(Windchill_US_UK__c ='Windchill US',Name='Test response5',Program__c=prog.id,Response_Description__c='Test desc', Pipeline_Opportunity__c = opp1.id);
        insert sr;
        List<String> List_sr=new list<String>();
        List_sr.add(sr.id);
         CREATE_UPDATE_HEADER_CA_RESPONSE.Create_Header(List_sr);
        
        EPA__c epa=new EPA__c(Closed__c=False);
        insert epa;
        
       
        
        Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Approved_Budget_Backend__c=NULL,
        FY_Spent_to_Date__c=NULL, Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open'));
        insert ca;
        test.starttest();
            CREATE_UPDATE_HEADER_CA_RESPONSE.Update_Header_UpdateResponse(List_sr);
            list<String> List_CA_id= new list<String>();
            List_CA_id.add(ca.id);
            CREATE_UPDATE_HEADER_CA_RESPONSE.CreateUpdate_Header_CA(List_CA_id,'Update');
        test.stoptest();
        }
        
         static testMethod void myUnitTestUK() { // UK Callout
            // TO DO: implement unit test
            Class_TriggerCheck.var_triggerOnUser = false;
            Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
            Class_TriggerCheck.var_TriggerOnAccount = false;
            Class_TriggerCheck.Var_StopProductHiearchy = false;
            Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
            Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
            Class_TriggerCheck.var_TriggerOnResponse = false;
            Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
            Class_TriggerCheck.var_IsStopEPABatchTrg = false;
            Class_TriggerCheck.Var_StopTriggerOnEPA = false;
            Class_TriggerCheck.Var_ShouldEPA_update = false;
            
            
             
                StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
                mock.setStaticResource('mockresponse');
                mock.setStatusCode(200);
                mock.setHeader('Content-Type', 'text/xml');
                Test.setMock(HttpCalloutMock.class, mock);
                HTTPResponse response =CREATE_UPDATE_HEADER_CA_RESPONSE.Main_Header_Info('Check');
            
           
                Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
                insert plat;
         
                Account acc = new Account(Name='Test Account SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS');
                insert acc; 
                id bA_Profile_id;
                id Sys_admin_Profile_id;
         
                list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
                for(Profile p: pro){
                    if(p.name=='Systems Business Admin'){
                        bA_Profile_id=p.Id;
                    }
                    else{
                        Sys_admin_Profile_id=p.id;
                    }
                }
                User u =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
                   localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
                insert u;
                   
                   
                User u2 =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
                   localesidkey = 'en_US',profileid=bA_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12453.com.sys.dev');
                insert u2;
       
                Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
                insert hierarchy;
        
                 
         
                opportunity opp1 = new opportunity(Name = 'Test opp1', StageName = label.OpportunityStage1, CloseDate = date.today(), Amount = 500, Firm_Anticipated__c = 'Anticipated', Platform_New__c = plat.id, accountId= acc.id, GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),MTG_Year__c = String.valueOf(System.Today().Year()));
                insert opp1;
           
                Sales_Request__c sr=new Sales_Request__c(Windchill_US_UK__c ='Windchill UK',Name='Test response5',Response_Description__c='Test desc', Pipeline_Opportunity__c = opp1.id);
                insert sr;
                
                List<String> List_sr=new list<String>();
                    List_sr.add(sr.id);
                
                CREATE_UPDATE_HEADER_CA_RESPONSE.Create_Header(List_sr);
        
                EPA__c epa=new EPA__c(Closed__c=False);
                insert epa;
        
        
                Cost_Authorization__c ca=new Cost_Authorization__c(Sales_Request__c=sr.id,EPA_Number__c=epa.id,Approved_Budget_Backend__c=NULL, FY_Spent_to_Date__c=NULL,Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open'));
                insert ca;
            test.starttest();
                CREATE_UPDATE_HEADER_CA_RESPONSE.Update_Header_UpdateResponse(List_sr);
                list<String> List_CA_id= new list<String>();
                List_CA_id.add(ca.id);
                CREATE_UPDATE_HEADER_CA_RESPONSE.CreateUpdate_Header_CA(List_CA_id,'Update');
                
            test.stoptest();
        }
         public static id findRecordtypeId(string objectType, string recordTypeName){
            return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
        } 
    }