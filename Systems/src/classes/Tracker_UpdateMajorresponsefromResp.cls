/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
        - UpdateMajorresponsefromResponse    
    Modified by Abhishek on 02/29/2016 for JIRA 498
    Modified by Abhishek on 09/18/2017 removed account platform
*************************************************************************************************************************************************/      

@istest
private class Tracker_UpdateMajorresponsefromResp {

    static testMethod void myTest() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.var_TriggerOnResponse = false;
        
        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor@birlasoft50.com.sys.dev');
        insert u;       
        
        List<Account> listOfAcc = new List<Account>();
        
        Account acc1 = new Account(Name = 'Test Account');
        listOfAcc.add(acc1);
        Account acc2 = new Account(Name = 'Test Account2');
        listOfAcc.add(acc2);
        Account acc3 = new Account(Name = 'Test Account3');
        listOfAcc.add(acc3);
        
        insert listOfAcc;
        
        System.runAs(u) {
        
            Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Civil', Segment__c = 'Regional Transports');
            insert plat;       
    
            Contact con = new Contact(Account = acc1, Email = 'neha.mittal@birlasoft.com', LastName = 'Test');        
         
    
            List<Product_Hiearchy_Object__c> lstForInsertPH = new List<Product_Hiearchy_Object__c>();
            
            Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child1');
            lstForInsertPH.add(hierarchy1);
    
            Product_Hiearchy_Object__c hierarchy2 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child2');
            lstForInsertPH.add(hierarchy2);
    
            Product_Hiearchy_Object__c hierarchy3 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child3');
            lstForInsertPH.add(hierarchy3);
    
            insert lstForInsertPH;
            
             Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy1.id,Name='Test Product',IsActive=true);
            insert p2;
            
            Product2 p3 = new Product2(Lookup_hiearchyname__c=hierarchy2.id,Name='Test Product1',IsActive=true);
            insert p3;
    
            
            // added Account_Platform__c for JIRA 498
            Opportunity opp = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1, CloseDate = date.today(),  Amount = 1000, Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, deal_type__c = 'Services', Lookup_hiearchyname__c = hierarchy1.id, Platform_New__c =plat.id, accountId= acc1.id);
            insert opp;
            // added Account_Platform__c for JIRA 498
            opportunity opp1 = new opportunity(Name = 'My Test opp', StageName =label.OpportunityStage1,CloseDate = date.today()+2, Contract_Term__c = 13,Amount = 1500, Firm_Anticipated__c = 'Anticipated', deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy2.id,Offering_Type__c ='OE / Services', Platform_New__c =plat.id, accountId= acc1.id);
           
             insert opp1;
            
            Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
            Probability__c = 111,Platform__c = plat.id,Parent_Account__c = acc1.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D');
            insert oli;
            
            Opportunity_Line_Item__c oli1 = new Opportunity_Line_Item__c(Opportunity__c = opp1.id, Product__c = p3.id, 
            Probability__c = 111,Platform__c = plat.id,Parent_Account__c = acc2.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test1',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today()+10);
            insert oli1;
            
            List<Major_Proposal__c> listOfMP = new List<Major_Proposal__c>();
    
            Major_Proposal__c mp = new Major_Proposal__c(Name = 'Test Proposal',Account__c=acc2.id,Platform__c=plat.id);
            listOfMP.add(mp);        
    
            Major_Proposal__c mp2 = new Major_Proposal__c(Name = 'Test Proposal',Account__c=acc3.id,Platform__c=plat.id);
            listOfMP.add(mp2);
                    
            insert listOfMP;
    
            List<Sales_Request__c> lstforInsertSR = new List<Sales_Request__c>();
    
            Sales_Request__c sr1 = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response', Proposal_Coordinator__c = con.Id, Major_Proposals__c = mp.Id,Pipeline_Opportunity__c=opp1.id);
            lstforInsertSR.add(sr1);                  
            
            insert lstforInsertSR; 
            
            Test.startTest();  
                Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = true;
                try {
                    sr1.Major_Proposals__c = mp2.Id; 
                    update lstforInsertSR;
                }
                catch(Exception ex) {
                }
            Test.stopTest();
        }
    } 
    
     static testMethod void myTest1() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false; 
        Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.var_TriggerOnResponse = false;
        
        Profile[] pro = [select Id from profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo', email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor@birlasoft50.com.sys.dev');
        insert u;       
        
        List<Account> listOfAcc = new List<Account>();
        
        Account acc1 = new Account(Name = 'Test Account');
        listOfAcc.add(acc1);
        Account acc2 = new Account(Name = 'Test Account2');
        listOfAcc.add(acc2);
        Account acc3 = new Account(Name = 'Test Account3');
        listOfAcc.add(acc3);
        
        insert listOfAcc;
        
        System.runAs(u) {
        
            Platform_Master__c plat = new Platform_Master__c(Name = '100', Civil_Military__c = 'Civil', Segment__c = 'Regional Transports');
            insert plat;       
    
            Contact con = new Contact(Account = acc1, Email = 'neha.mittal@birlasoft.com', LastName = 'Test');  
            
            List<Product_Hiearchy_Object__c> lstForInsertPH = new List<Product_Hiearchy_Object__c>();
            
            Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child1');
            lstForInsertPH.add(hierarchy1);
    
            Product_Hiearchy_Object__c hierarchy2 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child2');
            lstForInsertPH.add(hierarchy2);
    
            Product_Hiearchy_Object__c hierarchy3 = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child3');
            lstForInsertPH.add(hierarchy3);
    
            insert lstForInsertPH;
            
             Product2 p2 = new Product2(Lookup_hiearchyname__c=hierarchy1.id,Name='Test Product',IsActive=true);
            insert p2;
            
            Product2 p3 = new Product2(Lookup_hiearchyname__c=hierarchy2.id,Name='Test Product1',IsActive=true);
            insert p3;
    
            
            // added Account_Platform__c for JIRA 498
            Opportunity opp = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1, CloseDate = date.today(),  Amount = 1000, Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, deal_type__c = 'Services', Lookup_hiearchyname__c = hierarchy1.id, Platform_New__c = plat.id, accountId= acc1.id, Total_Margin__c = 20);
            insert opp;
            
            // added Account_Platform__c for JIRA 498
            opportunity opp1 = new opportunity(Name = 'My Test opp', StageName =label.OpportunityStage1,CloseDate = date.today()+2, Contract_Term__c = 13,Amount = 1500, Firm_Anticipated__c = 'Anticipated',deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb2, Lookup_hiearchyname__c = hierarchy2.id,Offering_Type__c ='OE / Services', Platform_New__c = plat.id, accountId= acc1.id, Total_Margin__c = 30);
           
             insert opp1;
             
             Opportunity oppServiceOld = new opportunity(Name = 'Test Service OLI', StageName = label.OpportunityStage1, CloseDate = date.today(),  Amount = 1000, Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, deal_type__c = 'Services', Lookup_hiearchyname__c = hierarchy1.id, Platform_New__c = plat.id, accountId= acc1.id, Total_Margin__c = 20);
            insert oppServiceOld;
            
             Opportunity_Line_Item__c oliServiceOld = new Opportunity_Line_Item__c(Opportunity__c = oppServiceOld.id, Product__c = p2.id, 
            Probability__c = 110,Platform__c = plat.id,Parent_Account__c = acc1.id,Revenue_Stream_top_level__c='Services', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Cost_Backend__c = 10);
            insert oliServiceOld;
            
            opportunity oppServiceNew = new opportunity(Name = 'My Service OLI', StageName =label.OpportunityStage1,CloseDate = date.today()+2, Contract_Term__c = 13,Amount = 1500, Firm_Anticipated__c = 'Anticipated',deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb2, Lookup_hiearchyname__c = hierarchy2.id,Offering_Type__c ='OE / Services', Platform_New__c = plat.id, accountId= acc1.id, Total_Margin__c = 30);
           
            insert oppServiceNew;
             
            Opportunity_Line_Item__c oliServiceNew = new Opportunity_Line_Item__c(Opportunity__c = oppServiceNew.id, Product__c = p3.id, 
            Probability__c = 111,Platform__c = plat.id,Parent_Account__c = acc2.id,Revenue_Stream_top_level__c='Services', Execution_Site__c = 'test1',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today()+10, Cost_Backend__c = 20);
            insert oliServiceNew;
            
            Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c(Opportunity__c = opp.id, Product__c = p2.id, 
            Probability__c = 110,Platform__c = plat.id,Parent_Account__c = acc1.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today(),Revenue_Stream_child_level__c = 'Funded R&D', Cost_Backend__c = 10);
            insert oli;
            
            Opportunity_Line_Item__c oli1 = new Opportunity_Line_Item__c(Opportunity__c = opp1.id, Product__c = p3.id, 
            Probability__c = 111,Platform__c = plat.id,Parent_Account__c = acc2.id,Revenue_Stream_top_level__c='Engineering', Execution_Site__c = 'test1',Quantity__c = 5,Unit_Price__c=1,of_Installments__c =111,Installment_Periods__c ='Annually',Schedule_Start_Date__c = system.today()+10, Cost_Backend__c = 20);
            insert oli1;
            
            List<Major_Proposal__c> listOfMP = new List<Major_Proposal__c>();
    
            Major_Proposal__c mp = new Major_Proposal__c(Name = 'Test Proposal',Account__c=acc2.id,Platform__c=plat.id);
            listOfMP.add(mp);        
    
            Major_Proposal__c mp2 = new Major_Proposal__c(Name = 'Test Proposal',Account__c=acc3.id,Platform__c=plat.id);
            listOfMP.add(mp2);
                    
            insert listOfMP;
    
            List<Sales_Request__c> lstforInsertSR = new List<Sales_Request__c>();
    
            Sales_Request__c sr1 = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test response',  Proposal_Coordinator__c = con.Id, Major_Proposals__c = mp.Id,Pipeline_Opportunity__c=opp1.id);
            lstforInsertSR.add(sr1);  
            Sales_Request__c srService = new Sales_Request__c(Windchill_US_UK__c = 'Windchill US', Name = 'Test service',  Proposal_Coordinator__c = con.Id, Major_Proposals__c = mp.Id,Pipeline_Opportunity__c=oppServiceOld.id);
            lstforInsertSR.add(srService);  
            
            insert lstforInsertSR;  
            
             List<Sales_Request__c> lstforUpdateSR = new List<Sales_Request__c>();
            Test.startTest(); 
                sr1.Pipeline_Opportunity__c = opp.id;
                sr1.Major_Proposals__c = mp.Id;  
                lstforUpdateSR.add(sr1);
                srService.Pipeline_Opportunity__c = oppServiceNew.id;
                srService.Major_Proposals__c = mp.id;
                lstforUpdateSR.add(srService);
                
                Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = true;
                //try {
                    update lstforUpdateSR;  
               // }
               // catch(Exception ex) {
               // }
            Test.stopTest();
        }
    } 
}