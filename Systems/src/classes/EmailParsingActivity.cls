/*
* ClassName: EmailParsingActivity 
* CreatedBy: SaiRam Agrahar
* LastModifiedBy: SaiRam Agrahar
* LastModifiedOn: 12 Sep 2012
* Used in: infoPathemailTemp
* Description: This will create the list of Binary attachments from email
* ---------------------------------------
* Revision History
* 12 Sep 2012 – Initial Creation
*/


public class EmailParsingActivity {
    
    public void attachToRecord(Messaging.InboundEmail.BinaryAttachment[] bAttachments, ID cimsRecId) {     
        String contents='';                       //ok
        List<Attachment> listAttach = new List<Attachment>();
        if(bAttachments != null) {
            for(Messaging.InboundEmail.BinaryAttachment bAttach : bAttachments){  //ok
                system.debug('attach file name ' + bAttach.filename);
                
                Attachment attach = new Attachment();
                
                /*
                //attach.Body = Blob.valueOf(bAttach.body.toString());
                String b64Data = EncodingUtil.base64Encode(bAttach.body);
                //Blob bab = bAttach.body;
                attach.Body = Blob.valueOf(b64Data);
                */
                
                attach.Body = bAttach.body;
                attach.Name = bAttach.filename;    // name of the attachement should be same a attached to email
                //attach.parentId = submitRecord.Id;
                attach.parentId = cimsRecId;
                listAttach.add(attach);        
            }
            system.debug('attach list size ' + listAttach.size());
            insert listAttach;
        }
    }
    
}


/*
public class EmailParsingActivity {
    
    public void attachToRecord(Messaging.InboundEmail.BinaryAttachment[] bAttachments, ID cimsRecId) {     
        String contents='';                       //ok
        List<Attachment> listAttach = new List<Attachment>();

        if(bAttachments != null) {
            for(Messaging.InboundEmail.BinaryAttachment bAttach : bAttachments){  //ok
                system.debug('attach file name ' + bAttach.filename);
                
                Attachment attach = new Attachment();
                
                //attach.Body = Blob.valueOf(bAttach.body.toString());
                String b64Data = EncodingUtil.base64Encode(bAttach.body);
                //Blob bab = bAttach.body;
                attach.Body = Blob.valueOf(b64Data);
                attach.Name = bAttach.filename;    // name of the attachement should be same a attached to email
                //attach.parentId = submitRecord.Id;
                attach.parentId = cimsRecId;
                listAttach.add(attach);        
            }
            system.debug('attach list size ' + listAttach.size());
            insert listAttach;
        }
    }
    */