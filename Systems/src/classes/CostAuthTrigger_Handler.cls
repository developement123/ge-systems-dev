/* *********************************************************************************************
    Developer : Mohan Kumar
    Date: 30/Jan/2015
    
    Action : This is handler class of trigger 'Trg_Update_EPA_owner_To_Response_Clone' and will 
                1. Update CA 'status' to 'closed' if 'Close Cost Authorization' checkbox is checked.
                2. Update EPA 'owner' to response as 'Current EPA owner' if CA 'status' is other than closed.
                3. If CA 'status' changes to 'Closed', then it will send an email to 'Sales Owner', 'Program Manager', 
                   'Response Manager' 'Response Owner', 'Engineering Manager', 'Engineering Cost Estimator',
                   'Additional Member', 'Contract Manager', 'Current EPA Owner' field related to Response.
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        12/03/2016      Created a new method "validateBPunderPipeline" to restrict user to create new b&p under pipeline if they already have a response  - US43365 
    Abhishek        12/03/2016      implemented this method "validateBPTaskPerEPA" - US43365 
    Abhishek        12/20/2016      have called this method "assignPipelineOppOnBPANDvalidateBusinessDevelopment" in method CostAuthHandler_method
    Abhishek        09/13/2017      replaced account platform lookup on opportunity with platform master lookup in following method validateBPTaskPerEPA, assignPipelineOppOnBPANDvalidateBusinessDevelopment
****************************************************************************************** */

public with sharing class CostAuthTrigger_Handler {
    
    public void CostAuthHandler_method(List<Cost_Authorization__c> newList, Map<id, Cost_Authorization__c> oldMap, Map<id, Cost_Authorization__c> newMap, String triggerType) {
       
        Set<id> setOfEpaId = new Set<id>();
        Set<id> setOfResponse = new Set<id>();            
        Set<id> setOfFilteredIds = new Set<id>();
        string responseName;
        string responseId;
        List<Cost_Authorization__c> listOfFilteredCA = new List<Cost_Authorization__c>();
        List<Sales_Request__c> listOfSRToBeUpdated = new List<Sales_Request__c>();      
        List<Messaging.Singleemailmessage> emails = new List<Messaging.Singleemailmessage>();
        
        Map<id, String> epaOwnerMap = new Map<id, String>();               
        List<Cost_Authorization__c> listOfBPInsert = new List<Cost_Authorization__c>();
        set<id> setOfPipeline = new set<id>();
        map<id, boolean> mapOfEPA = new map<id, boolean>();
           // following 2 for loops will run for both 'before insert' and 'before update' trigger.
           // If the 'Close Cost Authorization' checkbox is true then trigger will change the 'status' to 'closed'.
                       
        
        for(Cost_Authorization__c ca : newList) {
            if (ca.Close_Cost_Authorization__c)
                ca.Status__c = 'Closed';
            
            setOfEpaId.add(ca.EPA_Number__c);  
        } 
        
        if(!setOfEpaId.isEmpty()){
            for(EPA__c epa : [select id, EPA__c.Owner.Name,Business_Development__c  
                                from EPA__c 
                                where id IN : setOfEpaId])
                                {
                epaOwnerMap.put(epa.id, epa.Owner.Name);
                mapOfEPA.put(epa.id, epa.Business_Development__c);
            }
        }
        
        if(triggerType == 'Insert') {
            for(Cost_Authorization__c ca: newList) {
                if(ca.Status__c != 'Closed' && epaOwnerMap.get(ca.EPA_Number__c) != null      && ca.Sales_Request__c!=null)
                    listOfSRToBeUpdated.add(new Sales_Request__c(id = ca.Sales_Request__c, Current_EPA_Owner__c = epaOwnerMap.get(ca.EPA_Number__c)));
                
                if(ca.Approved__c==true)
                    ca.Current_Approved_Amount__c = ca.Pending_Approval_Amount__c;
                
                listOfBPInsert.add(ca);
                if(ca.Phase__c!='Phase 1'){
                    setOfResponse.add(ca.Sales_Request__c);
                }
                if(ca.Phase__c=='Phase 1'){
                    setOfPipeline.add(ca.Pipeline_Opportunity__c);
                }
            }
            // to check whether business development checkbox on EPA matches with Business development response or pipeline
            if(!listOfBPInsert.isEmpty()){
                assignPipelineOppOnBPANDvalidateBusinessDevelopment(listOfBPInsert, mapOfEPA, setOfResponse, setOfPipeline);
            }
        }

        if(triggerType == 'Update') {
            for(Cost_Authorization__c ca: newList) {             
            
                if(oldMap.get(ca.id).Status__c != 'Closed' 
                   && newMap.get(ca.id).Status__c == 'Closed'
                   && ca.Sales_Request__c!=null) {
                        setOfFilteredIds.add(ca.Sales_Request__c);
                        listOfFilteredCA.add(ca); 
                }                    
                
                if(ca.Status__c != 'Closed' && epaOwnerMap.get(ca.EPA_Number__c) != null && ca.Sales_Request__c!=null){
                    listOfSRToBeUpdated.add(new Sales_Request__c(id = ca.Sales_Request__c, Current_EPA_Owner__c = epaOwnerMap.get(ca.EPA_Number__c)));
                }    
                    if(ca.Approved__c==true)
                        ca.Current_Approved_Amount__c = ca.Pending_Approval_Amount__c;
            }
        }
        
        if( !listOfSRToBeUpdated.isEmpty()) {
            try {
                update listOfSRToBeUpdated;             
            }
            catch(Exception ex) {
                System.debug(' Following exception occured while updating Reponses in class CostAuthTrigger_Handler ' + ex.getMessage());
            }       
        }
        
        List<Sales_Request__c> listOfFilteredResp = new List<Sales_Request__c>();  
         
        if(!setOfFilteredIds.isEmpty()) {
            listOfFilteredResp = [select id, Name, Response_ID2__c, 
                                  Sales_Owner__c, Sales_Owner__r.email, 
                                  Program_Manager__c, Program_Manager__r.email,
                                  Proposal_Manager__c, Proposal_Manager__r.email,
                                  Proposal_Coordinator__c, Proposal_Coordinator__r.email, 
                                  Engineering_Lead__c, Engineering_Lead__r.email, 
                                  Cost_Estimator__c, Cost_Estimator__r.email,
                                  Additional_Member__c, Additional_Member__r.email, 
                                  Contract_Manager__c, Contract_Manager__r.email,
                                  Finance_Manager_Analyst__c, Finance_Manager_Analyst__r.email,
                                  Manufacturing_Cost_Estimator__c, Manufacturing_Cost_Estimator__r.email,
                                  Current_EPA_Owner__c 
                                  from Sales_Request__c
                                  where id IN : setOfFilteredIds];
        
            Set<String> lookup_user = new Set<String>();            

            Map<Id, Set<String>> mapOfEmailAddress = new Map<Id, Set<String>>();
            Map<Id, Sales_Request__c> mapOfResp = new Map<Id, Sales_Request__c>(); 
            
            for(Sales_Request__c sr : listOfFilteredResp) {
                
                mapOfResp.put(sr.id, sr);
                Set<String> setOfRespEmail = new Set<String>();
                
                if(sr.Sales_Owner__c != null)                   // lookup to user
                    setOfRespEmail.add(sr.Sales_Owner__r.email);
                if(sr.Program_Manager__c != null)               // lookup to user
                    setOfRespEmail.add(sr.Program_Manager__r.email);                
                if(sr.Proposal_Manager__c != null)              // lookup to user
                    setOfRespEmail.add(sr.Proposal_Manager__r.email);
                if(sr.Proposal_Coordinator__c != null)          // lookup to contact    
                    setOfRespEmail.add(sr.Proposal_Coordinator__r.email);      
                if(sr.Engineering_Lead__c != null)              // lookup to contact
                    setOfRespEmail.add(sr.Engineering_Lead__r.email);
                if(sr.Cost_Estimator__c != null)                // lookup to contact
                    setOfRespEmail.add(sr.Cost_Estimator__r.email);
                if(sr.Additional_Member__c != null)             // lookup to contact 
                    setOfRespEmail.add(sr.Additional_Member__r.email);  
                if(sr.Finance_Manager_Analyst__c != null)       // lookup to contact
                    setOfRespEmail.add(sr.Finance_Manager_Analyst__r.email);  
                if(sr.Manufacturing_Cost_Estimator__c != null)  // lookup to contact 
                    setOfRespEmail.add(sr.Manufacturing_Cost_Estimator__r.email);               
                if(sr.Contract_Manager__c != null)              // lookup to user
                    setOfRespEmail.add(sr.Contract_Manager__r.email);                
                mapOfEmailAddress.put(sr.id, setOfRespEmail);
                
                if(sr.Current_EPA_Owner__c != null)             // this is text field, so need to query for getting email.
                    lookup_user.add(sr.Current_EPA_Owner__c); 
            }
            
            Map<String, String> mapOfUserNameEmail = new Map<String, String>();
            
            for(User usr : [select Name, Email from User where Name IN : lookup_user]) {
                mapOfUserNameEmail.put(usr.Name, usr.Email);
            }
                
            for(Cost_Authorization__c caId : listOfFilteredCA) {
                Messaging.Singleemailmessage emailInstance = new Messaging.Singleemailmessage();               
                
                Set<String> tempSetOfEmails = new Set<String>();
                
                tempSetOfEmails = mapOfEmailAddress.get(caId.Sales_Request__c);                  
                if(!mapOfResp.isEMpty()){
                    tempSetOfEmails.add(mapOfUserNameEmail.get(mapOfResp.get(caId.Sales_Request__c).Current_EPA_Owner__c.trim()));
                     responseName = mapOfResp.get(caId.Sales_Request__c).Name;
                     responseId = mapOfResp.get(caId.Sales_Request__c).Response_ID2__c ;
                }
                List<String> finalEmailList = new List<String>();
                if(tempSetOfEmails !=null && !tempSetOfEmails.isEmpty()){
                    finalEmailList.addAll(tempSetOfEmails);
                    emailInstance.setToAddresses(finalEmailList);
            
                    emailInstance.setPlainTextBody('All, \n  \nThe Cost Authorization # ' + caId.Concat_EPA_Number__c + ' associated with Response \n' + responseName + ' and Response # ' + responseId + ' is now closed. \nSince you are listed as Team Lead on the said Response, you are in receipt of this email. \n \nRegards, \nSystems Salesforce.com  \n\nNote: This is a systems generated email and please do not respond to this email.');
                    emailInstance.setSubject('Cost Authorization Closed Notification');
                    emails.add(emailInstance);
                }
            }
            
            if(Class_TriggerCheck.Var_IsEmailsend == 1) { 
                Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;
              //  List<Messaging.Sendemailresult> result = Messaging.sendEmail(emails);
                
               // for (Messaging.Sendemailresult res: result) {
               //     if(res.isSuccess()) 
               //         System.debug('The email was sent successfully.');
               //     else 
               //         System.debug('The email failed to send: ' + result[0].getErrors()[0].getMessage());                
              //  } 
                Class_TriggerCheck.Var_IsEmailsend++;
            }   
        }                
    }
    
    // this method restrict user to create new b&p under pipeline if they already have a response
    
    public void validateBPunderPipeline(List<Cost_Authorization__c> newList) {
        if(Label.Label_validateB_PunderPipeline == 'true' ) {
            map<id,id> mapOfBPTOPipelineOpp = new map<id,id>();
            map<id,id> mapOfResponseTOPipelineOpp = new map<id,id>();
            for(Cost_Authorization__c  newCA : newList){
                if(newCA.Pipeline_Opportunity__c!=null && newCA.Phase__c=='Phase 1')
                    mapOfBPTOPipelineOpp.put(newCA.id, newCA.Pipeline_Opportunity__c);
            }
            if(!mapOfBPTOPipelineOpp.isEmpty()){
                for(sales_request__C sr : [select id,Pipeline_Opportunity__c from Sales_Request__c where Pipeline_Opportunity__c=:mapOfBPTOPipelineOpp.values()]){
                    if(sr.Pipeline_Opportunity__c!=null)
                        mapOfResponseTOPipelineOpp.put(sr.Pipeline_Opportunity__c,sr.id);
                }
            }
            if(!mapOfResponseTOPipelineOpp.isEMpty()){
                for(Cost_Authorization__c  newCA : newList){
                    if(mapOfResponseTOPipelineOpp.get(newCA.Pipeline_Opportunity__c)!=null)
                        newCA.addError(Label.validateB_PunderPipeline);
                }
            }
        }
    }
    
    // this method restricts one active (status<>closed) b&p task per EPA on pipeline opportunity or response 
    
    public void validateBPTaskPerEPA(List<Cost_Authorization__c> newList) {
        if(Label.Label_validateBPTaskPerEPA == 'true' ) {
            set<id> pipelineOppId = new set<id>();
            map<id,id> mapOfexistingPipelineOppToEPA = new map<id,id>();
            set<id> responseId = new set<id>();
            set<id> epaNumber = new set<id>();
            
            map<id,id> mapOfexistingResponseToEPA = new map<id,id>();
            for(Cost_Authorization__c  newCA : newList){
                if(newCA.Pipeline_Opportunity__c!=null && newCA.Phase__c=='Phase 1')
                    pipelineOppId.add(newCA.Pipeline_Opportunity__c);
                
                if(newCA.Sales_Request__c!=null && newCA.Phase__c!='Phase 1')
                    responseId.add(newCA.Sales_Request__c);
                    
                    epaNumber.add(newCA.EPA_Number__c);
            }
            // removed Account platform from below query and used Platform master(Platform_New__r) lookup of opportunity
            if(!pipelineOppId.isEmpty()){
                for(Cost_Authorization__c cAuthPipeline : [select id,Pipeline_Opportunity__c, EPA_Number__c, Pipeline_Opportunity__r.Platform_New__r.Business_Development__c from Cost_Authorization__c where Pipeline_Opportunity__c=:pipelineOppId and EPA_Number__c =: epaNumber and Status__c!='Closed' and Phase__c = 'Phase 1']){
                    if(!cAuthPipeline.Pipeline_Opportunity__r.Platform_New__r.Business_Development__c)
                    mapOfexistingPipelineOppToEPA.put(cAuthPipeline.Pipeline_Opportunity__c ,cAuthPipeline.EPA_Number__c);
                }
            }
            // removed Account platform from below query and used Platform master(Platform_New__r) lookup on opportunity
            if(!responseId.isEmpty()){
                for(Cost_Authorization__c cAuthResponse : [select id,Sales_Request__c,EPA_Number__c, Sales_Request__r.Pipeline_Opportunity__r.Platform_New__r.Business_Development__c from Cost_Authorization__c where Sales_Request__c=:responseId and EPA_Number__c =: epaNumber and Status__c!='Closed' and Phase__c in ('Phase 2','Phase 3')]){
                    if(!cAuthResponse.Sales_Request__r.Pipeline_Opportunity__r.Platform_New__r.Business_Development__c)
                    mapOfexistingResponseToEPA.put(cAuthResponse.Sales_Request__c, cAuthResponse.EPA_Number__c);
                }
            }
            for(Cost_Authorization__c  myNewCA : newList){
                if(myNewCA.Pipeline_Opportunity__c!=null){
                    if(mapOfexistingPipelineOppToEPA.get(myNewCA.Pipeline_Opportunity__c) == myNewCA.EPA_Number__c)
                        myNewCA.addError(Label.validateBPTaskPerEPA_Opp);
                }
                
                if(myNewCA.Sales_Request__c!=null){
                    
                    if(mapOfexistingResponseToEPA.get(myNewCA.Sales_Request__c) == myNewCA.EPA_Number__c)
                        myNewCA.addError(Label.validateBPTaskPerEPA_Response);
                }
                
            }
        }
    }
    
    public void calculatePipelineSpendTodate(List<Cost_Authorization__c> newList, 
                                        List<Cost_Authorization__c> oldList,
                                        Map<Id, Cost_Authorization__c> newMap, 
                                        Map<Id, Cost_Authorization__c> oldMap, 
                                        String eventType, 
                                        String triggerType) {
        if(Label.Label_calculatePipelineSpendTodate=='true'){
            Set<Id> setOfPipelineOppId = new Set<Id>();
            Set<Id> setOfResponseId = new Set<Id>();
            Set<Id> setOfResponsePipeline = new Set<Id>();
            map<id,id> mapofResponseToPipeline = new map<id,id>();
            // get all pipeline & response of inserted B&P
            if(eventType == 'After' && triggerType == 'Insert') {
                    for(Cost_Authorization__c costauthOpp : newList) {
                        if(costauthOpp.Pipeline_Opportunity__c != null && costauthOpp.Phase__c == 'Phase 1' && costauthOpp.FY_Spent_to_Date__c>0)
                            setOfPipelineOppId.add(costauthOpp.Pipeline_Opportunity__c);                        
                        if(costauthOpp.Sales_Request__c != null && costauthOpp.Phase__c != 'Phase 1' && costauthOpp.FY_Spent_to_Date__c>0)                
                            setOfResponseId.add(costauthOpp.Sales_Request__c);
                    }
            }
            if(eventType == 'After' && triggerType == 'Update') {
                    for(Cost_Authorization__c costauthUpdate : newList) {
                        system.debug('old value ++ ' + oldMap.get(costauthUpdate.Id).FY_Spent_to_Date__c );
                        system.debug('new value ++ ' + newMap.get(costauthUpdate.Id).FY_Spent_to_Date__c );
                        if(oldMap.get(costauthUpdate.Id).FY_Spent_to_Date__c != newMap.get(costauthUpdate.Id).FY_Spent_to_Date__c) {
                            if(costauthUpdate.Pipeline_Opportunity__c != null && costauthUpdate.Phase__c == 'Phase 1' )
                                setOfPipelineOppId.add(costauthUpdate.Pipeline_Opportunity__c);                        
                            if(costauthUpdate.Sales_Request__c != null && costauthUpdate.Phase__c != 'Phase 1')                
                                setOfResponseId.add(costauthUpdate.Sales_Request__c);
                        }   
                    }
            }
            system.debug('pipeline ++ ' + setOfPipelineOppId );
            if(eventType == 'After' && triggerType == 'Delete') {
                    for(Cost_Authorization__c costAuthDelete : oldList) {
                        if(costAuthDelete.Pipeline_Opportunity__c != null && costAuthDelete.Phase__c == 'Phase 1' && costAuthDelete.FY_Spent_to_Date__c>0)
                            setOfPipelineOppId.add(costAuthDelete.Pipeline_Opportunity__c);                        
                        if(costAuthDelete.Sales_Request__c != null && costAuthDelete.Phase__c != 'Phase 1' && costAuthDelete.FY_Spent_to_Date__c>0)                
                            setOfResponseId.add(costAuthDelete.Sales_Request__c);  
                    }
            }
            if(!setOfResponseId.isEmpty()){
                for(Sales_Request__c pipelineOnRes: [select Pipeline_Opportunity__c from Sales_Request__c where id=:setOfResponseId]){
                    setOfResponsePipeline.add(pipelineOnRes.Pipeline_Opportunity__c);
                    setOfPipelineOppId.add(pipelineOnRes.Pipeline_Opportunity__c);
                }
            }
            If(!setOfPipelineOppId.isEMpty()){
                for(id oppId : setOfPipelineOppId)
                    setOfResponsePipeline.add(oppId);
            }
                if(!setOfResponsePipeline.isEmpty()){
                    for(Sales_Request__c allRes: [select id,Pipeline_Opportunity__c from Sales_Request__c where Pipeline_Opportunity__c=:setOfResponsePipeline]){
                        setOfResponseId.add(allRes.id);
                        mapofResponseToPipeline.put(allRes.id, allRes.Pipeline_Opportunity__c);
                    }
                }
            
            system.debug('responses' + setOfResponseId);
            if(!setOfPipelineOppId.isEMpty() || !setOfResponseId.isEmpty()){
                map<id, double>pipelineSpendAmount = new map<id, double>();
                double spendamount = 0;
                for(Cost_Authorization__c finalCauth : [SELECT id,FY_Spent_to_Date__c,Sales_Request__c,
                Pipeline_Opportunity__c,Phase__c  from Cost_Authorization__c 
                where (Pipeline_Opportunity__c =: setOfPipelineOppId and Sales_Request__c=null) 
                or (Sales_Request__c =:setOfResponseId  and  Pipeline_Opportunity__c =null)
                ]){
                    system.debug('costauthId In Opp ++ ' + finalCauth.id );
                    if(finalCauth.Pipeline_Opportunity__c!=null && finalCauth.Phase__c == 'Phase 1'){
                        if(!pipelineSpendAmount.containsKey(finalCauth.Pipeline_Opportunity__c))
                            pipelineSpendAmount.put(finalCauth.Pipeline_Opportunity__c, finalCauth.FY_Spent_to_Date__c);
                        else{
                            spendamount = pipelineSpendAmount.get(finalCauth.Pipeline_Opportunity__c) + finalCauth.FY_Spent_to_Date__c;
                            pipelineSpendAmount.put(finalCauth.Pipeline_Opportunity__c, spendamount);
                        }
                    }
                    
                    if(finalCauth.Sales_Request__c!=null && finalCauth.Phase__c != 'Phase 1'){
                        system.debug('costauthId In Response ++ ' + finalCauth.id );
                        if(!pipelineSpendAmount.containsKey(mapofResponseToPipeline.get(finalCauth.Sales_Request__c)))
                            pipelineSpendAmount.put(mapofResponseToPipeline.get(finalCauth.Sales_Request__c), finalCauth.FY_Spent_to_Date__c);
                        else{
                            spendamount = pipelineSpendAmount.get(mapofResponseToPipeline.get(finalCauth.Sales_Request__c)) + finalCauth.FY_Spent_to_Date__c;
                            pipelineSpendAmount.put(mapofResponseToPipeline.get(finalCauth.Sales_Request__c), spendamount);
                        }
                    }
                }
                    list<Opportunity> listOfOppToUpdate = new list<Opportunity>();
                    if(!pipelineSpendAmount.isEMpty()){
                        for(Opportunity opp : [ select id, Pipeline_Spend_to_Date__c from Opportunity where id=:pipelineSpendAmount.keySet() ]){
                            opp.Pipeline_Spend_to_Date__c = pipelineSpendAmount.get(opp.id);
                            listOfOppToUpdate.add(opp);
                        }
                        if(!listOfOppToUpdate.isEmpty())
                            update listOfOppToUpdate;
                    }
            }
        }
            
    }
    
    // this method validate businessdevelopment checkbox should eqqual on EPA and Pipeline
    // assign pipeline opp on B&P(those created on response only)
    public void assignPipelineOppOnBPANDvalidateBusinessDevelopment(
                    List<Cost_Authorization__c> bpOnRespList,
                    map<id, boolean> epaMAP,
                    Set<id> responseId,Set<id> pipelineId){
        map<id,sales_request__C> mapofIdToResponse = new map<id,sales_request__C>(); 
         map<id,Opportunity> mapofIdToPipeline = new map<id,Opportunity>();        
        // removed Account platform from below query and used Platform master(Platform_New__r) lookup of opportunity
        if(!responseId.isEmpty()){
            for(sales_request__c sr : [select id,Pipeline_Opportunity__c, Pipeline_Opportunity__r.Platform_New__r.Business_Development__c from sales_request__c where id=:responseId]){
                mapofIdToResponse.put(sr.id, sr);
            }
        }
            
        if(!pipelineId.isEmpty()){
            for(Opportunity opp : [select id, Platform_New__r.Business_Development__c from Opportunity where id=:pipelineId]){
                mapofIdToPipeline.put(opp.id, opp);
            }
        }   
            for(Cost_Authorization__c cOauth : bpOnRespList){
                // B&p created on pipeline 
                if(cOauth.Pipeline_Opportunity__c!=null){               
                    if(epaMAP.get(cOauth.EPA_Number__c)!=mapofIdToPipeline.get(cOauth.Pipeline_Opportunity__c).Platform_New__r.Business_Development__c){
                        cOauth.addError(label.validateBusinessDevelopmentOnBPTask);
                    }
                }   
                
                // if B&p created on response
                if(cOauth.Sales_Request__c!=null){ 
                    if(epaMAP.get(cOauth.EPA_Number__c)!=mapofIdToResponse.get(cOauth.Sales_Request__c).Pipeline_Opportunity__r.Platform_New__r.Business_Development__c){
                        cOauth.addError(label.validateBusinessDevelopmentOnBPTask);
                    }
                    //cOauth.Pipeline_Opportunity__c = mapofIdToResponse.get(cOauth.Sales_Request__c).Pipeline_Opportunity__c;
                }   
                
            }
        
            
    } 
}