public without sharing class OpportunityTrigger_AddShare {
 public void InsertOppTeamNonPipelineOpp(List<Opportunity> newList, 
                                            List<Opportunity> oldList,
                                            Map<Id, Opportunity> newMap, 
                                            Map<Id, Opportunity> oldMap, 
                                            String eventType, String triggerType) {
                Map<ID,Schema.RecordTypeInfo> MapOfRecordTypeToOpp = Opportunity.sObjectType.getDescribe().getRecordTypeInfosById();
                List<OpportunityTeamMember> OpTeam = new List<OpportunityTeamMember>();
                
                if(eventType == 'After' && (triggerType == 'Insert' || triggerType == 'Undelete')) {
                    for(Opportunity opp : newList) {
                            if( MapOfRecordTypeToOpp.get(opp.recordTypeID).getName().containsIgnoreCase(Label.Label_POForecastOpportunity) || MapOfRecordTypeToOpp.get(opp.recordTypeID).getName().containsIgnoreCase(Label.Label_InitialOpportunity) || MapOfRecordTypeToOpp.get(opp.recordTypeID).getName().containsIgnoreCase(Label.Label_RenewalOpportunity) || MapOfRecordTypeToOpp.get(opp.recordTypeID).getName().containsIgnoreCase(Label.Label_PipelineOpportunity)){
                                OpTeam.add(new OpportunityTeamMember (OpportunityId = opp.Id, UserId = opp.CreatedById, OpportunityAccessLevel = 'Edit'));
                            }   
                        }
                    if(!OpTeam.IsEmpty()){
                        insert OpTeam;
                    }
                }
 }
 }