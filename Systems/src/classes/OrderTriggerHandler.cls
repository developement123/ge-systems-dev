/****************************************************************************************************
    Author   : Mohan Kumar
    Date     : 25/November/2015
    Action   : This is the handler class for trigger 'TriggerOnContract'.
    Change History
    =============

***************************************************************************************************************/

public with sharing class OrderTriggerHandler {

   
      //    Method 1 : This method is to autogenerate Order Numbers based on Contract Ids.
   
    
    public void autoGenerateOrderName(List<Order__c> newList) {
        
        Set<Id> setOfContractId = new Set<Id>();
        Map<Id, Contract_PO__c> mapOfContracts = new Map<Id, Contract_PO__c>();
        
        if(Label.Label_autoGenerateOrderName == 'true') {
            for(Order__c ord : newList) {
                setOfContractId.add(ord.Contract1__c);
            }
            
            if(!setOfContractId.isEmpty())  
            {         
                for(Contract_PO__c con : [select Id, name, Last_Order_Number__c
                                   from Contract_PO__c
                                   where Id IN : setOfContractId]) {
                    mapOfContracts.put(con.Id, con);
                }
            }
            string prefixZero;
            
            for(Order__c ord : newList) {
                if(mapOfContracts.get(ord.Contract1__c) != null && mapOfContracts.get(ord.Contract1__c).Last_Order_Number__c != null) {   
                    Integer numberToAdd = Integer.valueOf(mapOfContracts.get(ord.Contract1__c).Last_Order_Number__c) + 1;
                    if(numberToAdd<10){
                        prefixZero = '0'+String.valueof(numberToAdd);
                    }else{
                        prefixZero = String.valueof(numberToAdd);
                    }
                    ord.Name =  mapOfContracts.get(ord.Contract1__c).name + '-PO#' + prefixZero;
                    mapOfContracts.get(ord.Contract1__c).Last_Order_Number__c += 1;
                }
                else if(mapOfContracts.get(ord.Contract1__c) != null && mapOfContracts.get(ord.Contract1__c).Last_Order_Number__c == null) {   
                    Integer numberToAdd = 1;
                    mapOfContracts.get(ord.Contract1__c).Last_Order_Number__c = 0;
                    ord.Name =  mapOfContracts.get(ord.Contract1__c).name + '-PO#' + '0'+String.valueof(numberToAdd);
                    mapOfContracts.get(ord.Contract1__c).Last_Order_Number__c += 1;
                }
            }  
            
            if(!mapOfContracts.values().isEmpty()) {
                try {
                    update mapOfContracts.values();        
                }
                catch(Exception ex) {
                    System.debug(' Following exception occured while updating Contract in Class: OrderTriggerHandler ' + ex.getMessage());
                }    
            }  
        }
    }
    
    //  Method 2 : To prevent Order deletion, if order has PO Booked or ERP Forecast status
    public void OrderValidationOnDelete(List<Order__c> oldList){
         if(label.Label_OrderValidationOnDelete=='true'){
            for ( order__c od : oldList){
                 if(od.Status__c=='PO Booked' || od.Status__c=='ERP Forecast'){
                    od.addError(Label.OrderValidationOnDelete);
                 }
            }
         }
    }
}