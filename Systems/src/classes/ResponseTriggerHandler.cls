/************************************************************************************************
    Author   : Mohan Kumar
    Date     : 29/September/2015
    Action   : This is the handler class for trigger 'triggerOnResponse'.
    
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        02/18/2016      Created a new method "updateAccountsOnResponse" to update account on Response.Have call this method in method "autoGenerateResponseId"
    Abhishek        06/30/2016    Created a new method "IsResponseIdAlreadyExist" to check responseID uniqueness on Response.Have call this method in method "autoGenerateResponseId"
    Abhishek        07/01/2016     Created a new method "UpdateSalesOwner" to Update sales Owner.Have call this method in "autoGenerateResponseId" Jira 585
    Abhishek        07/05/2016     Created a new method "UpdateSalesOwnerOnUpdate" to cover update scneario.Have call this method in "Trigger" : Jira 585
    Abhishek        07/21/2016      Tokenization code commented as we are using process builder
    Abhishek        11/07/2016      Modified this method "UpdateOnHoldCalculation" as per US33819
    Abhishek        11/25/2016      Modified this method "UpdatePipelineOpportunityId" to update response fields created to support contract flow down in lightening
    ABhishek        12/26/2016      added a new method "updatePipelineOpportunityStage" to update pipleine stage to contract awarded on response change - US39238
    Abhishek        01/03/2016      added a new method "responseValidationOnUpdate" to restrict response stage change if open B&p exists.
    Abhishek        03/08/2017     "Cancelled" replaced with "7 - Abandoned", "Contract Awarded" replaced with "5 - Closed/Won", "No Award" replaced with "6 - Closed/Lost"
    Abhishek        04/17/2017      replaced platform&win probability with GE standard Probability
    Abhishek                        Created a HTML template Response Email HTML and used in code to replave VF template "Response Email" since formula field(Account Name) not working in VF email template
*************************************************************************************************/

public with sharing class ResponseTriggerHandler {

    
     //   Method 1 :  This method is to populate Response Id and will run in case of before insert.
     //               This method will only run when program is null. 

    
    public void autoGenerateResponseId(List<Sales_Request__c> newList) {
        if(Label.Label_autoGenerateResponseId == 'true') {
        
            Set<Id> setOfOppyId = new Set<Id>();
            Map<Id, Opportunity> mapOfOppys = new Map<Id, Opportunity>();
            string responseID;
            Boolean isExist = false;
            
            for(Sales_Request__c sr : newList) {
                if(sr.Program__c == null)
                    setOfOppyId.add(sr.Pipeline_Opportunity__c);
            }
            
            if(!setOfOppyId.isEmpty()) {         
                for(Opportunity opp : [select Id, Last_Response_Number__c, Opportunity_Id__c, Child_Account__c, AccountId, Offering_Type__c, OwnerId
                                   from Opportunity
                                   where Id IN : setOfOppyId])
                    mapOfOppys.put(opp.Id, opp);
            }
            if(!mapOfOppys.isEMpty())
                UpdateSalesOwner(newList, mapOfOppys) ; // called to update salesowner JIRA 585
            
            for(Sales_Request__c sr : newList) {
                updateAccountsOnResponse(sr, mapOfOppys); // called this method to update accounts on Response  
                if(sr.Program__c == null) {
                    if(mapOfOppys.get(sr.Pipeline_Opportunity__c) != null && mapOfOppys.get(sr.Pipeline_Opportunity__c).Last_Response_Number__c != null) {   
                        Integer numberToAdd = Integer.valueOf(mapOfOppys.get(sr.Pipeline_Opportunity__c).Last_Response_Number__c) + 1;
                        
                        responseID = 'R-' + mapOfOppys.get(sr.Pipeline_Opportunity__c).Opportunity_Id__c.subString(2, mapOfOppys.get(sr.Pipeline_Opportunity__c).Opportunity_Id__c.length()) + '-' + String.valueof(numberToAdd);
                        
                        isExist = IsResponseIdAlreadyExist(responseID);
                        
                        sr.auto__c = isExist ? responseID + '-' + String.valueof(numberToAdd) : responseID ;
                        
                        mapOfOppys.get(sr.Pipeline_Opportunity__c).Last_Response_Number__c += 1;
                    }
                    else if(mapOfOppys.get(sr.Pipeline_Opportunity__c) != null && mapOfOppys.get(sr.Pipeline_Opportunity__c).Last_Response_Number__c == null) {   
                        Integer numberToAdd = 1;
                        
                        mapOfOppys.get(sr.Pipeline_Opportunity__c).Last_Response_Number__c = 0;
                        
                        responseID = 'R-' + mapOfOppys.get(sr.Pipeline_Opportunity__c).Opportunity_Id__c.subString(2, mapOfOppys.get(sr.Pipeline_Opportunity__c).Opportunity_Id__c.length()) + '-' +String.valueof(numberToAdd);
                        
                        isExist = IsResponseIdAlreadyExist(responseID);
                        
                        sr.auto__c = isExist ? responseID + '-' + String.valueof(numberToAdd) : responseID ;
                        
                        mapOfOppys.get(sr.Pipeline_Opportunity__c).Last_Response_Number__c += 1;
                    }
                }
            }
            
           // System.debug(' mapOfOppys.values() ' + mapOfOppys.values());
            
            if(!mapOfOppys.values().isEmpty()) {
                try {
                    update mapOfOppys.values();        
                }
                catch(Exception ex) {
                    System.debug(' Following exception occured while updating Opportunity in Class: ResponseTriggerHandler ' + ex.getMessage());
                }    
            }
        }
    }
    
   
     //     Method 2 :  This method will
     //               1. Set up the unique Response Ids when Program is not null.                    
   
    
    public void updateResponseIdWhenProgramIsNotNull(List<Sales_Request__c> newList, List<Sales_Request__c> oldList, Map<Id, Sales_Request__c> oldMap, Map<Id, Sales_Request__c> newMap, String eventType, String triggerType) {
        
        if(Label.Label_updateResponseIdWhenProgramIsNotNull == 'true') {    
            Set<Id> setOfProgramId = new Set<Id>();
            Set<Id> setOfResponseId = new Set<Id>();                
            
            List<Program__c> lstPrgrmsToUpdate = new List<Program__c>();        
            List<Messaging.SingleEmailMessage> ListToSendEmails = new List<Messaging.SingleEmailMessage>();      
            
            Map<Id, Program__c> mapProgIdProgram = new Map<Id, Program__c>();
            Map<Id, List<Sales_Request__c>> mapPrgLatestResponse = new Map<Id, List<Sales_Request__c>>();
            
            if(triggerType == 'Insert' || triggerType == 'Update') {
                for(Sales_Request__c sr : newList) {
                    if(sr.Program__c != null) {
                        setOfResponseId.add(sr.Id);                      
                        setOfProgramId.add(sr.Program__c);   
                    }                   
                }
            }
            
            if(triggerType == 'Delete') {
                for(Sales_Request__c sr : oldList) { 
                    if(sr.Program__c != null) {
                        setOfResponseId.add(sr.Id);  
                        setOfProgramId.add(sr.Program__c);
                    }
                }
            }  
            
            List<Program__c> progList = new List<Program__c>();
            
            if(!setOfProgramId.isEmpty()) {                 
                for(Program__c prg : [Select Id, Name, Program_Id__c, Max_Response_Number__c,
                                        (Select Id, Auto__c 
                                        from Proposals__r
                                        order by CreatedDate desc Limit 1) 
                                        from program__c 
                                        where Id IN: setOfProgramId]) {
                    
                    progList.add(prg);
                    mapProgIdProgram.put(prg.Id, prg);
                }           
            }         
            
            if(!progList.isEmpty()) {
                for(Program__c prg : progList) {
                    if(!prg.Proposals__r.isEmpty()) {
                        for(Sales_Request__c sr : prg.Proposals__r) {                           
                            if (mapPrgLatestResponse.containsKey(prg.Id))
                                mapPrgLatestResponse.get(prg.Id).add(sr);
                            else
                                mapPrgLatestResponse.put(prg.Id, new List<Sales_Request__c>{sr});
                        }
                    }
                }            
            }    
            
    
            if(triggerType == 'Delete') {        
                for(Sales_Request__c sr : oldList) {
                    Program__c prog = new Program__c();
                    if(mapProgIdProgram.size() > 0 && mapProgIdProgram.get(sr.Program__c) != null)                     
                        prog = mapProgIdProgram.get(sr.Program__c);                
                                           
                    Integer LastMaxNumber;
                    if(mapPrgLatestResponse.size() > 0 
                        && mapPrgLatestResponse.get(sr.Program__c) != null) {
                        
                        String LastMaxResponseId = mapPrgLatestResponse.get(sr.Program__c)[0].Auto__c;                      
                        
                        List<String> tempStr = LastMaxResponseId.split('-');
                        LastMaxNumber = Integer.valueOf(tempStr[tempStr.size()-1]);  
                    }                    
                    if(prog != null) {
                        prog.Max_Response_Number__c = LastMaxNumber;
                        lstPrgrmsToUpdate.add(prog);
                    }                       
                }
            }
            
            if(triggerType == 'Insert' || triggerType == 'Update') {        
            
                for(Sales_Request__c sr : newList) {
                    if(sr.Program__c != null) {
                        
                        if(eventType == 'Before') {
                            Program__c prog = new Program__c();
                            if(mapProgIdProgram.size() > 0 
                                && mapProgIdProgram.get(sr.Program__c) != null) {
                                
                                prog = mapProgIdProgram.get(sr.Program__c);
                            }                            
                            
                            if(triggerType == 'Insert') {
                                String programId = prog.Program_Id__c;
                                String programId2 = programId.substringAfter('-');                          
                                Integer LastMaxNo_Res;
                                if(prog.Max_Response_Number__c == null) {
                                    Integer LastMaxNumber;
                                    if(mapPrgLatestResponse.size() > 0 
                                        && mapPrgLatestResponse.get(sr.Program__c) != null) {
                                        
                                        String LastMaxResponseId = mapPrgLatestResponse.get(sr.Program__c)[0].Auto__c;
                                        
                                        List<String> tempStr = LastMaxResponseId.split('-');
                                        LastMaxNumber = integer.valueOf(tempStr[tempStr.size()-1]);                                     
                                                                    
                                        LastMaxNo_Res = LastMaxNumber;
                                    }
                                    else 
                                        LastMaxNo_Res = 0;                            
                                }
                                else 
                                    LastMaxNo_Res = integer.valueOf(prog.Max_Response_Number__c);
                                                               
                                sr.Auto__c = 'R-' + programId2 + '-' + String.valueOf(LastMaxNo_Res + 1);                                 
                               
                                prog.Max_Response_Number__c = LastMaxNo_Res + 1;
                                lstPrgrmsToUpdate.add(prog);
                            }
                        }
                    }
                }
            }            
            
            if(!lstPrgrmsToUpdate.isEmpty()) {           
                try {
                    Class_TriggerCheck.Var_isProgam_Probability_change = true;                
                    update lstPrgrmsToUpdate;
                }
                catch(Exception ex) {
                      System.debug(' Following exception occured while updating Programs in class ResponseTrigger_Handler ' + ex.getMessage());
                }
            }
        }
    }    
    
   
   //   Method 3 :  This method will              
   //               send email to below members of Response 
   //                 ['Engineering Cost Estimator','Engineering Manager','Response Manager',
   //                     'Response Owner','Pricing Manager','Manufacturing Cost Estimator',
   //                     'Additional Member','Contract Manager','Sales Owner',
   //                     'Program Manager'] 
   //                 and in below scenario
   //                 a) - At a time of insert of Response having Submitted Date not null
   //                 b) - At a time of update but only when -
   //                         * Submitted date has been changed
  //                          * Windchill Folder Structure has been changed.                            
   
    
    public void sendEmailToResponseManagers(List<Sales_Request__c> newList, List<Sales_Request__c> oldList, Map<Id, Sales_Request__c> oldMap, Map<Id, Sales_Request__c> newMap, String eventType, String triggerType) {
        if(Label.Label_sendEmailToResponseManagers == 'true') {    
            Set<Id> setOfResponseId = new Set<Id>();
            List<Messaging.SingleEmailMessage> ListToSendEmails = new List<Messaging.SingleEmailMessage>();    
            Map<Id, Set<String>> mapOfEmailAddress = new Map<Id, Set<String>>();
                
            if(triggerType == 'Insert' || triggerType == 'Update') {
                for(Sales_Request__c sr : newList) {          
                    setOfResponseId.add(sr.Id);     
                    
                    
                }           
            }      
            
            if(!setOfResponseId.isEmpty() && (triggerType == 'Insert' || triggerType == 'Update')) {
                for(Sales_Request__c resp : [Select Cost_Estimator__c, Cost_Estimator__r.email, 
                                                Engineering_Lead__c, Engineering_Lead__r.email,
                                                Proposal_Manager__c,  Proposal_Manager__r.email,
                                                Proposal_Coordinator__c, Proposal_Coordinator__r.email,
                                                Pricing_Manager__c, Pricing_Manager__r.email,
                                                Manufacturing_Cost_Estimator__c, Manufacturing_Cost_Estimator__r.email,
                                                Additional_Member__c, Additional_Member__r.email,
                                                Contract_Manager__c, Contract_Manager__r.email,
                                                Sales_Owner__c, Sales_Owner__r.email,
                                                Program_Manager__c, Program_Manager__r.email, 
                                                Finance_Manager_Analyst__c, Finance_Manager_Analyst__r.email
                                                from Sales_Request__c 
                                                where Id IN : setOfResponseId]) {
                                            
                    Set<String> setOfRespEmail = new Set<String>();
                    
                    if(resp.Cost_Estimator__c != null)
                        setOfRespEmail.add(resp.Cost_Estimator__r.email);
                    if(resp.Engineering_Lead__c != null)
                        setOfRespEmail.add(resp.Engineering_Lead__r.email);
                    if(resp.Proposal_Manager__c != null)
                        setOfRespEmail.add(resp.Proposal_Manager__r.email);
                    if(resp.Proposal_Coordinator__c != null)
                        setOfRespEmail.add(resp.Proposal_Coordinator__r.email);
                    if(resp.Pricing_Manager__c != null)
                        setOfRespEmail.add(resp.Pricing_Manager__r.email);
                    
                    if(resp.Manufacturing_Cost_Estimator__c != null)
                        setOfRespEmail.add(resp.Manufacturing_Cost_Estimator__r.email);
                    if(resp.Additional_Member__c != null)
                        setOfRespEmail.add(resp.Additional_Member__r.email);                    
                    if(resp.Contract_Manager__c != null)
                        setOfRespEmail.add(resp.Contract_Manager__r.email);                
                    if(resp.Sales_Owner__c != null)
                        setOfRespEmail.add(resp.Sales_Owner__r.email);                    
                    if(resp.Program_Manager__c != null)
                        setOfRespEmail.add(resp.Program_Manager__r.email);                    
                    if(resp.Finance_Manager_Analyst__c != null)
                        setOfRespEmail.add(resp.Finance_Manager_Analyst__r.email);
                        
                    mapOfEmailAddress.put(resp.Id, setOfRespEmail);
                }
            }
            
            if(triggerType == 'Insert' || triggerType == 'Update') {        
                OrgWideEmailAddress owd = [select Id, Address, DisplayName from OrgWideEmailAddress where DisplayName =: Label.OWE_for_SendMailToRespTeam];         
            
                for(Sales_Request__c sr : newList) {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    Messaging.SingleEmailMessage emailOnDateChange = new Messaging.SingleEmailMessage();
                    List<String> emailAddList = new List<String>();         
                    
                    if(eventType == 'After') {                    
                        emailAddList.addAll(mapOfEmailAddress.get(sr.Id));
                        
                        if(triggerType == 'Insert') {                                
                            if(sr.Response_Submitted__c != null) {
                                                            
                                emailOnDateChange.setTargetObjectId(sr.Proposal_Coordinator__c);
                                emailOnDateChange.setToAddresses(emailAddList);
                                EmailTemplate et2 = [select id from EmailTemplate where name = 'Response Email on Date Change'];                            
                                emailOnDateChange.setSaveAsActivity(false);
                                emailOnDateChange.setTemplateId(et2.id);
                                emailOnDateChange.setWhatId(sr.Id);
                                emailOnDateChange.setorgWideEmailAddressId(owd.Id);
                                ListToSendEmails.add(emailOnDateChange);
                            }
                        }
                        if(triggerType == 'Update') {  
                           
                            if(oldmap.get(sr.Id).Windchill_Folder_Structure__c != newmap.get(sr.Id).Windchill_Folder_Structure__c) {                                                           
                               
                                email.setTargetObjectId(sr.Proposal_Coordinator__c);                                
                                email.setToAddresses(emailAddList);
                                EmailTemplate et = [select id from EmailTemplate where DeveloperName = 'Response_Email_HTML'];                            
                                email.setSaveAsActivity(false);
                                email.setTemplateId(et.id);
                                email.setWhatId(sr.Id);
                                email.setorgWideEmailAddressId(owd.Id);                                    
                                ListToSendEmails.add(email);                               
                            }                    
                            
                            if((oldmap.get(sr.Id).Response_Submitted__c != newmap.get(sr.Id).Response_Submitted__c)
                                 && (oldmap.get(sr.Id).Response_Submitted__c == null)) {
                                
                                emailOnDateChange.setTargetObjectId(sr.Proposal_Coordinator__c);
                                emailOnDateChange.setToAddresses(emailAddList);
                                EmailTemplate et2 = [select id from EmailTemplate where name = 'Response Email on Date Change'];
                                emailOnDateChange.setSaveAsActivity(false);
                                emailOnDateChange.setTemplateId(et2.id);
                                emailOnDateChange.setWhatId(sr.Id);
                                emailOnDateChange.setorgWideEmailAddressId(owd.Id);
                                ListToSendEmails.add(emailOnDateChange);
                            }
                        }
                    }           
                }
            }  
            
            if(eventType == 'After') {               
                if(Class_TriggerCheck.Var_IsEmailSendResponse == 1) {
                    try {                        
                        List<Messaging.SendEmailResult> result = Messaging.sendEmail(ListToSendEmails);
                        
                        for(Messaging.SendEmailResult res: result) {
                            if(res.isSuccess()) 
                                System.debug(' The email was sent successfully ');
                            else 
                                System.debug(' The email failed to send ' + result[0].getErrors()[0].getMessage());
                        }
                    }
                    catch(System.EmailException ex) {
                        System.debug(' ex.getmessage ' + ex);
                    }
                    Class_TriggerCheck.Var_IsEmailSendResponse++;
                }  
            }
        } 
    } 
    
    //  Method 4 :  This method will call in before insert of response record and it 
    //              updates the field "Duplicate Pipeline Id" with pipeline opportunity 18 character id.                            
   
    
    public void UpdatePipelineOpportunityId(List<Sales_Request__c> newList, String eventType, String triggerType) {      
    
        if(Label.Label_UpdatePipelineOpportunityId == 'true') {
            Set<Id> setOppId = new Set<Id>();
            Map<id, Opportunity> mapOpp = new Map<id, Opportunity>();
            Map<id, boolean> mapOppIdToIsDuplicate = new Map<id, boolean>();
            
            for(Sales_Request__c sr : newList) {
                if((sr.SR_Status__c == 'Pre Proposal' || sr.SR_Status__c == 'Opened' || sr.SR_Status__c == 'On Hold' || sr.SR_Status__c == 'Post Proposal') && sr.Pipeline_Opportunity__c != null)
                    mapOppIdToIsDuplicate.put(sr.Pipeline_Opportunity__c, true);
                    
                    setOppId.add(sr.Pipeline_Opportunity__c);
            }
            
            if(!setOppId.isEmpty()) {
                for(Opportunity opp : [select id, Opportunity_ID_18_Chars__c, closedate, Last_Revenue_Schedule_Date__c, Price_Escalation_COLA__c, Price_Escalation_Start_Date__c, First_Ship_Date_min2__c from opportunity where id in : setOppId])            
                    mapOpp.put(opp.id,opp);
            }
            
            for(Sales_Request__c sr : newList) {
                if(mapOppIdToIsDuplicate.containsKey(sr.Pipeline_Opportunity__c)){
                    sr.Pipeline_Id_Duplicate__c = mapOpp.get(sr.Pipeline_Opportunity__c).Opportunity_ID_18_Chars__c;
                }    
                  //  sr.Contract_Start_date__c = mapOpp.get(sr.Pipeline_Opportunity__c).closedate;
                  //  sr.Contract_End_Date__c = mapOpp.get(sr.Pipeline_Opportunity__c).Last_Revenue_Schedule_Date__c;
                  //  sr.Price_Escalation_COLA__c = mapOpp.get(sr.Pipeline_Opportunity__c).Price_Escalation_COLA__c;
                  //  sr.Price_Escalation_date__c = mapOpp.get(sr.Pipeline_Opportunity__c).Price_Escalation_Start_Date__c;
                  //  sr.First_Ship_Date__c = mapOpp.get(sr.Pipeline_Opportunity__c).First_Ship_Date_min2__c;
            }
        } 
    }  
    
   
     //     Method 5 :        
     //         following method 'tokenizeRespWhenOppyIsTokenized' tokenize the 'Response' if its related 
     //         Opportunity is tokenized on insertion.
  
    
    public void tokenizeRespWhenOppyIsTokenized(List<Sales_Request__c> newList) {
        if(Label.Label_tokenizeRespWhenOppyIsTokenized == 'true') {
            Set<Id> setOfOppyIds = new Set<Id>(); 
            Map<Id, Opportunity> mapOfOppys = new Map<Id, Opportunity>();
            
            for(Sales_Request__c resp: newList) 
                setOfOppyIds.add(resp.Pipeline_Opportunity__c);
            
            for(Opportunity opp: [select Id, Is_Tokenized__c 
                                    from Opportunity
                                    where Id IN: setOfOppyIds])
                mapOfOppys.put(opp.Id, opp);       
           if(!mapOfOppys.isEmpty())
           {
            for(Sales_Request__c resp : newList) {
                if(mapOfOppys.get(resp.Pipeline_Opportunity__c).Is_Tokenized__c){     
                    //resp.Is_Tokenized__c = true;
                }
            }
           } 
        }
    }
    
    // Method 6 : This method will update parent and child account on Response 
    public void updateAccountsOnResponse(Sales_Request__c sr, Map<Id, Opportunity> myOpp)   {
                if(myOpp.get(sr.Pipeline_Opportunity__c) != null && myOpp.get(sr.Pipeline_Opportunity__c).accountId != null){
                    sr.Account_Name__c = myOpp.get(sr.Pipeline_Opportunity__c).accountId;
                    sr.Child_Account__c = myOpp.get(sr.Pipeline_Opportunity__c).Child_Account__c;
                }
            
    
    }
    
     // Method 7 : This method is being called inside method "autoGenerateResponseId". This method just check
     //            whether build responseID already exists in system or not?     
    public boolean IsResponseIdAlreadyExist(string respID)   {
             Boolean isPresent = false;
             if( [select id from Sales_Request__c where Auto__c = :respID].size() > 0 ) {
                isPresent = true;
             }
             return isPresent;
    
    }
    
    // Method 8 JIRA 585 : This method will assign response's salesowner to pipeline Opp's owner if
    // 1. Opp's offering type = Digital Solutions OR 
    // 2. For OE/Services - if user didn't select Sales Owner then Sales Owner=Pipeline Opportunity Owner   
    public void UpdateSalesOwner(List<Sales_Request__c> listOfResponse, Map<Id, Opportunity> mapofIdToPipelineOpp)   {
             for(Sales_Request__c sReq : listOfResponse){
                if( ( mapofIdToPipelineOpp.get(sReq.Pipeline_Opportunity__c).Offering_Type__c == 'Digital Solutions') || ( mapofIdToPipelineOpp.get(sReq.Pipeline_Opportunity__c).Offering_Type__c == 'OE / Services' && sReq.Sales_Owner__c == null ) ){
                    sReq.Sales_Owner__c = mapofIdToPipelineOpp.get(sReq.Pipeline_Opportunity__c).OwnerId;
                }
             }
    
    }
     // Method 9 JIRA 585 : To cover update condition, THis method has been created
    // 1. For both offering type, if user didn't select Sales Owner then Sales Owner=Pipeline Opportunity Owner 
    // 2. For both offering type, Sales Owner and pipeline opportunity owner can be different in case of UPDATE 
     public void UpdateSalesOwnerOnUpdate(List<Sales_Request__c> newList, List<Sales_Request__c> oldList, Map<Id, Sales_Request__c> oldMap, Map<Id, Sales_Request__c> newMap, String eventType, String triggerType) {
        if(Label.Label_UpdateSalesOwnerOnUpdate == 'true' && eventType == 'Before' && triggerType == 'Update') {
        
            Set<Id> setOfMyOpp = new Set<Id>();
            Map<Id, Opportunity> mapOfRelOppys = new Map<Id, Opportunity>();
            
            for(Sales_Request__c sr : newList) {
                if(sr.Program__c == null && ( newMap.get(sr.Id).Sales_Owner__c==null) )
                    setOfMyOpp.add(sr.Pipeline_Opportunity__c);
            }
            
            if(!setOfMyOpp.isEmpty()) {         
                for(Opportunity opp : [select Id,     Offering_Type__c, OwnerId                                   from Opportunity
                                   where Id IN : setOfMyOpp])
                    mapOfRelOppys.put(opp.Id, opp);
            }
            if(!mapOfRelOppys.isEmpty())
                UpdateSalesOwner(newList, mapOfRelOppys) ; // called to update salesowner JIRA 585
        }
     }  
     
     public void UpdateOnHoldCalculation(List<Sales_Request__c> newList, List<Sales_Request__c> oldList, Map<Id, Sales_Request__c> oldMap, Map<Id, Sales_Request__c> newMap, String eventType, String triggerType) {
        if(Label.Label_UpdateOnHoldCalculation == 'true'){
             List<Sales_Request__c> listOfOnHoldSales = new List<Sales_Request__c>();
             List<Sales_Request__c> listOfOutHoldSales = new List<Sales_Request__c>();
             map<id, string> mapTest = new map<id, string>();
             set<id>InHoldId = new set<id>();
             set<id>OutHoldId = new set<id>();
             map<id, integer> responseWithOutStatus =  new map<id, integer>();
             
             if(eventType=='Before' && triggerType=='Insert' ){
                for(Sales_Request__c SROnHold : newList){
                    if(label.StatusToRemoveFromTAT.contains(SROnHold.SR_Status__c)){
                        SROnHold.On_Hold_Date_Time__c = System.now();
                        SROnHold.On_Hold_Minutes__c = 0;
                    }
                }
             } 
            
            if(eventType=='Before' && triggerType=='Update'){
                 double DiffinMinutes = 0;
                 double DiifInMilisec = 0;
                 double onHoldMinutes = 0;
                    for(Sales_Request__c SROnHold : newList){
                       if(( label.StatusToRemoveFromTAT.contains(oldMap.get(SROnHold.id).SR_Status__c) && !label.StatusToRemoveFromTAT.contains(newMap.get(SROnHold.id).SR_Status__c) )){      
                             if(SROnHold.On_Hold_Minutes__c!=null)
                                onHoldMinutes = SROnHold.On_Hold_Minutes__c;
                             if(SROnHold.On_Hold_Date_Time__c !=null)
                                DiifInMilisec = System.now().getTime() - SROnHold.On_Hold_Date_Time__c.getTime();
                             DiffinMinutes = (DiifInMilisec/1000.00)/60.00;
                             if(Class_TriggerCheck.var_StopRecursiveOnHoldCalculation){ // because of workflow "Update Response Stage", this method executes twice.To stop this, we used a static variable 
                                Class_TriggerCheck.var_StopRecursiveOnHoldCalculation = false; 
                                SROnHold.On_Hold_Minutes__c = onHoldMinutes + DiffinMinutes;
                             }
                        }
                    DiffinMinutes = 0; 
                    DiifInMilisec = 0;
                      if( ( !label.StatusToRemoveFromTAT.contains(oldMap.get(SROnHold.id).SR_Status__c) && label.StatusToRemoveFromTAT.contains(newMap.get(SROnHold.id).SR_Status__c) )){
                        SROnHold.On_Hold_Date_Time__c = System.now();
                      }
                }
            }
             
        } 
      
    }
    //To Update pipeline Opp stage on response status change to 5 - Closed/Won - US39238
    public void updatePipelineOpportunityStage(
                                                list<Sales_Request__c> newList, 
                                                List<Sales_Request__c> oldList, 
                                                Map<Id, Sales_Request__c> oldMap, 
                                                Map<Id, Sales_Request__c> newMap, 
                                                String eventType, String triggerType) {      
    
        if(Label.Label_updatePipelineOpportunityStage == 'true') {
            Set<Id> setCAOppId = new Set<Id>();
            List<Opportunity> listOfOppToUpdate = new List<Opportunity>();
            integer startIndex = 0;
            for(Sales_Request__c sr : newList) {
                if(sr.Pipeline_Opportunity__c != null 
                   && sr.SR_Status__c == 'Contract Awarded'
                   && sr.SR_Status__c != oldMap.get(sr.id).SR_Status__c)
                    setCAOppId.add(sr.Pipeline_Opportunity__c);
            }
            
            if(!setCAOppId.isEmpty()) {
                for(Opportunity opp : [select id, GE_Standard_Probability__c,Probability, StageName from opportunity where id in : setCAOppId  and StageName!=:label.OpportunityStage5]){ // 5= close won
                    opp.Probability = 100.0;
                    opp.GE_Standard_Probability__c = label.GEStandardProb3; // 3 = high
                    opp.StageName = label.OpportunityStage5;
                    listOfOppToUpdate.add(opp);
                }
            }
            
            if(!listOfOppToUpdate.isEmpty()){
                try{
                    update listOfOppToUpdate;
                }catch(exception myex){
                    if(myex.getMessage().lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')!=-1){
                        startIndex = myex.getMessage().lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION');
                        startIndex = startIndex + 35;
                            newList[0].addError(myex.getMessage().substring(startIndex));
                       // newList[0].addError(label.OppOrResponseTOVValidation);
                    }else{
                        newList[0].addError(myex.getMessage());
                    }
                }
            }
        } 
    } 
    
   
     //January,3rd 2016        
     //             Following method responseValidationOnUpdate is implemeneted to response status //changedto "5 - Closed/Won,6 - Closed/Lost,7 - Abandoned" if open B&P exist
  

    public void responseValidationOnUpdate(List<sales_request__c> newList, Map<Id, sales_request__c> oldMap ) {
    if(Label.Label_responseValidationOnUpdate == 'true') {

        set<id>responseIdForOpenBp = new set<id>();
        map<id, id> mapOfResponseToOpenBPId = new map<id, id>();
            for(sales_request__c resUp: newList) {    
                if( (label.StagesForOpenB_PNotValid_Res.containsIgnoreCase(resUp.SR_Status__c) && resUp.SR_Status__c != oldMap.get(resUp.id).SR_Status__c) && !(userinfo.getUserName().contains('integration'))){
                    responseIdForOpenBp.add(resUp.id);
                }
            }
                
            if(!responseIdForOpenBp.isEmpty()){
                for(Cost_Authorization__c co : [select id, Sales_Request__c from Cost_Authorization__c where Sales_Request__c=:responseIdForOpenBp and Status__c!='closed' and Phase__c!='Phase 1']){
                    mapOfResponseToOpenBPId.put(co.Sales_Request__c, co.id);
                }
            }
            
            for(sales_request__c resErr : newList){
                if(mapOfResponseToOpenBPId.containsKey(resErr.id)){
                    resErr.addError(label.OpenBPOnResponse_msg);
                }   
            }
        }
    } 
    
}