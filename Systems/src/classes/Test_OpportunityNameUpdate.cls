/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * modified on 02/29/2016 for JIRA 498
 Change desription:
    Developer         Date                  Description
    Abhishek          7/7/2016        Removed the reference of "Oppty_SR_Intersection__c" to delete the same object "Jira 564"
    Abhishek          09/18/2017      removed account platform
 */

@isTest(SeeAllData=true)
private class Test_OpportunityNameUpdate {

    static testMethod void myUnitTest() {
    

    StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');
        
        
        Test.setMock(HttpCalloutMock.class, mock);
     List<Opportunity> oppsList= new list<Opportunity>();
    List<Sales_Request__c> srList= new list<Sales_Request__c>();
    List<OpportunityLineItem> olList= new list<OpportunityLineItem>();
    list<User> UserList= new list<User>();
     id bA_Profile_id;
     id Sys_admin_Profile_id;

     Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
     insert plat;
     
     Account acc = new Account(Name='Test Account SSSSSSSSSSSSS');
     insert acc; 
     Account acc2 = new Account(Name='GE GEAS');
     insert acc2; 

     list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
   for(Profile p: pro)
   {
     if(p.name=='Systems Business Admin')
     {
        bA_Profile_id=p.Id;
     }
     else
     {
        Sys_admin_Profile_id=p.id;
     }
   }
     User u =new User( alias = 'skapo',email = 'sunny.kapoor@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'kapoor',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=bA_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'sunny.kapoor@birlasoft12.com.sys.dev');
               UserList.add(u);
     User u2 =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft123.com.sys.dev');
               UserList.add(u2);
               insert UserList;
               system.debug('#############################');
     
     Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
     insert hierarchy;
      
     Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name='Test hierarchy1',Product_Group__c='Test group1',Product_Area__c='Test area1',Product_Family__c='Test family1',Product_Child__c='Test child1');
     insert hierarchy1;

     Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
     Program_Probability__c='100% - Funded');
     insert  prog;
   
    // list_prog_toadd.add(prog);
     
     Program__c prog2=new Program__c(Lookup_hiearchyname__c=hierarchy1.id,Product_Sales_Owner__c=u.id,
     Program_Probability__c='100% - Funded');
     insert prog2;
 
    // added Account_Platform__c for JIRA 498
    Opportunity opp=new opportunity();
    opp.Name='Test oppSSSSSSSSSSSSSSS';
    opp.StageName=label.OpportunityStage1;
    opp.CloseDate=date.today();
    opp.Program__c=prog.id;
    opp.Amount=1000;
    opp.Firm_Anticipated__c='Anticipated';
    opp.GE_Standard_Probability__c=label.GEStandardProb3;
    opp.deal_type__c='Services';
    opp.Lookup_hiearchyname__c = hierarchy.id;
    opp.Platform_New__c = plat.id;
    opp.accountId= acc.id;
    oppsList.add(opp);
    
    // added Account_Platform__c for JIRA 498
    Opportunity opp1=new opportunity();
    opp1.Name='Test opp';
    opp1.StageName=label.OpportunityStage1;
    opp1.CloseDate=date.today();
    opp1.Program__c=prog2.id;
    opp1.Amount=1000;
    opp1.Firm_Anticipated__c='Anticipated';
    opp1.GE_Standard_Probability__c=label.GEStandardProb3;
    opp1.deal_type__c='Services';
    opp1.Lookup_hiearchyname__c = hierarchy1.id;
    opp1.Platform_New__c = plat.id;
    opp1.accountId= acc.id;
    oppsList.add(opp1);
     insert oppsList;
   
   
     Class_TriggerCheck.Var_StopProdVarMultipleExecution = true;
     
     system.debug('==========check==========='+Class_TriggerCheck.Var_StopProdVarMultipleExecution);
     Class_TriggerCheck.Var_IsProgramUPdated=false;
       
     
     opp.Program__c=prog2.id;
     try
     {
     update opp;    
     }catch(Exception e)
     {
        system.debug('Program changed in opp>>exception is>>>'+e);
     }
     opp.Name='2121121';
      try
     {
     update opp;    
     }catch(Exception e1)
     {
        system.debug('Program name changed in opp>>exception is>>>'+e1);
     }
     // Changing opp Hierarchy gives error " Cannot change product Hierarchy",
     // if Prog Hierarchy is not having Across Multiple Products . 
    /*deployment opp.Product_Group__c='Avionics';
     opp.Product_Area__c=Label.ProductHierarchyMultiple;
     opp.Product_Family__c=Label.ProductHierarchyMultiple;
     opp.Product_Family_Child__c=Label.ProductHierarchyMultiple;deployment*/
      try
     {
     update opp;    
     }catch(Exception e2)
     {
        system.debug('Product group has been changed in opp>>exception is>>>'+e2);
     }
     
     system.runAs(u)
     {
        system.debug('>>>>User>>inside u>>'+system.Userinfo.getUserName());
        system.debug('>>>>User profile id>>inside u>>'+system.Userinfo.getProfileId());
   //deployment      opp1.Product_Group__c='Avionics';
   //deployment      opp1.Product_Area__c='Generator Systems';
       //  opp1.Product_Family__c='Generator Systems';
        // opp1.Product_Family_Child__c='Autoflight';
         try
         {
         update opp1;   
         }catch(Exception e3)
         {
            system.debug('Product group has been changed in opp in case of CAS for Business Admin>>exception is>>>'+e3);
         } 
         
    
     // update prog;
            
     }
     
     
     Class_TriggerCheck.Var_IsProgramUPdated=false;
     
  system.runAs(u2)
     {
         System.debug('>>>>User>>>inside u2>>>'+system.Userinfo.getUserName());
      /*deployment   opp1.Product_Group__c='Avionics';
         opp1.Product_Area__c='Generator Systems';
         opp1.Product_Family__c='Generator Systems';
         opp1.Product_Family_Child__c='Autoflight';deployment*/
          try
         {
         update opp1;   
         }catch(Exception e3)
         {
            system.debug('Product group has been changed in opp in case of CAS for Sys Admin>>exception is>>>'+e3);
         }
     //deployment    opp1.Product_Group__c='Power';
          try
         {
         update opp1;   
         }catch(Exception e3)
         {
            system.debug('Product group has been changed in opp in case of CAS for Sys Admin>>exception is>>>'+e3);
         } 
            
     }
     
    }
}