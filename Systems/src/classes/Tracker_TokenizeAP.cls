/*****************************************************************************************************
This test class was created to provide code coverage for the following trigger:
- TokenizeAP
Version     Author(s)               Date              Details 
---------------------------------- ---------------------------------------------------------------------------------------------------------------
1.0         Mohan kumar          10/15/2014        Initial Creation 
            Abhishek             04/29/2015        Modified line no 89-100 to increase code coverage as per Jora 325. Code coverage got increased from 57%-75%.  
Change desription:
=================
    Developer         Date                  Description
    Abhishek          7/7/2016        Removed the reference of "Major_Proposal__c" to delete the same object "Jira 564"         
*****************************************************************************************************/                
@istest
public class Tracker_TokenizeAP {

    static testmethod void test_TokenizeAP1() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;  
        
        Class_TriggerCheck.var_triggerOnUser = false; // no need to fire User's trigger
        Class_TriggerCheck.var_TriggerOnAccount = false;  // no need to fire Account's trigger
        
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');

        Test.setMock(HttpCalloutMock.class, mock);        

        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator'
            limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
    
        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
            Forecast_Platform__c = pm.id, Is_Tokenized__c = false,OwnerId = u.Id,Legal_entity__c = 'GE AVIATION');
            insert fore;
            
            

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India',AccountId = acc1.id);
                            insert con;

           opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated',Account_Platform__c =fore.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services',Is_Tokenized__c  = false,Margin_Escalator__c= true);
           insert opp;
           
           opportunity opp2 = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 500, Firm_Anticipated__c = 'Anticipated',Account_Platform__c =fore.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services',Is_Tokenized__c  = false,Margin_Escalator__c= true);
           insert opp2;
          

            Sales_Request__c sr = new Sales_Request__c(Name = 'Test response5',Pipeline_Opportunity__c = opp.id,Type__c = 'test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = u.id,Sales_Owner__c = u.id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c = 'Windchill UK',Is_Tokenized__c = false);
            insert sr;
           
            Sales_Request__c sr1 = new Sales_Request__c(Name = 'Test response5',Pipeline_Opportunity__c = opp2.id,Type__c = 'test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = u.id,Sales_Owner__c = u.id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c = 'Windchill UK',Is_Tokenized__c = false);
            insert sr1;
                      
            EPA__c epa = new EPA__c( Closed__c = False);
            insert epa;

            Cost_Authorization__c ca = new Cost_Authorization__c(Sales_Request__c = sr.id, EPA_Number__c = epa.id, Approved_Budget_Backend__c = NULL,
                FY_Spent_to_Date__c = NULL, Status__c = 'Approved', Phase__c = 'Phase 2', recordTypeId=findRecordtypeId('Cost_Authorization__c','Phase2_3_B_P_Task_Open') );
            insert ca;
            
            Platform_Model__c pmodel = new Platform_Model__c(Name = 'prs_model1' ,Platform_Master__c= pm.id, Is_Tokenized__c = true);
            insert pmodel;
            
            
                 
            pm.Name = 'prs_ChangedName';      
            pm.Is_Tokenized__c = true;
            pm.Segment__c = 'prs_Regional Transports';
            update pm;
            
            fore.Name = 'prs_NameChanged'; 
            fore.Legal_entity__c = 'GE AVIATION SYSTEMS';
            fore.Is_Tokenized__c = true;
            fore.Other_Comments__c = 'prs_test';
            fore.Civil_Military__c = 'Military';
            //update fore;
            update fore;
                 
              
        }
    }  
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }
     
   
    
}