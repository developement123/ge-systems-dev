/*
This class would update Old response TAT on executing it anonymously, need to delete along with test class "TestUpdateTATOnOldResponse" after 
successfull data upload.  
*/
Public class UpdateTATOnOldResponse {

public Void findResponseNotOnHold() {
Map<Sales_Request__c, List<Sales_Request__History>> mapofResponseToHostory = new Map<Sales_Request__c, List<Sales_Request__History>>();
   
        for(Sales_Request__c SR: [select id,CreatedDate, (select OldValue,NewValue,ParentId, createddate FROM Histories where field = 'SR_Status__c' order by ParentId, createddate asc, id asc)  from Sales_Request__C where Pipeline_Opportunity__c!=null and SR_Status__c not in ('On Hold','Pre Proposal') and id in ( select parentid from Sales_Request__History where field = 'SR_Status__c')]) {
            for(Sales_Request__History srHistory : SR.Histories){
                if( (srHistory.OldValue!=null  && srHistory.NewValue!=null) &&
                ( label.StatusToRemoveFromTAT.contains(String.valueOf(srHistory.OldValue)) || label.StatusToRemoveFromTAT.contains(String.valueOf(srHistory.NewValue)) ) ){
                    if(!mapofResponseToHostory.containsKey(SR))
                        mapofResponseToHostory.put(SR,new List<Sales_Request__History>{srHistory});
                    else
                         mapofResponseToHostory.get(SR).add(srHistory);
                                
                        
                }
                    
            }
        }
    
        if(!mapofResponseToHostory.IsEmpty())
            ProcessSalesResponse( mapofResponseToHostory,0 ) ;
}

public Void findResponseOnHold() {
Map<Sales_Request__c, List<Sales_Request__History>> mapofOnHoldResponseToHistory = new Map<Sales_Request__c, List<Sales_Request__History>>();

        for(Sales_Request__c SROnHold: [select id, CreatedDate, (select OldValue,NewValue,ParentId, createddate FROM Histories where field = 'SR_Status__c' order by ParentId, createddate asc, id asc)  from Sales_Request__C where Pipeline_Opportunity__c!=null and SR_Status__c in ('On Hold','Pre Proposal') and id in ( select parentid from Sales_Request__History where field = 'SR_Status__c')]) {
            for(Sales_Request__History OnHoldsrHistory : SROnHold.Histories){
                if((OnHoldsrHistory.OldValue!=null  && OnHoldsrHistory.NewValue!=null) && (label.StatusToRemoveFromTAT.contains(String.valueOf(OnHoldsrHistory.OldValue)) || label.StatusToRemoveFromTAT.contains(String.valueOf(OnHoldsrHistory.NewValue)))){
                    if(!mapofOnHoldResponseToHistory.containsKey(SROnHold))
                        mapofOnHoldResponseToHistory.put(SROnHold,new List<Sales_Request__History>{OnHoldsrHistory});
                    else
                         mapofOnHoldResponseToHistory.get(SROnHold).add(OnHoldsrHistory);
                                
                        
                }
                    
            }
        }
    
        if(!mapofOnHoldResponseToHistory.IsEmpty())
            ProcessSalesResponse( mapofOnHoldResponseToHistory,0 ) ;
}

    
 Public void ProcessSalesResponse( Map<Sales_Request__c, List<Sales_Request__History>> mapofResponseToHistory, Integer Debug ){
    Datetime inHoldDate;
    Datetime OutHoldDate; 
    double DiffinMinutes = 0;
    double DiifInMilisec = 0;
    list<sales_request__c> srToUpdate = new list<sales_request__c>();
    for(Sales_Request__c sr : mapofResponseToHistory.KeySet()){ 
        //inHoldDate = sr.CreatedDate;
        DiffinMinutes = 0;
        for(Sales_Request__History sh : mapofResponseToHistory.get(sr)){

            if(!test.isRunningtest()){
                if( !label.StatusToRemoveFromTAT.contains( String.valueOf(sh.OldValue) ) && label.StatusToRemoveFromTAT.contains( String.valueOf(sh.NewValue) )){ 
                    inHoldDate = sh.createddate; 
                }
                
                if( label.StatusToRemoveFromTAT.contains(String.valueOf( sh.OldValue) ) && !label.StatusToRemoveFromTAT.contains( String.valueOf(sh.NewValue) ) && inHoldDate!=null){ 
                    OutHoldDate = sh.createddate; 
                    DiifInMilisec = OutHoldDate.getTime() - inHoldDate.getTime(); 
                    DiffinMinutes = DiffinMinutes + ((DiifInMilisec/1000.00)/60.00);
                }
                
                if(Debug==1)
                    system.debug( ' On Hold Minutes ++ ' + (DiifInMilisec/1000.00)/60.00  );
                    
                DiifInMilisec = 0; 
            }
        }
        
        sr.On_Hold_Date_Time__c = inHoldDate;
        sr.On_Hold_Minutes__c = DiffinMinutes;
        srToUpdate.add(sr);
        
        if(Debug==1)
           system.debug( 'Response ID ++ ' + sr.Id + ' On Hold Minutes ++ ' + DiffinMinutes + ' Hold DatetTime ++ ' + inHoldDate );
           
        
    }
    
    if(!srToUpdate.IsEmpty()){
       if(Debug!=1)
            update srToUpdate;
    }
    
 } 
 
}