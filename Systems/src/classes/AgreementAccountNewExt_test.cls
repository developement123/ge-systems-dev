/**********************************************

    Version     Written by        last modified Date              
    ---------------------------------- --------------------------------------------------------------------------------------------------------------
    1.0         Abhishek            18/9/2017 removed account platform
***********************************************/
@isTest
private class AgreementAccountNewExt_test{
 static testmethod void AgreementNewMethod(){
 
    Test.starttest();
    
    //Create Account/Contact    
     
    Account acc1 = new Account(Name = 'Hello');
    insert acc1;
    
    Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India');
    insert con;
     // Create Opty
     
     Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Digital Solutions', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
     
     Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                                                    Segment__c = 'Regional Transports');
     insert pm;    
     
     
     opportunity oppDigital = new opportunity(Name = 'Test opp1', StageName = 'Open'
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', Lookup_hiearchyname__c = hierarchy.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'Digital Solutions'
                             , Contract_Term__c = 2.0
                             );
     insert oppDigital;               
    
    //creating Contract/PO
    recordtype crt = [select id from RecordType where SobjectType='Contract_PO__c' and DeveloperName='Digital_Solutions' Limit 1];
    
    Contract_PO__c ContrDigital = new Contract_PO__c  (Account_Name__c = acc1.id
                                       , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
                                       , Pipeline_Opportunity__c = oppDigital.id, Number_of_PO_s__c = 50.0
                                       , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Monthly'
                                       , Revenue_Increment__c = 'Annually'
                                       , Renewed_no_changes__c = false
                                      
                                       , Price_Escalation_COLA__c = 10  
                                       , Price_Escalation_Start_Date__c = system.today()
                                       , RecordTypeID = crt.Id
                                       , Status__c = 'Draft');
                  
                  insert ContrDigital ;
   //Create agreement record
    PageReference pageRef = Page.AgreementAccountNew;
    Apttus__APTS_Agreement__c agr=new Apttus__APTS_Agreement__c();
    agr.Name='Agreement Name';
    recordtype rt = [select id from RecordType where SobjectType='Apttus__APTS_Agreement__c' and DeveloperName='GE_Digital_Services' Limit 1];
    agr.RecordTypeId = rt.id ;
    agr.Apttus__Contract_Start_Date__c=System.today();
    agr.Apttus__Account__c=acc1.id;
    agr.Apttus__Contract_End_Date__c = System.today();
    agr.Apttus__Description__c = 'Test Description';  
    agr.Related_Contract_PO__c = ContrDigital.Id; 
    insert agr;
    
     

    
    ApexPages.StandardController sc = new ApexPages.standardController(agr);
    AgreementAccountNewExt agrext=new AgreementAccountNewExt(sc);
    agrext.saveRec();

    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('agreementId',agr.id);
    ApexPages.currentPage().getParameters().put('cancelURL','/apex/apttus__cancelactioninterceptor');
    ApexPages.currentPage().getParameters().put('actionName','create_new_agreement');
    ApexPages.currentPage().getParameters().put('rollbackId',agr.id);
    ApexPages.currentPage().getParameters().put('retURL',agr.id);
    ApexPages.currentPage().getParameters().put('objectType','a0x');

    
    Test.stoptest();
 }
}