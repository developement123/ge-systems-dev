//Comments added By Abhishek: Jan.- 2016:
//This test class is not required as corresponding trigger is inactivated for Opportunity Optimization Requirement.
//End
/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - SendMailToRespTeam_Clone
    
    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          10/15/2014        Initial Creation
    
    Last Modified By : Mohan Kumar(16 march 2015)
            
*************************************************************************************************************************************************/      

@isTest
private class Tracker_SendMailToRespTeam {

   /*    static testmethod void test_SendMailToRespTeam() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;

        Profile[] pro = [select Id from profile where name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
                          localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;
        
        Contact con = new Contact(AccountId = acc1.Id, Email = 'neha.mittal@birlasoft.com', FirstName = 'Abc', LastName = 'Test', MailingCity = 'Abc', MailingCountry = 'Test Country', Function__c = 'Legal/Contracts');
        insert con;

        System.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                                                           Segment__c = 'Regional Transports');
            insert pm;

            Forecast__c fore = new Forecast__c(Name = 'Test12345', Airframer__c = acc1.id,
                                               Forecast_Platform__c = pm.id, Is_Tokenized__c = false);
            insert fore;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            List<Program__c> listOfProg = new List<Program__c>();

            Program__c prog = new Program__c(Name = 'Program', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id, Contract_Manager__c  = con.Id, User__c = u.Id,
                                             Forecast__c = fore.id, Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            listOfProg.add(prog);  
           
            insert listOfProg;

            opportunity opp2 = new opportunity(Name = 'Test opp2', StageName = 'Prospecting', CloseDate = date.today(),
                                               Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                                               Probability__c = '100% - Won', Lookup_hiearchyname__c = hierarchy.id);
            insert opp2;
            
        Test.startTest(); 
            Sales_Request__c sr = new Sales_Request__c(Name = 'Test response5', Program__c = prog.id, Response_Description__c = 'Test desc', Proposal_Manager__c = con.Id, Proposal_Coordinator__c = con.Id, Pricing_Manager__c = con.Id,
                                                      Additional_Member__c = con.id, Response_Submitted__c = Date.today(), Cost_Estimator__c = con.Id, Engineering_Lead__c = con.Id, Manufacturing_Cost_Estimator__c = con.id,
                                                      Windchill_Folder_Structure__c = 'ksbdbs');
            insert sr;  
            
            sr.Windchill_Folder_Structure__c ='er4tyu';
            sr.Response_Submitted__c = null;           
            Class_TriggerCheck.var_SendMailToRespTeamTrg = true;
            update sr;
            
            sr.Response_Submitted__c = date.Today() - 2;
            Class_TriggerCheck.var_SendMailToRespTeamTrg = true;
            update sr;
            
            Class_TriggerCheck.var_SendMailToRespTeamTrg = true;
            delete sr;
        Test.stopTest();   
        }
    }   */
}