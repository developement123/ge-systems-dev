/****************************************************
   This class is used as extensions in vf page 'RAS'.

Change History
=============
Developer       Modified On     Change description
Abhishek        05/18/2016      Changed as per comment added on 12th May on Jira 556 
Abhishek        07/07/2016        Removed the reference of "Oppty_SR_Intersection__c" to delete the same object "Jira 564"
Abhishek        09/21/2017        added property for Account
Rosine          04/17/2018      Add query to get all Associated Products on Pipeline Opportunity related to Response
    
****************************************************/

public with sharing class clsRAS { 

    public String recordNameFromVar{get;set;}
     public String recordDescFromVar{get;set;}
       public String recordBusinessSegmentFromVar{get;set;}        
    public String recordProductAreaFromVar{get;set;}
    public String recordplatformFromVar{get;set;}
    public String tempIsToken;
    public String currentDT{get;set;}
    public User usr{get;set;}
    public String recordAccountFromVar{get;set;}
    
    public Map<String, String> mapOfOppy{get;set;}
    public List<Opportunity_Line_Item__c> ap{get;set;}
    
    public String recordProdFamlilyFromVar{get;set;}
    public String recordProdChildFromVar{get;set;}
    public Boolean recordFromIsTokenize{get;set;}    
    public Boolean showLastShipDate {get;set;} 
    public date lastShipDate{get;set;}
    public Decimal noOfInstallment{get;set;}
    public clsRAS(ApexPages.StandardController controller) {
        
        //Commenting out description for tokenized records for now 
        //recordDescFromVar = Apexpages.currentpage().getparameters().get('var1');
        recordNameFromVar = Apexpages.currentpage().getparameters().get('var2');
        recordBusinessSegmentFromVar = Apexpages.currentpage().getparameters().get('var2'); //Not required for JIRA 441 
        recordProductAreaFromVar = Apexpages.currentpage().getparameters().get('var3'); 
        recordplatformFromVar = Apexpages.currentpage().getparameters().get('var4'); 
        recordProdFamlilyFromVar = Apexpages.currentpage().getparameters().get('var5');
        recordProdChildFromVar = Apexpages.currentpage().getparameters().get('var8');
        tempIsToken= Apexpages.currentpage().getparameters().get('isTokenizeVariable');        
        currentDT = System.now().format(); // Changes done as per JIRA - 363
        
        //getting opp data from RedirectToRAS page
        //Added by Suman Gupta on 13 Aug 2015 to display toeknized Opp data in normal text in PDF in Perspecsys URL - JIRA 441
        String strOppData = Apexpages.currentpage().getparameters().get('var6');
        recordAccountFromVar = Apexpages.currentpage().getparameters().get('var7');
        
        /**System.Debug('@@@'+recordDescFromVar +', '+recordNameFromVar +', '+recordProductAreaFromVar );
        System.Debug('@@@'+recordplatformFromVar +', '+recordProdFamlilyFromVar +', '+strOppData );
        System.Debug('@@@'+recordAccountFromVar +', '+recordProdChildFromVar );**/
        
        mapOfOppy = new Map<String, String>();
        if(strOppData != null && strOppData != '') {            
            for(String strOpp: strOppData.split(';')) {               
              mapOfOppy.put(strOpp.substringBefore(':'), strOpp.substringAfter(':'));
            }
        }
        //END
        
        usr = [select Id, Name from User where Id =: userInfo.getuserId()];
        if(tempIsToken == 'false') 
            recordFromIsTokenize = false;       
        else if(tempIsToken == 'true') 
            recordFromIsTokenize = true;  
        
        // added below line of codes as per jira 556
        showLastShipDate = True;
        lastShipDate = null;
        
        Sales_Request__c resp = [select id,Sales_Request__c.Pipeline_Opportunity__C,  Sales_Request__c.Pipeline_Opportunity__r.ContractID1__c, Sales_Request__c.Pipeline_Opportunity__r.Offering_Type__c, Sales_Request__c.Pipeline_Opportunity__r.Last_Revenue_Schedule_Date__c from Sales_Request__c where id =: Apexpages.currentpage().getparameters().get('Id')][0];
        If(resp.Pipeline_Opportunity__r.Offering_Type__c=='OE / Services'){
            if(resp.Pipeline_Opportunity__r.ContractID1__c!=null){
                list<opportunity> PoOpportunity = new list<opportunity>();
                PoOpportunity = [select id, Last_Revenue_Schedule_Date__c from opportunity where ContractID1__c =: resp.Pipeline_Opportunity__r.ContractID1__c and recordType.name =: System.Label.Label_POForecastOpportunity];
                if(!PoOpportunity.isEmpty()){
                    lastShipDate = PoOpportunity[0].Last_Revenue_Schedule_Date__c;
                }Else{
                    lastShipDate = resp.Pipeline_Opportunity__r.Last_Revenue_Schedule_Date__c;  
                }
                
            }Else{
                    noOfInstallment = 0;
                    noOfInstallment = (Decimal) [ select max(of_Installments__c) installment from Opportunity_Line_Item__c where Opportunity__c =: resp.Pipeline_Opportunity__C ][0].get('installment');
                showLastShipDate = false;
            }
        }Else{
            lastShipDate = resp.Pipeline_Opportunity__r.Last_Revenue_Schedule_Date__c;
        }
        
        ap = new List<Opportunity_Line_Item__c>([select id, Opportunity_Line_Item__c.Product__r.Name, Opportunity_Line_Item__c.Product_Code__c from Opportunity_Line_Item__c where Opportunity_Line_Item__c.Opportunity__r.Id =: resp.Pipeline_Opportunity__C]);   
    } 
}