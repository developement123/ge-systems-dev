/********************************************************************************
    This test class was created to provide code coverage for the following class and trigger:
    - poForecastOrder 
     Version     Written by                last modified Date              
  -------------------------------------------------------------------------------------------------
                Abhishek                       10/04/2016  
                Abhishek                       18/09/2016    removed account platform   
    ***************************************************************/      
@isTest
public class Tracker_poForecastOrder {   
   
    public static id findRecordtypeId(string objectType, string recordTypeName){
        return [select id from recordtype where SobjectType =: objectType and developername=:recordTypeName][0].id;
    }
   
    static testmethod void test_TriggerOnContract() {
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        
        Class_TriggerCheck.var_TriggerOnAssociatedProduct = false;
        Class_TriggerCheck.var_triggerOnUser = false;
        Class_TriggerCheck.var_TriggerOnContract = false;
        
        
        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;
        
        Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India');
        
         System.runAs(u) {
            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Digital Solutions', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Product_Hiearchy_Object__c hierarchy_OE = new Product_Hiearchy_Object__c(Name = 'OE / Services', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy_OE;
            
            // Offering_Type__c == 'OE / Services'
            opportunity oppservicesAnnual = new opportunity(Name = 'Test opp', StageName = label.OpportunityStage1
                             , CloseDate = date.today(), Amount = 500
                             , Firm_Anticipated__c = 'Anticipated', GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id
                             , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                             , Platform_New__c = pm.id, accountId= acc1.id, Offering_Type__c = 'OE / Services' );
            insert oppservicesAnnual;
            
            Product2 p2 = new Product2(Name = 'Test Product', Lookup_hiearchyname__c = hierarchy_OE.id, CanUseQuantitySchedule = True, CanUseRevenueSchedule = True, IsActive = true);
            insert p2;     
            
            Opportunity_Line_Item__c oliAnnual = new Opportunity_Line_Item__c(Opportunity__c =  oppservicesAnnual.Id
                    , Quantity__c = 1, Unit_Price__c = 0, Probability__c = 50, Product__c = p2.id
                    , Installment_Periods__c = 'Annually') ;       
            insert oliAnnual;
            
            Date mydate = Date.newInstance(System.Today().Year(), 1, 5);
            Date mydate1 = Date.newInstance(System.Today().Year(), 10, 5);
            
            List<Schedule__c> lstOfSch = new List<Schedule__c>();
            for(Integer i=0;i<=49;i++)
            {
              Schedule__c sc = new Schedule__c();
              sc.Opportunity_Line_Item__c = oliAnnual.id;
              sc.Schedule_Date__c  = mydate.addMonths(i);
              sc.Quantity__c = 1;
              sc.Escalated_Revenue__c = 3;
              sc.Opportunity__c = oppservicesAnnual.ID;
              lstOfSch.add(sc);
            } 
            
            Contract_PO__c ContrAnnual = new Contract_PO__c (Account_Name__c = acc1.id, Status__c = 'Draft' , Contract_Start_Date__c = system.today(), Contract_End_Date__c = system.today()
            , Legal__c = con.ID, Program_Manager__c = u.Id
            , Finance__c = con.ID, Sales__c = u.Id , Comm_Ops__c = u.Id    
            , Pipeline_Opportunity__c = oppservicesAnnual.id, Number_of_PO_s__c = 50.0
            , First_PO_Award_Date__c = system.today(),PO_Increment__c = 'Annual'
            , First_PO_Ship_Date__c  = system.today()
            , Revenue_Increment__c = 'Annually', Renewed_no_changes__c = false );
            
            insert ContrAnnual;
                 
                 opportunity poForecastOpp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today()
                                , Amount = 500, Firm_Anticipated__c = 'Anticipated'
                                , GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy_OE.id
                                , Contract_Award_Date__c = date.today(), Margin_Escalator__c = true
                                , Platform_New__c = pm.id, accountId= acc1.id
                                , recordtypeId = findRecordtypeId('Opportunity', 'POForecast')
                                , ContractID1__c=ContrAnnual.Id
                                , Offering_Type__c = 'OE / Services' );
                 insert poForecastOpp;
                 
                 ContrAnnual.PO_Forecast_Opportunity__c = poForecastOpp.id;
                 update ContrAnnual;
                 
                 Date dateInstance  = Date.newInstance(System.Today().Year(),1,30); 
                 Order__c order = new Order__c(Name = 'Test12345',Contract1__c = ContrAnnual.id,PO_Schedule_Opportunity__c = poForecastOpp.id, PO_Award_Start_Date__c = dateInstance, PO_Award_End_Date__c = dateInstance);
                 insert order;
                 
                 List<Schedule__c> lstOfPoSch = new List<Schedule__c>();
                 
                    Schedule__c sc = new Schedule__c(Opportunity_Line_Item__c = oliAnnual.id, Schedule_Date__c = dateInstance , Quantity__c = 1, Escalated_Revenue__c = 3, Opportunity__c = poForecastOpp.ID);
                    lstOfPoSch.add(sc);
               
                 insert lstOfPoSch;
         
            Test.startTest();
                   poForecastOrder.regeneratePoOrder(ContrAnnual.id);
            Test.stopTest();
              
            
            }
    
    } 
}