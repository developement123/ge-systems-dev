/*************************************************************************************************************************************************
    This test class was created to provide code coverage for the following trigger:
    - setMTGcheckboxvalueinME

    Version     Author(s)               Date              Details 
    ---------------------------------- ---------------------------------------------------------------------------------------------------------------
    1.0         Mohan kumar          12/03/2014        Initial Creation

    Last modified by : Mohan Kumar(13 Jan 2015) for JIRA 364.    
    Last modified by : Abhishek(28 April 2015) for JIRA 553. 
    Last modified by : Abhishek(18 Sep 2017) removed account platform
*************************************************************************************************************************************************/
      
      
@istest(SeeAllData = true)
private class Tracker_setMTGcheckboxvalueinME {

     static testmethod void test_setMTGcheckboxvalueinME() {

        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.var_trgRollupSummaryTriggerTrg = false;
        Class_TriggerCheck.Var_isProgam_Probability_change = true;
        Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
        Class_TriggerCheck.Var_StopOpportunityTrigger = false;
        Class_TriggerCheck.Var_StopUpdateRelationship = false;
        Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;         
        Class_TriggerCheck.var_updateMETrg = false;
        Class_TriggerCheck.var_updateSTonOppyOwnerUpdateTrg = false;
        Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;
        Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;  

        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        

        Profile[] pro = [select Id from Profile where Name = 'System Administrator' limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
            localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
        
        Account acc1 = new Account(Name = 'Hello');
        insert acc1;

        System.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;

            Program__c prog = new Program__c(Name = 'Program1',Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id,
                 Program_Probability__c = '100% - Funded',Is_Tokenized__c = false);
            insert prog;

            opportunity opp = new opportunity(Name = 'Test opp2', StageName = label.OpportunityStage1, CloseDate = date.today(),
                Program__c = prog.id, Amount = 500, Firm_Anticipated__c = 'Anticipated',
                GE_Standard_Probability__c = label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Contract_Award_Date__c = date.today(),Margin_Escalator__c = false
                , Platform_New__c = pm.id, accountId= acc1.id);

            insert opp;
            
            List<Margin_Escalator__c> listOfME = new List<Margin_Escalator__c>();
            
            listOfME.add(new Margin_Escalator__c(Opportunity__c=opp.Id, Year__c = String.valueOf(System.Today().Year())));
            listOfME.add(new Margin_Escalator__c(Opportunity__c=opp.Id, Year__c = String.valueOf(System.Today().Year() + 1)));
            
            insert listOfME;
       
           
         Test.startTest(); 
             opp.Margin_Escalator__c = true;
             update opp;
             delete opp;
             undelete opp;                        
         Test.stopTest();   
        }
    }    
}