//Comments added By Abhishek: Jan.- 2016:
//This test class is not required as corresponding trigger is inactivated for Opportunity Optimization Requirement.
//End
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 
 */
 
 // This is the test class to cover the code coverage of trigger trgAutoUpdateOpportunity.
@isTest(SeeAllData=true)
private class Test_trgAutoUpdateOpportunity {
/*
    static testMethod void myUnitTest() {
        
   
     Platform_Master__c plat=new Platform_Master__c(Name='100',Civil_Military__c='Military',Segment__c='Regional Transports');
     insert plat;
     
     Account acc = new Account(Name='Test Account SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS');
     insert acc; 
     id bA_Profile_id;
     id Sys_admin_Profile_id;
     
   list<Profile> pro=[SELECT ID,name FROM PROFILE WHERE NAME='System Administrator' or Name='Systems Business Admin'];
   for(Profile p: pro)
   {
     if(p.name=='Systems Business Admin')
     {
        bA_Profile_id=p.Id;
     }
     else
     {
        Sys_admin_Profile_id=p.id;
     }
   }
     User u =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=Sys_admin_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12321.com.sys.dev');
               insert u;
               
               
     User u2 =new User( alias = 'skapo',email = 'Jeetesh.bahuguna@birlasoft.com', emailencodingkey = 'UTF-8', lastname = 'Bahuguna',languagelocalekey = 'en_US', 
               localesidkey = 'en_US',profileid=bA_Profile_id,country = 'India', timezonesidkey = 'Europe/London', username = 'Jeetesh.bahuguna@birlasoft12453.com.sys.dev');
         insert u2;
     Forecast__c fore=new Forecast__c(Airframer__c=acc.id,Forecast_Platform__c=plat.id);
     insert fore;
     
     Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name='Test hierarchy',Product_Group__c='Test group',Product_Area__c='Test area',Product_Family__c='Test family',Product_Child__c='Test child');
     insert hierarchy;
     
     Product_Hiearchy_Object__c hierarchy1 = new Product_Hiearchy_Object__c(Name='Test hierarchy1',Product_Group__c='Test group',Product_Area__c='Test area1',Product_Family__c='Test family1',Product_Child__c='Test child1');
     insert hierarchy1;
     
    // Program__c prog=new Program__c(Product_Group__c='Avionics',
    // Product_Area__c=Label.ProductHierarchyMultiple, Product__c=Label.ProductHierarchyMultiple,
    // Product_Family_Child__c=Label.ProductHierarchyMultiple, Product_Sales_Owner__c=u.id,
    // Forecast__c=fore.id,Program_Probability__c='100% - Funded');
    // deployment
     
     Program__c prog=new Program__c(Lookup_hiearchyname__c=hierarchy.id,Product_Sales_Owner__c=u.id,
     Forecast__c=fore.id,Program_Probability__c='100% - Funded');
     insert prog;
     
     
    //deployment Program__c prog2=new Program__c(Product_Group__c='Power',
    // Product_Area__c='Power Conversion & Control',Product__c='Power Conversion & Control',
    // Product_Family_Child__c='Regulated Transformer Rectifier Units (RTRUs)SSSSSSSSSSSSSSSSSSSSS',Product_Sales_Owner__c=u.id,
    // Forecast__c=fore.id,Program_Probability__c='100% - Funded');
    // deployment
     
     Program__c prog2=new Program__c(Lookup_hiearchyname__c=hierarchy1.id,Product_Sales_Owner__c=u.id,
     Forecast__c=fore.id,Program_Probability__c='100% - Funded');
     insert prog2;
     
     
    Opportunity opp=new opportunity();
    opp.Name='Test oppssssssssssssssssssssssssssssssssssssss';
    opp.StageName='Prospecting';
    opp.CloseDate=date.today();
    opp.Program__c=prog.id;
    opp.Amount=1000;
    opp.Firm_Anticipated__c='Anticipated';
    opp.Probability__c='100% - Won';
    opp.Lookup_hiearchyname__c = hierarchy.id;
    insert opp;
    
    Opportunity opp1=new opportunity();
     opp1.Name='Test opp1';
    opp1.StageName='Prospecting';
    opp1.CloseDate=date.today();
    opp1.Program__c=prog.id;
    opp1.Amount=1000;
    opp1.Firm_Anticipated__c='Anticipated';
    opp1.Probability__c='100% - Won';
    opp1.Lookup_hiearchyname__c = hierarchy.id;
    insert opp1;
    
    test.startTest();
    Class_TriggerCheck.Var_isProgam_Probability_change=false;
    prog.Program_Probability__c='25% - Potential funding with negative outlook';
    update prog;
    
    Class_TriggerCheck.Var_isProgam_Probability_change=false;
    opp.Probability__c='25% - Three or more competitors / non-favored position';
    try{
        update opp;
    }catch(Exception ex){} 
    Class_TriggerCheck.Var_isProgam_Probability_change=false;
    prog.Program_Probability__c='0% - Not funded';
    update prog;
    
    Class_TriggerCheck.Var_isProgam_Probability_change=false;
    prog.Program_Probability__c='100% - Funded';
    update prog;
        
   
    
    Class_TriggerCheck.Var_isProgam_Probability_change=false;
    Class_TriggerCheck.Var_IsProgramUPdated=false;
    //Commented below line as Program Hierarchy can't be changed/updated, if hierarchy is having 'Accross Muptiple Products' .
    //prog.Product_Group__c='Power'; 
    prog.Program_Probability__c= '75% - RFP issued and/or requirement certain';
    update prog;
    
    
    system.runAs(u){    
        Class_TriggerCheck.Var_IsProgramUPdated=false;  
        opp.Name='tesdsddasdssd';
        try{
            update opp1;
           }catch(Exception oppE1){}  
        Class_TriggerCheck.Var_IsProgramUPdated=false;  
         opp.Program__c=prog2.id;
         try{
            update opp1;
           }catch(Exception oppE2){}  
        Class_TriggerCheck.Var_IsProgramUPdated=false;  
     //deployment    opp1.Product_group__c='Avionics';
           try{
            update opp1;
           }catch(Exception oppE3){}     
    }
    
    system.runAs(u2){    
        Class_TriggerCheck.Var_IsProgramUPdated=false;  
        opp.Name='tesdsddasdssdsasa';
        try{
            update opp1;
           }catch(Exception oppE1){}  
        Class_TriggerCheck.Var_IsProgramUPdated=false;  
         opp.Program__c=prog2.id;
         try{
            update opp1;
           }catch(Exception oppE2){}  
        Class_TriggerCheck.Var_IsProgramUPdated=false;  
      //deployment   opp.Product_group__c='Power';
           try{
            update opp1;
           }catch(Exception oppE3){}     
    }
    
     Class_TriggerCheck.Var_isProgam_Probability_change=false;
    delete opp1;
    Class_TriggerCheck.Var_Epa_OwnerChanged=
    
    Class_TriggerCheck.Var_ShouldEPA_update=true; 
    Class_TriggerCheck.Var_ShouldCA_update=true; 
    Class_TriggerCheck.Var_IsProgramUPdated=false;
    Class_TriggerCheck.Var_Epa_OwnerChanged=false; 
    Class_TriggerCheck.Var_isProgam_Probability_change=false; 
    Class_TriggerCheck.Var_IsEmailsend=1;
   
     test.stopTest();
    
    }    */
}