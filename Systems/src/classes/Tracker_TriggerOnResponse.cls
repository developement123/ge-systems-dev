/***************************************************************************************************************
This test class was created to provide code coverage for Trigger "TriggerOnResponse" on 
Response object and handler class "ResponseTriggerHandler"
Version     Author(s)         Date              Details 
---------------------------------------------------------------------------------------------------------------
1.0         Jhansi          1/4/2016        Initial Creation  
            Abhishek        18/9/2017       removed account platform          
*****************************************************************************************************************/     
@istest
public class Tracker_TriggerOnResponse {

      private static testmethod void testTriggerOnResponse(){
          Class_TriggerCheck.var_triggerOnUser = false;
          Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
          Class_TriggerCheck.Var_StopProductHiearchy = false;
          Class_TriggerCheck.var_TriggerOnAccount = false;
          Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
          Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');

        Test.setMock(HttpCalloutMock.class, mock);     
         
         Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator'
            limit 1];

       User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
       localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
       insert u;
       Account acc1 = new Account(NAME = 'GE GEAS');
       insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;


            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India',AccountId = acc1.id,Email = 'abc@gmail.com');
                            
                            
            List<Program__c> listOfProg = new List<Program__c>();

            Program__c prog = new Program__c(Name = 'Program', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id, Contract_Manager__c  = con.Id, User__c = u.Id, Program_Probability__c = '100% - Funded',Max_Response_Number__c = null);
            listOfProg.add(prog);  
           
            insert listOfProg;

           opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated', Platform_New__c = pm.id, accountId= acc1.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services',Last_Response_Number__c = 111,Margin_Escalator__c= true);
           insert opp;
           

          Test.startTest(); 
          Sales_Request__c sr = new Sales_Request__c(Name = 'Test response5',Pipeline_Opportunity__c = opp.id,Type__c ='test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = u.id,Sales_Owner__c =u.id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c ='Windchill UK',Response_Submitted__c = system.Today(),Cost_Estimator__c = con.id,Engineering_Lead__c = con.id,Pricing_Manager__c = con.id,Manufacturing_Cost_Estimator__c = con.id,Additional_Member__c = con.id,Program_Manager__c = u.id,Finance_Manager_Analyst__c = con.id,Windchill_Folder_Structure__c = 'ksbdbs',Program__c = prog.id);
         insert sr;

            sr.Windchill_Folder_Structure__c ='er4tyu';
            sr.Response_Submitted__c = null;           
            Class_TriggerCheck.var_SendMailToRespTeamTrg = true;
            update sr;
            
            sr.Response_Submitted__c = date.Today() - 2;
            Class_TriggerCheck.var_SendMailToRespTeamTrg = true;
            update sr;
            
           Class_TriggerCheck.var_SendMailToRespTeamTrg = true;
            delete sr;
        Test.stopTest();   
  
        }
     }
     
     
     
      private static testmethod void testTriggerOnResponse1(){
          Class_TriggerCheck.var_triggerOnUser = false;
          Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
          Class_TriggerCheck.Var_StopProductHiearchy = false;
          Class_TriggerCheck.var_TriggerOnAccount = false;
          Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
          Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');

        Test.setMock(HttpCalloutMock.class, mock);     
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator'
        limit 1];

       User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',
       localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
       insert u;
       Account acc1 = new Account(NAME = 'GE GEAS');
       insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;
            
            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India',AccountId = acc1.id,Email = 'neha.mittal@ge.com');
                            
                            
            List<Program__c> listOfProg = new List<Program__c>();

          Program__c prog = new Program__c(Name = 'Program', Lookup_hiearchyname__c = hierarchy.id, Product_Sales_Owner__c = u.id, Contract_Manager__c  = con.Id, User__c = u.Id, Program_Probability__c = '100% - Funded',Max_Response_Number__c = null);
          listOfProg.add(prog);  
           
           insert listOfProg;

          opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated',Platform_New__c = pm.id, accountId= acc1.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services',Last_Response_Number__c = null,Margin_Escalator__c= true);
          insert opp;

          
          opportunity opp2 = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated',Platform_New__c = pm.id, accountId= acc1.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services',Last_Response_Number__c = 111,Margin_Escalator__c= true);
          insert opp2;
         
          Sales_Request__c sr1 = new Sales_Request__c(Name = 'Test response5',Pipeline_Opportunity__c = opp.id,Type__c ='test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = u.id,Sales_Owner__c =u.id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c ='Windchill UK',Response_Submitted__c = system.Today(),Cost_Estimator__c = con.id,Engineering_Lead__c = con.id,Pricing_Manager__c = con.id,Manufacturing_Cost_Estimator__c = con.id,Additional_Member__c = con.id,Program_Manager__c = u.id,Finance_Manager_Analyst__c = con.id,Windchill_Folder_Structure__c = 'ksbdbs');
          insert sr1;
          
          Sales_Request__c salesReq = new Sales_Request__c(Name = 'Test response5',Pipeline_Opportunity__c = opp2.id,Type__c ='test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = u.id,Sales_Owner__c =u.id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c ='Windchill UK',Response_Submitted__c = system.Today(),Cost_Estimator__c = con.id,Engineering_Lead__c = con.id,Pricing_Manager__c = con.id,Manufacturing_Cost_Estimator__c = con.id,Additional_Member__c = con.id,Program_Manager__c = u.id,Finance_Manager_Analyst__c = con.id,Windchill_Folder_Structure__c = 'ksbdbs');
          insert salesReq;
         
 
  
        }
     } 
     
      private static testmethod void testTrg_sendRESP(){ // cover trigger Trg_sendRESP
        
        Class_TriggerCheck.var_triggerOnUser = false;  
        Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
        Class_TriggerCheck.Var_StopProductHiearchy = false;
        Class_TriggerCheck.var_TriggerOnAccount = false;
        Class_TriggerCheck.var_TriggerOnOpportunityTrg = false;
        
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('mockresponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'text/xml');

        Test.setMock(HttpCalloutMock.class, mock);     
        
        Profile[] pro = [SELECT ID FROM PROFILE WHERE NAME = 'System Administrator'
        limit 1];

        User u = new User(alias = 'skapo12', Legal_Entity__c = 'GE AVIATION SYSTEMS', email = 'neha.mittal@ge.com', emailencodingkey = 'UTF-8', lastname = 'kapoor', languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pro[0].id, country = 'India', timezonesidkey = 'Europe/London', username = 'neha.mittal12@ge.com.sys.dev');
        insert u;
       
        Account acc1 = new Account(NAME = 'GE GEAS');
        insert acc1;

        system.runAs(u) {

            Platform_Master__c pm = new Platform_Master__c(Name = '170', Civil_Military__c = 'Military',
                Segment__c = 'Regional Transports');
            insert pm;

            Product_Hiearchy_Object__c hierarchy = new Product_Hiearchy_Object__c(Name = 'Test hierarchy', Product_Group__c = 'Test group', Product_Area__c = 'Test area', Product_Family__c = 'Test family', Product_Child__c = 'Test child');
            insert hierarchy;
            
            Contact con = new Contact (LastName='Test1', FirstName='FirstName1',
                            MailingCity='Delhi',
                            MailingCountry='India',AccountId = acc1.id,Email = 'neha.mittal@ge.com');

          opportunity opp = new opportunity(Name = 'Test opp', StageName =label.OpportunityStage1,CloseDate = date.today(), Contract_Term__c = 3,Amount = 1000, Firm_Anticipated__c = 'Anticipated',Platform_New__c = pm.id, accountId= acc1.id,  deal_type__c = 'Services',GE_Standard_Probability__c=label.GEStandardProb3, Lookup_hiearchyname__c = hierarchy.id,Offering_Type__c ='OE / Services',Last_Response_Number__c = null,Margin_Escalator__c= true);
          insert opp;
 
         
          Sales_Request__c sr1 = new Sales_Request__c(Name = 'Test response5',Pipeline_Opportunity__c = opp.id,Type__c ='test',Deal_Lane__c = 'test',Proposal_Coordinator__c = con.id,Proposal_Manager__c = u.id,Sales_Owner__c =u.id,Contract_Manager__c = con.id,Response_Description__c = 'Test desc',Windchill_US_UK__c ='Windchill UK',Response_Submitted__c = system.Today(),Cost_Estimator__c = con.id,Engineering_Lead__c = con.id,Pricing_Manager__c = con.id,Manufacturing_Cost_Estimator__c = con.id,Additional_Member__c = con.id,Program_Manager__c = u.id,Finance_Manager_Analyst__c = con.id,Windchill_Folder_Structure__c = 'ksbdbs');
          
          CREATE_UPDATE_HEADER_CA_RESPONSE.firstRun = true;
          insert sr1;
          
          CREATE_UPDATE_HEADER_CA_RESPONSE.firstRun = true;
          sr1.Name = 'Update';
          Update sr1;
          
         
 
  
        }
     } 
}