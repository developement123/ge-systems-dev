({
	doInit : function(cmp, event, helper) {
        if(confirm('This will delete existing Open Orders. Orders that are marked PO Booked or ERP Forecast will not be affected.'))
        { 
        	var action = cmp.get("c.regeneratePoOrder");
        	action.setParams({ "conId" : cmp.get("v.recordId") });
        	// Create a callback that is executed after 
    		// the server-side action returns
     		action.setCallback(this, function(response) {
            	var state = response.getState();
            	if (state === "SUCCESS") {
               		if(action.getReturnValue() == -1){
                    	alert("{!$Label.c.labelName}");
                	}else if(action.getReturnValue() == 1){
                    	$A.get("e.force:refreshView").fire();
                	}else{
                    	alert("{!$Label.c.Regenerate_Order_GenericError}");
                	}
            	}else if (state === "ERROR") {
                	var errors = response.getError();
                	if (errors) {
                    	if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    	}
                	} else {
                    	console.log("Unknown error");
                	}
           		}	
           
            	// Close the action panel
				var dismissActionPanel = $A.get("e.force:closeQuickAction");
				dismissActionPanel.fire();
         
        	});
        
        // optionally set storable, abortable, background flag here
        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
        
        }
         // Close the action panel
            // alert($A.get("e.force:closeQuickAction"));
				var dismissActionPanel1 = $A.get("e.force:closeQuickAction");
				dismissActionPanel1.fire();  
    }
             
})