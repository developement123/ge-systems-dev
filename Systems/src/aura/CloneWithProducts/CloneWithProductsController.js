({
	doInit : function(component, event, helper) {
		// call class method
		var action = component.get("c.getOpportunity");
        action.setParams({"id" : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
            	var theItems = response.getReturnValue();
                var Var_gameFilmCount = theItems.Count_Game_Films__c;
                var Var_AllStagesRequiredGameFilm = $A.get('{!$Label.c.Stage_Game_Film_Validation}').toLowerCase(); 
                var Var_Growth = theItems.Growth__c;
                var Var_stageName = theItems.StageName.toLowerCase();
                if((Var_AllStagesRequiredGameFilm.indexOf(Var_stageName)!= -1) && Var_gameFilmCount==0 && Var_Growth==1) 
				{ 
					alert($A.get('{!$Label.c.Label_Clone_With_Product}')) ;
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
					dismissActionPanel.fire();
				}else{ 
                     var action_2 = component.get("c.Schedules_Webservice");
                     action_2.setParams({"OppId" : component.get("v.recordId")});
       				 action_2.setCallback(this, function(response) {
                         var state = response.getState();
                         if(state=="SUCCESS"){
                             var oppid = response.getReturnValue();
                             var navEvt = $A.get("e.force:navigateToSObject");
							 navEvt.setParams({
  												"recordId": oppid,
												});
							navEvt.fire();
                               //var urlEvent = $A.get("e.force:navigateToURL");
    							//urlEvent.setParams({
      							//	"url": "/"+oppid+'/edit'
   								 //});
    						//urlEvent.fire();
                         }
                     });
                    $A.enqueueAction(action_2);
                }
            }
            
        });
        
         $A.enqueueAction(action);
         
	}
})