/****************************************************************************
    Developer : Abhishek
    Date      : 30/June/2016    
    Action    : This trigger invokes "EPATriggerHandler"

    Change desription:
    Developer         Date                  Description
*****************************************************************************/
trigger TriggerOnEPA on EPA__c (Before delete) {

if(Label.Label_TriggerOnEPA == 'true'){
    if(Class_TriggerCheck.Var_StopTriggerOnEPA){
        EPATriggerHandler trgHandler = new EPATriggerHandler();
        if(trigger.isBefore && trigger.isDelete) {
           trgHandler.ValidateEPATOnDelete(trigger.oldMap, 'Before', 'Delete');
        }
    }
 }
}