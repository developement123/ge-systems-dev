/****************************************************************************
    Developer : Abhishek
    Date      : 29/June/2016    
    Action    : This trigger invokes handler class "PlatformMasterTriggerHandler"

    Change desription:
    Developer         Date                  Description
*****************************************************************************/
trigger TriggerOnPlatformMaster on Platform_Master__c (Before delete) {

if(Label.Label_TriggerOnPlatformMaster== 'true'){
    if(Class_TriggerCheck.Var_StopPlatMasterTrigger){
        PlatformMasterTriggerHandler trgHandler = new PlatformMasterTriggerHandler();
        if(trigger.isBefore && trigger.isDelete) {
           trgHandler.ValidatePlatformMasterOnDelete(trigger.oldMap, 'Before', 'Delete');
        }
    }
 }
}