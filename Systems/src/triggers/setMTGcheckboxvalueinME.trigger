/***************************************************************************************************************************************************
    This Apex trigger was written to update the value of 'MTG' checkbox in ME when 
    'Margin Escalator' checkbox is updated in associated opportunity.
    
    written by : Mohan Kumar
    date : 7/November/2014
    
    Last modified by : Mohan Kumar(28 Jan 2015) for JIRA 369.
    Last modified by : Mohan Kumar(28 april 2015) for JIRA 407.
    
****************************************************************************************************************************************************/

trigger setMTGcheckboxvalueinME on Opportunity(before update, after update, before delete, after delete, after undelete) {
    String OppRecId;
    if(Trigger.New != null){
        OppRecId = Trigger.New[0].RecordTypeId;
    } else {
        OppRecId = Trigger.Old[0].RecordTypeId;
    }
    
    if(Label.Handle_Execution_of_MTG_Checkbox_Trigger == 'true' && (OppRecId <> Label.Unison_Record_Type_Label1 || OppRecId <> Label.Unison_Record_Type_Label2 || OppRecId <> Label.Unison_Record_Type_Label3)) {    
        //Suman Gupta : Dec. 2015 - Commented this before update event code to avoid Too many SOQL. As, same
        //functionaltiy is done in ME trigger to update CY_Margin_Escalator1__c field.
        /*if(trigger.isBefore && trigger.isUpdate && Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg) {     // added for JIRA 369.
    
            Set<Id> setOfOppyId = new Set<Id>();            
            Map<Id, Double> oppyCYMap = new Map<Id, Double>();
            
            for(Opportunity opp : trigger.new) {
                //Checking if ME records count against Opp is greater than zero, only then fire query on ME
                if(opp.ME_Records_Count__c > 0)
                    setOfOppyId.add(opp.id);
            }
            
            if(Label.CY_Margin_Escalator_Year != null) {            
                String yearfromLabel = Label.CY_Margin_Escalator_Year.trim();
                
                if(!setOfOppyId.isEmpty())
                {
                    for(Margin_Escalator__c me : [select Id, Opportunity__c, CY_Margin_Escalator__c 
                                                        from Margin_Escalator__c
                                                        where Opportunity__c IN: setofoppyId 
                                                        AND Year__c =: yearfromLabel]) { 
                        oppyCYMap.put(me.Opportunity__c, me.CY_Margin_Escalator__c);                   
                    } 
                }               
            }
            
            for(Opportunity opp : trigger.new) {
                if(oppyCYMap.get(opp.id) != null) 
                {
                    System.debug('Suman : oppyCYMap.get(opp.id) ' + oppyCYMap.get(opp.id));
                    opp.CY_Margin_Escalator1__c = oppyCYMap.get(opp.id);  
                }
                else if(oppyCYMap.get(opp.id) == null)
                    opp.CY_Margin_Escalator1__c = 0.0;              
            }
        }*/
    
        if(Trigger.isAfter && Trigger.isUpdate && Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg) {
            Set<Id> setOfOppyId = new Set<Id>();            
            List<Margin_Escalator__c> meRecList = new List<Margin_Escalator__c>();
            List<Opportunity> listToUpdateMTGYear = new List<Opportunity>();     // added for JIRA 364.
            Map<Id, List<Margin_Escalator__c>> mapOfOppyME = new Map<Id, List<Margin_Escalator__c>>(); 
            
            for(Opportunity oppy : trigger.new) {        
                if(((trigger.oldMap.get(oppy.Id).Margin_Escalator__c == false 
                    && trigger.newMap.get(oppy.Id).Margin_Escalator__c == true) 
                    || (trigger.oldMap.get(oppy.Id).Margin_Escalator__c == true 
                          && trigger.newMap.get(oppy.Id).Margin_Escalator__c == false))
                    && (oppy.CloseDate.Year() == System.Today().Year())) {            
                        setOfOppyId.add(oppy.Id);            
                }
            }  
            
            if(!setOfOppyId.isEmpty()) {      //  if condition added for JIRA 407 dated 28 april 2015.
                for(Margin_Escalator__c me : [select Id, Year__c, Opportunity__c, MTG__c from Margin_Escalator__c where Opportunity__c IN: setofoppyId order by Year__c]) {       
                    if (mapOfOppyME.containsKey(me.Opportunity__c))
                        mapOfOppyME.get(me.Opportunity__c).add(me);
                    else
                        mapOfOppyME.put(me.Opportunity__c,new List<Margin_Escalator__c>{me});
                }
            }
            
            if(!mapOfOppyME.isEmpty()) {
                for(Opportunity oppy: trigger.new) {
                    if(setOfOppyId.contains(oppy.Id)) {                
                        for(Margin_Escalator__c me : mapOfOppyME.get(oppy.Id)) {                
                            if(Integer.valueOf(me.Year__c) < System.Today().Year() && me.MTG__c == true) {                        
                                break;
                            }
                            
                            else if(Integer.valueOf(me.Year__c) == System.Today().Year()) {                        
                                Margin_Escalator__c meRec = new Margin_Escalator__c();          
                                meRec.id = me.Id;
                                meRec.MTG__c = trigger.newMap.get(oppy.Id).Margin_Escalator__c;
                                
                                meRecList.add(meRec);
                                
                                if(meRec.MTG__c)         // added for JIRA 364.
                                    listToUpdateMTGYear.add(new Opportunity(id = oppy.id, MTG_Year__c = me.Year__c)); 
                            }
                        }
                    }
                }
            }  
              
           if(!meRecList.isEmpty()) {
                try {
                    Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false;
                    Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;
                    Class_TriggerCheck.Var_StopPlatMasterTrigger = false;
                    Class_TriggerCheck.Var_isProgam_Probability_change = true;
                    Class_TriggerCheck.Var_StopProdVarMultipleExecution = false;
                    Class_TriggerCheck.Var_StopOpportunityTrigger = false;
                    //Class_TriggerCheck.Var_StopUpdateRelationship = false; //commented for JIRA-381
                    Class_TriggerCheck.var_execute_createMEmanuallyTrg = false;
                    
                    update meRecList;               
                }
                catch(Exception e) {}  
            }
            
            if(!listToUpdateMTGYear.isEmpty()) {     // added for JIRA 364.
                try {
                    update listToUpdateMTGYear;     // this part will put the ME 'Year' value in Oppy 'MTG Year' field.
                }
                catch(Exception e) {}
            }
        }
        
        /*
          following update trigger is written because when 'Stage' or 'contract award date'
           or 'MTG year' or 'cy margin escalator' or 'amount' is updated then,
           related sales target needs to be updated.
        */
        //Suman Gupta : 14 Jan. 2016 - Commented this after update event code to stop the execution of Sales Target 
        //functionaltiy for Opportunity Optimization Requirement Release
        /*if(Trigger.isAfter && Trigger.isUpdate && Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg) {
                       
            Set<Id> setofOpprtyIds = new Set<Id>();            
            Set<Id> setOfOwnerId = new Set<Id>();
            List<Sales_Target__c> listOfSTtoUPdate = new List<Sales_Target__c>();
            
            for(Opportunity op : Trigger.new) {
                if((Trigger.oldMap.get(op.Id).StageName != Trigger.newMap.get(op.Id).StageName) ||
                   (Trigger.oldMap.get(op.Id).Contract_Award_Date__c != Trigger.newMap.get(op.Id).Contract_Award_Date__c) || 
                   (Trigger.oldMap.get(op.Id).MTG_Year__c != Trigger.newMap.get(op.Id).MTG_Year__c) ||
                   (Trigger.oldMap.get(op.Id).CY_Margin_Escalator1__c != Trigger.newMap.get(op.Id).CY_Margin_Escalator1__c) ||
                   (Trigger.oldMap.get(op.Id).Amount != Trigger.newMap.get(op.Id).Amount) ||
                   (Trigger.oldMap.get(op.Id).Growth__c != Trigger.newMap.get(op.Id).Growth__c))
                
                        setofOpprtyIds.add(op.Id);
            }
            
            if(!setofOpprtyIds.isEmpty()) {      //  if condition added for JIRA 407 dated 28 april 2015.
                for(OpportunitySplit osplt : [select Id,Sales_Target__c,SplitOwnerId,Contract_Award_Date_Year__c,SplitType.MasterLabel,MTG_Year__c,Stage__c,SplitAmount from OpportunitySplit where OpportunityId IN: setofOpprtyIds]) {
                    setOfOwnerId.add(osplt.SplitOwnerId);
                }
            }
            
            if(!setOfOwnerId.isEmpty()) {       //  if condition added for JIRA 407 dated 28 april 2015.
                listOfSTtoUPdate = [select Id from Sales_Target__c where Sales_Person__c IN : setOfOwnerId];
            }
            
            if(!listOfSTtoUPdate.isEmpty()) {
                try {
                    Class_TriggerCheck.var_preventUpdate_UpdateSalesTargetTrg = true;
                    update listOfSTtoUPdate;
                }
                catch(Exception e) {}
            }
        }*/
        
       /* 
           When opportunity is deleted, related splits also gets deleted then,            
           related Sales target needs to be updated.       
       */          
         //Suman Gupta : 14 Jan. 2016 - Commented this before delete event code to stop the execution of Sales Target 
         //functionaltiy for Opportunity Optimization Requirement Release
         
         /*if(Trigger.isBefore && Trigger.isDelete && Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg) {
            Set<Id> oppyId = new Set<Id>();
            Set<Id> setOfOwnerId = new Set<Id>();
            Set<String> setOfYears = new Set<String>();                        
            
            List<Sales_Target__c> listOfSTtoUPdate = new List<Sales_Target__c>();
            
            Map<String,Id> mapOfSalesTarget = new Map<String,Id>();   
            
            for(Opportunity opp : Trigger.old) {        
                oppyId.add(opp.Id);     
            }
            
            for(OpportunitySplit osplt : [select Id,Sales_Target__c,SplitOwnerId,Contract_Award_Date_Year__c,SplitType.MasterLabel,MTG_Year__c,Stage__c,SplitAmount from OpportunitySplit where OpportunityId IN:oppyId]) {
                setOfOwnerId.add(osplt.SplitOwnerId); 
                
                if(osplt.Contract_Award_Date_Year__c != null)              
                    setOfYears.add(osplt.Contract_Award_Date_Year__c);
                if(osplt.MTG_Year__c != null) 
                    setOfYears.add(osplt.MTG_Year__c);                
            }           
            
            for(Sales_Target__c st : [select Id,Sales_Year__c,Sales_Person__c,Growth_Achieved__c,Growth_Pipeline__c,MTG_Achieved__c,MTG_Pipeline__c from Sales_Target__c where Sales_Person__c IN : setOfOwnerId AND Sales_Year__c IN : setOfYears]) {                                    
                mapOfSalesTarget.put(st.Sales_Person__c + '-' + st.Sales_Year__c,st.id);   
            }            
           
            if(!mapOfSalesTarget.values().isEmpty()) {
                Class_TriggerCheck.listofSalesTargetinClass = mapOfSalesTarget.values();            
            }
         }*/
         
         /*
             following part will run for after delete trigger. after Oppy has been deleted,
             we are updating the related sales target. hence, sales target trigger will run
             and recalculate the field values.
         */
         //Suman Gupta : 14 Jan. 2016 - Commented this after delete event code to stop the execution of Sales Target 
         //functionaltiy for Opportunity Optimization Requirement Release
         /*if(Trigger.isAfter && Trigger.isDelete && Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg) {
             
             Set<Id> tempSet = new Set<Id>();
             tempSet.addAll(Class_TriggerCheck.listofSalesTargetinClass); 
                          
             List<Sales_Target__c> listOfST = new List<Sales_Target__c>([Select id from Sales_Target__c where Id IN : tempSet]);
             
             if(!listOfST.isEmpty()) {
                 try {
                     Class_TriggerCheck.var_preventUpdate_UpdateSalesTargetTrg = true; 
                     update listOfST;
                 }
                 catch(Exception e) {}
             }
         }*/
         
         /*
             following part is for after undelete trigger. When oppy is undeleted, 
             we query the sales target id's in a list and update the sales target list.
             in this way sales target trigger will run and automatically updates the
             sales targets.    
         */
         //Suman Gupta : 14 Jan. 2016 - Commented this after undelete event code to stop the execution of Sales Target 
         //functionaltiy for Opportunity Optimization Requirement Release
         /*if(Trigger.isAfter && Trigger.isUndelete && Class_TriggerCheck.var_setMTGcheckboxvalueinMETrg) {
            
             Class_TriggerCheck.var_update_ST_onSplitupdateTrg = false;             
             Class_TriggerCheck.var_preventUpdate_UpdateSalesTargetTrg = true;
             
             Set<Id> setOfUndeletedId = new Set<Id>();
             Set<Id> setOfOwnerId = new Set<Id>();
             Set<String> setOfYears = new Set<String>();
             
             Map<String,Sales_Target__c> mapOfSalesTarget = new Map<String,Sales_Target__c>();
             
             for(Opportunity op : trigger.new) {
                 setOfUndeletedId.add(op.Id);        
             }
             
             for(OpportunitySplit osplt : [select Id,Sales_Target__c,SplitOwnerId,Contract_Award_Date_Year__c,SplitType.MasterLabel,MTG_Year__c,Stage__c,SplitAmount from OpportunitySplit where OpportunityId IN : setOfUndeletedId]) {
                setOfOwnerId.add(osplt.SplitOwnerId);                
                if(osplt.Contract_Award_Date_Year__c != null)              
                    setOfYears.add(osplt.Contract_Award_Date_Year__c);
                if(osplt.MTG_Year__c != null) 
                    setOfYears.add(osplt.MTG_Year__c);                
            }
            for(Sales_Target__c st : [select Id,Sales_Year__c,Sales_Person__c,Growth_Achieved__c,Growth_Pipeline__c,MTG_Achieved__c,MTG_Pipeline__c from Sales_Target__c where Sales_Person__c IN : setOfOwnerId AND Sales_Year__c IN : setOfYears]) {                                    
                mapOfSalesTarget.put(st.Sales_Person__c + '-' + st.Sales_Year__c,st);   
            }           
            
            if(!mapOfSalesTarget.values().isEmpty()) {
                try { 
                    Class_TriggerCheck.var_preventUpdate_UpdateSalesTargetTrg = true;                   
                    update mapOfSalesTarget.values();
                }
                catch(Exception e) {}
            }
        }*/
    }
}