trigger TriggerOnAccount on Account (Before Delete) {
 
if(Label.Label_TriggerOnAccount== 'true'){
    if(Class_TriggerCheck.var_TriggerOnAccount){
        AccountTriggerHandler trgHandler = new AccountTriggerHandler();
        
        if(trigger.isBefore && trigger.isDelete) {
           trgHandler.AccountValidationOnDelete(trigger.oldMap, 'Before', 'Delete');
        }
    }
 } 
  
}