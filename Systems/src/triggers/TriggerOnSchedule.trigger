/******************************************************************************************************************************************
    Author : Mohan Kumar
    Date     : 08/October/2015
    Action   : This trigger is invoking handler class: ScheduleTriggerHandler.
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek                        Commented this method "updateOrderIdOnSchedule" on "Before Update" - US15675
    Abhishek                        called this method "rollUpScheduleQuantityAountOnOrder" - US15675
    Abhishek        09/24/2016      called this method "updateQuantityOnSchedule" to update quantity on schedule - US15668
    Abhishek        11/02/2016      Called this method "rollUpScheduleQuantityAountOnOrder" on "Insert event" also to update quantity on manual insertion
******************************************************************************************************************************************/

trigger TriggerOnSchedule on Schedule__c(before insert, before update, after insert, after update, after delete, after undelete) {
    if(Label.Label_TriggerOnSchedule == 'true') {   
        if(Class_TriggerCheck.var_TriggerOnSchedule) {            
            
            ScheduleTriggerHandler trgHandler = new ScheduleTriggerHandler();
            
            if(trigger.isBefore && trigger.isInsert) {   // if the trigger was fired due to insert operation.            
                trgHandler.updateOrderIdOnSchedule(trigger.new, null,'Insert');
                trgHandler.tokenizeSchWhenOliIsTokenized(trigger.new);
            }
            if(trigger.isBefore && trigger.isUpdate) {   // if the trigger was fired due to update operation.            
              // commented on Sep 19th 2016 as per US15675
              //  trgHandler.updateOrderIdOnSchedule(trigger.new, trigger.oldMap,'Update');
              //trgHandler.updateQuantityOnSchedule(trigger.new, trigger.oldMap,'Update');
            }            
            if(trigger.isAfter && trigger.isInsert) {    // if the trigger was fired due to insert operation.
               // trgHandler.updateShippingDatesAndRevenueAmounts(trigger.new, null, null, null, 'After', 'Insert');
                trgHandler.updateCY_MEfromSchedules(trigger.new, null, null, null, 'After', 'Insert');
                trgHandler.updateShippingDatesAndRevenueAmounts(trigger.new, null, null, null, 'After', 'Insert');
                trgHandler.rollUpScheduleQuantityAountOnOrder(trigger.new, null, null, null, 'After', 'Insert');
                trgHandler.updateQuantityOnSchedule(trigger.new, null,'Insert');
            }
            else if(trigger.isAfter && trigger.isUpdate) {   // if the trigger was fired due to update operation.
                trgHandler.updateShippingDatesAndRevenueAmounts(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');
                trgHandler.updateCY_MEfromSchedules(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');
                trgHandler.rollUpScheduleQuantityAountOnOrder(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');
            }
            else if(trigger.isAfter && trigger.isDelete) {   // if the trigger was fired due to delete operation.
                trgHandler.updateShippingDatesAndRevenueAmounts(null, trigger.old, null, null, 'After', 'Delete');
                trgHandler.updateCY_MEfromSchedules(null, trigger.old, null, null, 'After', 'Delete');
                trgHandler.rollUpScheduleQuantityAountOnOrder(null, trigger.old, null, null, 'After', 'Delete');
            }
            else if(trigger.isAfter && trigger.isUnDelete) {   // if the trigger was fired due to undelete operation.
                trgHandler.updateShippingDatesAndRevenueAmounts(trigger.new, null, null, null, 'After', 'Undelete');      
                trgHandler.updateCY_MEfromSchedules(trigger.new, null, null, null, 'After', 'Undelete');  
                trgHandler.rollUpScheduleQuantityAountOnOrder(trigger.new, null, null, null, 'After', 'Undelete');
            } 
        }
    }
}