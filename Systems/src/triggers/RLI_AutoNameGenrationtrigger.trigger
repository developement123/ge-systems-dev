/**********************************************************************************************************************************************************
    Author       :   Mohan Kumar 
    Created Date :   15/April/2015
    Action       :   This trigger
                     1. Generates the 'Name' for 'Response Line Item'
                          like the format - [ResponseId-RLI-{AutoNumber}]
                     2. Captures 'Response Line Item History' when RLI is inserted/deleted.
                             
**********************************************************************************************************************************************************/

trigger RLI_AutoNameGenrationtrigger on Response_Line_Item__c(before insert, after insert, before update, after update, after delete) {
    if(Label.Label_RLI_AutoNameGenrationtrigger == 'true') {
        if(Class_TriggerCheck.var_AutoNameGenrationTrg) {       // this trigger will only run when this static variable is true.
            
            RLITrigger_Handler  trgHandler = new RLITrigger_Handler();
            
            if(trigger.isBefore && trigger.isInsert) { 
                trgHandler.RLIHandler_method(trigger.new, null, null, null, 'Before', 'Insert');    
            }
            if(trigger.isBefore && trigger.isUpdate) { 
                trgHandler.RLIHandler_method(trigger.new, null, null, null, 'Before', 'Update');
            }
            if(trigger.isAfter && trigger.isInsert) { 
                trgHandler.RLIHandler_method(trigger.new, null, null, null, 'After', 'Insert');
            }
            if(trigger.isAfter && trigger.isUpdate) { 
                trgHandler.RLIHandler_method(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, 'After', 'Update');
            }            
            if(trigger.isAfter && trigger.isDelete) { 
                trgHandler.RLIHandler_method(null, trigger.old, null, null, 'After', 'Delete');
            }       
        }
    }
}