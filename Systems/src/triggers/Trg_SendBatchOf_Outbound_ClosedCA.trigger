// Commented Windhill callout "CLONE_Batch_CA_Processing" as it is no mmore needed after B&P changes - US46069
Trigger Trg_SendBatchOf_Outbound_ClosedCA on EPA__c (Before Update) 
{
    if(Label.Handle_EPA_Callout_Trigger == 'true') //Use the Custom label value to handle the execution of this trigger)
    {
        if(Class_TriggerCheck.var_IsStopEPABatchTrg == false){
            Set<Id> St_EPA_Id = new Set<Id>();
            List<String> St_CA_Id = new List<String>();
            
            // Changes done by Kashish Dwivedi on Date - 07-Aug-2014
            // https://devcloud.swcoe.ge.com/jira02/browse/ASSFDC-297
            for(EPA__c objEPA : Trigger.new)
            {
                if(trigger.oldmap.get(objEPA.Id).Closed__c == false && objEPA.Closed__c == true)
                {
                    St_EPA_Id.add(objEPA.Id);   
                }
            }
            
           // CLONE_Batch_CA_Processing ForBatchProcess = new CLONE_Batch_CA_Processing (); 
            Batch_SendCAtoCobra executeBatchSendCAtoCobra = new Batch_SendCAtoCobra(); // Add the line for JIRA-297(https://devcloud.swcoe.ge.com/jira02/browse/ASSFDC-297) by Kashish Dwivedi on Dated - 07-Aug-2014.
            if(!St_EPA_Id.isEmpty())
            {
              // ForBatchProcess.query = 'Select Id from Cost_Authorization__c WHERE EPA_Number__c = \''+ St_EPA_Id +'\'';
              //  String Check = ForBatchProcess.query.remove('{');
              //  String CheckOne = Check.remove('}');
              //  ForBatchProcess.query = CheckOne;
                
                // changes done by Kashish Dwivedi on Dated-07-Aug-2014 for below Jira
                // https://devcloud.swcoe.ge.com/jira02/browse/ASSFDC-297
                string query_CA = 'Select ID,Concat_EPA_Number__c,Percent_Complete__c,EPA_Number__r.Kronos_Instance__c  from Cost_Authorization__c WHERE EPA_Number__c = \''+ St_EPA_Id +'\'';
                query_CA = query_CA.remove('{');
                query_CA = query_CA.remove('}');
                executeBatchSendCAtoCobra.query = query_CA; 
                // -- Changes done --
                
                if(Class_TriggerCheck.Var_SendBatch_ClosedCA){
                  //  Database.Executebatch(ForBatchProcess,1); 
                  // Added isRunningTest to avoid Callout Error during test class execution - March, 23rd 2017.
                  // Batch class is being covered by another test class "Test_Batch_SendCAtoCobra"
                  if(!Test.isRunningTest())
                        Database.Executebatch(executeBatchSendCAtoCobra,1); // Add the line for JIRA-297(https://devcloud.swcoe.ge.com/jira02/browse/ASSFDC-297) by Kashish Dwivedi on Dated - 07-Aug-2014.
                }
            }   
         
            
        
            Class_TriggerCheck.Var_SendBatch_ClosedCA = false;  
        }
    }
}