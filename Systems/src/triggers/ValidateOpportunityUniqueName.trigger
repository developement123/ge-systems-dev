Trigger ValidateOpportunityUniqueName on Opportunity (before insert, before update) 
{
    String OppRecId = Trigger.New[0].RecordTypeId;
    
  if(Label.Label_TriggerOnOpportunity=='true' && (OppRecId == Label.Unison_Record_Type_Label1 || OppRecId == Label.Unison_Record_Type_Label2 || OppRecId == Label.Unison_Record_Type_Label3)){  
    System.debug('#####In ValidateOpportunityUniqueName');
    Set<Id> AccountIDset = new Set<ID>();
    Set<Id> applicationidset = new Set<ID>();
    Set<string> forstoreoppname = new Set<string>();
    set<string> Existingopportset = new set<string>();
    Map<string,Opportunity> ExistingOpportMap = new Map<string,Opportunity>();
    List<opportunity> checkduplicacy = new List<opportunity>();
    if(trigger.isbefore)
    {
        if(trigger.isinsert && Class_TriggerCheck.var_preventOppyNameUpdate_Recursion)
        {
            Class_TriggerCheck.var_preventOppyNameUpdate_Recursion = false;      
            for (Opportunity oppt: trigger.new)
            {
                AccountIDset.add(oppt.AccountID);
                applicationidset.add(oppt.L3_Platform_Lookup__c);
            }
            Map<ID,Account>AccountNameMap = new Map<ID,Account>([select id,name from Account where id IN:AccountIDset]);
            Map<ID,Application__c>ApplicationMap = new Map<ID,Application__c>([select id,name from Application__c where id IN:applicationidset]);
            for (Opportunity opptnew: trigger.new)
            {
                system.debug('==================================AccountNameMap.get(opptnew.AccountID).name '+AccountNameMap.get(opptnew.AccountID).name);
                system.debug('==================================ApplicationMap.get(opptnew.L3_Platform_Lookup__c).name '+ApplicationMap.get(opptnew.L3_Platform_Lookup__c).name);
                system.debug('==================================opptnew.Name '+opptnew.Name);
                if(forstoreoppname.Contains(AccountNameMap.get(opptnew.AccountID).name+' - '+ApplicationMap.get(opptnew.L3_Platform_Lookup__c).name+' - '+opptnew.Name))
                {
                    opptnew.addError('You are inserting two opportunity of same name');
                }
                else
                {
                    forstoreoppname.add(AccountNameMap.get(opptnew.AccountID).name+' - '+ApplicationMap.get(opptnew.L3_Platform_Lookup__c).name+' - '+opptnew.Name);
                }
            }
            /**for(Opportunity Existingopport: [select Name from Opportunity where Name IN :forstoreoppname])
            {
               Existingopportset.add(Existingopport.name);  
            }    **/
            system.debug('==================================Existingopportset'+Existingopportset);
            for(Opportunity NewOppty: Trigger.new)
            {
                //RecordType recordtypename = [select id, name from Recordtype where SobjectType = 'opportunity' and id=:NewOppty.recordtype limit 1];
                RecordType recordtypename = [Select Id,name From RecordType  Where SobjectType = 'opportunity' and id=:NewOppty.recordtypeid limit 1];
                system.debug('==================================AccountNameMap.get(NewOppty.AccountID).name'+AccountNameMap.get(NewOppty.AccountID).name);
                system.debug('==================================ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name'+ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name);
                system.debug('==================================NewOppty.Name'+NewOppty.Name);
                if(Existingopportset.contains(AccountNameMap.get(NewOppty.AccountID).name+' - '+ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name+' - '+NewOppty.Name))
                {
                    //NewOppty.addError('Opportunity name already Exist, please enter different value in Oppty Name.');
                    NewOppty.addError('The Opportunity Name is already in use.  Please enter a different value in the Opportunity Unique Identifier field.');
                }
                else
                {
                    if(recordtypename.name == OpportunityRecordType__c.getValues('opp_recd_typ').typename2__c)             //for record type < 500K
                    {
                        if((string.valueof(NewOppty.closedate)) != null)
                        {
                            date getdate = NewOppty.closedate;
                            date newdate;
                            if((string.valueof(NewOppty.Contract_Duration_Years__c)) != null)
                            {
                                newdate = getdate.addYears(Integer.valueOf(NewOppty.Contract_Duration_Years__c));
                            }
                            else
                            {
                                newdate = getdate.addYears(Integer.valueOf(1));
                            }
                            NewOppty.Exp_date__c = newdate;
                            NewOppty.Exp_notification_date__c = newdate.addMonths(-6);
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        if((string.valueof(NewOppty.Contract_Start_Date__c)) != null)
                        {
                            date getdate = NewOppty.Contract_Start_Date__c;
                            date newdate;
                            if((string.valueof(NewOppty.Contract_Duration_Years__c)) != null)
                            {
                                newdate = getdate.addYears(Integer.valueOf(NewOppty.Contract_Duration_Years__c));
                            }
                            else
                            {
                                newdate = getdate.addYears(Integer.valueOf(1));
                            }
                            NewOppty.Exp_date__c = newdate;
                            NewOppty.Exp_notification_date__c = newdate.addMonths(-6);
                        }
                    }
                    /*if((string.valueof(NewOppty.closedate)) != null)
                    {
                        date getdate = NewOppty.closedate;
                        date newdate;
                        if((string.valueof(NewOppty.Contract_Duration_Years__c)) != null)
                        {
                            newdate = getdate.addYears(Integer.valueOf(NewOppty.Contract_Duration_Years__c));
                        }
                        else
                        {
                            newdate = getdate.addYears(Integer.valueOf(1));
                        }
                        NewOppty.Exp_date__c = newdate;
                        NewOppty.Exp_notification_date__c = newdate.addMonths(-6);
                    }*/
                    NewOppty.Name = AccountNameMap.get(NewOppty.AccountID).name+' - '+ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name+' - '+NewOppty.Name;
                }   
            }
        }
        if(trigger.isupdate && Class_TriggerCheck.var_preventOppyNameUpdate_Recursion)
        {   
            System.debug('##### In Update section');
            if(trigger.new[0].AccountId != trigger.old[0].AccountId || trigger.new[0].L3_Platform_Lookup__c != trigger.old[0].L3_Platform_Lookup__c || trigger.new[0].Name != trigger.old[0].Name){
                System.debug('##### In Account/L3 Product/Name change section');
                for (Opportunity oppt: trigger.new)
                {
                    AccountIDset.add(oppt.AccountID);
                    applicationidset.add(oppt.L3_Platform_Lookup__c);
                }
                Map<ID,Account>AccountNameMap = new Map<ID,Account>([select id,name from Account where id IN:AccountIDset]);
                Map<ID,Application__c>ApplicationMap = new Map<ID,Application__c>([select id,name from Application__c where id IN:applicationidset]);
                for (Opportunity opptnew: trigger.new)
                {
                    if(forstoreoppname.Contains(AccountNameMap.get(opptnew.AccountID).name+' - '+ApplicationMap.get(opptnew.L3_Platform_Lookup__c).name+' - '+opptnew.Name))
                    {
                        opptnew.addError('you are updating two opportunity of same name');
                    }
                    else
                    {
                        forstoreoppname.add(AccountNameMap.get(opptnew.AccountID).name+' - '+ApplicationMap.get(opptnew.L3_Platform_Lookup__c).name+' - '+opptnew.Name);
                    }
                }
                /**for(Opportunity Existingopport: [select id, Name from Opportunity where Name IN :forstoreoppname])
                {
                   Existingopportset.add(Existingopport.name);
                   ExistingOpportMap.put(Existingopport.name,Existingopport);   
                }  **/
                for(Opportunity NewOppty: Trigger.new)
                {
                    RecordType recordtypename = [Select Id,name From RecordType  Where SobjectType = 'opportunity' and id=:NewOppty.recordtypeid limit 1];
                    if(Existingopportset.contains(AccountNameMap.get(NewOppty.AccountID).name+' - '+ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name+' - '+NewOppty.Name))
                    {
                        string opportunityname = AccountNameMap.get(NewOppty.AccountID).name+' - '+ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name+' - '+NewOppty.Name;
                        if(ExistingOpportMap.get(opportunityname).id != NewOppty.id)
                        {
                            //NewOppty.addError('Opportunity name already Exist, please update different value in Oppty Name.');
                            NewOppty.addError('The Opportunity Name is already in use.  Please enter a different value in the Opportunity Unique Identifier field.');
                        }
                        else
                        {
                            if(recordtypename.name == OpportunityRecordType__c.getValues('opp_recd_typ').typename2__c)             //for record type < 500K
                            {
                                if((string.valueof(NewOppty.closedate)) != null)
                                {                   
                                    date getdate = NewOppty.closedate;
                                    date newdate;
                                    if((string.valueof(NewOppty.Contract_Duration_Years__c)) != null)
                                    {
                                        newdate = getdate.addYears(Integer.valueOf(NewOppty.Contract_Duration_Years__c));
                                    }
                                    else
                                    {
                                        newdate = getdate.addYears(1);
                                    }
                                    NewOppty.Exp_date__c = newdate;
                                    NewOppty.Exp_notification_date__c = newdate.addMonths(-6);
                                }
                                else
                                {
                                }
                            }
                            else
                            {
                                if((string.valueof(NewOppty.Contract_Start_Date__c)) != null)
                                {
                                    date getdate = NewOppty.Contract_Start_Date__c;
                                    date newdate;
                                    if((string.valueof(NewOppty.Contract_Duration_Years__c)) != null)
                                    {
                                        newdate = getdate.addYears(Integer.valueOf(NewOppty.Contract_Duration_Years__c));
                                    }
                                    else
                                    {
                                        newdate = getdate.addYears(Integer.valueOf(1));
                                    }
                                    NewOppty.Exp_date__c = newdate;
                                    NewOppty.Exp_notification_date__c = newdate.addMonths(-6);
                                }
                                else
                                {
                                }
                            }
                            NewOppty.Name = AccountNameMap.get(NewOppty.AccountID).name+' - '+ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name+' - '+NewOppty.Name;
                        }
                    }
                    else
                    {
                        if(recordtypename.name == OpportunityRecordType__c.getValues('opp_recd_typ').typename2__c)             //for record type < 500K
                        {
                            if((string.valueof(NewOppty.closedate)) != null)
                            {                   
                                date getdate = NewOppty.closedate;
                                date newdate;
                                if((string.valueof(NewOppty.Contract_Duration_Years__c)) != null)
                                {
                                    newdate = getdate.addYears(Integer.valueOf(NewOppty.Contract_Duration_Years__c));
                                }
                                else
                                {
                                    newdate = getdate.addYears(1);
                                }
                                NewOppty.Exp_date__c = newdate;
                                NewOppty.Exp_notification_date__c = newdate.addMonths(-6);
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                            if((string.valueof(NewOppty.Contract_Start_Date__c)) != null)
                            {
                                date getdate = NewOppty.Contract_Start_Date__c;
                                date newdate;
                                if((string.valueof(NewOppty.Contract_Duration_Years__c)) != null)
                                {
                                    newdate = getdate.addYears(Integer.valueOf(NewOppty.Contract_Duration_Years__c));
                                }
                                else
                                {
                                    newdate = getdate.addYears(Integer.valueOf(1));
                                }
                                NewOppty.Exp_date__c = newdate;
                                NewOppty.Exp_notification_date__c = newdate.addMonths(-6);
                            }
                            else
                            {
                            }
                        }
                        NewOppty.Name = AccountNameMap.get(NewOppty.AccountID).name+' - '+ApplicationMap.get(NewOppty.L3_Platform_Lookup__c).name+' - '+NewOppty.Name;
                    }   
                }
            }
        }
    }
  }  
 }