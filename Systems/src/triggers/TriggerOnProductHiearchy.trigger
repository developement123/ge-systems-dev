/****************************************************************************
    Developer : Abhishek
    Date      : 29/June/2016    
    Action    : This trigger invokes handler class "ProductHierarchyTriggerHandler"

    Change desription:
    Developer         Date                  Description
*****************************************************************************/
trigger TriggerOnProductHiearchy on Product_Hiearchy_Object__c (Before delete) {

if(Label.Label_TriggerOnProductHiearchy== 'true'){
    if(Class_TriggerCheck.Var_StopProductHiearchy){
        ProductHierarchyTriggerHandler trgHandler = new ProductHierarchyTriggerHandler();
        if(trigger.isBefore && trigger.isDelete) {
           trgHandler.ValidateProductHierarchyOnDelete(trigger.oldMap, 'Before', 'Delete');
        }
    }
 }
}