/******************************************************************************************************************************************
    Author   : Jhansi B    
    Date     : 15/October/2015
    Action   : This trigger is invoking handler class 'MarginEscalatorTriggerHandler'.   
******************************************************************************************************************************************/

trigger TriggerOnMarginEscalator on Margin_Escalator__c(before insert, after insert, before update, after update, after delete, after undelete) {
      if(Label.start_stop_trigger_when_ME_created_manually == 'true') {  
     //   if(Class_TriggerCheck.var_TriggerOnMarginEscalator) { //No need of this as of now, because separate static variables has been used to stop the logic in before/after events in method 'createUpdateMarginEscalator'
            
            MarginEscalatorTriggerHandler trgHandler = new MarginEscalatorTriggerHandler();
            
            if(trigger.isBefore && trigger.isInsert)   
                trgHandler.createUpdateMarginEscalator(trigger.new, null, null, null, 'Before', 'Insert');
                
            else if(trigger.isBefore && trigger.isUpdate)  
                trgHandler.createUpdateMarginEscalator(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'Before', 'Update');  
           
            else if(trigger.isAfter && trigger.isInsert)
                trgHandler.createUpdateMarginEscalator(trigger.new, null, null, null, 'After', 'Insert');
           
            else if(trigger.isAfter && trigger.isUpdate) 
                trgHandler.createUpdateMarginEscalator(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');   
           
            else if(trigger.isAfter && trigger.isDelete)
                trgHandler.createUpdateMarginEscalator(null, trigger.old, null, null, 'After', 'Delete');
           
            else if(trigger.isAfter && trigger.isUnDelete)
                trgHandler.createUpdateMarginEscalator(trigger.new, null, null, null, 'After', 'Undelete');   
     //   }    
    }          
}