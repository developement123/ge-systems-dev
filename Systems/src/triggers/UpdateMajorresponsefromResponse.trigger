// Developer: Sunny Kapoor
// This trigger will setup Rollup for Total Amount, CM%,Avg Close Probability, Engineering Cost, Manufacturing Cost, Service Cost, Total Approved Budget, 
// Total Spend fields calculated from Related Responses

// Last Modified by - Kashish Dwivedi on 10-March-2015 as per JIRA - 296
// Changes done - comment out the SOQL query written at Line no 23 and put the
// query inside the condition (to check set as non empty) on line no 25,26.
 
trigger UpdateMajorresponsefromResponse on Sales_Request__c (after update) {
    if(Label.Label_UpdateMajorresponsefromResponse == 'true') {
        if(Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg) {
            Set<Id> MajRespId = new Set<Id>(); 
            List <Major_Proposal__c> MajorrespList  = new List <Major_Proposal__c>();
         
                 
            if(Trigger.isupdate)
            { 
            
                for(Sales_Request__c resp : trigger.new)
                {
                    MajRespId.add(resp.Major_Proposals__c);
                }
                
                //MajorrespList = [Select ID,Name,Engineering_Cost__c,Manufacturing_Cost__c,Service_Cost__c,Total_Approved_Budget__c,Avg_Close_Probability__c,Total_Amount__c,CM__c,Total_Spend__c,(Select Id,CM2__c,Total_Spent__c,Estimated_Manufacturing_Cost__c,Total_Approved_Budget__c,Avg_Close_Probability2__c,Total_Amount2__c,Estimated_Engineering_Cost__c,Service_Cost__c From Sales_Requests__r where Id IN: trigger.new) From Major_Proposal__c where ID IN: MajRespId];
                if(!MajRespId.isEmpty()){
                    MajorrespList = [Select ID,Name,Engineering_Cost__c,Manufacturing_Cost__c,Service_Cost__c,Total_Approved_Budget__c,Avg_Close_Probability__c,Total_Amount__c,CM__c,Total_Spend__c,(Select Id,CM2__c,Total_Spent__c,Estimated_Manufacturing_Cost__c,Total_Approved_Budget__c,Avg_Close_Probability2__c,Total_Amount2__c,Estimated_Engineering_Cost__c,Service_Cost__c From Sales_Requests__r where Id IN: trigger.new) From Major_Proposal__c where ID IN: MajRespId];
                }
            
                for(Major_Proposal__c mr : MajorrespList)
                { 
                    if (mr.Manufacturing_Cost__c == NULL)
                    {
                        mr.Manufacturing_Cost__c = 0;
                    }
                    if (mr.Total_Approved_Budget__c == NULL)
                    {
                        mr.Total_Approved_Budget__c = 0;
                    }
                    if (mr.Avg_Close_Probability__c == NULL)
                    {
                        mr.Avg_Close_Probability__c = 0;
                    }  
                    if (mr.Total_Amount__c == NULL)
                    {
                        mr.Total_Amount__c = 0;
                    }   
                    if (mr.CM__c == NULL)
                    {
                        mr.CM__c = 0;
                    }       
                    if (mr.Engineering_Cost__c == NULL)
                    {
                        mr.Engineering_Cost__c = 0;
                    }           
                    if (mr.Service_Cost__c == NULL)
                    {
                        mr.Service_Cost__c = 0;
                    }
                    if (mr.Total_Spend__c == NULL)
                    {
                        mr.Total_Spend__c = 0;
                    }
                    
                    for(Sales_Request__c resp : mr.Sales_Requests__r)
                    {
                        // If Major Response Lookup is different***************************
                 
                        if(Trigger.oldMap.get(resp.id).Major_Proposals__c != Trigger.newMap.get(resp.id).Major_Proposals__c) 
                        { 
                            // Manufacturing Cost**************************
                            if(Trigger.oldMap.get(resp.id).Estimated_Manufacturing_Cost__c ==  Trigger.newMap.get(resp.id).Estimated_Manufacturing_Cost__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).Estimated_Manufacturing_Cost__c != NULL)
                                {
                                    mr.Manufacturing_Cost__c += resp.Estimated_Manufacturing_Cost__c; 
                                }
                            }
                            
                            // Total Approved Budget**************************
                            if(Trigger.oldMap.get(resp.id).Total_Approved_Budget__c ==  Trigger.newMap.get(resp.id).Total_Approved_Budget__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).Total_Approved_Budget__c != NULL)
                                {
                                    mr.Total_Approved_Budget__c += resp.Total_Approved_Budget__c; 
                                }
                            }
                            
                            // Avg Close Probability**************************
                            if(Trigger.oldMap.get(resp.id).Avg_Close_Probability2__c ==  Trigger.newMap.get(resp.id).Avg_Close_Probability2__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).Avg_Close_Probability2__c != NULL)
                                {
                                    mr.Avg_Close_Probability__c += resp.Avg_Close_Probability2__c; 
                                }
                            }
                        
                            // Total Amount**************************
                            if(Trigger.oldMap.get(resp.id).Total_Amount2__c ==  Trigger.newMap.get(resp.id).Total_Amount2__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).Total_Amount2__c != NULL)
                                {
                                    mr.Total_Amount__c += resp.Total_Amount2__c; 
                                }
                            }
                            
                            // CM%**************************
                            if(Trigger.oldMap.get(resp.id).CM2__c ==  Trigger.newMap.get(resp.id).CM2__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).CM2__c != NULL)
                                {
                                    mr.CM__c += resp.CM2__c; 
                                }
                            }
                            
                            // Engineering Cost**************************
                            if(Trigger.oldMap.get(resp.id).Estimated_Engineering_Cost__c ==  Trigger.newMap.get(resp.id).Estimated_Engineering_Cost__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).Estimated_Engineering_Cost__c != NULL)
                                {
                                    mr.Engineering_Cost__c += resp.Estimated_Engineering_Cost__c; 
                                }
                            }
                            
                            // Service Cost**************************
                            if(Trigger.oldMap.get(resp.id).Service_Cost__c ==  Trigger.newMap.get(resp.id).Service_Cost__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).Service_Cost__c != NULL)
                                {
                                    mr.Service_Cost__c += resp.Service_Cost__c; 
                                }
                            }
                            
                            // Total Spend**************************
                            if(Trigger.oldMap.get(resp.id).Total_Spent__c ==  Trigger.newMap.get(resp.id).Total_Spent__c) 
                            { 
                                if(Trigger.newMap.get(resp.id).Total_Spent__c != NULL)
                                {
                                    mr.Total_Spend__c += resp.Total_Spent__c; 
                                }
                            }
                        }    
        
                        // If Major Response Lookup is same***************************
                        
                        else if(Trigger.oldMap.get(resp.id).Major_Proposals__c == Trigger.newMap.get(resp.id).Major_Proposals__c)
                        {
                             // Manufacturing Cost**************************
                            if(Trigger.oldMap.get(resp.id).Estimated_Manufacturing_Cost__c !=  Trigger.newMap.get(resp.id).Estimated_Manufacturing_Cost__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).Estimated_Manufacturing_Cost__c != NULL){
                                    mr.Manufacturing_Cost__c -= Trigger.oldMap.get(resp.id).Estimated_Manufacturing_Cost__c; 
                                }
                                mr.Manufacturing_Cost__c += Trigger.newMap.get(resp.id).Estimated_Manufacturing_Cost__c; 
        
                            } 
                            
                            // Total Approved Budget**************************
                            
                            if(Trigger.oldMap.get(resp.id).Total_Approved_Budget__c !=  Trigger.newMap.get(resp.id).Total_Approved_Budget__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).Total_Approved_Budget__c != NULL){
                                    mr.Total_Approved_Budget__c -= Trigger.oldMap.get(resp.id).Total_Approved_Budget__c; 
                                }
                                mr.Total_Approved_Budget__c += Trigger.newMap.get(resp.id).Total_Approved_Budget__c; 
        
                            }
                            
                            // Avg Close Probability**************************
                            
                            if(Trigger.oldMap.get(resp.id).Avg_Close_Probability2__c !=  Trigger.newMap.get(resp.id).Avg_Close_Probability2__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).Avg_Close_Probability2__c != NULL){
                                    mr.Avg_Close_Probability__c -= Trigger.oldMap.get(resp.id).Avg_Close_Probability2__c; 
                                }
                                mr.Avg_Close_Probability__c += Trigger.newMap.get(resp.id).Avg_Close_Probability2__c; 
        
                            }
                            
                            // Total Amount**************************
                            
                            if(Trigger.oldMap.get(resp.id).Total_Amount2__c !=  Trigger.newMap.get(resp.id).Total_Amount2__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).Total_Amount2__c != NULL){
                                    mr.Total_Amount__c -= Trigger.oldMap.get(resp.id).Total_Amount2__c; 
                                }
                                mr.Total_Amount__c += Trigger.newMap.get(resp.id).Total_Amount2__c; 
        
                            }
                            
                            // CM%**************************
                            
                            if(Trigger.oldMap.get(resp.id).CM2__c !=  Trigger.newMap.get(resp.id).CM2__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).CM2__c != NULL){
                                    mr.CM__c -= Trigger.oldMap.get(resp.id).CM2__c; 
                                }
                                mr.CM__c += Trigger.newMap.get(resp.id).CM2__c; 
        
                            }
                            
                            // Engineering Cost**************************
                            
                               if(Trigger.oldMap.get(resp.id).Estimated_Engineering_Cost__c !=  Trigger.newMap.get(resp.id).Estimated_Engineering_Cost__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).Estimated_Engineering_Cost__c != NULL){
                                    mr.Engineering_Cost__c -= Trigger.oldMap.get(resp.id).Estimated_Engineering_Cost__c; 
                                }
                                mr.Engineering_Cost__c += Trigger.newMap.get(resp.id).Estimated_Engineering_Cost__c; 
        
                            }
                            
                            // Service Cost**************************
                            
                               if(Trigger.oldMap.get(resp.id).Service_Cost__c !=  Trigger.newMap.get(resp.id).Service_Cost__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).Service_Cost__c != NULL){
                                    mr.Service_Cost__c -= Trigger.oldMap.get(resp.id).Service_Cost__c; 
                                }
                                mr.Service_Cost__c += Trigger.newMap.get(resp.id).Service_Cost__c; 
        
                            }
                            
                            // Total Spend**************************
                            
                               if(Trigger.oldMap.get(resp.id).Total_Spent__c !=  Trigger.newMap.get(resp.id).Total_Spent__c)
                            {   
                                if(Trigger.oldMap.get(resp.id).Total_Spent__c != NULL){
                                    mr.Total_Spend__c -= Trigger.oldMap.get(resp.id).Total_Spent__c; 
                                }
                                mr.Total_Spend__c += Trigger.newMap.get(resp.id).Total_Spent__c; 
        
                            }
                        }       
                    } 
                }
            }
            
            update MajorrespList;  
            
        Class_TriggerCheck.var_UpdateMajorresponsefromResponseTrg = false;
        }
    }   
}