/*
    Author: Jeetesh Bahuguna
    This trigger will change the status of related cost authorization record 
    to closed when EPA closed checkbox is checked.
    */
trigger Trg_update_Cost_Authorization_status on EPA__c (after update) {
    if(Label.Label_Trg_update_Cost_Authorization_status == 'true'){
        if(Class_TriggerCheck.Var_ShouldEPA_update) {
            list<Cost_Authorization__c> list_Cost_authToupdate= new list<Cost_Authorization__c>();//list of CostAuthrazation object to hold the records which needs to be update
            List<EPA__c> List_EpaTowork=Trigger.new; // list of EPA objects which holds the records on which we need to work.
            set<id> set_EpaIds= new set<id>();//set of EPA id's to get the related cost Autharization records
            set<id> set_OpenEpaIds= new set<id>();
            set<Cost_Authorization__c> set_Cost_authToupdate=new set<Cost_Authorization__c>();
            map<id, Boolean> map_isClosedChecked= new map<id, Boolean>();
            id EPA_owner;
            Boolean isClosed;
    
            for(EPA__c loop_Epa: List_EpaTowork) // iterating the list
            {
                isClosed=false;
                if(loop_Epa.Closed__c==true)// check if Closed checkbox in EPA object is checked.
                {
                    EPA_owner=loop_Epa.OwnerId;
                    set_EpaIds.add(loop_Epa.Id); // putting the EPA is in a set.
                    isClosed= true;
                } 
                else 
                {
                    for(EPA__c oldEPA: Trigger.old)
                    {
                        if(loop_Epa.OwnerId!=oldEPA.OwnerId)
                        {
                            set_EpaIds.add(loop_Epa.Id);
                        }
                    }
                    
                }
                map_isClosedChecked.put(loop_Epa.id,isClosed);    
            }
    
            //Query for cost autharization records those are related EPA.closed is checked
            List<Cost_Authorization__c> list_costAuth=[select id, Cost_Authorization__c.Status__c,EPA_Number__c from Cost_Authorization__c where Cost_Authorization__c.EPA_Number__c IN:set_EpaIds];
            system.debug('List size>>>--'+list_costAuth.size());
            
            for(Cost_Authorization__c loop_costAuth: list_costAuth)
            {
                if(map_isClosedChecked.get(loop_costAuth.EPA_Number__c)){
                    loop_costAuth.Status__c='Closed'; //Changing the status for cost autharization records.
                    set_Cost_authToupdate.add(loop_costAuth);//adding value in a set
                }
                else if(loop_costAuth.Status__c!='Closed')
                {
                    set_Cost_authToupdate.add(loop_costAuth);
                    Class_TriggerCheck.Var_Epa_OwnerChanged=true;
                }
                
            }
    
            list_Cost_authToupdate.addAll(set_Cost_authToupdate); // Adding updated records in list to update.
            Class_TriggerCheck.Var_ShouldCA_update=false;
            Class_TriggerCheck.Var_ShouldEPA_update = false;    
            Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg = false;  // Add condition for JIRA-297 to control the execution of CA outbound trigger (Trg_sendCA). 
            Class_TriggerCheck.var_IsExecuteResOutboundCallTrg = false; // Add condition for JIRA-297 to control the execution of Response outbound trigger (Trg_sendRESP). 
            Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg = false;   // Added for JIRA 360.
           
            update list_Cost_authToupdate;    //Update the the records. 
        }
    }
}