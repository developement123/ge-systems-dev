/************************************************************************************************************
    Author   : Mohan Kumar
    Date     : 25/November/2015
    Action   : This trigger is invoking handler class : 'CustomContractTriggerHandler'.
    
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        07/26/2016      called this method "validateProgramMangerOnContract" on insert and update event 
    Abhishek        09/20/2016      called this method "updateChildAccountOnPOForecastOpp" on update event - US18854
    Abhishek        10/17/2016      called his method "tokenizeContractWhenOppIsTokenized" on before update event also to update account platform if needed
    Abhishek        10/26/2016      called this method "updateContractonOppRes" on After Update
************************************************************************************************************/
trigger TriggerOnCustomContract on Contract_PO__c(before insert, after insert, before update, after update, before delete) {
    if(Label.Label_TriggerOnContract == 'true') { 
        if(Class_TriggerCheck.var_TriggerOnContract) {
        
            CustomContractTriggerHandler trgHandler = new CustomContractTriggerHandler();
            if(trigger.isBefore && trigger.isInsert) {
               trgHandler.tokenizeContractWhenOppIsTokenized(trigger.new, null, 'before', 'insert');
               trgHandler.validateProgramMangerOnContract(trigger.new); 
               trgHandler.updateAccountsOnContract(trigger.new); 
            } else if(trigger.isAfter && trigger.isInsert) {
               trgHandler.generateOrdersFromContract(trigger.new); 
               trgHandler.updateContractonOppRes(trigger.new, null, 'After', 'Insert');
            } else if(trigger.isBefore && trigger.isUpdate){   // for update renewal opportunityes when checkboxes checked.
                trgHandler.tokenizeContractWhenOppIsTokenized(trigger.new, trigger.oldMap, 'before', 'update');
                trgHandler.validateProgramMangerOnContract(trigger.new);
                trgHandler.UpdateRenewalOppOnContractCheckboxes(trigger.new, trigger.oldMap, 'Before', 'Update');
                } else if(trigger.isAfter && trigger.isUpdate){   // To rebuild Initial and Renewal Opp on InitialTermDate update
                    trgHandler.generateOppOnIntialTermEndDateUpdate(trigger.new, trigger.oldMap,trigger.newMap, 'after', 'Update');
                    trgHandler.updateChildAccountOnPOForecastOpp(trigger.new, trigger.oldMap,trigger.newMap);
                    trgHandler.updateContractonOppRes(trigger.new,trigger.oldMap,'After','Update');
                    } else if(trigger.isBefore && trigger.isDelete){  // for update renewal opportunityes when checkboxes checked.
                        trgHandler.ContractValidationOnDelete(trigger.old, 'Before', 'Delete');
                        trgHandler.deleteOrdersFromContract(trigger.oldMap, 'Before', 'Delete'); // put always at end of Delete Trigger
                    } 
        } 
    }   
}