/**************************************************************************************************************************************************************
    Developer : Mohan Kumar
    Date      : 30/Jan/2015    
    Action    : This trigger will invoke handler class: OpportunityTrigger_Handler.  

    Change desription:
    Developer         Date                  Description
    Jhansi            22d March 2016        Called this method "SendingEmailtoFinanceandsalesOnDelete" to send emails before Opp deletion 
    Abhishek          04/08/2016(mm/dd/yyyy)  Called this method "deleteContractPoOppOnPipelineUpdate" to delete Contract/Po Forecast opp as per Jira - 537
    Abhishek          04/15/2016              Called this method "deleteSchedulesOnOppDelete" to delete schedules as per Jira - 511
    Abhishek          05/20/2016              Called this method "UpdateSendEmailFieldOnResponse" to update email field on response -  Jira  570
    Abhishek           07/13/2015             Called this method "UpdateSalesOwnerOnResponse" to update response' sales owner -  Jira  585
    Abhishek                                  Called this method "InsertOppTeamNonPipelineOpp" to add Pipeline Opp Creator in team meber in order to avoid NPE issue at time PO/Intial and Renewal generation
    Abhishek          12/23/2016              called this method "UpdateDigitalCOntract" to update status of Digital opp - US39238(Prev it was being done by process builder but same cannot be used as per new change)
**************************************************************************************************************************************************************/

trigger TriggerOnOpportunity on Opportunity(before insert, after insert,before update, after update, before delete, after delete, after undelete) {
    String OppRecId;
    if(Trigger.New != null){
        OppRecId = Trigger.New[0].RecordTypeId;
    } else {
        OppRecId = Trigger.Old[0].RecordTypeId;
    }
    
    if(Label.Label_TriggerOnOpportunity == 'true' && (OppRecId <> Label.Unison_Record_Type_Label1 && OppRecId <> Label.Unison_Record_Type_Label2 && OppRecId <> Label.Unison_Record_Type_Label3)) {  
        System.debug('####In TriggerOnOpportunity');
        if(Class_TriggerCheck.var_TriggerOnOpportunityTrg) {  
            OpportunityTrigger_Handler oppyHandler = new OpportunityTrigger_Handler();
            OpportunityTrigger_AddShare oppyShare = new OpportunityTrigger_AddShare();
            if(trigger.isBefore && trigger.isInsert) {
                oppyHandler.populateLookupHierfromProg(trigger.new); 
                oppyHandler.autoGenerateOppyname(trigger.new, null, null, null, 'Before', 'Insert');
                oppyHandler.tokenizeOppyWhenAccPlatIsTokenized(trigger.new); 
            }
            
            if(trigger.isBefore && trigger.isUpdate) {
                oppyHandler.autoGenerateOppyname(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, 'Before', 'Update');
                oppyHandler.OppValidationOnUpdate(trigger.new, trigger.oldMap);
                oppyHandler.deleteContractPoOppOnPipelineUpdate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, 'Before', 'Update');
                oppyHandler.UpdateSendEmailFieldOnResponse(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
            }
            
            if(trigger.isAfter) {
                if(trigger.isInsert) {
                    oppyShare.InsertOppTeamNonPipelineOpp(trigger.new, null, null, null, 'After', 'Insert');
                    oppyHandler.updateIdeationFromOppy(trigger.new, null, null, null, 'After', 'Insert');
                    oppyHandler.updateInitailTermOppfromOpp(trigger.new, null, null, null, 'After', 'Insert');
                }
                
                if(trigger.IsAfter && trigger.isUpdate) {
                    oppyHandler.updateIdeationFromOppy(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, 'After', 'Update');
                    oppyHandler.tokenizeAssoProdWhenOppyisTokenized(trigger.new, trigger.oldMap);
                    oppyHandler.updateColaAmount(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, 'After', 'Update');
                    oppyHandler.UpdateAssociatedProductFields(trigger.new, null, trigger.newMap, trigger.oldMap, 'After', 'Update');
                    oppyHandler.tokenize_AP_Res_When_Opp_Tokenized(trigger.new, trigger.newMap, trigger.oldMap);
                    oppyHandler.UpdateSalesOwnerOnResponse(trigger.new, trigger.newMap, trigger.oldMap);
                    oppyHandler.UpdateDigitalCOntract(trigger.new, trigger.newMap, trigger.oldMap);
                }
               
                
                if(trigger.isDelete) {
                    oppyHandler.updateIdeationFromOppy(null, trigger.old, null, null, 'After', 'Delete');
                }
                
                if(trigger.isUndelete) 
                    oppyHandler.updateIdeationFromOppy(trigger.new, null, null, null, 'After', 'Undelete');
            }
            
             if(trigger.IsBefore && trigger.isDelete){
                  oppyHandler.OppValidationOnDelete(trigger.oldmap, 'Before', 'Delete');
                  oppyHandler.SendingEmailtoFinanceandsalesOnDelete(trigger.oldMap);
                  oppyHandler.deleteSchedulesOnOppDelete(trigger.oldMap);
                }
        }
    }
}