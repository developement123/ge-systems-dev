/********************************************************************************************************
 * Created By       :   Manjari Singh
 * LastModifiedBy   :   
 * LastmodifiedOn   :   
 * Description      :   The trigger is for creating a feed item when document is associated with the library.
 *                      It also associates the document with the other Agreement folder
 * ---------------------------------------------------------------------------------------------------------
 * Revision History
 * January 24, 2017 - Initial creation
 
 ****************************************************************************************************/
trigger InsertContentDoc on ContentVersion (after update) {
    try{
        List<FeedItem> fi = new List<FeedItem>();
        List <ContentWorkspaceDoc> cwd = new List <ContentWorkspaceDoc>();
      
        for(ContentVersion cv : trigger.new)
        {
            if(cv.Agreement__c!=null)
            {   
                List <FeedItem> isfi = [select id, RelatedRecordId from feeditem where parentId=:cv.Agreement__c];
                integer i = 0;
                for(FeedItem fitem : isfi)
                {
                    if(fitem.RelatedRecordId == cv.Id)
                        i++;
                }
                system.debug(i);
                if(i==0)
                {
                    FeedItem feed = new feeditem(   
                        PARENTID= cv.Agreement__c,
                        //Visibility='AllUsers',
                        type='ContentPost',
                        RelatedRecordId=cv.Id);
                        fi.add(feed);
                    
                 }
                    List <ContentWorkspace> cw = [select id from ContentWorkspace where name = 'Agreements - All'];
                    List <ContentWorkspaceDoc> iscw = [select id from ContentWorkspaceDoc where ContentWorkspaceId = :cw[0].id and ContentDocumentId = :cv.ContentDocumentId];
                    system.debug('***************'+iscw.size()+'************************'); 
                           
                    if(iscw.size() == 0)
                    {
                            system.debug('************************'+cw+'************************');
                            system.debug('*************************'+cw[0].Id+'******************');
                            ContentWorkspaceDoc docLink = new ContentWorkspaceDoc();
                            docLink.ContentDocumentId = cv.ContentDocumentId;
                            docLink.ContentWorkspaceId = cw[0].id;
                            cwd.add(docLink);                       
                            
                    }
               
            }    
            insert fi;
            insert cwd;
        }
    }
    catch(DMLException e)
    {
        System.debug('Error:' + e);  
    }
    finally{}
}