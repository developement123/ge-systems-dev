/*********************************************************************************
    Author : 
    Date   : 
    
    Action : This trigger will set true for UK military flag on Major Response on creation if it is associated with the tokenized Platform master.
    
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        09/12/2017      old code was not bulkified so bulkified this trigger using map and also change the logic 
                                    to consider platform master for UK military major response 
*********************************************************************************/

trigger TokenizeMR on Major_Proposal__c (Before Insert) {
    if(Label.Label_TokenizeMR == 'true'){

        set<Id> platformIds = new set<Id>();
        map<id, Platform_Master__c> mapOfIdToPlatformMaster= new map<id, Platform_Master__c>();
        
        for(Major_Proposal__c mp : trigger.new){
            platformIds.add(mp.Platform__c);
        }
        
        if(!platformIds.isEmpty()){
            for(Platform_Master__c pm : [Select Id, Is_Tokenized__c from Platform_Master__c where Id IN : platformIds] ){
                mapOfIdToPlatformMaster.put(pm.id, pm);
            }
            
            for(Major_Proposal__c mp: trigger.new){
               if(mapOfIdToPlatformMaster.containsKey(mp.id) && mapOfIdToPlatformMaster.get(mp.id).Is_Tokenized__c){
                   mp.Is_Tokenized__c = true;
               }
            } 
        }
   }
}