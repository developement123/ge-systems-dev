/***************************************************************************************************************************************************************************
    Developer : Mohan Kumar
    Date: 30/Jan/2015
    
    Action : This trigger is modified version of trigger 'Trg_Update_EPA_owner_To_Response' and will 
                    invoke handler class 'CostAuthTrigger_Handler'.                
     Change History
    =============
    Developer       Modified On     Change description
    Abhishek        12/03/2016       have called this method "validateBPunderPipeline" - US43365            
**************************************************************************************************************************************************************************/

trigger Trg_Update_EPA_owner_To_Response_Clone on Cost_Authorization__c(before insert, before update, after insert, after update) {
   if(Label.Label_Trg_Update_EPA_owner_To_Response == 'true') {    
        if(Class_TriggerCheck.var_Trg_Update_EPA_owner_To_ResponseTrg) {
            CostAuthTrigger_Handler trgHandler = new CostAuthTrigger_Handler();
    
            if(trigger.IsBefore) {
                if(trigger.isInsert) {
                    trgHandler.validateBPTaskPerEPA(trigger.new);
                    trgHandler.validateBPunderPipeline(trigger.new);
                    trgHandler.CostAuthHandler_method(trigger.new, null, null, 'Insert');
                }
                
                else if(trigger.isUpdate) {
                    trgHandler.CostAuthHandler_method(trigger.new, trigger.oldMap, trigger.newMap, 'Update');
                }
            }  
            if(trigger.IsAfter) {
                if(trigger.isInsert) {
                    trgHandler.calculatePipelineSpendTodate(trigger.new, null, null, null, 'After', 'Insert');
                }
                
                else if(trigger.isUpdate) {
                    trgHandler.calculatePipelineSpendTodate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, 'After', 'Update');
                }
            }
        }
    } 
}