trigger GEAS_trgContactDuplicatePreventer on Contact (before insert, before update) {
    if(Label.Label_trgContactDuplicatePreventer == 'true'){
        GEAS_trgContactDuplicatePreventer_Helper contactduplicatehelper = new GEAS_trgContactDuplicatePreventer_Helper();
    	contactduplicatehelper.Contactduplicate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, 'Before','insert');
    }

}