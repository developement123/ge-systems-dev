/**************************************************************************************************
    Author   : Mohan Kumar
    Date     : 25/November/2015
    Action   : This trigger is invoking handler class : 'OrderTriggerHandler'.
    Change History
    =============
    Abhishek        03/03/2016   Called this method "OrderValidationOnDelete" on before delete to prevent order deletion   JIRA 474
***********************************************************************************************/

trigger TriggerOnOrder on Order__c(before insert, before delete) {
    if(Label.Label_TriggerOnOrder == 'true') { 
        if(Class_TriggerCheck.var_TriggerOnOrder) {
            OrderTriggerHandler trgHandler = new OrderTriggerHandler();
            
            if(trigger.isBefore && trigger.isInsert) {
                trgHandler.autoGenerateOrderName(trigger.new); 
            }
            
        if(trigger.isBefore && trigger.isDelete) {
           trgHandler.OrderValidationOnDelete(trigger.old);
        }
            
        }
    }
}