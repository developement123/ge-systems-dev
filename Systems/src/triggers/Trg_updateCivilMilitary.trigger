/*Author: Mayank Joshi
   Date: 11/7/2012 5:58 PM
   Action: This trigger is updating/inserting- Civil/Militray Attributes 
           on Account Platform from Platform Master Object.
   Logic Last Modified By: Mayank Joshi,  11/27/2012 .
   Logic Last Modified By: Abhishek,  09/12/2017  - not needed
*/
trigger Trg_updateCivilMilitary on Forecast__c(before Insert, before Update) {
  /*  if(Label.Label_Trg_updateCivilMilitary == 'true'){
        if (Class_TriggerCheck.Var_StopPlatMasterTrigger == true) {
            Set <ID> St_ForcstIDs = new Set <ID>(); //Set of Account Platform Ids
            For(Forecast__c Forecast: Trigger.new)
                //Adding Lookup Id of Platform Master to Set St_ForcstIDs 
            St_ForcstIDs.add(Forecast.Forecast_Platform__c);
    
            Map <ID, Platform_Master__c> Map_Prog_Mastr = new Map <ID, Platform_Master__c>
                ([Select ID, Civil_Military__c FROM Platform_Master__c where ID IN: St_ForcstIDs]);
            //Logic to Perform on Inserting New Records
            If(Map_Prog_Mastr.size() > 0 && Trigger.IsInsert) {
                    For(Forecast__c Frcst: trigger.new) {
                        Frcst.Civil_Military__c = Map_Prog_Mastr.get(Frcst.Forecast_Platform__c).Civil_Military__c;
                    }
                }
                //Logic to Perform on Updating Values .
            If(Map_Prog_Mastr.size() > 0 && Trigger.IsUpdate) {
                For(Forecast__c Frcst: trigger.new) {
                    Frcst.Civil_Military__c = Map_Prog_Mastr.get(Frcst.Forecast_Platform__c).Civil_Military__c;
                }
            }
        }
    }
    */
}