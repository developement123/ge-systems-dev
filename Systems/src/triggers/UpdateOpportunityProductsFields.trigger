trigger UpdateOpportunityProductsFields on OpportunityLineItem (after insert, after update, after delete) {
Set <ID> setOppID = new Set<ID>();

if(Trigger.isInsert)
{
OpportunityLineItem optyLineItem ;
Opportunity opty;
if(Trigger.isInsert)
{
for(OpportunityLineItem  lineItem: Trigger.new)
{
setOppID.add(lineItem.opportunityid);
}
Map<ID,OpportunityLineItem>  mapLineItem = new Map<ID,OpportunityLineItem>(  [Select id, PricebookEntry.Product2.L1_Product__c,PricebookEntry.Product2.L2_Product__c,PricebookEntry.Product2.Name from OpportunityLineItem where id in :Trigger.newMap.keyset()]);
Map<ID, Opportunity> mapOppty = new Map<id,Opportunity>([select id,L1_Products_List__c,L2_Product_List__c,L3_Products_List__c from Opportunity where id in :setOppID]);
for(OpportunityLineItem  lineItem: Trigger.new)
{
    optyLineItem = mapLineItem.get(lineItem.id);
    opty = mapOppty.get(lineItem.opportunityid);
    if( opty.L1_Products_List__c != null)
    {System.debug('1-------');
    if( !opty.L1_Products_List__c.contains(optyLineItem.PricebookEntry.Product2.L1_Product__c))
    {System.debug('2-------');
        opty.L1_Products_List__c = opty.L1_Products_List__c+';'+ optyLineItem.PricebookEntry.Product2.L1_Product__c;
    
    }
    }
    else
    {System.debug('3-------');
         opty.L1_Products_List__c = optyLineItem.PricebookEntry.Product2.L1_Product__c;
    }
    if( opty.L2_Product_List__c != null)
    {
    if( !opty.L2_Product_List__c.contains(optyLineItem.PricebookEntry.Product2.L2_Product__c ))
    {
        opty.L2_Product_List__c = opty.L2_Product_List__c+';'+ optyLineItem.PricebookEntry.Product2.L2_Product__c;
    
    }
    }
    else
    {
         opty.L2_Product_List__c = optyLineItem.PricebookEntry.Product2.L2_Product__c;
    }
    if( opty.L3_Products_List__c != null)
    {
    if( !opty.L3_Products_List__c.contains(optyLineItem.PricebookEntry.Product2.Name ))
    {
        opty.L3_Products_List__c =opty.L3_Products_List__c+ ';'+ optyLineItem.PricebookEntry.Product2.Name;
    
    }
    }
    else
    {
         opty.L3_Products_List__c = optyLineItem.PricebookEntry.Product2.Name;
    }
    
    mapOppty.put(opty.id, opty);
    }
    
    if(mapOppty != null && mapOppty.size() > 0)
    try{
    update mapOppty.values();
    }
    catch(Exception E)
    {
    
    //Trigger.New[0].adderror('Please change the Opportunity record type first which matches with Total Sales/Revenue '+e);
    String ExceptionType=E.getTypeName();
    if(ExceptionType.contains('DmlException'))
    {
    Trigger.New[0].adderror(''+E.getDmlMessage(0)+ ' (on the opportunity record)');
    System.debug('Exception in UpdateOpportunityProductsFields trigger'+e);
    }
    else
    {
    Trigger.New[0].adderror('Error:Please contact the System Admin');
    System.debug('Exception in UpdateOpportunityProductsFields trigger'+e);
    }
    }
    }
    }
    if(Trigger.IsUpdate)
    {
    
    for(OpportunityLineItem  lineItem: Trigger.new)
        {
            setOppID.add(lineItem.opportunityid);
        }
    Map<ID, List<OpportunityLineItem>> mapOpty_LineItemID = new Map<ID,List<OpportunityLineItem>>();
    List<Opportunity> lstOppty = new List<Opportunity>();//([select id,L1_Products_List__c,L2_Product_List__c,L3_Products_List__c from Opportunity where id in :setOppID]);
    For(OpportunityLineItem objOLI : [Select id,OpportunityId, PricebookEntry.Product2.L1_Product__c,PricebookEntry.Product2.L2_Product__c,PricebookEntry.Product2.Name from OpportunityLineItem where OpportunityId in :setOppID])
    
    {
        if(mapOpty_LineItemID.get(objOLI.OpportunityId)!= null)
        {
            
            mapOpty_LineItemID.get(objOLI.OpportunityId).add(objOLI);
        
        }
        else
        {
        mapOpty_LineItemID.put(objOLI.OpportunityId,New List<OpportunityLineItem>());
        mapOpty_LineItemID.get(objOLI.OpportunityId).add(objOLI);
        }
    }
    for(Opportunity objOpty : [select id,L1_Products_List__c,L2_Product_List__c,L3_Products_List__c from Opportunity where id in :setOppID])
    {
        objOpty.L1_Products_List__c ='';
        objOpty.L2_Product_List__c ='';
        objOpty.L3_Products_List__c ='';
    for(OpportunityLineItem optyLineItem : mapOpty_LineItemID.get(objOpty.id) ) 
    {
         if( objOpty.L1_Products_List__c != null)
    {
    if( !objOpty.L1_Products_List__c.contains(optyLineItem.PricebookEntry.Product2.L1_Product__c))
    {
        objOpty.L1_Products_List__c = objOpty.L1_Products_List__c+';'+ optyLineItem.PricebookEntry.Product2.L1_Product__c;
    
    }
    }
    else
    {
         objOpty.L1_Products_List__c = optyLineItem.PricebookEntry.Product2.L1_Product__c;
    }
    if( objOpty.L2_Product_List__c != null)
    {
    if( !objOpty.L2_Product_List__c.contains(optyLineItem.PricebookEntry.Product2.L2_Product__c ))
    {
        objOpty.L2_Product_List__c = objOpty.L2_Product_List__c+';'+ optyLineItem.PricebookEntry.Product2.L2_Product__c;
    
    }
    }
    else
    {
         objOpty.L2_Product_List__c = optyLineItem.PricebookEntry.Product2.L2_Product__c;
    }
    if( objOpty.L3_Products_List__c != null)
    {
    if( !objOpty.L3_Products_List__c.contains(optyLineItem.PricebookEntry.Product2.Name ))
    {
        objOpty.L3_Products_List__c =objOpty.L3_Products_List__c+ ';'+ optyLineItem.PricebookEntry.Product2.Name;
    
    }
    }
    else
    {
         objOpty.L3_Products_List__c = optyLineItem.PricebookEntry.Product2.Name;
    }
    
   
    }
     lstOppty.add(objOpty);
    }
        
 if(lstOppty != null && lstOppty.size() > 0)
 try{
    update lstOppty;
    }
    catch(Exception E)
    {
    
   // Trigger.New[0].adderror('Please change the Opportunity record type first which matches with Total Sales/Revenue.');
    String ExceptionType=E.getTypeName();
    if(ExceptionType.contains('DmlException'))
    {
    Trigger.New[0].adderror(''+E.getDmlMessage(0)+ ' (on the opportunity record)');
    System.debug('Exception in UpdateOpportunityProductsFields trigger'+e);
    }
    else
    {
    Trigger.New[0].adderror('Error:Please contact the System Admin');
    System.debug('Exception in UpdateOpportunityProductsFields trigger'+e);
    }
    
    
    
    
    }
    }

    if (Trigger.isDelete)
    {
    for(OpportunityLineItem  lineItem: Trigger.Old)
        {
            setOppID.add(lineItem.opportunityid);
        }
    Map<ID, List<OpportunityLineItem>> mapOpty_LineItemID = new Map<ID,List<OpportunityLineItem>>();
    List<Opportunity> lstOppty = new List<Opportunity>();//([select id,L1_Products_List__c,L2_Product_List__c,L3_Products_List__c from Opportunity where id in :setOppID]);
    For(OpportunityLineItem objOLI : [Select id,OpportunityId, PricebookEntry.Product2.L1_Product__c,PricebookEntry.Product2.L2_Product__c,PricebookEntry.Product2.Name from OpportunityLineItem where OpportunityId in :setOppID])
    
    {
        if(mapOpty_LineItemID.get(objOLI.OpportunityId)!= null)
        {
            
            mapOpty_LineItemID.get(objOLI.OpportunityId).add(objOLI);
        
        }
        else
        {
        mapOpty_LineItemID.put(objOLI.OpportunityId,New List<OpportunityLineItem>());
        mapOpty_LineItemID.get(objOLI.OpportunityId).add(objOLI);
        }
    }
    for(Opportunity objOpty : [select id,L1_Products_List__c,L2_Product_List__c,L3_Products_List__c from Opportunity where id in :setOppID])
    {
        objOpty.L1_Products_List__c ='';
        objOpty.L2_Product_List__c ='';
        objOpty.L3_Products_List__c ='';
if( mapOpty_LineItemID.get(objOpty.id)!= null)
{
    for(OpportunityLineItem optyLineItem : mapOpty_LineItemID.get(objOpty.id) ) 
    {
         if( objOpty.L1_Products_List__c != null)
    {
    if( !objOpty.L1_Products_List__c.contains(optyLineItem.PricebookEntry.Product2.L1_Product__c))
    {
        objOpty.L1_Products_List__c = objOpty.L1_Products_List__c+';'+ optyLineItem.PricebookEntry.Product2.L1_Product__c;
    
    }
    }
    else
    {
         objOpty.L1_Products_List__c = optyLineItem.PricebookEntry.Product2.L1_Product__c;
    }
    if( objOpty.L2_Product_List__c != null)
    {
    if( !objOpty.L2_Product_List__c.contains(optyLineItem.PricebookEntry.Product2.L2_Product__c ))
    {
        objOpty.L2_Product_List__c = objOpty.L2_Product_List__c+';'+ optyLineItem.PricebookEntry.Product2.L2_Product__c;
    
    }
    }
    else
    {
         objOpty.L2_Product_List__c = optyLineItem.PricebookEntry.Product2.L2_Product__c;
    }
    if( objOpty.L3_Products_List__c != null)
    {
    if( !objOpty.L3_Products_List__c.contains(optyLineItem.PricebookEntry.Product2.Name ))
    {
        objOpty.L3_Products_List__c =objOpty.L3_Products_List__c+ ';'+ optyLineItem.PricebookEntry.Product2.Name;
    
    }
    }
    else
    {
         objOpty.L3_Products_List__c = optyLineItem.PricebookEntry.Product2.Name;
    }
    }
   
    }
     lstOppty.add(objOpty);
    }
        
 if(lstOppty != null && lstOppty.size() > 0)
  try{  update lstOppty;
    }
    catch(Exception E)
    {
    
    //Trigger.Old[0].adderror(E+'Please change the Opportunity record type first which matches with Total Sales/Revenue '+e);
    String ExceptionType=E.getTypeName();
    if(ExceptionType.contains('DmlException'))
    {
    Trigger.Old[0].adderror(''+E.getDmlMessage(0)+ ' (on the opportunity record)');
    System.debug('Exception in UpdateOpportunityProductsFields trigger'+e);
    }
    else
    {
    Trigger.Old[0].adderror('Error:Please contact the System Admin');
    System.debug('Exception in UpdateOpportunityProductsFields trigger'+e);
    }
    }
    
    }

    
    
    
    
}