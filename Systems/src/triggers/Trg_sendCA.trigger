/*
  Author: Mayank Joshi
  Date: 3/15/2013 9:57 AM
  Action: This trigger is updating/inserting- CA Attributes and sending Outbound Message to Web Methods
          using class CREATE_UPDATE_HEADER_CA_RESPONSE.
          
          Logic Modified : 9th July 2013 ,For Sending CA attributes to COBRA Application in Create,Update,Closed Workpackages .
  Logic Last Modified By: Mayank Joshi,  07/09/2013 11:14 PM .
  
  Last Modified By: Abhishek on August, 25th 2016. Used a label "RestrictedIntegrationUserAlias" to compare restricted user's alias with label's value - US22105
  Last Modified By: Abhishek on August, 4th Oct 2016 : Removed all Debug statements. To increase the code coverage, we used method "isRunningTest()" standard method to stop calling of those methods which are doing callout for eg. CreateUpdate_Header_CA
                    It also helps in resolving the issue "Please roll back uncomitted work" during test execution.  
  Last Modified by Abhishek on December,3rd 2016 : Commented windchill callout "CREATE_UPDATE_HEADER_CA_RESPONSE" on both insert&Update as per B&P project - US43365
  
*/
Trigger Trg_sendCA on Cost_Authorization__c (Before Insert,Before Update,After Insert,After Update)
{ 
    Class_TriggerCheck.var_IsStopEPABatchTrg = true;
    if(Label.Handle_CA_Callout_Trigger == 'true') //Use the Custom label value to handle the execution of this trigger
    {   
        if(Class_TriggerCheck.var_IsExecuteCAOutboundCallTrg) // Add condition for JIRA-297 to control the execution in case update happnened from EPA triggers
        {                                                       
            If(!Class_TriggerCheck.Var_Epa_OwnerChanged)
            {
                
                map<Id,string> mapSRId_SRLocationval = new map<Id,string>();
                
                //Checking for CA's Status .
                
                 String CA_Status;
                 boolean bIsChanged;
                 boolean Is_FYSpentNotChanged;
                 boolean Is_Cost_Authorization_NotClosed_FYSpent_NotChanged;
                 String Get_Old_Status;
                 boolean IsApproved;
                 boolean IsApproved_AfterUpdate;
                 boolean Isclosed = True; 
                 boolean IsCAClosed ;
                 String IsCARejected ;
                 String IsCANotClosed ;
                 string RestrictedUserAlias;
                 
                 RestrictedUserAlias = Label.RestrictedIntegrationUserAlias;
                
               
                If(Trigger.IsBefore)
                {
                   For(Cost_Authorization__c CA_Before:Trigger.new)
                   {
                       If(CA_Before.Status__c == 'Closed') {
                          CA_Before.Percent_Complete__c = 100; // Set Value 100 for Closed CA .
                       }
                   } 
                } 
                 
                        
                // INSERTION CRITERIA
                         
                If(Trigger.IsAfter){
                    If(Class_TriggerCheck.IsPreventing) 
                    {
                        If(Trigger.IsInsert)
                        {  
                            String Operation_Create = 'CREATE';
                            String Operation_Closed = 'CLOSE'; 
                            String WP_Create = 'True' ;
                            List<String> CA_Insert_Cobra = new List<String>(); 
                            List<String> CA_Closed_Cobra = new List<String>(); 
                            For(Cost_Authorization__c CA:Trigger.new)
                            {  
                                If( CA.Status__c == 'Approved'){
                                    
                                    CA_Insert_Cobra.add(CA.Id); 
                                }
                                if(CA.Approved__c){
                                    CA_Insert_Cobra.add(CA.Id); 
                                }
                                Else If(CA.Status__c == 'Closed'){
                                    CA_Closed_Cobra.add(CA.Id); 
                                }
                            }
                            
                            if(!test.isRunningTest())
                            {
                                /* 
                                SFDC > COBRA Outbound Integration Sending Approved CA to Cobra : CREATE WORK PACKAGE
                                removed UserName == RestrictedUserAlias on Dec 2016
                                Outbound call for Create_Work_Package with Operation 'CREATE', at the time of creation only   */
                                if(!CA_Insert_Cobra.isEmpty())
                                    Cobra_Create_Update_ApprovedCA.Create_ApprovedCA(CA_Insert_Cobra,Operation_Create,WP_Create);
                                
                                // Outbound call for Closed_Work_Package with Operation 'CREATE', at the time of creation only   
                                if(!CA_Closed_Cobra.isEmpty())
                                    Cobra_Create_Update_ApprovedCA.Closed_CA(CA_Closed_Cobra,Operation_Closed );
                                 
                                 //  End of SFDC > COBRA Outbound Integration for INSERT Event 
                            }   
                        } 
                    }
                }       
            
                              
                // UPDATION CRITERIA
                // Removed :  && UserName == RestrictedUserAlias on Dec 2016
                
                If(Trigger.IsBefore){
                    If(trigger.IsUpdate){
                          String Operation_First_Create = 'CREATE';
                          String WP_App_Create = 'True';
                          List<String> CA_First_Approved_Cobra = new List<String>();
                         For(Cost_Authorization__c CA_Before:Trigger.New){
                            Cost_Authorization__c Old_CA_Before = trigger.oldMap.get(CA_Before.ID);
                            If( (CA_Before.FY_Spent_to_Date__c == Old_CA_Before.FY_Spent_to_Date__c) ||
                                (CA_Before.Cost_Authorization_Closed__c ==  Old_CA_Before.Cost_Authorization_Closed__c) ||
                                (
                                         
                                    (CA_Before.Cost_Authorization_Closed__c == Old_CA_Before.Cost_Authorization_Closed__c) 
                                    &&(CA_Before.FY_Spent_to_Date__c == Old_CA_Before.FY_Spent_to_Date__c)
                                       
                                ) 
                              ) 
                              {
                                   If(CA_Before.Status__c == 'Approved'){                                   
                                        If(CA_Before.IsApproved__c != True)
                                        {
                                             
                                            CA_First_Approved_Cobra.add(CA_Before.Id); 
                                            // True for identifying record as Create_WorkPackage 
                                            CA_Before.IsApproved__c = True; 
                                            IsApproved = CA_Before.IsApproved__c; 
                                        }
                                   }
                               }
                               If(CA_Before.Pending_Approval_Amount__c != Old_CA_Before.Pending_Approval_Amount__c && CA_Before.Status__c=='Approved' && CA_Before.Approved__c == false && CA_Before.Pending_Approval_Amount__c > 0)
                                  CA_Before.Status__c='In Work';
                               
                           }
                           // CREATE WORK PACKAGE (If B&P is Approved, for the first Time .) 
                            If(Cobra_Create_Update_ApprovedCA.firstCheck){
                                If(!CA_First_Approved_Cobra.isEmpty()){  
                                    // Outbound call for Create_Work_Package with Operation 'CREATE', at the time of creation only   
                                    if(!test.isRunningTest())
                                    Cobra_Create_Update_ApprovedCA.Create_ApprovedCA(CA_First_Approved_Cobra,Operation_First_Create,WP_App_Create );  
                                    
                                    Cobra_Create_Update_ApprovedCA.firstCheck = False; 
                                }
                                
                            }
                            
                        } 
                }       
                
                If(Trigger.IsAfter){
                    If(trigger.IsUpdate){
                        String Operation_AfterFirst_Created = 'UPDATE';
                        String WP_SecondApp_Create = 'False';
                        String Operation_Closed_Cobra = 'CLOSE';
                        List<String> CA_AfterFirst_Approved_Cobra = new List<String>();
                        List<String> CA_Closed_Cobra = new List<String>();
                        For (Cost_Authorization__c oCA : trigger.new){
                            Cost_Authorization__c oOldCA = trigger.oldMap.get(oCA.Id);
                            CA_Status = oCA.Status__c ;
                            Get_Old_Status = oOldCA.Status__c;
                            If(oCA.Status__c == 'Closed' ){
                                IsCAClosed = (oCA.Status__c == oOldCA.Status__c);
                            }
                       
                           If(oCA.Status__c != 'Closed' ){
                             IsCANotClosed = oCA.Status__c;
                           }Else  {
                             IsCANotClosed = 'Closed';
                           }
                       
                           If(oCA.Status__c == 'Rejected'){
                            IsCARejected = 'Rejected';
                           }Else{
                             IsCARejected = oCA.Status__c;
                           }
                           system.debug('status + ' + CA_Status);
                           system.debug('Old status + ' + oOldCA.Status__c);
                           system.debug('new approved nudget + ' + oCA.Approved_Budget_Backend__c);
                           system.debug('Old approved nudget + ' + oOldCA.Approved_Budget_Backend__c);
                           
                            If( (CA_Status == 'Approved'  && oCA.Approved_Budget_Backend__c != oOldCA.Approved_Budget_Backend__c) || (oCA.Approved__c && oCA.Approved__c!=oOldCA.Approved__c) ){  
                                If(Cobra_Create_Update_ApprovedCA.firstCheck == True){
                                    If(Class_TriggerCheck.IsUpdated_CA){
                                      CA_AfterFirst_Approved_Cobra.add(oCA.Id); 
                                    } 
                                } 
                            }
                           //  CLOSED WORK PACKAGE (If CA Status is Closed.)          
                            Else If(CA_Status == 'Closed' && IsCAClosed == False && oCA.IsApproved__c == true){ 
                                If(Class_TriggerCheck.Isclosed_CA){
                                    CA_Closed_Cobra.add(oCA.Id); 
                                } 
                            }
                        }
                        // Below is the code block written for SFDC > COBRA Outbound Integration 
                        // UPDATE WORK PACKAGE (If CA is Approved, not the first Time.) 
                        If(!CA_AfterFirst_Approved_Cobra.isEmpty()){  
                                // Outbound call for Update_Work_Package with Operation 'UPDATE', at the time of creation only   
                            if(!test.isRunningTest())
                                Cobra_Create_Update_ApprovedCA.Create_ApprovedCA(CA_AfterFirst_Approved_Cobra,Operation_AfterFirst_Created,WP_SecondApp_Create  );  
                        
                            Class_TriggerCheck.IsUpdated_CA = False;
                        } 
                        
                        If(!CA_Closed_Cobra.isEmpty()){  
                            // Outbound call for Closed_Work_Package with Operation 'CLOSED', at the time of creation only   
                            if(!test.isRunningTest())
                                Cobra_Create_Update_ApprovedCA.Closed_CA(CA_Closed_Cobra,Operation_Closed_Cobra );  
                        
                            Class_TriggerCheck.Isclosed_CA = False; 
                        }
                          

                        // End of for SFDC > COBRA Outbound Integration 
                        
                    }
                }       
            }
        }
    }
    if(!test.isRunningTest())
        Class_TriggerCheck.IsPreventing = False;
}