/*******************************************************************************************
    Developer : Abhishek
    Date      : 12/Apr/2016    
    Action    : This trigger will invoke handler class: userTriggerHandler.(Jira 546)  
**********************************************************************************************/
trigger triggerOnUser on User (before insert, after insert,before update, after update, before delete, after delete, after undelete) {
   if(Label.Label_triggerOnUser=='true' && Class_TriggerCheck.var_triggerOnUser){
        userTriggerHandler userHandler = new userTriggerHandler();
        
        if(trigger.isAfter && trigger.isInsert) {
                userHandler.assignPermsandOthersOnUser(trigger.new, trigger.newMap,trigger.oldMap, 'Before', 'Insert');
            }
        if(trigger.isBefore && trigger.isUpdate) {
                userHandler.assignPermsandOthersOnUser(trigger.new, trigger.newMap,trigger.oldMap, 'Before', 'Update');
            }
        if(trigger.IsAfter && trigger.isUpdate) {
                userHandler.updatePermsOnUserUpdate(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');
                userHandler.DeletePermsOnUserUpdate(trigger.new,trigger.oldMap, trigger.newMap, 'after', 'Update');
         }      
            
    } 
}