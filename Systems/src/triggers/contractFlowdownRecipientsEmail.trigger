/*********************************************
Created By Tim Lomison
Created On Jan 20 2017
Description When a Contract Flowdown record is created or updated, query the Contract Flowdown Recipients object's records for results matching the Document Type and put their emails into the Flowdown Recipients Email fields on the Contract Flowdown Record
*********************************************/
trigger contractFlowdownRecipientsEmail on Contract_Flowdown__c (before insert,before update) {
      string DocumentType;
      Boolean datasharing;
      
      if(trigger.isbefore && (trigger.isinsert || trigger.IsUpdate)) {
          
          List<Contract_Flowdown_Recipients__c> CFRC_list = new List<Contract_Flowdown_Recipients__c>();
          
          
          for(Contract_Flowdown__c contractnew : trigger.new){
          
              
                  DocumentType = contractnew.Document_Type__c ;
                  datasharing = contractnew.Data_Share_Language__c;
                  
                  string Query = 'select id,Email__c,Document_Types__c, Role__c, Data_Share_Language__c From Contract_Flowdown_Recipients__c where Document_Types__c like \'%'+ DocumentType +'%\' AND Data_Share_Language__c = :datasharing  order by CreatedDate desc ';
                  
                  system.debug('--------Query----'+Query);   
                  
                  CFRC_list = database.Query(Query);
                  
          }
          set<String> setReciepientEmail = new set<String>();
          
      system.debug('=======CFRC_list1=========='+CFRC_list);
      
      if(!CFRC_list.isEmpty()){
        
        for(Contract_Flowdown_Recipients__c objContractRecipent : CFRC_list){
      
          setReciepientEmail.add(objContractRecipent.email__c);
        }
      }
      
      system.debug('=======setReciepientEmail=========='+setReciepientEmail);
      
      List<String> contractRecipentUniquelst = new List<String>();
      if(!setReciepientEmail.isEmpty()){
        
        for(String recipientEmail : setReciepientEmail){
        
          contractRecipentUniquelst.add(recipientEmail);
        }
      }
      
          system.debug('-------contractRecipentUniquelst-----'+contractRecipentUniquelst); 
          
          system.debug('---------Flowdown-----'+contractRecipentUniquelst);
          system.debug('---------Flowdown-----Size'+contractRecipentUniquelst.size());
          for(Contract_Flowdown__c contractnew : trigger.new)
          {
          
              if(trigger.IsUpdate )
              {
                  Contract_Flowdown__c oldContract = Trigger.oldMap.get(contractnew.Id);
  
                  if(contractnew.Document_Type__c != oldContract.Document_Type__c || contractnew.Data_Share_Language__c != oldContract.Data_Share_Language__c )
                  {
                          System.debug('======before=====>'+contractnew.Flowdown_Recipient_Email_1__c);
                          contractnew.Flowdown_Recipient_Email_1__c=null;
                          contractnew.Flowdown_Recipient_Email_2__c=null;
                          contractnew.Flowdown_Recipient_Email_3__c=null;
                          contractnew.Flowdown_Recipient_Email_4__c=null;
                          contractnew.Flowdown_Recipient_Email_5__c=null;
                          contractnew.Flowdown_Recipient_Email_6__c=null;
                          contractnew.Flowdown_Recipient_Email_7__c=null;
                          contractnew.Flowdown_Recipient_Email_8__c=null;
                          contractnew.Flowdown_Recipient_Email_9__c=null;
                          contractnew.Flowdown_Recipient_Email_10__c=null;
                          contractnew.Flowdown_Recipient_Email_11__c=null;
                          contractnew.Flowdown_Recipient_Email_12__c=null;
                          contractnew.Flowdown_Recipient_Email_13__c=null;
                          contractnew.Flowdown_Recipient_Email_14__c=null;
                          contractnew.Flowdown_Recipient_Email_15__c=null;
                          contractnew.Flowdown_Recipient_Email_16__c=null;
                  }
              }
              
                System.debug('======After=====>'+contractnew.Flowdown_Recipient_Email_1__c);
                
              if(!contractRecipentUniquelst.isEmpty())
              {
                for(integer i=0 ;i<contractRecipentUniquelst.size();i++){
  
                    if(i==0)
                        contractnew.Flowdown_Recipient_Email_1__c=contractRecipentUniquelst[i];
                    if(i==1)
                        contractnew.Flowdown_Recipient_Email_2__c=contractRecipentUniquelst[i];
                    if(i==2)
                        contractnew.Flowdown_Recipient_Email_3__c=contractRecipentUniquelst[i];
                    if(i==3)
                        contractnew.Flowdown_Recipient_Email_4__c=contractRecipentUniquelst[i];
                    if(i==4)
                        contractnew.Flowdown_Recipient_Email_5__c=contractRecipentUniquelst[i];
                    if(i==5)
                        contractnew.Flowdown_Recipient_Email_6__c=contractRecipentUniquelst[i];
                    if(i==6)
                        contractnew.Flowdown_Recipient_Email_7__c=contractRecipentUniquelst[i];
                    if(i==7)
                        contractnew.Flowdown_Recipient_Email_8__c=contractRecipentUniquelst[i];
                    if(i==8)
                        contractnew.Flowdown_Recipient_Email_9__c=contractRecipentUniquelst[i];
                    if(i==9)
                        contractnew.Flowdown_Recipient_Email_10__c=contractRecipentUniquelst[i];
                    if(i==10)
                        contractnew.Flowdown_Recipient_Email_11__c=contractRecipentUniquelst[i];
                    if(i==11)
                        contractnew.Flowdown_Recipient_Email_12__c=contractRecipentUniquelst[i];
                    if(i==12)
                        contractnew.Flowdown_Recipient_Email_13__c=contractRecipentUniquelst[i];
                    if(i==13)
                        contractnew.Flowdown_Recipient_Email_14__c=contractRecipentUniquelst[i];
                    if(i==14)
                        contractnew.Flowdown_Recipient_Email_15__c=contractRecipentUniquelst[i];
                    if(i==15)
                        contractnew.Flowdown_Recipient_Email_16__c=contractRecipentUniquelst[i];
                }
                
              }  
          } 
      }
}