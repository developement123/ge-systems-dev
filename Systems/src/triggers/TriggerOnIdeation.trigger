/***************************************************************************************************************************************************
    Author : Mohan Kumar
    Date    : 16 July 2015    
    Note    : This Apex trigger was written to tokenize the Ideation records when
                'Civil_Military__c' value is Military and 
                 Site__c is Cheltenham/Dowty/Southampton/Hamble.
                  

***************************************************************************************************************************************************/

trigger TriggerOnIdeation on Ideation__c(before insert, before update) {
    if(Label.Label_TriggerOnIdeation == 'true') {
        if(Class_TriggerCheck.var_TriggerOnIdeationTrg) {
        
            IdeationTriggerHandler ideaHandler = new IdeationTriggerHandler();
            
            if(trigger.isBefore && trigger.isInsert)  
                ideaHandler.IdeationHandler_method(trigger.new, null, null, 'Before', 'Insert');
            
            if(trigger.isBefore && trigger.isUpdate) 
                ideaHandler.IdeationHandler_method(trigger.new, trigger.oldMap, trigger.newMap, 'Before', 'Update');
        }    
    }
}