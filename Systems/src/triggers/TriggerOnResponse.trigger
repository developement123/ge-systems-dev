/****************************************************************************************************
    Author   : Mohan Kumar
    Date     : 29/November/2015
    Action   : This trigger is invoking handler class : 'ResponseTriggerHandler'.  
    
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        07/05/2016      Called this method "UpdateSalesOwnerOnUpdate" on BEFORE UPDATE
    Abhishek        12/23/2016      Added a new method "updatePipelineOpportunityStage" to update pipleine stage to contract awarded on response change - US39238(prev. we were using Process builder but same cannot be used now)
    Abhishek        01/04/2017      Added a new method "responseValidationOnUpdate" - US50165
******************************************************************************************************/

trigger TriggerOnResponse on Sales_Request__c(before insert, after insert, after update, before delete, before update) {
    if(Label.Label_triggerOnResponse == 'true') {  
        if(Class_TriggerCheck.var_TriggerOnResponse) {  
            ResponseTriggerHandler trgHandler = new ResponseTriggerHandler();
            
            if(trigger.IsBefore) {
                if(trigger.isInsert) {
                    trgHandler.autoGenerateResponseId(trigger.new);
                    trgHandler.updateResponseIdWhenProgramIsNotNull(trigger.new, null, null, null, 'Before', 'Insert');
                    trgHandler.UpdatePipelineOpportunityId(trigger.new, 'Before', 'Insert');
                    trgHandler.tokenizeRespWhenOppyIsTokenized(trigger.new);
                    trgHandler.UpdateOnHoldCalculation(trigger.new, null, null, null, 'Before', 'Insert');
                }       
                
                if(trigger.isUpdate) {
                    trgHandler.responseValidationOnUpdate(trigger.new, trigger.oldMap);
                    trgHandler.UpdateSalesOwnerOnUpdate(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'Before', 'Update'); 
                    trgHandler.UpdateOnHoldCalculation(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'Before', 'Update');
                }
                
                if(trigger.isDelete) 
                    trgHandler.updateResponseIdWhenProgramIsNotNull(null, trigger.old, null, null, 'Before', 'Delete');
            }
            
            if(trigger.IsAfter) {
                if(trigger.isInsert) { 
                    trgHandler.updateResponseIdWhenProgramIsNotNull(trigger.new, null, null, null, 'After', 'Insert');
                    trgHandler.sendEmailToResponseManagers(trigger.new, null, null, null, 'After', 'Insert');
                }
                else if(trigger.isUpdate) {
                    trgHandler.updateResponseIdWhenProgramIsNotNull(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update'); 
                    trgHandler.sendEmailToResponseManagers(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update'); 
                    trgHandler.updatePipelineOpportunityStage(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update'); 
                }
            }
        }
    } 
}