/*Author: Mayank Joshi
   Date: 3/14/2013 11:31 AM
   Action: This trigger is updating/inserting- Fields on 
           on Response and sending Outbound message to Web Methods using class
           CREATE_UPDATE_HEADER_CA_RESPONSE .
   Logic Last Modified By: Mayank Joshi,  3/22/2013  .
   Last Modified By: Abhishek on August, 25th 2016. Used a label "RestrictedIntegrationUserAlias" to compare restricted user's alias with label's value - US22105
   Logic Last Modified By: Abhishek 10/6/2016 added isRunningTest method to increase code coevarge
*/
Trigger Trg_sendRESP on Sales_Request__c (After Insert, After Update)
{
    System.debug('chekc Resp for Ispreventing'+ Class_TriggerCheck.IsPreventing);
    if(Label.Handle_Response_Callout_Trigger == 'true'){ //Use the Custom label value to handle the execution of this trigger
        if(Class_TriggerCheck.var_IsExecuteResOutboundCallTrg)  // Add condition for JIRA-297 to control the execution in case update happnened from EPA triggers
        {
            If(Class_TriggerCheck.IsPreventing)
            { 
                if(!Class_TriggerCheck.Var_Epa_OwnerChanged)
                {
                  string RestrictedUserAlias = Label.RestrictedIntegrationUserAlias;
                  // Checking for Current User 
                  List<User> Lst_user =[Select ID,Name,alias from User where Id =:UserInfo.getUserId()];
                  String UserName;
                  
                  For(User usr:Lst_user )
                  {
                    system.debug(usr.alias);
                    UserName = usr.alias; // replaced name with alias as per Rally US22105
                  }
                  If(UserName != RestrictedUserAlias) // To by pass below Logic ,if user is Integration User .
                  { 
                       If(trigger.IsInsert)
                       {   
                             List<String> salesRequest = new List<String>();  
                             For(Sales_Request__c SR:Trigger.new)
                             {
                                  salesRequest.add(SR.Id); 
                             }
                             If(CREATE_UPDATE_HEADER_CA_RESPONSE.firstRun)
                             {
                                if(!salesRequest.isEmpty() )
                                {
                                  CREATE_UPDATE_HEADER_CA_RESPONSE.Create_Header(salesRequest); //Create response 
                                }
                             }
                             CREATE_UPDATE_HEADER_CA_RESPONSE.firstRun=false;
                       }
                      If(Trigger.IsUpdate )
                      {
                           If(CREATE_UPDATE_HEADER_CA_RESPONSE.firstRun == True)
                           { 
                             List<String> salesRequestUpdate = new List<String>(); 
                               For(Sales_Request__c SR:Trigger.new)
                               {
                                   salesRequestUpdate.add(SR.Id);
                               }
                               System.debug('CHECK RES UPDATED '+ salesRequestUpdate);  
                               if(!salesRequestUpdate.isEmpty() )
                               {   
                                  // Logic for updating Response along with Active CA is built in Method--Update_Header_UpdateResponse
                                  CREATE_UPDATE_HEADER_CA_RESPONSE.Update_Header_UpdateResponse(salesRequestUpdate);//Update response 
                                  //update salesRequest ;
                               }
                           }
                      }
                 }
              
                }
                if(!test.isRunningTest())
                Class_TriggerCheck.IsPreventing = False;
                System.debug('TESTfor checkRESP'+ Class_TriggerCheck.IsPreventing);
            }
        }
    } 
}