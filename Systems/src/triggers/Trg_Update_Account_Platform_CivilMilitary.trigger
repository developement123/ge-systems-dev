/*/Author: Mayank Joshi
   Date: 11/7/2012 
   Action: This trigger is updating/inserting Civil/Militray Attributes 
           on 'Account Platform' with events of update on 'Platform Master'Object
   Logic/Documentation Last Modified By: Mayank Joshi ,12/12/2012 .
   Logic Last Modified By: Nupur Gupta , 23/10/2013 .
   Last modified by ABhishek on 21 Nov 2016 as per DE4386 to update account platform on 
   platform master'name update
   Logic Last Modified By: Abhishek,  09/12/2017  - not needed
*/

Trigger Trg_Update_Account_Platform_CivilMilitary on Platform_Master__c (After Update) 
{
 /*
 if(Label.Label_Trg_Update_Account_Platform_CivilMilitary == 'true'){
     if(Class_TriggerCheck.Var_StopPlatMasterTrigger == true){
     List<Forecast__c> List_childrenToUpdate_Account_Platform= new List<Forecast__c> ();
     set<id> setOfPlatformMasterUpdate = new set<id>(); // to hold platform master id whose name changes
     //If(Trigger.IsUpdate){
     For(Platform_Master__c parent: Trigger.new)
     {
      if(label.AccountPlatformToUpdate=='true'){
         if(parent.name!=Trigger.oldmap.get(parent.id).name){
            setOfPlatformMasterUpdate.add(parent.id);
         }
      }
      List<Forecast__c> Lst_children_Account_Platform = 
      [SELECT ID,Civil_Military__c,Airframer__r.name,Forecast_Platform__c,Forecast_Platform__r.name FROM Forecast__c
      where Forecast_Platform__c= :parent.Id];
       
      If(Lst_children_Account_Platform.size()>0 && Trigger.IsUpdate)
      {
       For(Forecast__c thischild: Lst_children_Account_Platform )
       {
     //Checking for Field Civil/Military 
     //if values are different then perform below Logic 
        if( (thischild.Civil_Military__c!= parent.Civil_Military__c) || setOfPlatformMasterUpdate.contains(thischild.Forecast_Platform__c) )
        {
         // Assigning Values from Platform Master to Account Plaform
         thischild.Civil_Military__c=parent.Civil_Military__c;  
         
         if(label.AccountPlatformToUpdate=='true' && setOfPlatformMasterUpdate.contains(thischild.Forecast_Platform__c))
             thischild.name = thischild.Airframer__r.name+' '+thischild.Forecast_Platform__r.name;
         
         
         List_childrenToUpdate_Account_Platform.add(thischild);
         system.debug('##Check1: For List of Account Platform to Update/Insert##'+List_childrenToUpdate_Account_Platform); 
        }
       } 
      }
     }
     //}
    try{
        Update List_childrenToUpdate_Account_Platform;
    }catch(exception ex){
        string expMsg = ex.getMessage();
        if(expMsg.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,')!= -1){
           // expMsg = 'Record cannot be tokenized because it has Account Platform records associated with it.';
            expMsg = expMsg.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,',': [');
            
        }
        system.debug('expMsg value:'+expMsg);
        trigger.new[0].addError(expMsg);
    }
    system.debug('##Check2: For List of Account Platform to Update/Insert##'+List_childrenToUpdate_Account_Platform);
    
    }
}
*/
}