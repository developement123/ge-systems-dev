/*********************************************************************************************************
    Author : Mohan Kumar
    Date     : 24/september/2015
    Action   : This trigger is invoking handler class 'AssociatedProductTriggerHandler'.
    
    Change History
    =============
    Developer       Modified On     Change description
    Abhishek        04/15/2016      have called this method "DeleteSchedules" on before delete condition.
                                    if you put any condition that will prevent deletion of Products in that case call this method on always at the end to avoid any situation where schedules got deleted but not related product. 
    
***********************************************************************************************************/

trigger TriggerOnAssociatedProduct on Opportunity_Line_Item__c(before insert, after insert, after update, after delete, after undelete, before delete) {
    if(Label.Label_TriggerOnAssociatedProduct == 'true') {   
        if(Class_TriggerCheck.var_TriggerOnAssociatedProduct) { 
            
            AssociatedProductTriggerHandler trgHandler = new AssociatedProductTriggerHandler();
            
            if(trigger.isBefore && trigger.isInsert) {   // if the trigger was fired due to before insert operation.
                trgHandler.tokenizeOliWhenOppyIsTokenized(trigger.new);
            }
            
            if(trigger.isAfter && trigger.isInsert) {   // if the trigger was fired due to after insert operation.
                trgHandler.updateTotalMarginInOppy(trigger.new, null, null, null, 'After', 'Insert');
                trgHandler.generateSchedules(trigger.new, null, null, null, 'After', 'Insert');
            }
            
            else if(trigger.isAfter && trigger.isUpdate) {   // if the trigger was fired due to after update operation.
                trgHandler.updateTotalMarginInOppy(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');
                trgHandler.generateSchedules(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');
                trgHandler.UpdateIniitalAndRenewalSchedules(trigger.new, trigger.old, trigger.oldMap, trigger.newMap, 'After', 'Update');
                trgHandler.tokenize_Sched_When_Oli_Tokenized(trigger.new, trigger.newMap, trigger.oldMap);
                
            }
            
            else if(trigger.isBefore && trigger.isDelete){
                trgHandler.DeleteSchedules(trigger.new, trigger.old, null, null, 'before', 'Delete');
            }
            
            else if(trigger.isAfter && trigger.isDelete) {    // if the trigger was fired due to after delete operation.
                trgHandler.updateTotalMarginInOppy(null, trigger.old, null, null, 'After', 'Delete');
                trgHandler.updateQuatersOnDeleteOfOli(trigger.old);
                trgHandler.updateMEWhenAssociateProDeleted(trigger.old);
            }
            
            else if(trigger.isAfter && trigger.isUnDelete) {   // if the trigger was fired due to after undelete operation.
                trgHandler.updateTotalMarginInOppy(trigger.new, null, null, null, 'After', 'Undelete');   
            }
        }    
    }
}