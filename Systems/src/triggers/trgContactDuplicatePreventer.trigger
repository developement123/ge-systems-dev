trigger trgContactDuplicatePreventer on Contact (before insert, before update)
{
    if(Label.Label_trgContactDuplicatePreventer == 'true'){
        Map<String, Contact> contactMapEmail = new Map<String, Contact>();// This is for Email
        Map<string, Contact> contactMapLN = new Map<String, Contact>(); // This is for Last Name
        Map<string, Contact> contactMapFN = new Map<String, Contact>(); // This is for First Name
        for (Contact cont : System.Trigger.new)
        {
            
            // This logic will get bypassed when email address, firstname, last name are not changing.
        
                 if ((cont.Email != null && cont.LastName !=null && cont.FirstName!=null) &&
                    (System.Trigger.isInsert ||
                    ((cont.Email != System.Trigger.oldMap.get(cont.Id).Email)||
                      (cont.LastName != System.Trigger.oldMap.get(cont.Id).LastName)||
                      (cont.FirstName != System.Trigger.oldMap.get(cont.Id).FirstName))))
              {
            
                // Logic to check newly added contact isn't a duplicate.  
        
                if (contactMapEmail.containsKey(cont.Email)&&
                     contactMapLN.containsKey(cont.LastName)
                     && contactMapFN .containsKey(cont.FirstName))
                {
                    cont.Email.addError('Duplicate contact exists with' 
                     + 'same email address, LastName, FirstName');
                
                } 
                
                else
                {
                    contactMapEmail.put(cont.Email, cont);
                    contactMapLN.put(cont.LastName, cont);
                    contactMapFN.put(cont.FirstName, cont);
                }
           }
           
           
        }
        
        // Using a single database query, find all the Contacts in 
        
        // the database that have the same email address, last name, first name  as any  
        
        // of the Contacts being inserted or updated.  
        
        for (Contact cont : [SELECT Email, LastName, FirstName FROM Contact
                          WHERE Email IN :contactMapEmail.KeySet() And LastName IN :contactMapLN.KeySet() And FirstName IN :contactMapFN.KeySet()]) {
            Contact newContact = contactMapEmail.get(cont.Email);
            newContact.addError('A Contact with this Email, '
                                   + 'First Name and Last Name already exists.');
        }
    }
}