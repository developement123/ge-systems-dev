/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_MDM_Events_Web_Service {
    global GE_MDM_Events_Web_Service() {

    }
    webService static GEMDM.GE_MDM_Events_Web_Service.event insertGeMdmEvent(GEMDM.GE_MDM_Events_Web_Service.event eventProcessing) {
        return null;
    }
global class event {
    @WebService
    webService String alternateTradeStyle;
    @WebService
    webService String billToAddressLine1;
    @WebService
    webService String billToAddressLine2;
    @WebService
    webService String billToAddressLine3;
    @WebService
    webService String billToAddressLine4;
    @WebService
    webService String billToCity;
    @WebService
    webService String billToCountryCode;
    @WebService
    webService String billToStateProvince;
    @WebService
    webService String billToZip;
    @WebService
    webService String childDUNS;
    @WebService
    webService Boolean concernCountryFlag;
    @WebService
    webService Boolean customBoolean1;
    @WebService
    webService Boolean customBoolean2;
    @WebService
    webService Boolean customBoolean3;
    @WebService
    webService Boolean customBoolean4;
    @WebService
    webService Boolean customBoolean5;
    @WebService
    webService String CustomerConnectRequestStatus;
    @WebService
    webService String customerName;
    @WebService
    webService String customString1;
    @WebService
    webService String customString10;
    @WebService
    webService String customString2;
    @WebService
    webService String customString3;
    @WebService
    webService String customString4;
    @WebService
    webService String customString5;
    @WebService
    webService String customString6;
    @WebService
    webService String customString7;
    @WebService
    webService String customString8;
    @WebService
    webService String customString9;
    @WebService
    webService String dDUNSName;
    @WebService
    webService String dDUNSNumber;
    @WebService
    webService String domesticUltimateName;
    @WebService
    webService String DUNSAssociated;
    @WebService
    webService String DUNSDomesticUltimate;
    @WebService
    webService String DUNSGlobalUltimate;
    @WebService
    webService String DUNSNumber;
    @WebService
    webService String DUNSParent;
    @WebService
    webService String DUNSPrevious;
    @WebService
    webService Date eventDate;
    @WebService
    webService String eventType;
    @WebService
    webService String fromDUNS;
    @WebService
    webService String geAccountExecutiveName;
    @WebService
    webService String geGlobalCustomerIndicator;
    @WebService
    webService String geMarketingName;
    @WebService
    webService String geVertical;
    @WebService
    webService String globalUltimateName;
    @WebService
    webService String goldId;
    @WebService
    webService Boolean GovernmentEntityFlag;
    @WebService
    webService String governmentReportingValue;
    @WebService
    webService String hqParentName;
    @WebService
    webService Boolean internalCustomer;
    @WebService
    webService String itpFlag;
    @WebService
    webService String Latitude;
    @WebService
    webService String locationType;
    @WebService
    webService String Longitude;
    @WebService
    webService String mailToAddress;
    @WebService
    webService String mailToCity;
    @WebService
    webService String mailToCountryCode;
    @WebService
    webService String mailToStateProvince;
    @WebService
    webService String mailToZip;
    @WebService
    webService Boolean outofBusinessIndicator;
    @WebService
    webService String parentAccountName;
    @WebService
    webService String parentAccountType;
    @WebService
    webService String parentDUNS;
    @WebService
    webService String parentSegment;
    @WebService
    webService Date publishDate;
    @WebService
    webService String relationshipChangeSubType;
    @WebService
    webService Date relationshipDateFrom;
    @WebService
    webService Date relationshipDateTo;
    @WebService
    webService Boolean sanctionedCountryFlag;
    @WebService
    webService String segment;
    @WebService
    webService String shipToAddressLine1;
    @WebService
    webService String shipToAddressLine2;
    @WebService
    webService String shipToAddressLine3;
    @WebService
    webService String shipToAddressLine4;
    @WebService
    webService String shipToCity;
    @WebService
    webService String shipToCountryCode;
    @WebService
    webService String shipToStateProvince;
    @WebService
    webService String shipToZip;
    @WebService
    webService String SICCode;
    @WebService
    webService String SiteUseCode;
    @WebService
    webService String sourceCustomerId;
    @WebService
    webService String status;
    @WebService
    webService String toDUNS;
    @WebService
    webService String upsertSubType;
    @WebService
    webService String vatNumber;
    @WebService
    webService String watchlistDetail;
    @WebService
    webService Boolean WatchlistFlag;
    global event() {

    }
}
}
