/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_MDM_AccountSearchController {
global class Facet implements System.Comparable {
    global Integer count {
        get;
        set;
    }
    global String facetIcon {
        get;
        set;
    }
    global String facetType {
        get;
        set;
    }
    global String facetValue {
        get;
        set;
    }
    global Facet() {

    }
    global Integer compareTo(Object compareTo) {
        return null;
    }
}
}
