/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_MDM_WebServiceMock_getSubcription implements System.WebServiceMock {
    global GE_MDM_WebServiceMock_getSubcription() {

    }
    global void doInvoke(Object stub, Object request, Map<String,Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {

    }
}
