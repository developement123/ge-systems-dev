/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_XSell_HubRestClientService {
    global GE_XSell_HubRestClientService() {

    }
    global GE_XSell_HubRestClientService(String SFDCInstance) {

    }
    global String GetAccesToken() {
        return null;
    }
    global void UpdateReferral(String refid, Map<String,String> mpFldValues) {

    }
}
