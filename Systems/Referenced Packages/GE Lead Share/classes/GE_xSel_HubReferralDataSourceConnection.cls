/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_xSel_HubReferralDataSourceConnection extends DataSource.Connection {
    global GE_xSel_HubReferralDataSourceConnection(DataSource.ConnectionParams connectionParams) {

    }
    global override List<DataSource.DeleteResult> deleteRows(DataSource.DeleteContext c) {
        return null;
    }
    global override DataSource.TableResult query(DataSource.QueryContext c) {
        return null;
    }
    global override List<DataSource.TableResult> search(DataSource.SearchContext c) {
        return null;
    }
    global override List<DataSource.Table> sync() {
        return null;
    }
    global override List<DataSource.UpsertResult> upsertRows(DataSource.UpsertContext c) {
        return null;
    }
}
