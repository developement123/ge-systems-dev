/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_xSell_MDM_Account_SubscriptionCtrl {
    global GE_xSell_MDM_Account_SubscriptionCtrl() {

    }
    @Future(callout=true)
    global static void Subscription_Callout(String dunsNumber, String AcctId) {

    }
    @Future(callout=true)
    global static void Subscription_CalloutBulkify(Map<String,String> SubscriptionList) {

    }
}
