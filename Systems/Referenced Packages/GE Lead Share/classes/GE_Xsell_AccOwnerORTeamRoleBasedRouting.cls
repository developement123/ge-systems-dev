/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/AccOwnerORTeamRoleBasedRouting/*')
global class GE_Xsell_AccOwnerORTeamRoleBasedRouting {
    global GE_Xsell_AccOwnerORTeamRoleBasedRouting() {

    }
    @HttpPost
    global static String accOwnerORTeamRoleBasedRouting(String dunsNumber, String referralId, String teamRole) {
        return null;
    }
}
