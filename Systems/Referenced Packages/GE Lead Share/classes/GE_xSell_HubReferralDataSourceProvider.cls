/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_xSell_HubReferralDataSourceProvider extends DataSource.Provider {
    global GE_xSell_HubReferralDataSourceProvider() {

    }
    global override List<DataSource.AuthenticationCapability> getAuthenticationCapabilities() {
        return null;
    }
    global override List<DataSource.Capability> getCapabilities() {
        return null;
    }
    global override DataSource.Connection getConnection(DataSource.ConnectionParams connectionParams) {
        return null;
    }
}
