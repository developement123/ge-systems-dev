/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/FetchUserDeatils/*')
global class GE_xSell_FetchUserDeatils {
    global GE_xSell_FetchUserDeatils() {

    }
    @HttpPost
    global static String fetchUserDeatils(String suggestedSSOId) {
        return null;
    }
}
