/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/WSBasedRouting/*')
global class GE_Xsell_WSBasedRouting {
    global GE_Xsell_WSBasedRouting() {

    }
    @HttpPost
    global static String wsBasedRouting(String dunsNumber, String potentialAmount) {
        return null;
    }
}
