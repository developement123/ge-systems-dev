/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GE_XSell_ExtDataSourceLoginRefreshBatch implements Database.AllowsCallouts, Database.Batchable<SObject> {
    global GE_XSell_ExtDataSourceLoginRefreshBatch() {

    }
    global void execute(Database.BatchableContext BC, List<GE_X_Sell__Referral__x> referrals) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
