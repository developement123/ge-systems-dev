<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>GE_XSell_Update_External_ID</fullName>
        <description>Update Opportunity External id field when opportunity is created.</description>
        <field>Opportunity_External_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>GE XSell: Update External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GE XSell%3A Update External ID on Opportunity</fullName>
        <actions>
            <name>GE_XSell_Update_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Opportunity External id field when opportunity is created.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
