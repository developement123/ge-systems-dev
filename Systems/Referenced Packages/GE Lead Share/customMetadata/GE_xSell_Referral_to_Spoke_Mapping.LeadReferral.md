<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LeadReferral</label>
    <protected>false</protected>
    <values>
        <field>Source_Field__c</field>
        <value xsi:type="xsd:string">ExternalId</value>
    </values>
    <values>
        <field>Target_Object__c</field>
        <value xsi:type="xsd:string">Lead</value>
    </values>
    <values>
        <field>Target_field__c</field>
        <value xsi:type="xsd:string">00N5800000DFd1S</value>
    </values>
</CustomMetadata>
