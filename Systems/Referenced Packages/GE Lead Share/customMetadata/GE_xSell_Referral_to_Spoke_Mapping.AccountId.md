<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AccountId</label>
    <protected>false</protected>
    <values>
        <field>Source_Field__c</field>
        <value xsi:type="xsd:string">AccountId</value>
    </values>
    <values>
        <field>Target_Object__c</field>
        <value xsi:type="xsd:string">Opportunity</value>
    </values>
    <values>
        <field>Target_field__c</field>
        <value xsi:type="xsd:string">opp4</value>
    </values>
</CustomMetadata>
