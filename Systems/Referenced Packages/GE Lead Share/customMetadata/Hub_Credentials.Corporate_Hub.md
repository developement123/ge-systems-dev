<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Corporate Hub</label>
    <protected>false</protected>
    <values>
        <field>clientid__c</field>
        <value xsi:type="xsd:string">3MVG9KI2HHAq33Rw6tNBWv3bClLyMqJB.30aQZ0LaMEDiGpR4q.1aUbyGDAMGBZr1XBkY.068O1Z..WholJfy</value>
    </values>
    <values>
        <field>endpoint__c</field>
        <value xsi:type="xsd:string">https://ge.my.salesforce.com/</value>
    </values>
    <values>
        <field>namespace__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>password__c</field>
        <value xsi:type="xsd:string">Welcome123</value>
    </values>
    <values>
        <field>secret__c</field>
        <value xsi:type="xsd:string">7309418630332042925</value>
    </values>
    <values>
        <field>username__c</field>
        <value xsi:type="xsd:string">aviationprodhubuser@ge.com.hub</value>
    </values>
</CustomMetadata>
